<!---
Please read this!
## IMPORTANT: Write a descriptive issue title above

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "bug" label and verify the issue you're about to submit isn't a duplicate

- https://gitlab.com/BlackBlex/netsmb/-/issues?label_name%5B%5D=bug

--->


**Describe the bug**

A clear and concise description of what the bug is.

**To Reproduce**

Steps to reproduce the behavior:
1. Go to '...'
2. Click on '....'
3. Scroll down to '....'
4. See error

**Expected behavior**

A clear and concise description of what you expected to happen.

**Screenshots**

If applicable, add screenshots to help explain your problem.

**System info (please complete the following information):**

 - OS: [e.g. Linux, Windows, Mac OS]
 - OS Version: [e.g. Debian 10, 10, Big Sur]
 - NetSMB version: [e.g. 0.1.0a or specify the git commit hash if using a development build]

**Additional context**

Add any other context about the problem here.

**Possible fixes**

Any idea to fix the issue or a link to the line of code that might be the cause for this problem


/label ~bug
