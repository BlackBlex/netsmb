<!---
Please read this!
## IMPORTANT: Write a descriptive issue title above

Before opening a new feature suggestion, make sure to search for keywords in the issues
filtered by the "enhancement" or "suggestion" label and verify the issue you're about to submit isn't a duplicate

- https://gitlab.com/BlackBlex/netsmb/-/issues?label_name%5B%5D=enhancement
- https://gitlab.com/BlackBlex/netsmb/-/issues?label_name%5B%5D=suggestion

--->

**Is your feature request related to a problem? Please describe.**

A clear and concise description of what the problem is. Ex. I'm always frustrated when [...]

**Describe the solution you'd like**

A clear and concise description of what you want to happen.

**Describe alternatives you've considered**

A clear and concise description of any alternative solutions or features you've considered.

**Describe why we should spend time on this feature and who'd be helped by adding it**

A clear and concise description of the nature and the purpose of the feature requested.

**Additional context**

Add any other context or screenshots about the feature request here.

/label ~suggestion
