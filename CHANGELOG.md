# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/),
and this project adheres to [Semantic Versioning](https://semver.org/).

# [Unreleased] | Item update

## Added

- `gd-plug` addon was added

## Changed

- Now `addons` were added in the file `plug.gd`

## Removed

- All addons were removed in favor of using a plugin manager (`gd-plug`)

___

# [0.4.0a] | Update a project things | 2021-08-12

## Added

- In `Version class` state and state_number property were added to represent alpha, beta, release candidate, release and its number
- Alert is shown when a level is loaded with a version more recent than the editor
- `Projectile class` was added to enable shot on powerups
- __*Action system*__ was added, you can define the action that the block, npc or player depending on its category, subcategory, or variant; available actions _on_hit_, _on_break_, _on_bounce_, _on_no_more_, _on_kill_, _on_visible_, _on_talk_
- Issues templates were added
- Autoload load all global_script_classes of project
- Level editor converts old json to new json (path -> class name)
- Level editor converts the map from version 0.1.0.0-alpha.0 ([0.1.0a]) to 0.4.0.0-alpha.0 ([0.4.0a])
- In `Factory class` the __not_visible__, __disable__ and __not_transformable__ methods were added
- In `Grid class` step, spacing and section size were added
- `Grid selectable class` was added
	- __on_cell_selected__ was emited when left clicked
- Block ground viewer: you can create the tiles of the blocks from an image with the option of doing it manually or with a mapping, all this from an easy-to-use wizard
- Level layers were added
- In `DataInfo class` the __set_visible__ method was added
- In `BaseEvent class` the __pre_do__, __do__, __post_do__ methods were added
- In `Alerts class` the input dialog was added
- In `FieldComponent class` the __grab_focus__ method was added to request the focus
- Audio buses were added
- In `Audio class` the __finish_effect__ method was added
- In `EventManager class` the __any_subscribe__ method was added
- In `Layer class` the __get_entities_of_layer__, __get_entities_by_category__, __get_entities_by_subcategory__ methods were added
- In `LevelRoot class`:
	- __get_layer__, __get_layer_id__, __get_player__ methods were added
	- When the event is subscribed, now its setup is executed
	- Visibility of the layer is set after the level is created
- In `Entity class` and `Block class` now have which layer they are in
- In `Block class` and `ObjectNpc class` the __get_transform__ method was added
- In `BaseAction class` the ___LEVEL__ property was added
- In `EventList class` the __putted__ property was added
- In `LevelEditor class` the __add_event__, __create_layer__ methos were added
- File structure was added
- Attach property was added in `Creator class`
- Activatable npc was added:
	- With it you can execute events in the level editor when it was added, and attach events to the levels to create systems
- Red ring and coin system were added
- In `Version class` dev state was added
- In `LevelEditor` version label was added into status bar
- In `Grid selectable` color_rect_border was added

## Changed

- In `Alerts class` the __show_confirm_dialog__ method, __funcref_confirmed__ parameter type us changed to `FuncRefExt` and __funcref_cancelled__ was added
- In `LevelEditor class` the code was accommodated
- In `Block class` actions were moved to separate files using __*Action system*__
- In `Utility class` when you load the same script, it is duplicated so as not to have the same reference of this
- In `Entity class` was changed how definitions, physical constants and states are loaded, now use the way the action system works
- In `Npc class` was changed how the behaviours are loaded, now use the way the action system works
- In extended `Enemy class` actions were moved to separate files using __*Action system*__
- In `JSONObject class` use __get_class__ method for class conversion to json
- Now the ground blocks are no longer instances and become tiles
- Separate the function of painting grid, and cursor in separate files
- Now instantiating a block or object loads its replacement instead of when the pswitch event activates
- Updated event examples
- FuncRefExt checks if the method to call requires arguments or not
- Organization to check the version of the level when loading it
- In `Audio class` was changed to use the buses
	- __play_effect__ method now accept a method that will be executed when the event finishes playing
- In `LevelData class` the __has_tile__ method, a parameter to indicate the layer was added
- File names for better structure
- Utility class becomes static class
- Autoload class contains file_loader and directory_loader
- Classes that used the utility class as an instance
- In `Entity class` the __build_action__ becomes a static method
- In `Factory class` the __instance___ methods now accept a layer id to put in correct layer
- In `LevelEditor class` the __add_data_info__ method now reports putted event
- Formatted code
- In `Autoload class` the PathList -> Paths, and Events -> EventList
- In `BaseScript class` was changed to Reference

## Deprecated

- In `JSONObject class` use of __get_path__ method

## Removed

- ECS plugin to avoid future errors
- Editor_state in `GlobalData class`, and move to `Version class` with state and state_number
- Unused files
- Unused code and comments
- `EnumUtil class`

## Fixed

- In `Version class` error in greater and less comparators were fixed
- In `Utility class` error when duplicating a script that extended from another script with relative path is fixed
- In `EntityExt class` error in __is_enemy__, __is_shell__, __is_koopa_troopa__ methods when a null argument was passed were fixed
- Typos in files and code
- Improved performance in editor and test level _(__Known issues__ #1)_
- Some errors in `Entity, PowerupInfo, Block classes`, actions and states
- Image sizes larger than 32x32 were scaled for correct viewing
- In `LevelData class` bug when layer did not exist were fixed
- In `Layer class` errors in __add___ method when visibility was applied
- In `Factory class` errors in __disable__ method was fixed
- In `on_hit file` of Block/Brick error when you instanced an object
- In `HtmlLayout class` table size was fixed
- In `LevelEditor` grid and cursor were not aligned was fixed
- z-index problem was fixed

## Known issues

1. __*Nothing*__

___

# [0.1.0a] | The first look at the public | 2021-06-10

## Added

- A hud for level (which can be customized using gdscript, the default hud is working in gdscript)
- Global event system _(GES)_: there is a list of events by default, and soon it will be possible to register own events through a scripting system
	- Level event system _(LES)_: You can use the list of events _(GES)_ to trigger events by level
- Block mechanics: break, bounce, get npc from it (question blocks)
- Enemy movement: move from side to side, fall detector
- Powerups system: at the moment there is only the one to become big and fire flower _(See __Known issues__ #2)_
- Level editor: you can put blocks, delete them, resize the layer, save and load levels
	- Has autotile
	- You can lock the axis to put blocks with __Ctrl__
	- You can remove blocks with __Right click__, and put with __Left click__
	- You can move around the map with the keys: Right (__🡆__), Left (__🡄__), Up (__🡅__), Down (__🡇__)
- Creator: you have multiple options from creating an npc, a block, a player, an object, a powerup
	- Has a file explorer to make finding files easy
		- __Double click__ to select a file or move within directories
		- You can refresh current directory
	- You can define animation, collision, detectors, name and behavior (it creates with gdscript, and is taken from the __base folder__ or from the __level folder__)
	- You can only processing textures, only scripting editor, or only processing a background
	- ### __NOTE__ at the moment you can only create enemies and process textures
- Scripting system: you will be able to modify the behavior of any npc or player through gd files in the gdscript language, this applies to the creation of new behaviors, such as the replacement of these for specific levels (__base folder__ or __level folder__)
- Objects available:
	- Coin:
		- SMB style
		- SMB2 style
		- SMB3 style:
			- Normal & red
		- SMW style:
			- Normal & blue
- Enemies available:
	- Goomba:
		- SMB3 style:
			- Normal & red
		- SMB style:
			- Normal & blue
	- Koopa troppa:
		- SMB3 style & SMB style:
			- Normal & red
	- Shell:
		- SMB3 style & SMB style:
			- Normal & red
- Blocks available:
	- Ground:
		- SMB3 style:
			- Wood theme
		- SMW style:
			- Cave theme
			- Grass theme
	- Actions:
		- Brinks:
			- SMB style:
				- Brick
				- Question
				- Solid
			- SMB2 style
			- SMB3 style:
				- Brick
				- Question
				- Solid & solid red
			- SMW style:
				- Brick
				- Question
				- Solid
		- Pushables:
			- SMB3 style:
				- Pswitch with its events _(LES)_
- Powerups available:
	- Big _(Mushroom)_
	- Fire flower _(See __Known issues__ #2)_
- Players available:
	- Mario

## Known issues

1. __*Poor performance in editor and test level*__
2. __*The shot of powerups is not implemented yet*__
3. __*In creator, you can only create enemies and process textures*__
3. __*Scripting and Event system are implemented in a basic way*__

___

[Unreleased]: https://gitlab.com/BlackBlex/netsmb/-/compare/v0.4.0a...develop
[0.4.0a]: https://gitlab.com/BlackBlex/netsmb/-/compare/v0.1.0a...v0.4.0a
[0.1.0a]: https://gitlab.com/BlackBlex/netsmb/-/releases#v0.1.0a
