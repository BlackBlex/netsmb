# NetSMB

Online/Offline (LAN/Singleplayer) Game | Creation of levels, multiplayer, share levels, and more!

It is an open source SMB engine developed from scratch with Godot Engine (taking inspiration from SMBX and the SMM series), in which you can create levels freely, share levels in a simple way, customize them with graphics and scripts without limits, this it is possible since it is exposed:

 - All the scripting part, thus being able to easily access all the physical implementation, actions and events of any object, giving freedom to:
	- Replace the physics, action or event if they are in the level folder
	- Allow the correction of errors, bugs, or improvement without development intervention (having to wait for a patch to be released), thus being able to contribute to the project easily
 - All the graphics part, organized by folders and with ease of adding (without replacing the existing ones) new blocks, npc, objects, enemies, power up, etc. *

_*For the addition of the new resource to be carried out correctly, it must be processed before, in order to provide it with its final functionality, the project does not use png files, and it is only used when processing the file_

___

Author: __@BlackBlex__ [Twitter](https://twitter.com/blackblex) | [Bitbucket](https://bitbucket.org/BlackBlex)

License: __MIT__

___

## Information
 - *Functional project is in the **master** branch*
 - *All development is in the **develop** branch, using **GitFlow***
 - *__Current state:__ [0.4.0a]*
 - **Note:** *Godot Engine __3.4.2__ with GDScript is used*
 - *Multilanguage system*

___

## Mechanics available | 2021-08-12
- __Block mechanics__: break, bounce, get npc from it (question blocks)
- __Enemy movement__: move from side to side, fall detector
- __Powerups system__: at the moment there is only the one to become big and fire flower
- __Scripting system__: you will be able to modify the behavior of any npc or player through gd files in the gdscript language, this applies to the creation of new behaviors, such as the replacement of these for specific levels (__base folder__ or __level folder__)
- __Layer system__: you can separate your level into several layers
- __Creator__: Enemy, Object and Block ground with mapped file
- __Activable system__: items that launch events to the editor when they are added; allows you to attach events or scripts

___

## Level editor

![](docs/images/screenshots/NetSMB.png)
You can put blocks, delete them, resize the layer, save and load levels
- Has autotile
- You can lock the axis to put blocks with __Ctrl__
- You can remove blocks with __Right click__, and put with __Left click__
- You can move around the map with the keys: Right (__🡆__), Left (__🡄__), Up (__🡅__), Down (__🡇__)
- Json files are used to store level data
- __Double click__ to select a event

___

## Test level

<center><img src="docs/images/screenshots/NetSMB_Test_level.png"></center>

- A hud for level (which can be customized using gdscript, the default hud is working in gdscript)
- Global event system _(GES)_: there is a list of events by default, and soon it will be possible to register own events through a scripting system
	- Level event system _(LES)_: You can use the list of events _(GES)_ to trigger events by level

___

## Creator

![](docs/images/screenshots/NetSMB_Creator.png)
You have multiple options from creating an npc, a block, a player, an object, a powerup
- Has a file explorer to make finding files easy
	- __Double click__ to select a file or move within directories
	- You can refresh current directory
- You can define animation, collision, detectors, name and behavior (it creates with gdscript, and is taken from the __base folder__ or from the __level folder__)
- You can only processing textures, only scripting editor, or only processing a background
- __Double click__ to select a behavior
- ### __NOTE__ at the moment you can only create enemies, process textures, and block ground mapped

![](docs/images/screenshots/NetSMB_Creator_NPC.png)

___

### Creator - Block ground

![](docs/images/screenshots/NetSMB_Creator_block_ground.png)
You can easily create or parse the blocks ground

___

# How to run NetSMB?
## _Unfortunately there will be no executables, until it becomes a release version_

___

__You can only run it if you have:__
 - __[Godot Engine 3.4.x](https://godotengine.org/download)__
 - __Git__

__Steps:__

 1. Clone this repository:
	```sh
	git clone https://gitlab.com/BlackBlex/netsmb.git
	```
 2. Open the directory in terminal and run the following command
	```sh
	godot --no-window -s plug.gd install
	```
	_The above command is used to download the addons that are needed for the project_

 3. Open Godot and import this project
 4. Run it

___

### _Note:_
_Don't commit the `addons/gut` directory, related to issue [#13][i13]_

___

# Links

## [Youtube devlog's](https://www.youtube.com/playlist?list=PL8r5DecOQSfPB_UUssL4x_44VMJnlo-cN)


[0.4.0a]: https://gitlab.com/BlackBlex/netsmb/-/releases#v0.4.0a
[i13]: https://github.com/bitwes/gut-extension/issues/13
