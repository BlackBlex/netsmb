extends BaseScript

func run(level: LevelData, data: LevelContainerData) -> LevelData:
	var keys: Array = level.data.keys()
	var is_correct: bool = true

	for block_position in keys:
		if not level.data[block_position] is BlockInfo:
			continue

		var block_info: BlockInfo = level.data[block_position]

		if "ground" in block_info.category:
			var tile_id: int = data.get_tile_id(block_info.variant)

			if tile_id != -1:
				if not level.tiles.has(tile_id):
					level.tiles[tile_id] = {1: []}

				level.tiles[tile_id][1].append(block_position)
				var _err = level.data.erase(block_position)

				if not _err:
					is_correct = false
					Logger.msg_info("Could not remove block at position: %s" % block_position, Logger.CATEGORY_SYSTEM)

	if is_correct:
		var backup_values: Dictionary = level.data

		level.data = {}
		level.data[1] = backup_values

		level.editor_version = Version.new().init_number(0, 4, 0, 0, Version.State.ALPHA, 0)
		return level
	else:
		return null
