extends "../../on_hit.gd"

func is_valid(args: Array) -> bool:
	var hit = false

	# Check if is player or Enemy
	if args[0] is Player or args[0] is Enemy:
		# Enemy
		if EntityExt.is_shell(args[0]) and not args[0].is_grabbed_by_player:
			hit = args[1] == Vector2.LEFT or args[1] == Vector2.RIGHT
		# Player
		# Verify if normal it's correct with powerUp
		elif not "coin" in _OWNER.info.get_image().animation and args[0] is Player:
			if args[0].info.powerup_current <= Enums.PowerupType.FIRE_FLOWER:
				hit = args[1] == Vector2.DOWN
			else:
				#Check if had tanookie or other powerup
				pass

	return hit

func do(args: Array):
	if not _OWNER.info.has_reward():
		#if player isn't small, break no block question
		if EntityExt.is_shell(args[0]) or args[0] is Player:
			if (EntityExt.is_shell(args[0]) or args[0].info.powerup_current != Enums.PowerupType.SMALL) and not _OWNER.info.is_question():
				if _OWNER.info.get_image().animation != "solid":
					_OWNER.break_block(args[0])
			elif EntityExt.is_shell(args[0]) or args[0].info.powerup_current >= Enums.PowerupType.SMALL:
				if _OWNER.info.get_image().animation != "solid":
					_OWNER.bounce(args[1])
	else:
		if _OWNER.info.reward.total >= 1:
			var npc_identifier: int = _OWNER.info.reward.npc_identificator
			var npc_to_instance: NpcInfo = GlobalData.current_level.rewards[npc_identifier]

			var new_npc_info: NpcInfo = npc_to_instance.clone()
			var new_npc_instance: Npc = null
			var npc_animation: AnimatorExecuter = null

			match (new_npc_info.type):
				Enums.DataInfoType.ENEMY:
					var enemy_npc: Enemy = Resources.ENEMY.instance() as Enemy
					enemy_npc.info = new_npc_info as EnemyInfo
					enemy_npc.position = _OWNER.position

					_LEVEL.get_layer().call_deferred("add_enemy", _OWNER.layer_id, enemy_npc, true)
					new_npc_instance = enemy_npc
				Enums.DataInfoType.POWERUP:
					var powerup_npc = Resources.POWERUP.instance() as Powerup
					powerup_npc.info = new_npc_info as PowerupInfo
					powerup_npc.position = _OWNER.position

					_LEVEL.get_layer().call_deferred("add_powerup", _OWNER.layer_id, powerup_npc, true)
					new_npc_instance = powerup_npc
				Enums.DataInfoType.OBJECT:
					var object_npc = Resources.OBJECT_NPC.instance() as ObjectNpc
					object_npc.info = new_npc_info as ObjectInfo
					object_npc.is_transformable = false
					object_npc.position = _OWNER.position

					_LEVEL.get_layer().call_deferred("add_npc", _OWNER.layer_id, object_npc, true)
					new_npc_instance = object_npc

			yield(new_npc_instance, "ready")

			npc_animation = EntityExt.animator(new_npc_instance)

			var size_y: float = _OWNER.info.get_image().frames.get_frame(_OWNER.info.get_image().animation, 0).get_size().y

			var position_final_y: float = new_npc_instance.position.y - size_y

			_OWNER.bounce()
			if new_npc_instance.info is ObjectInfo and new_npc_instance.info.object_given == Enums.ObjectNpcType.COIN:
				position_final_y -= size_y * 2

				var _val = npc_animation.interpolate_property(new_npc_instance, "position:y", new_npc_instance.position.y, position_final_y, 0.3).interpolate_property(new_npc_instance, "position:y", position_final_y, position_final_y + size_y * 2, 0.3, 0, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN, true)

				Event.subscribe(A.EventList.common.completed, new_npc_instance, "_on_area_body_entered", npc_animation, [args[0]])
				npc_animation.start()
			else:
				var _val = npc_animation.interpolate_property(new_npc_instance, "position:y", new_npc_instance.position.y, position_final_y, 0.2)
				Audio.play_effect("mushroom.ogg")
				Event.subscribe(A.EventList.common.completed, new_npc_instance, "activate_sm", npc_animation)
				npc_animation.start()

			_OWNER.info.reward.total -= 1

		if _OWNER.info.reward.total == 0:
			_OWNER.info.get_image().animation = "solid"
