extends "../../../on_hit.gd"

var current_music_position: float = 0

func is_valid(args: Array) -> bool:
	# Check if is player
	return args[0] is Player and args[1] == Vector2.UP

func pre_do(_args: Array):
	Audio.play_effect("pswitch.ogg")
	Event.publish(_OWNER, A.EventList.pswitch.started)

func do(_args: Array):
	_OWNER.info.get_image().animation = "pressed"
	_OWNER.call_deferred("disable", true)
	current_music_position = Audio.get_track_position()
	Audio.stop_music()
	Audio.play_music("smw-switch.spc")
	yield(_OWNER.info.get_image(), "animation_finished")
	yield(_OWNER.get_tree().create_timer(0.4), "timeout")
	_OWNER.info.get_image().visible = false
	yield(_OWNER.get_tree().create_timer(19.6), "timeout")

func post_do(_args: Array):
	Event.publish(_OWNER, A.EventList.pswitch.ended)
	Audio.stop_music()
	Audio.play_music(GlobalData.current_level.music, current_music_position)
	_OWNER.destroy()
