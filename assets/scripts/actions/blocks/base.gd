extends BaseAction

# args[0] = body who triggered the action
# args[1] = normal side where it hit
func is_valid(_args: Array) -> bool:
	return false

# args[0] = body who triggered the action
# args[1] = normal side where it hit
func execute(args: Array) -> void:
	var wait = pre_do(args)
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = do(args)
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = post_do(args)
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

# args[0] = body who triggered the action
# args[1] = normal side where it hit
func pre_do(_args: Array):
	pass

# args[0] = body who triggered the action
# args[1] = normal side where it hit
func do(_args: Array):
	pass

# args[0] = body who triggered the action
# args[1] = normal side where it hit
func post_do(_args: Array):
	pass
