extends "base.gd"

func is_valid(args: Array) -> bool:
	var hit = args[1] == Vector2.DOWN

	if EntityExt.is_shell(args[0]):
		if not args[0].is_grabbed_by_player:
			hit = args[1] == Vector2.LEFT or args[1] == Vector2.RIGHT
		else:
			hit = false

	return hit

func pre_do(args: Array):
	Event.publish(self, A.EventList.block.hit, [args[0], _OWNER])

func post_do(_args: Array):
	Audio.play_effect("block-hit.ogg")
