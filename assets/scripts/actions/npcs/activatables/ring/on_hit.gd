extends "../../on_hit.gd"

func is_valid(body: Node) -> bool:
	return body is Player

func pre_do(_body: Node):
	Audio.play_effect("ring/circle.wav")

func do(_body: Node):
	if _LEVEL:
		var layer_id: int = _LEVEL.get_layer_id("red coins")
		if layer_id != -1:
			_LEVEL.get_layer().set_layer_visible(layer_id, true)

func post_do(body: Node):
	# breakpoint
	Event.publish(_OWNER, A.EventList.npc.grabbed, [body])
	_OWNER.destroy()
