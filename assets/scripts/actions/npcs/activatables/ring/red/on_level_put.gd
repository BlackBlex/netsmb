extends "../../../base_level_editor.gd"

var info_temp: ActivatableInfo = null

func is_valid(info: DataInfo) -> bool:
	info_temp = info
	var to_result = false

	if info is ActivatableInfo and "activatables" in info.category and not GlobalData.current_level.layers.has("red coins"):
		to_result = true
		if info.type_select == ActivatableInfo.TypeSelect.EVENT:
			to_result = not GlobalData.current_level.events.has(info.item)

	return to_result

func do() -> void:
	_OWNER.create_layer("red coins")
	if info_temp.type_select == ActivatableInfo.TypeSelect.EVENT:
		_OWNER.add_event(info_temp.item)
