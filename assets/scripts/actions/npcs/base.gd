extends BaseAction

# body: who triggered the action
func is_valid(_body: Node) -> bool:
	return false

# args[0] = body who triggered the action
func execute(args: Array) -> void:
	var wait = pre_do(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = do(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = post_do(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

# body: who triggered the action
func pre_do(_body: Node):
	pass

# body: who triggered the action
func do(_body: Node):
	pass

# body: who triggered the action
func post_do(_body: Node):
	pass
