extends BaseAction

var _correct: bool = false

# _info: who triggered the action
func is_valid(_info: DataInfo) -> bool:
	return false

func setup() -> void:
	_correct = _OWNER != null

	if not _correct:
		Logger.msg_fatal("Level editor isn't setupped", Logger.CATEGORY_ERROR)

func execute(_args: Array) -> void:
	if _correct:
		var wait = pre_do()
		if wait is GDScriptFunctionState and wait.is_valid():
			yield(wait, "completed")

		wait = do()
		if wait is GDScriptFunctionState and wait.is_valid():
			yield(wait, "completed")

		wait = post_do()
		if wait is GDScriptFunctionState and wait.is_valid():
			yield(wait, "completed")

func pre_do():
	pass

func do():
	pass

func post_do():
	pass
