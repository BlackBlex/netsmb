extends "../on_hit.gd"

# body: who triggered the action
func is_valid_player(_body: Node) -> bool:
	return false

# args[0] = body who triggered the action
func execute_player(args: Array) -> void:
	var wait = pre_do_player(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = do_player(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = post_do_player(args[0])
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

# body: who triggered the action
func pre_do_player(_body: Node):
	pass

# body: who triggered the action
func do_player(_body: Node):
	pass

# body: who triggered the action
func post_do_player(_body: Node):
	pass
