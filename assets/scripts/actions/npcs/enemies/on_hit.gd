extends "base_hit.gd"

func is_valid(body: Node) -> bool:
	return body is Entity or not _OWNER.is_colliding

func is_valid_player(body: Node) -> bool:
	return body is Player or not _OWNER.is_colliding

func pre_do(_body: Node):
	_OWNER.is_colliding = true

func pre_do_player(body: Node):
	pre_do(body)

func do(body: Node):
	if body is Player and not body.is_invincible and not _OWNER.is_grabbed_by_player:
		body.powerup_current = Enums.PowerupType.DAMAGE

func do_player(body: Node) -> void:
	if body is Player:
		body.velocity.y = body.sm.get_state_current().get_physic_constants().MAX_JUMP_VELOCITY * 0.65
		_OWNER.destroy([body])

func post_do(_body: Node):
	if _OWNER.is_colliding:
		_OWNER.collision_timer.start()

func post_do_player(body: Node):
	post_do(body)
