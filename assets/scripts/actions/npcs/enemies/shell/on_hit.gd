extends "../on_hit.gd"

func do(body: Node):
	if body is Player and not body.is_invincible and not _OWNER.is_grabbed_by_player:
		if _OWNER.sm.get_state_current_name() == "idle":
			if body.input.get_run_pressed() and body.sm.get_state("hold").entity_grabbed == null:
				_OWNER.area_hit.set_collision_mask_bit(Enums.Collision.PLAYER, false)
				_OWNER.area_player.set_collision_mask_bit(Enums.Collision.PLAYER, false)
				_OWNER.set_collision_mask_bit(Enums.Collision.BLOCK, false)
				_OWNER.area_hit.set_collision_mask_bit(Enums.Collision.BLOCK, false)
				body.sm.get_state("hold").entity_grabbed = _OWNER
				body.sm.transition("hold")
			else:
				Audio.play_effect("smw_kick.ogg")
				_OWNER.input.direction.x = 1 if body.position.x < _OWNER.position.x else -1
				_OWNER.kicked_by = body
				_OWNER.is_colliding = false
		else:
			body.powerup_current = Enums.PowerupType.DAMAGE
	elif body is Entity and body != _OWNER:
		var dir: int = 1
		if EntityExt.is_shell(body):
			if body.is_grabbed_by_player:
				if body.player_who_grabbed:
					body.player_who_grabbed.sm.get_state("hold").entity_grabbed = null
					body.player_who_grabbed.sm.get_state("hold").entity_parent = null
					body.player_who_grabbed.sm.get_state("kick").entity_grabbed = null
					body.player_who_grabbed.sm.get_state("kick").entity_parent = null
					body.player_who_grabbed.sm.transition("walk")
					body.position = body.player_who_grabbed.position
					dir = int(body.player_who_grabbed.position_pivot.scale.x)
				_OWNER.destroy([body, dir])
				body.destroy([_OWNER, dir * -1])
			elif body.sm.get_state_current_name() == "walk" or body.kicked_by is Player:
				_OWNER.destroy([body])
		elif EntityExt.is_shell(_OWNER) and EntityExt.is_enemy(body):
			if _OWNER.is_grabbed_by_player:
				if _OWNER.player_who_grabbed:
					_OWNER.player_who_grabbed.sm.get_state("hold").entity_grabbed = null
					_OWNER.player_who_grabbed.sm.get_state("hold").entity_parent = null
					_OWNER.player_who_grabbed.sm.get_state("kick").entity_grabbed = null
					_OWNER.player_who_grabbed.sm.get_state("kick").entity_parent = null
					_OWNER.player_who_grabbed.sm.transition("walk")
					_OWNER.position = _OWNER.player_who_grabbed.position
					dir = int(_OWNER.player_who_grabbed.position_pivot.scale.x)
				body.destroy([_OWNER, dir])
				_OWNER.destroy([body, dir * -1])
			elif _OWNER.sm.get_state_current_name() == "walk" or _OWNER.kicked_by is Player:
				body.destroy([_OWNER])

func do_player(body: Node) -> void:
	if body is Player:
		body.velocity.y = body.sm.get_state_current().get_physic_constants().MAX_JUMP_VELOCITY * 0.65
		if _OWNER.sm.get_state_current_name() == "walk":
			_OWNER.sm.transition("idle")
			Audio.play_effect("stomped.ogg")
			_OWNER.input.direction.x = 0
			_OWNER.kicked_by = null
		else:
			Audio.play_effect("smw_kick.ogg")
			_OWNER.input.direction.x = 1 if body.position.x < _OWNER.position.x else -1
			_OWNER.kicked_by = body
