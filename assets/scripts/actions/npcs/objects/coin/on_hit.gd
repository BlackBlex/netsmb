extends "../on_hit.gd"

func do(body: Node):
	.do(body)

	player.info.coins += _OWNER.info.total
	player.info.score += 10
	Audio.play_effect("coin.ogg")
