extends "../on_hit.gd"

var player: Player = null

func is_valid(body: Node) -> bool:
	var to_result: bool = false

	if not _OWNER.grabbed:
		to_result = body is Player or (body is Enemy and body.kicked_by is Player)

	return to_result

func pre(_body: Node):
	_OWNER.grabbed = true

func do(body: Node):
	if body is Player:
		player = body as Player
	else:
		player = body.kicked_by as Player

	var color: String = "#ffffff"
	var size_half: Vector2 = _OWNER.get_size() / 2
	var data: Image = (_OWNER.image.frames.get_frame(_OWNER.image.animation, 0) as AtlasTexture).atlas.get_data()

	data.lock()
	color = data.get_pixelv(size_half).lightened(0.2).to_html()
	data.unlock()

	EntityExt.emit_particle(_OWNER, "sparkle", color, 0.5)

func post_do(_body: Node):
	Event.publish(_OWNER, A.EventList.npc.grabbed, [player])
	_OWNER.destroy()
