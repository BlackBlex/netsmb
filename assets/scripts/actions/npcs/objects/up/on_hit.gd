extends "../on_hit.gd"

func do(body: Node):
	.do(body)

	player.info.life += _OWNER.info.total
	player.info.score += 1000
	Audio.play_effect("1up.ogg")
