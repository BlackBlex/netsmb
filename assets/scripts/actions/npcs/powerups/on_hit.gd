extends "../on_hit.gd"

func is_valid(body: Node) -> bool:
	return body is Player

func do(body: Node):
	var player: Player = body as Player
	player.powerup_variant = _OWNER.info.variant
	player.powerup_current = _OWNER.info.powerup_given
	player.info.score += 1000

func post_do(_body: Node):
	_OWNER.destroy()
