extends BaseBehaviour

func _init() -> void:
	description = "Basic movement to left or right with fall detector"

func setup() -> void:
	_OWNER.raycast_look_to.enabled = true
	_OWNER.raycast_down.enabled = true
	_OWNER.raycast_down.position = Vector2(_OWNER.raycast_look_to.position.x, _OWNER.raycast_down.position.y)

func execute() -> void:
	if _OWNER.raycast_look_to.is_colliding() or (not _OWNER.raycast_down.is_colliding() and _OWNER.is_on_floor()):
		_OWNER.input.direction = _OWNER.input.direction * -1
		_OWNER.velocity = Vector2(_OWNER.velocity.x * -1, _OWNER.velocity.y)
