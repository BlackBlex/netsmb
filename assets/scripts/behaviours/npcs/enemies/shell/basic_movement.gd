extends BaseBehaviour

func _init() -> void:
	description = "Basic movement to left or right without fall detector"

func setup() -> void:
	_OWNER.raycast_look_to.enabled = true

func execute() -> void:
	if _OWNER.sm.get_state_current_name() == "idle":
		return

	if _OWNER.raycast_look_to.is_colliding():
		_OWNER.input.direction = _OWNER.input.direction * -1
		_OWNER.velocity = Vector2(_OWNER.velocity.x * -1, _OWNER.velocity.y)
