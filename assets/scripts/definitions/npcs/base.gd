extends BaseDefinition

func _init() -> void:
	initial_state = "walk"

func state_machine() -> Dictionary:
	return {
		"walk" : ["walk"],
	}
