extends "../../base.gd"

func _init() -> void:
	initial_state = "idle"

func state_machine() -> Dictionary:
	var states: Dictionary = .state_machine()
	states["idle"] = ["walk"]
	states["walk"].append("idle")
	return states
