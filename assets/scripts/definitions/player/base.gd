extends BaseDefinition

func _init() -> void:
	initial_state = "idle"

func state_machine() -> Dictionary:
	return {
		"idle" : ["jump", "walk"],
		"jump" : ["idle", "walk", "run"],
		"walk" : ["jump", "idle", "run"],
		"run" : ["jump", "walk", "idle"],
	}
