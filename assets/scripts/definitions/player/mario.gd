extends "base.gd"

#Override initial_state
#func _init() -> void:
#    initial_state = "walk"

func state_machine() -> Dictionary:
	var states: Dictionary = .state_machine()

	states["run_jump"] = ["walk", "idle", "run"]
	states["hold"] = ["kick", "hold_jump", "walk"]
	states["hold_jump"] = ["kick", "hold"]
	states["kick"] = ["walk", "idle"]

	var id_to_remove: int = states["run"].find("jump")
	states["run"].remove(id_to_remove)

	states["crouch"] = ["idle", "jump"]
	states["look_up"] = ["walk", "idle", "jump"]

	states["jump"].append("crouch")
	states["jump"].append("hold")
	states["run"].append("crouch")
	states["run"].append("run_jump")
	states["run"].append("hold")
	states["run_jump"].append("crouch")
	states["run_jump"].append("hold")
	states["walk"].append("crouch")
	states["walk"].append("hold")
	states["idle"].append("crouch")
	states["idle"].append("look_up")
	states["idle"].append("hold")
	states["look_up"].append("hold")

	states["throw"] = states.keys()
	id_to_remove = states['throw'].find("crouch")
	states["throw"].remove(id_to_remove)
	id_to_remove = states['throw'].find("hold")
	states["throw"].remove(id_to_remove)
	id_to_remove = states['throw'].find("hold_jump")
	states["throw"].remove(id_to_remove)
	id_to_remove = states['throw'].find("kick")
	states["throw"].remove(id_to_remove)
	id_to_remove = states['throw'].find("look_up")
	states["throw"].remove(id_to_remove)

	for key in states["throw"]:
		states[key].append("throw")

	return states
