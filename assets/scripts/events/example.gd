extends BaseEvent

func _init() -> void:
	name = "Example event"
	description = "Example event, occurs when level on loaded"

func attach() -> String:
	return A.EventList.level.started

func do() -> void:
	print_message("Works on level started!", "Example event")
