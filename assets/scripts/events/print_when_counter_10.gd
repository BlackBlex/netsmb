extends BaseEvent

var last_correct_seconds: int = 0
var left: int = 0
var is_correct: bool = false

func _init() -> void:
	name = "Prints every 10 seconds"
	description = "Example event, which prints 'Hello' every 10 seconds"

func setup() -> void:
	last_correct_seconds = _OWNER.get_left_time(false)

func pre_do() -> void:
	left = _OWNER.get_left_time()

func do() -> void:
	is_correct = (last_correct_seconds - left) >= 10

func post_do() -> void:
	if is_correct:
		last_correct_seconds = left
		print_message("Hello", "Print every 10 seconds event")
