extends BaseEvent

var is_correct: bool = false
var coins: Array = []
var total_coins: int = 8
var collected_coins: int = 0
var layer_id: int = -1
var timer_sound: AudioStreamPlayer = null

func _init() -> void:
	name = "Red ring"
	description = "Used for ring npc, occurs when this is grabbed"

func attach() -> String:
	return A.EventList.level.started

func setup() -> void:
	layer_id = _OWNER.get_layer_id("red coins")
	_OWNER.get_layer().set_layer_visible(layer_id, false)
	if layer_id != -1:
		var entities: Array = _OWNER.get_layer().get_entities_of_layer(layer_id)
		for entity in entities:
			if entity is ObjectNpc:
				if "coin" in entity.info.subcategory:
					entity.get_transform().destroy()
					Event.unsubscribe_all_events(entity)
					coins.append(entity)

					Event.subscribe(A.EventList.npc.grabbed, self, "_on_coin_grabbed", entity)

		var rings: Dictionary = _OWNER.get_layer().get_entities_by_subcategory("ring")
		if not rings.empty():
			for layer in rings:
				for ring in rings[layer]:
					Event.subscribe(A.EventList.npc.grabbed, self, "_on_ring_grabbed", ring)
					break
				break

		total_coins = coins.size()

func _on_coin_grabbed(_entity: Entity) -> void:
	collected_coins += 1

	if collected_coins == total_coins:
		if timer_sound:
			Audio.finish_effect(timer_sound)
		Audio.play_effect("ring/coin_all.wav", 1, Instanceator.make_funcref_ext(self, "_on_play_effect_coin_all_finished"))
	else:
		Audio.play_effect("ring/coin_%d.wav" % collected_coins)

func _on_play_effect_coin_all_finished() -> void:
	Audio.play_effect("item-dropped.ogg")
	var object: Array = Factory.npc.instance_powerup(1, "big", "smb3", _OWNER.get_player().position - Vector2(0, 4 * GlobalData.BLOCK_SIZE))
	if object[0] == OK:
		yield(_OWNER.get_tree().create_timer(0.2), "timeout")
		object[1].call_deferred("activate_sm")

func _on_ring_grabbed(_entity: Entity) -> void:
	yield(_OWNER.get_tree().create_timer(0.5), "timeout")
	timer_sound = Audio.play_effect("ring/timer.wav", 1, Instanceator.make_funcref_ext(self, "_on_timer_finish"))

func _on_timer_finish() -> void:
	_OWNER.get_layer().set_layer_visible(layer_id, false)

func _on_animation_finished(object: Npc) -> void:
	object.activate_sm()
