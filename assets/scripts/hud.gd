extends BaseHUD

var life: LifeSection = null
var coin: CoinSection = null
var item_slot: ItemSlotSection = null
var time: TimeSection = null
var score: ScoreSection = null
var player_info: PlayerInfo = null

func setup() -> void:
	player_info = _LEVEL.get_player_info()

	life = get_life_section()
	coin = get_coin_section()
	item_slot = get_item_slot_section()
	time = get_time_section()
	score = get_score_section()

	add_child(life)
	add_child(coin)
	add_child(item_slot)
	add_child(time)
	add_child(score)

	life.set_image(load_resource("local://images/icons/hud/mario.tex"))
	life.rect_position = Vector2(4, 4)
	life.rect_size = Vector2(120, 34)

	coin.set_animated_image(load_resource("local://images/npcs/objects/coin/smb3.frames.res"))
	coin.rect_position = Vector2(4, 38)
	coin.rect_size = Vector2(200, 34)

	#item_slot.set_background(load_resource("local://themes/hud/item_slot.res"))
	item_slot.set_background_image(load_resource("local://images/icons/hud/itemslot.tex"))
	item_slot.rect_position = Vector2(372, 4)
	item_slot.rect_size = Vector2(56, 56)

	time.set_image(load_resource("local://images/icons/hud/time.tex"))
	time.rect_position = Vector2(347, 65)
	time.rect_size = Vector2(106, 28)

	score.rect_position = Vector2(579, 4)
	score.rect_size = Vector2(217, 34)

func loop(_delta: float) -> void:
	if (life.get_value() != player_info.lifes):
		life.set_value(player_info.lifes)

	if (coin.get_value() != player_info.coins):
		coin.set_value(player_info.coins)

	if (score.get_value() != player_info.score):
		score.set_value(player_info.score)

	if (item_slot.get_powerup() != player_info.powerup_reserved):
		item_slot.set_powerup(player_info.powerup_reserved, _LEVEL.get_player_powerup_reserved_variant())

	if is_instance_valid(_LEVEL):
		time.set_value(_LEVEL.get_left_time())
