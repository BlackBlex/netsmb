extends StatePhysic

func on_enter_state() -> void:
	"""
	if !is_enter_state_disabled:
		if _OWNER != null:
			_OWNER.Image.play(id)
	"""

func move(delta: float, direction: Vector2 = Vector2.INF, multipler_acceleration: float = _PHYSIC_CONST.MULTIPLER_ACCELERATION, _multipler_deceleration: float = _PHYSIC_CONST.MULTIPLER_DECELERATION, speed: float = _PHYSIC_CONST.SPEED) -> void:
	if _OWNER == null:
		return

	if direction == Vector2.INF:
		direction = _OWNER.input.direction

	max_speed = speed

	#Acceleration
	_OWNER.velocity.x = lerp(_OWNER.velocity.x, direction.x * max_speed, multipler_acceleration * _PHYSIC_CONST.FACTOR_ACCELERATION)

	#Apply gravity
	_OWNER.velocity.y += (_PHYSIC_CONST.GRAVITY * delta) + 1

	#//TODO: Slopes
	_OWNER.velocity = _OWNER.move_and_slide(_OWNER.velocity, _PHYSIC_CONST.FLOOR_NORMAL)

	#Last Corrections
	if _OWNER.is_on_wall():
		_OWNER.velocity.x = 0
