extends "../../idle.gd"

func on_enter_state() -> void:
	if not is_enter_state_disabled:
		if _OWNER != null:
			_OWNER.image.playing = false
			_OWNER.image.frame = 0

func on_physics_process(delta: float) -> void:
	if _OWNER.is_grabbed_by_player:
		return

	.on_physics_process(delta)
