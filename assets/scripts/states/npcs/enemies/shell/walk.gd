extends "../../walk.gd"

func on_enter_state() -> void:
	if !is_enter_state_disabled:
		if _OWNER != null:
			_OWNER.image.playing = true

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x == 0:
		if _OWNER.velocity.x == 0:
			_OWNER.sm.transition("idle")
			return

	move(delta, Vector2.INF, _PHYSIC_CONST.MULTIPLER_RUN_ACCELERATION, _PHYSIC_CONST.MULTIPLER_DECELERATION, _PHYSIC_CONST.SPEED_RUN * 1.5)
