extends "base_state.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x == 0:
		if _OWNER.velocity.x == 0:
			_OWNER.sm.transition("idle")
			return

	move(delta)
