extends StatePhysic

func on_enter_state() -> void:
	if !is_enter_state_disabled:
		if _OWNER != null:
			_OWNER.image.play(name)

func move(delta: float, direction: Vector2 = Vector2.INF, multipler_acceleration: float = _PHYSIC_CONST.MULTIPLER_ACCELERATION, multipler_deceleration: float = _PHYSIC_CONST.MULTIPLER_DECELERATION) -> void:
	if _OWNER == null:
		return

	if direction == Vector2.INF:
		direction = _OWNER.input.direction

	is_running = _OWNER.input.get_run_pressed() || _OWNER.input.get_altrun_pressed()

	# If not direction in x or
	# If not running but player speed is greather than max speed
	if direction.x == 0 || (!is_running && abs(_OWNER.velocity.x) > _PHYSIC_CONST.SPEED):
		# Deceleration
		if abs(_OWNER.velocity.x) != 0:
			if abs(_OWNER.velocity.x) <= _PHYSIC_CONST.FIX_MIN_ERROR:
				_OWNER.velocity.x = 0
			else:
				_OWNER.velocity.x -= ((multipler_deceleration * _PHYSIC_CONST.FACTOR_DECELERATION) * sign(_OWNER.velocity.x))
	else:
		# Acceleration
		_OWNER.velocity.x += direction.x * (multipler_acceleration * _PHYSIC_CONST.FACTOR_ACCELERATION)

		# Clamp to maximum speed
		max_speed = _PHYSIC_CONST.SPEED_RUN if is_running else _PHYSIC_CONST.SPEED
		# Reduce max speed if jumping and running
		if is_running && !_OWNER.is_on_floor():
			max_speed *= 0.85

		_OWNER.velocity.x = clamp(_OWNER.velocity.x, -max_speed, max_speed)

	# Apply gravity && Clamp to maximum gravity
	if _OWNER.velocity.y < 0:
		_OWNER.velocity.y += _PHYSIC_CONST.GRAVITY * delta
	else:
		_OWNER.velocity.y += _PHYSIC_CONST.FALL_GRAVITY * delta

	#//TODO: Slopes
	_OWNER.velocity = _OWNER.move_and_slide(_OWNER.velocity, _PHYSIC_CONST.FLOOR_NORMAL)

	var collisions: int = _OWNER.get_slide_count()
	if collisions > 0:
		var body: KinematicCollision2D = _OWNER.get_slide_collision(collisions - 1)

		if body.normal == _PHYSIC_CONST.FLOOR_NORMAL:
			_OWNER.velocity.y = 0

	# Last Corrections
	if _OWNER.is_on_wall():
		_OWNER.velocity.x = 0
