extends StatePhysic

func on_enter_state() -> void:
	if not is_play_imagen_disabled:
		if _OWNER != null:
			_OWNER.image.play(name)

func move(delta: float, direction: Vector2 = Vector2.INF, multipler_acceleration: float = _PHYSIC_CONST.MULTIPLER_ACCELERATION, multipler_deceleration: float = _PHYSIC_CONST.MULTIPLER_DECELERATION) -> void:
	if _OWNER == null:
		return

	if direction == Vector2.INF:
		direction = _OWNER.input.direction

	is_running = _OWNER.input.get_run_pressed() || _OWNER.input.get_altrun_pressed()

	max_speed = _PHYSIC_CONST.SPEED if not is_running else _PHYSIC_CONST.SPEED_RUN
	# Reduce max speed if jumping and running
	if is_running && !_OWNER.is_on_floor():
		max_speed *= 0.70 # 30%

	if direction.x != 0:
		# Acceleration
		_OWNER.velocity.x = lerp(_OWNER.velocity.x, direction.x * max_speed, multipler_acceleration * _PHYSIC_CONST.FACTOR_ACCELERATION)
	else:
		# Deceleration
		if abs(_OWNER.velocity.x) <= _PHYSIC_CONST.FIX_MIN_ERROR: # Fix issue in small value speed
			_OWNER.velocity.x = 0
		else:
			_OWNER.velocity.x = lerp(_OWNER.velocity.x, 0, multipler_deceleration * _PHYSIC_CONST.FACTOR_DECELERATION)

	# Apply gravity
	if _OWNER.velocity.y < 0:
		_OWNER.velocity.y += _PHYSIC_CONST.GRAVITY * delta
	else:
		_OWNER.velocity.y += _PHYSIC_CONST.FALL_GRAVITY * delta

	#//TODO: Slopes
	_OWNER.velocity = _OWNER.move_and_slide(_OWNER.velocity, _PHYSIC_CONST.FLOOR_NORMAL)

	# Last Corrections
	if _OWNER.is_on_wall():
		_OWNER.velocity.x = 0
