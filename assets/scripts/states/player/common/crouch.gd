extends "../base_state.gd"

var old_run_speed: float = 0

func on_enter_state() -> void:
	.on_enter_state()

	old_run_speed = _PHYSIC_CONST.SPEED_RUN
	#// REFACTOR MakeCollision
	#_OWNER.MakeCollision(3.7f)

	_PHYSIC_CONST.SPEED_RUN = _PHYSIC_CONST.SPEED

func on_leave_state():
	_PHYSIC_CONST.SPEED_RUN = old_run_speed

	if _OWNER.input.direction.y == 0:
		if _OWNER.raycast_up.is_colliding():
			pass
			#// TODO ACTIVATE
			#await ToSignal(GetTree().CreateTimer(0.3f), "timeout")

		#// REFACTOR MakeCollision
		#_OWNER.MakeCollision()

func on_physics_process(delta: float) -> void:
	if _OWNER.is_on_floor():
		if abs(_OWNER.velocity.x) != 0:
			_OWNER.velocity.x -= _PHYSIC_CONST.FACTOR_DECELERATION * _OWNER.input.direction.x

		if _OWNER.input.get_jump_just_pressed():
			_OWNER.velocity.y = 0
			_OWNER.sm.transition("jump")
			return

		if _OWNER.input.direction.x != 0:
			_OWNER.input.direction = Vector2(0, _OWNER.input.direction.y)
	else:
		if _OWNER.input.direction.y == 0:
			var jump_state: State = _OWNER.sm.get_state("jump")
			jump_state.is_enter_state_physics_disabled = true
			_OWNER.sm.transition("jump")
			jump_state.is_enter_state_physics_disabled = false
			return

	if _OWNER.input.direction.y == 0:
		_OWNER.sm.transition("idle")
		return

	move(delta)
