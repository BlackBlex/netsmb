extends "../base_state.gd"

var entity_grabbed: Entity = null
var entity_parent: Node = null
var processed: bool = false

func on_enter_state() -> void:
	.on_enter_state()

	if not processed:
		processed = true
		if entity_grabbed:
			entity_grabbed.velocity = Vector2.ZERO
			entity_grabbed.input.direction = Vector2.ZERO
			if _OWNER.powerup_current > Enums.PowerupType.SMALL:
				entity_grabbed.position = Vector2((_OWNER.get_size().x / 4) + (entity_grabbed.get_size().x / 2), (entity_grabbed.get_size().y / 2) - 4)
			else:
				entity_grabbed.position = Vector2((_OWNER.get_size().x / 4) + (entity_grabbed.get_size().x / 2), 0)

			entity_parent = entity_grabbed.get_parent()
			entity_parent.call_deferred("remove_child", entity_grabbed)
			_OWNER.position_pivot.call_deferred("add_child", entity_grabbed)
			_OWNER.position_pivot.call_deferred("move_child", entity_grabbed, 0)
			_OWNER.sm.get_state("kick").entity_grabbed = entity_grabbed
			_OWNER.sm.get_state("kick").entity_parent = entity_parent
		else:
			_OWNER.sm.transition(_OWNER.sm.get_state_last_name)

func on_physics_process(delta: float) -> void:
	_OWNER.image.playing = _OWNER.velocity.x != 0

	if not _OWNER.image.playing:
		_OWNER.image.frame = 0

	if _OWNER.input.get_jump_just_pressed() and _OWNER.is_on_floor():
		_OWNER.velocity.y = 0
		_OWNER.sm.transition("hold_jump")
		return

	if _OWNER.input.get_run_released():
		_OWNER.sm.transition("kick")
		return

	move(delta)
