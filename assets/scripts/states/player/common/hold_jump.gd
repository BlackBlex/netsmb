extends "jump.gd"

func on_enter_state() -> void:
	is_play_imagen_disabled = true
	.on_enter_state()
	is_play_imagen_disabled = false
	_OWNER.image.frame = 1

func on_physics_process(delta: float) -> void:
	_OWNER.image.frame = 1
	is_jumping = _OWNER.velocity.y == 0

	if _OWNER.is_on_floor() and not is_jumping:
		_OWNER.sm.transition("hold")

	if _OWNER.input.get_run_released():
		_OWNER.sm.transition("kick")
		return

	move(delta)
