extends "../jump.gd"

var is_crounched: bool = false

func on_enter_state() -> void:
	is_crounched = _OWNER.input.direction.y == 1

	if not is_enter_state_disabled && not is_crounched:
		.on_enter_state()

	if not is_enter_state_physics_disabled && is_crounched:
		_OWNER.velocity.y = _PHYSIC_CONST.MAX_JUMP_VELOCITY
		is_jumping = true

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.y == 1 && not is_crounched:
		_OWNER.sm.transition("crouch")
		return

	is_crounched = _OWNER.input.direction.y == 0

	.on_physics_process(delta)
