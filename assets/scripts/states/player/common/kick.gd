extends "../base_state.gd"

var entity_grabbed: Entity = null
var entity_parent: Node = null

func on_enter_state() -> void:
	.on_enter_state()

	if entity_grabbed:
		entity_grabbed.set_collision_mask_bit(Enums.Collision.BLOCK, true)
		entity_grabbed.area_hit.set_collision_mask_bit(Enums.Collision.BLOCK, true)
		entity_grabbed.get_parent().call_deferred("remove_child", entity_grabbed)
		entity_parent.call_deferred("add_child", entity_grabbed)
		entity_grabbed.position += (_OWNER.position + Vector2(8, 0)) * _OWNER.position_pivot.scale.x # + Vector2((entity_grabbed.position.x + 8) * _OWNER.position_pivot.scale.x, -(entity_grabbed.get_size().y / 2))

		entity_grabbed.kicked_by = _OWNER
		if _OWNER.input.direction.y == 0:
			entity_grabbed.input.direction.x = _OWNER.position_pivot.scale.x
		elif _OWNER.input.direction.y == -1:
			entity_grabbed.velocity.x = _OWNER.velocity.x
			entity_grabbed.velocity.y = _PHYSIC_CONST.MAX_JUMP_VELOCITY * 1.5
		else:
			entity_grabbed.kicked_by = null

		entity_grabbed.area_hit.set_collision_mask_bit(Enums.Collision.PLAYER, true)
		entity_grabbed.area_player.set_collision_mask_bit(Enums.Collision.PLAYER, true)

		Audio.play_effect("smw_kick.ogg")
		entity_grabbed = null
		entity_parent = null
		_OWNER.sm.get_state("hold").entity_grabbed = null
		_OWNER.sm.get_state("hold").entity_parent = null
		_OWNER.sm.get_state("hold").processed = false
	else:
		_OWNER.sm.transition(_OWNER.sm.get_state_last_name())

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x == 0 and _OWNER.velocity.x == 0:
		_OWNER.sm.transition("idle")
		return

	if _OWNER.input.direction.x != 0 or _OWNER.velocity.x != 0:
		_OWNER.sm.transition("walk")
		return

	move(delta)
