extends "../base_state.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.y == 0:
		_OWNER.sm.transition("idle")
		return

	_OWNER.input.direction.x = 0
	move(delta)
