extends "../run.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.get_jump_just_pressed() && _OWNER.is_on_floor():
		_OWNER.velocity.y = 0
		_OWNER.sm.transition("run_jump")
		return

	if _OWNER.input.direction.y == 1:
		_OWNER.sm.transition("crouch")
		return

	.on_physics_process(delta)
