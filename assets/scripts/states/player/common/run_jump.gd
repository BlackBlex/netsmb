extends "jump.gd"

func on_enter_state() -> void:
	is_enter_state_physics_disabled = true
	.on_enter_state()
	is_enter_state_physics_disabled = false

	_OWNER.velocity.y = _PHYSIC_CONST.MAX_JUMP_VELOCITY * _PHYSIC_CONST.MULTIPLER_RUN_JUMP_IMPULSE
	is_jumping = true

	if is_jumping:
		Audio.play_effect("player-jump.ogg")
