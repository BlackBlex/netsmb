extends "../base_state.gd"

var _in_other_state: bool = false

func on_enter_state() -> void:
	_OWNER.image.connect("animation_finished", self, "_return_last_state")
	.on_enter_state()

	Audio.play_effect("fireball.ogg")

	var projectil: Projectile = Resources.PROJECTILE.instance() as Projectile
	var info: ProjectileInfo = ProjectileInfo.new()
	info.variant = _OWNER.powerup_current_variant
	info.load_frames()
	projectil.info = info
	projectil.position = _OWNER.position
	_OWNER.get_tree().root.add_child(projectil, true)
	projectil.set_direction(_OWNER.position_pivot.scale * Vector2(1, 0))

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x != 0:
		_in_other_state = true
		_OWNER.sm.transition("walk")

	move(delta)

func on_leave_state() -> void:
	_OWNER.image.disconnect("animation_finished", self, "_return_last_state")

func _return_last_state() -> void:
	if not _in_other_state:
		_OWNER.sm.transition(_OWNER.sm.get_state_last_name())

	_in_other_state = false
