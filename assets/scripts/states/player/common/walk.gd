extends "../walk.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.y == 1:
		_OWNER.sm.transition("crouch")
		return

	.on_physics_process(delta)
