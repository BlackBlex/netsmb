extends "base_state.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x != 0:
		_OWNER.sm.transition("walk")
		return

	if _OWNER.input.get_jump_just_pressed() && _OWNER.is_on_floor():
		_OWNER.velocity.y = 0
		_OWNER.sm.transition("jump")
		return

	move(delta)
