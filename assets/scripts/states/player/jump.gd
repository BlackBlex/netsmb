extends "base_state.gd"

var is_jumping: bool = false

func on_enter_state() -> void:
	if not is_enter_state_disabled:
		.on_enter_state()

	if not is_enter_state_physics_disabled:
		_OWNER.velocity.y = _PHYSIC_CONST.MAX_JUMP_VELOCITY
		is_jumping = true

	if is_jumping:
		Audio.play_effect("player-jump.ogg")

func on_physics_process(delta: float) -> void:
	if _OWNER.input.get_jump_released() and _OWNER.velocity.y < _PHYSIC_CONST.MIN_JUMP_VELOCITY:
		_OWNER.velocity.y = _PHYSIC_CONST.MIN_JUMP_VELOCITY

	if _OWNER.is_on_floor() and not is_jumping:
		if abs(_OWNER.velocity.x) > _PHYSIC_CONST.SPEED:
			_OWNER.sm.transition("run")
			return
		else:
			_OWNER.sm.transition("walk")
			return

	is_jumping = _OWNER.velocity.y == 0

	move(delta)
