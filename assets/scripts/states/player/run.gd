extends "base_state.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.get_jump_just_pressed() && _OWNER.is_on_floor():
		_OWNER.velocity.y = 0
		_OWNER.sm.transition("jump")
		return

	if abs(_OWNER.velocity.x) < _PHYSIC_CONST.SPEED:
		_OWNER.image.frames.set_animation_speed("run", GlobalData.FPS_ANIMATION)
		_OWNER.sm.transition("walk")
		return
	else:
		if abs(_OWNER.velocity.x) < _PHYSIC_CONST.SPEED_RUN:
			var speedAnim: float = _OWNER.image.frames.get_animation_speed("run")

			if _OWNER.input.direction.x != 0:
				_OWNER.image.frames.set_animation_speed("run", speedAnim + 0.05)
			else:
				_OWNER.image.frames.set_animation_speed("run", speedAnim - 0.05)
		else:
			_OWNER.image.frames.set_animation_speed("run", GlobalData.FPS_ANIMATION * 2)

	move(delta, Vector2.INF, _PHYSIC_CONST.MULTIPLER_RUN_ACCELERATION, _PHYSIC_CONST.MULTIPLER_RUN_DECELERATION)
