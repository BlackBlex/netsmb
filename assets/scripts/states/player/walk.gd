extends "base_state.gd"

func on_physics_process(delta: float) -> void:
	if _OWNER.input.direction.x == 0:
		if _OWNER.velocity.x == 0:
			_OWNER.sm.transition("idle")
			return

	if _OWNER.input.get_jump_just_pressed() && _OWNER.is_on_floor():
		_OWNER.velocity.y = 0
		_OWNER.sm.transition("jump")
		return

	if _OWNER.input.get_run_pressed() || _OWNER.input.get_altrun_pressed():
		if abs(_OWNER.velocity.x) > _PHYSIC_CONST.SPEED:
			_OWNER.image.frames.set_animation_speed("walk", GlobalData.FPS_ANIMATION)
			_OWNER.sm.transition("run")
			return
		else:
			var speedAnim: float = _OWNER.image.frames.get_animation_speed("walk")

			if speedAnim <= 0.1:
				speedAnim = GlobalData.FPS_ANIMATION

			if _OWNER.input.direction.x != 0:
				if _OWNER.velocity.x == 0:
					_OWNER.image.frames.set_animation_speed("walk", GlobalData.FPS_ANIMATION)
				else:
					_OWNER.image.frames.set_animation_speed("walk", speedAnim + 0.1)
			else:
				_OWNER.image.frames.set_animation_speed("walk", speedAnim - 0.1)

	move(delta)
