extends "res://addons/gd-plug/plug.gd"

func _plugging():
	# Declare plugins with plug(repo, args)
	# For example, clone from github repo("user/repo_name")
	# plug("imjp94/gd-YAFSM") # By default, gd-plug will only install anything from "addons/" directory
	# Or you can explicitly specify which file/directory to include
	# plug("imjp94/gd-YAFSM", {"include": ["addons/"]}) # By default, gd-plug will only install anything from "addons/" directory

	connect("updated", self, "post_update")

	# Library for music in spc
	plug("MightyPrinny/godot-FLMusicLib", {"tag": "0.5.1", "install_root": "addons/FLMusicLib", "include": ["godot-FLMusicLib/FLMusicLib/"]})

	# Logger
	plug("https://gitlab.com/godot-stuff/gs-logger/", {"branch": "3.3"})

	# SQLite
	#plug("2shady4u/godot-sqlite", {"tag": "v3.0", "install_root": "addons/godot-sqlite", "include": ["godot-FLMusicLib/FLMusicLib/"]})

	# Unit test
	plug("bitwes/Gut")
	#plug("MikeSchulze/gdUnit3", {"branch": "master", "dev": true})

	# Extras
	plug("zaevi/godot-filesystem-view", {"dev": true})

func post_update(plugin):
	print("%s updated" % plugin.name)
	print(str(plugin))
