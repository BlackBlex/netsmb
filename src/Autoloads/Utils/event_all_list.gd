class_name EventAllList
extends BaseScript

var common: _CommonEvents = _CommonEvents.new()
var level: _LevelEvents = _LevelEvents.new()
var level_editor: _LevelEditorEvents = _LevelEditorEvents.new()
var pswitch: _PSwitchEvents = _PSwitchEvents.new()
var editor: _EditorEvents = _EditorEvents.new()
var particle: _ParticleEvents = _ParticleEvents.new()
var player: _PlayerEvents = _PlayerEvents.new()
var npc: _NpcEvents = _NpcEvents.new()
var block: _BlockEvents = _BlockEvents.new()

class _CommonEvents:
	const completed = "common_completed"
	const finished = "common_finished"
	const successed = "common_successed"
	const loaded = "common_loaded"
	const changed = "common_changed"
	const selected = "common_selected"

class _LevelEvents:
	#Occurs when level is initialized (contains: LevelRoot levelInitialized)
	const initialized = "level_initialized"
	#Occurs when level is started (contains: LevelRoot levelStarted)
	const started = "level_started"
	#Occurs when level is loaded (contains: LevelRoot levelLoaded)
	const loaded = "level_loaded"
	#Occurs when level is finished (contains: int exitType)
	const finished = "level_finished"

class _LevelEditorEvents:
	#Occurs when user put a new data info (contains: DataInfo infoPlace)
	const putted = "level_editor_putted"

class _BlockEvents:
	#Occurs when block is spawned in level, only ocurr when FactoryObject is used (contains: Entity whoSpawned, Block instance)
	const spawned = "block_spawned"
	#Occurs when block is hit in level, only ocurr when FactoryObject is used (contains: Entity whoHitted, Block instance)
	const hit = "block_hit"
	#Occurs when block is break in level, only ocurr when FactoryObject is used (contains: Entity whoBreaked)
	const breakk = "block_break"

class _PSwitchEvents:
	#Occurs when p-switch is pressed (contains: Entity whoCall)
	const started = "pswitch_started"
	#Occurs when time of p-switch is finished
	const ended = "pswitch_ended"

class _EditorEvents:
	#Occurs when user add new block (contains: string blockPath, DataInfoType type)
	const block_added = "editor_block_added"
	#Occurs when user add new ground block (contains: string nameBlock, string Style)
	const block_ground_added = "editor_block_ground_added"
	#Occurs when user add new npc (contains: string npcPath, DataInfoType type)
	const npc_added = "editor_npc_added"
	#Occurs when user load a resource (image or binary file)
	const resource_loaded = "editor_resource_loaded"

class _ParticleEvents:
	#Occurs when particle is created (contains: Entity whoCall, Particle instance)
	const created = "particle_created"
	#Occurs when particle is finished (contains: Entity whoCall)
	const finished = "particle_finished"

class _PlayerEvents:
	#Occurs when player is died
	const died = "player_died"
	#Occurs when player is given a life
	const up_given = "player_up_given"

class _NpcEvents:
	#Occurs when enemy is created, only ocurr when FactoryObject is used (contains: Enemy instance)
	const enemy_created = "enemy_created"
	#Occurs when powerUp is created, only ocurr when FactoryObject is used (contains: Powerup instance)
	const powerup_created = "powerup_created"
	#Occurs when object is created, only ocurr when FactoryObject is used (contains: Object instance)
	const object_created = "object_created"
	#Ocurrs when npc is grabbed by player or other entity (contains: Entity whoGrabbed)
	const grabbed = "npc_grabbed"
