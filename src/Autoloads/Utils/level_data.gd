class_name LevelData
extends JSONObject

var editor_version: Version
var name: String = "New level"
var background: String = "None"
var music: String = "None"
var identifier: String = ""
var map_size: Vector2
var map_origin: Vector2
var connect_sides_horizontally: bool = false
var connect_sides_vertically: bool = false
var disable_left_movement: bool = false

# Dictionary of int with NpcInfo
var rewards: Dictionary = {} #//TODO: Security
# Dictionary of int with Dictionary of int with Array of Vector2
var tiles: Dictionary = {} #//TODO: Security
# Dictionary of int with Dictionary of vector2 with DataInfo
var data: Dictionary = {} #//TODO: Security
# Dictionary of vector2 with NpcInfo
var grabbed: Dictionary = {} #//TODO: Security
# Dictionary of vector2 with DataInfo
var delete: Dictionary = {} #//TODO: Security
# Dictionary of string with variant
var values: Dictionary = {} #//TODO: Security
# Array of string
var events: Array = [] #//TODO: Security
# Dictionary of int with Array of Variant
var layers: Dictionary = {1: ["Default", true]} #//TODO: Security

func _ready() -> void:
	map_size = Vector2(25, 19)
	map_origin = Vector2(0, 3)

func get_class() -> String:
	return "LevelData"

func get_map_size_real() -> Vector2:
	return Utility.convert_position_to_real(map_size)

func get_map_origin_real() -> Vector2:
	return Utility.convert_position_to_real(map_origin)

func has_tile(layer_id: int, position: Vector2) -> bool:
	var to_result: bool = false

	for tile in tiles:
		if tiles[tile].has(layer_id):
			to_result = tiles[tile][layer_id].has(position)

		if to_result:
			break

	return to_result
