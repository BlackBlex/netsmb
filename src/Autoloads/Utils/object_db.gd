class_name ObjectDB

#Dictionary of string with DataInfoType
var blocks: Dictionary = {} setget _set_dictionary_blocks, _get_dictionary_blocks
#Dictionary of string with DataInfoType
var npcs: Dictionary = {} setget _set_dictionary_npcs, _get_dictionary_npcs
#Dictionary of string with Structure {top: Region/Image, left: Region/Image, right: Region/Image, bottom: Region/Image}
var backgrounds: Dictionary = {} setget _set_dictionary_backgrounds, _get_dictionary_backgrounds
#Dictionary of string with Keyword
var keywords: Dictionary = {} setget _set_dictionary_keywords, _get_dictionary_keywords


func add_block(name: String, data_info_type: int) -> void:
	if not blocks.has(name):
		Event.publish(self, A.EventList.editor.block_added, [name, data_info_type])
		blocks[name] = data_info_type
		var new_block := {"name": name, "type": data_info_type}
		GlobalData.db.insert_row("blocks", new_block)

func add_npc(name: String, data_info_type: int) -> void:
	if not npcs.has(name):
		Event.publish(self, A.EventList.editor.npc_added, [name, data_info_type])
		npcs[name] = data_info_type
		var new_npc := {"name": name, "type": data_info_type}
		GlobalData.db.insert_row("npcs", new_npc)

func parse_backgrounds() -> void:
	for key in backgrounds.keys():
		var sides: Dictionary = backgrounds[key]
		for side in sides.keys():
			var data = sides[side]

			if typeof(data) == TYPE_RECT2:
				var image = Resources.get_image("backgrounds/%s" % key) as Texture

				if image:
					backgrounds[key][side] = image.get_data().get_rect(data)
				else:
					Logger.msg_error("Cannot process backgrounds/%s" % key, Logger.CATEGORY_ASSET)


func _set_dictionary_backgrounds(new_dictionary: Dictionary) -> void:
	if backgrounds.empty():
		backgrounds = new_dictionary

func _set_dictionary_blocks(new_dictionary: Dictionary) -> void:
	if blocks.empty():
		blocks = new_dictionary

func _set_dictionary_npcs(new_dictionary: Dictionary) -> void:
	if npcs.empty():
		npcs = new_dictionary

func _set_dictionary_keywords(new_dictionary: Dictionary) -> void:
	if keywords.empty():
		keywords = new_dictionary

func _get_dictionary_backgrounds() -> Dictionary:
	return backgrounds

func _get_dictionary_blocks() -> Dictionary:
	return blocks

func _get_dictionary_npcs() -> Dictionary:
	return npcs

func _get_dictionary_keywords() -> Dictionary:
	return keywords
