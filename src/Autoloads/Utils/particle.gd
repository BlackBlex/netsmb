class_name Particle
extends CPUParticles2D

var _emitting: bool = false
var _life_time: float = 0
var _who_call: Object = null

func _process(delta: float) -> void:
	if _emitting and one_shot and _who_call != null:
		_life_time -= delta
		if _life_time < 0:
			_emitting = false
			Event.publish(self, A.EventList.particle.finished, [_who_call])
			queue_free()


func set_who_call(who_call: Object) -> void:
	_who_call = who_call

func set_emit(emit: bool) -> void:
	_emitting = emit

	if emit:
		_life_time = lifetime
