extends CanvasLayer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _funcref_clean: FuncRefExt = null setget _set_funcref_clean
#Array of FuncRefExt
var _funcrefs_actions_confirmed: Array = []
#Array of FuncRefExt
var _funcrefs_actions_cancelled: Array = []

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _dialog: AcceptDialog = $Dialog
onready var _confirm: ConfirmationDialog = $ConfirmDialog
onready var _input: AcceptDialog = $InputDialog
onready var _input_value: FieldComponent = $InputDialog/Value

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_funcref_clean = Instanceator.make_funcref_ext(self, "_clean_confirmation_dialog")
	_input_value.set_value("")

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_cancelled_dialog() -> void:
	for func_ref in _funcrefs_actions_cancelled:
		if func_ref is FuncRefExt and func_ref.is_valid():
			func_ref.call_func()

	_funcrefs_actions_cancelled.clear()

func _on_confirmed_dialog(confirm_dialog: bool) -> void:
	for func_ref in _funcrefs_actions_confirmed:
		if func_ref is FuncRefExt and func_ref.is_valid():
			if confirm_dialog:
				func_ref.call_func()
			else:
				func_ref.call_func(_input_value.get_value())

	_funcrefs_actions_confirmed.clear()

func _on_input_value_text_changed(_value: String) -> void:
	if _input.visible:
		_input.emit_signal("confirmed")
		_input.hide()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func show_alert(message: String, size: Vector2 = Vector2.ZERO) -> void:
	_show_message(tr("KEY_ALERT"), message, size)

func show_confirm_dialog(title: String, message: String, funcref_confirmed: FuncRefExt = null, funcref_cancelled: FuncRefExt = null, size: Vector2 = Vector2.ZERO) -> void:
	_confirm.window_title = title
	_confirm.dialog_text = message
	_confirm.rect_min_size = size
	var _err = _confirm.connect("confirmed", self, "_on_confirmed_dialog", [true])

	if _err != OK:
		printerr("Can't connect _confirm to _on_confirmed_dialog")
		return

	_err = _confirm.get_cancel().connect("pressed", self, "_on_cancelled_dialog")

	if _err != OK:
		printerr("Can't connect _confirm.cancel to _on_cancelled_dialog")
		return

	_funcrefs_actions_confirmed.append(_funcref_clean)
	_funcrefs_actions_cancelled.append(_funcref_clean)

	if funcref_confirmed and funcref_confirmed.is_valid():
		_funcrefs_actions_confirmed.append(funcref_confirmed)

	if funcref_cancelled and funcref_cancelled.is_valid():
		_funcrefs_actions_cancelled.append(funcref_cancelled)

	_confirm.popup_centered()

func show_error(message: String, size: Vector2 = Vector2.ZERO) -> void:
	_show_message("Error", message, size)

func show_information(message: String, size: Vector2 = Vector2.ZERO) -> void:
	_show_message(tr("KEY_INFORMATION"), message, size)

func show_input_dialog(title: String, funcref_confirmed: FuncRefExt = null, size: Vector2 = Vector2.ZERO) -> void:
	_input.window_title = title
	_input.rect_min_size = size

	var _err = _input.connect("confirmed", self, "_on_confirmed_dialog", [false])

	if _err != OK:
		printerr("Can't connect _confirm to _on_confirmed_dialog")
		return

	_funcrefs_actions_confirmed.append(_funcref_clean)
	if funcref_confirmed and funcref_confirmed.is_valid():
		_funcrefs_actions_confirmed.append(funcref_confirmed)

	_input.popup_centered()
	_input_value.grab_focus()


func _clean_confirmation_dialog() -> void:
	if _confirm.is_connected("confirmed", self, "_on_confirmed_dialog"):
		_confirm.disconnect("confirmed", self, "_on_confirmed_dialog")

	if _confirm.get_cancel().is_connected("pressed", self, "_on_cancelled_dialog"):
		_confirm.get_cancel().disconnect("pressed", self, "_on_cancelled_dialog")

	if _input.is_connected("confirmed", self, "_on_confirmed_dialog"):
		_input.disconnect("confirmed", self, "_on_confirmed_dialog")

func _set_funcref_clean(_newFunc: FuncRefExt) -> void:
	pass

func _show_message(title: String, message: String, size: Vector2) -> void:
	_dialog.window_title = title
	_dialog.dialog_text = message
	_dialog.rect_min_size = size
	_dialog.popup_centered()
