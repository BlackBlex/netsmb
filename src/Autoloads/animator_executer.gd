class_name AnimatorExecuter

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var contain_animations: bool = false setget _set_contain_animations, _get_contain_animations

var _helper: Helper = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_animator_finished() -> void:
	Event.publish(self, A.EventList.common.completed)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup(parent: Node) -> void:
	_helper = Helper.new()
	_helper.name = "%sAnimation" % parent.name

	Event.subscribe(A.EventList.common.finished, self, "_on_animator_finished", _helper)

	parent.get_tree().root.add_child(_helper, true)

func alternate_sprite(animated_sprite: AnimatedSprite, frames: SpriteFrames, other_frames: SpriteFrames, duration: float = 1, interval: float = 0.1) -> AnimatorExecuter:
	_helper.alternate_sprite(animated_sprite, frames, other_frames, duration, interval)

	return self

func blink(canvas: CanvasItem, alpha: float = 0, duration: float = 1, interval: float = 0.1, wait_to_finish_last: bool = false) -> AnimatorExecuter:
	_helper.blink(canvas, alpha, duration, interval, wait_to_finish_last)

	return self

func interpolate_property(node: Object, property: String, value_initial, value_final, duration: float = 1, delay: float = 0, transition: int = Tween.TRANS_LINEAR, ease_type: int = Tween.EASE_OUT_IN, wait_to_finish_last: bool = false) -> AnimatorExecuter:
	_helper.interpolate_property(node, property, value_initial, value_final, duration, delay, transition, ease_type, wait_to_finish_last)

	return self

func shake(node: Node2D, intensity: float = 1.8, variable_intensity: bool = false, duration: float = 0.5, interval: float = 0.1, wait_to_finish_last: bool = false) -> AnimatorExecuter:
	_helper.shake(node, intensity, variable_intensity, duration, interval, wait_to_finish_last)

	return self

func _set_contain_animations(_value: bool) -> void:
	pass

func _get_contain_animations() -> bool:
	return _helper.contain_animations

func start() -> void:
	_helper.start()

##############################################################################
#
#----------------------------------------------------------------------------
#	INNER CLASS
#----------------------------------------------------------------------------
#
##############################################################################
class Helper extends Node:
	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	signal animated_sprite_finished()

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	CONSTANTS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ENUMS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	EXPORTED VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################
	var contain_animations: bool = false setget _set_contain_animations, _get_contain_animations

	var _animator: Tween = Tween.new()
	var _timer_animated_sprite: Timer = Timer.new()
	var _animated_sprite: AnimatedSprite = null

	#Array of AnimationFrames
	var _animations: Array = []
	#Array of SpriteFrames
	var _spriteframes: Array = []

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ONREADY VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VIRTUAL METHODS FROM GODOT
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func _ready() -> void:
		_animator.name = "%sAnimator" % name
		_timer_animated_sprite.name = "%sTimerAnimatedSprite" % name
		add_child(_animator, true)
		add_child(_timer_animated_sprite, true)

		Event.subscribe(A.EventList.common.finished, self, "_on_animator_finished", self)

		var _err = _timer_animated_sprite.connect("timeout", self, "_on_timer_animated_sprite_timeout")

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS CALLBACK
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func _on_animator_finished() -> void:
		_animator.queue_free()
		_timer_animated_sprite.queue_free()
		queue_free()

	func _on_timer_animated_sprite_timeout() -> void:
		if not _spriteframes.empty():
			_animated_sprite.frames = _spriteframes.pop_front()
			_timer_animated_sprite.start()
			return

		emit_signal("animated_sprite_finished")

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	FUNCTIONS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func start() -> void:
		var first: bool = true
		var contains: bool = false
		var configured: bool = false

		while not _animations.empty():
			var animation: AnimationFrames = _animations.pop_front()

			if animation.is_animated_sprite:
				if not configured:
					configured = true
					_animated_sprite = animation.animated_sprite
					_timer_animated_sprite.wait_time = animation.duration
					_timer_animated_sprite.one_shot = false

			if animation.wait_to_finish_last:
				contains = false

				if _timer_animated_sprite.is_stopped() and not _spriteframes.empty() and not first:
					_timer_animated_sprite.start()

				if not _animator.is_active() and not first:
					var _d = _animator.start()
					yield(_animator, "tween_all_completed")

				if not _timer_animated_sprite.is_stopped():
					yield(self, "animated_sprite_finished")

			for frame in animation.frames:
				if animation.is_animated_sprite:
					_spriteframes.push_back(frame.value_initial as SpriteFrames)
				else:
					if not contains:
						contains = true

					var _d = _animator.interpolate_property(animation.canvas if animation.node == null else animation.node, animation.property, frame.value_initial, frame.value_final, animation.duration, animation.transition, animation.ease_type, frame.delay)

			if first:
				first = false

		if not _spriteframes.empty():
			_timer_animated_sprite.start()

		if contains:
			var _d = _animator.start()
			yield(_animator, "tween_all_completed")

		if not _timer_animated_sprite.is_stopped():
			yield(self, "animated_sprite_finished")

		Event.publish(self, A.EventList.common.finished)

	func alternate_sprite(animated_sprite: AnimatedSprite, frames: SpriteFrames, other_frames: SpriteFrames, duration: float, interval: float) -> void:
		var total: int = _calculate_final_interactions(duration, interval)

		var current_frame: SpriteFrames = frames
		# Array of AnimationFrame
		var list_frames: Array = []
		var new_frame: AnimationFrame = null

		for current in range(1, total):
			if current % 2 == 0:
				current_frame = frames
			else:
				current_frame = other_frames

			new_frame = AnimationFrame.new()
			new_frame.value_initial = current_frame

			list_frames.push_back(new_frame)

		var animation_frame: AnimationFrames = AnimationFrames.new()
		animation_frame.wait_to_finish_last = false
		animation_frame.is_animated_sprite = true
		animation_frame.animated_sprite = animated_sprite
		animation_frame.frames = list_frames
		animation_frame.duration = interval
		_animations.push_back(animation_frame)

	func interpolate_property(node: Object, property: String, value_initial, value_final, duration: float, delay: float, transition: int, ease_type: int, wait_to_finish_last: bool) -> void:
		var frames: Array = []

		var animation_frame: AnimationFrame = AnimationFrame.new()
		animation_frame.value_initial = value_initial
		animation_frame.value_final = value_final
		animation_frame.delay = delay
		frames.push_back(animation_frame)

		var animation_frames: AnimationFrames = AnimationFrames.new()
		animation_frames.node = node
		animation_frames.property = property
		animation_frames.wait_to_finish_last = wait_to_finish_last
		animation_frames.frames = frames
		animation_frames.duration = duration
		animation_frames.transition = transition
		animation_frames.ease_type = ease_type
		_animations.push_back(animation_frames)

	func blink(canvas: CanvasItem, alpha: float, duration: float, interval: float, wait_to_finish_last: bool) -> void:
		var total: int = _calculate_final_interactions(duration, interval)

		var color_original: Color = canvas.modulate
		var color_transparent: Color = _modify_alpha_color(color_original, alpha)

		var color_last: Color = color_original
		var color_new: Color = color_transparent

		var list_frames: Array = []
		var new_frame: AnimationFrame = null

		for current in range(1, total):
			if current % 2 == 0:
				# Transparent
				color_last = color_new
				color_new = color_transparent
			else:
				# Normal
				color_last = color_new
				color_new = color_original

			new_frame = AnimationFrame.new()
			new_frame.value_initial = color_last
			new_frame.value_final = color_new
			new_frame.delay = interval * current

			list_frames.push_back(new_frame)

		var animation_frames: AnimationFrames = AnimationFrames.new()
		animation_frames.canvas = canvas
		animation_frames.property = "modulate"
		animation_frames.wait_to_finish_last = wait_to_finish_last
		animation_frames.frames = list_frames
		animation_frames.duration = interval
		animation_frames.transition = Tween.TRANS_LINEAR
		animation_frames.ease_type = Tween.EASE_OUT_IN
		_animations.push_back(animation_frames)

	func shake(node: Node2D, intensity: float, variable_intensity: bool, duration: float, interval: float, wait_to_finish_last: bool) -> void:
		var total: int = _calculate_final_interactions(duration, interval)

		var position_original: Vector2 = node.position
		var position_last: Vector2 = position_original
		var position_new: Vector2 = position_last

		var list_frames: Array = []
		var new_frame: AnimationFrame = null

		var intensity_factor: float = -1
		var intensity_half: float = intensity * 0.5

		for current in range(1, total):
			intensity_factor = 0

			if variable_intensity:
				intensity_factor = rand_range(-intensity_half, intensity_half)

			if current % 2 == 0:
				# Left
				position_new.x = position_last.x - (intensity + intensity_factor)
			else:
				# Right
				position_new.x = position_last.x + (intensity + intensity_factor)

				if current == total:
					# Normal position
					position_new = position_original

			new_frame = AnimationFrame.new()
			new_frame.value_initial = position_last
			new_frame.value_final = position_new
			new_frame.delay = interval * current

			position_last = position_new
			list_frames.push_back(new_frame)

		var animation_frames: AnimationFrames = AnimationFrames.new()
		animation_frames.node = node
		animation_frames.property = "position"
		animation_frames.wait_to_finish_last = wait_to_finish_last
		animation_frames.frames = list_frames
		animation_frames.duration = interval
		animation_frames.transition = Tween.TRANS_LINEAR
		animation_frames.ease_type = Tween.EASE_OUT_IN
		_animations.push_back(animation_frames)


	func _calculate_final_interactions(duration: float, interval: float) -> int:
		var to_result: int = int(duration / interval)
		to_result -= 1 if to_result % 2 else 0

		return to_result

	func _modify_alpha_color(color: Color, alpha: float) -> Color:
		if alpha > 1:
			alpha /= 255

		return Color(color.r, color.g, color.b, alpha)

	func _set_contain_animations(_value: bool) -> void:
		pass

	func _get_contain_animations() -> bool:
		return not _animations.empty() or not _spriteframes.empty()

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	INNER CLASS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	class AnimationFrames:
		var wait_to_finish_last: bool
		var is_animated_sprite: bool
		var frames: Array
		var animated_sprite: AnimatedSprite
		var node: Object
		var canvas: CanvasItem
		var property: NodePath
		var duration: float
		var transition: int
		var ease_type: int

	class AnimationFrame:
		var value_initial
		var value_final
		var delay: float
