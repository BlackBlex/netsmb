extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var player_spc = FLMusicLib.new()
var play_spc: bool = false
var volume_general: float = 0 setget _set_volume_general, _get_volume_general
var volume_effect: float = 0 setget _set_volume_effect, _get_volume_effect

var _position: float = 0.0
var _position_effect: float = 0.0
var _bus_id_music: int = -1
var _bus_id_effect: int = -1
var _bus_id_on_water: int = -1

var _on_finish_funcref: Dictionary = {}

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var player: AudioStreamPlayer = $Player
onready var effects: Node = $Effects

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	add_child(player_spc)
	player_spc.set_gme_buffer_size(2048*5)
	player_spc.set_volume(0)

	_bus_id_music = AudioServer.get_bus_index("Music")
	_bus_id_effect = AudioServer.get_bus_index("Effect")
	_bus_id_on_water = AudioServer.get_bus_index("OnWater")

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_finish(effect: AudioStreamPlayer) -> void:
	var on_finish: FuncRefExt = null

	if _on_finish_funcref.has(effect.name):
		on_finish = _on_finish_funcref[effect.name]

		if on_finish.is_valid():
			on_finish.call_func()
			_on_finish_funcref.erase(effect.name)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clean_effects() -> void:
	for audio in effects.get_children():
		audio.stop()
		audio.queue_free()

func get_track_position() -> float:
	if play_spc:
		return player_spc.get_position_msec()
	else:
		return player.get_playback_position()

func finish_effect(effect: AudioStreamPlayer) -> void:
	if effect != null and effect.stream != null:
		var duration: float = effect.stream.get_length()
		effect.seek(duration - 0.05)

func play_music(name: String, position: float = 0, pitch: float = 1) -> void:
	var file: String = "music/%s" % name
	play_spc = name.ends_with("spc")
	if name.ends_with("spc"):
		file = A.Paths.audios.plus_file(file)
		player_spc.play_music(file, 0, false, 0, 0, position)
	else:
		var music: AudioStream = Resources.get_audio(file) as AudioStream

		player.stream = music
		player.pitch_scale = pitch
		player.play(position)

func play_effect(name: String, pitch: float = 1, on_finish: FuncRefExt = null) -> AudioStreamPlayer:
	var effect: AudioStreamPlayer = AudioStreamPlayer.new()
	effect.stream = Resources.get_audio("sound/%s" % name) as AudioStream
	effect.bus = "Effects"
	effect.name = "Effect-%s" % name
	effect.pitch_scale = pitch
	effects.add_child(effect)
	var _err = effect.connect("finished", self, "_on_finish", [effect])
	if _err == OK:
		if on_finish != null:
			_on_finish_funcref[effect.name] = on_finish

		effect.play()

		return effect
	else:
		effects.queue_free()

		return null

func stop_music() -> void:
	if not play_spc:
		player.stop()
	else:
		player_spc.stop_music()

func toggle() -> void:
	if not play_spc:
		if player.playing:
			_position = player.get_playback_position()
			player.stop()
		else:
			player.play(_position)
			_position = 0
	else:
		player_spc.toggle_pause()


func _calc_volume(volume: float) -> float:
	return ((volume * 70.0) / 100.0) - 70.0

func _get_volume_general() -> float:
	return volume_general

func _get_volume_effect() -> float:
	return volume_effect

func _set_volume_general(new_volume: float = 100) -> void:
	if new_volume >= 0 and new_volume <= 100:
		var volume: float = _calc_volume(new_volume)
		AudioServer.set_bus_volume_db(_bus_id_music, volume)
		player_spc.set_volume(volume)

		volume_general = new_volume
	else:
		Logger.msg_warn("Value for volume general is not in the range of 0 to 100")

func _set_volume_effect(new_volume: float = 100) -> void:
	if new_volume >= 0 and new_volume <= 100:
		var volume: float = _calc_volume(new_volume)
		AudioServer.set_bus_volume_db(_bus_id_effect, volume)
		volume_effect = new_volume
	else:
		Logger.msg_warn("Value for volume effect is not in the range of 0 to 100")
