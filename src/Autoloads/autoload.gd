extends Node

var Paths: PathList = load("res://src/Autoloads/path_list.gd").new() as PathList #setget _unused_set_path, _get_path
var EventList: EventAllList = load("res://src/Autoloads/Utils/event_all_list.gd").new() as EventAllList

var file_loader: File = File.new()
var directory_loader: Directory = Directory.new()
var cache_scripts: Dictionary = {}

var _classes: Dictionary = {}

func _ready() -> void:
	print("""#
#	 _   _      _    _____ __  __ ____
#	| \\ | |    | |  / ____|  \\/  |  _ \\
#	|  \\| | ___| |_| (___ | \\  / | |_) |
#	| . ` |/ _ \\ __|\\___ \\| |\\/| |  _ <
#	| |\\  |  __/ |_ ____) | |  | | |_) |
#	|_| \\_|\\___|\\__|_____/|_|  |_|____/
#
#	Version: %s
#	  Theme: %s
#""" % [GlobalData.editor_version, GlobalData.editor_theme])

	# ECS.print_info()
	Logger.print_info()

	Logger.logger_appenders.clear()
	var html = Logger.add_appender(FileAppender.new())
	html.layout = HtmlExtendsLayout.new()
	html.logger_level = Logger.LOG_LEVEL_ALL
	html.logger_format = Logger.LOG_FORMAT_FULL

	Logger.msg_info("Loading classes with its paths", Logger.CATEGORY_SYSTEM)
	_load_classes()

func _load_classes() -> void:
	_classes.clear()

	var classes: Array = ProjectSettings.get_setting("_global_script_classes")

	for class_info in classes:
		_classes[class_info["class"]] = class_info.path

func get_path_of_class(name: String) -> String:
	if _classes.has(name):
		return _classes[name]

	return ""
