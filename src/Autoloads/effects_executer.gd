class_name EffectsExecuter

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _helper: Helper = null
var _animator: AnimatorExecuter = null
var _is_animate: bool = false

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup(parent: Node2D) -> void:
	_helper = Helper.new()
	_helper.name = "%sEffects" % parent.name

	parent.get_tree().root.add_child(_helper, true)

func create() -> void:
	_helper.create()

	if _is_animate and _animator.contain_animations:
		_animator.start()
		yield(Event.wait(A.EventList.common.completed, _animator), "completed")
		_helper.destroy()
	else:
		yield(_helper, "effector_completed")
		_helper.destroy()

func attach(node: Node) -> EffectsExecuter:
	_helper.attach(node)

	return self

func animate() -> AnimatorExecuter:
	if not _is_animate:
		_animator = AnimatorExecuter.new()
		_animator.setup(_helper)
		_is_animate = true

	return _animator

func attach_visibility_notifier(on_screen: FuncRefExt = null, out_screen: FuncRefExt = null, view_port: bool = false) -> EffectsExecuter:
	if on_screen != null or out_screen != null:
		_helper.attach_visibility_notifier(on_screen, out_screen, view_port)

	return self

func breakk(slices: int = 4) -> EffectsExecuter:
	_helper.breakk(slices)

	return self

func get_fx_object() -> AnimatedSprite:
	if _helper:
		return _helper.animated_sprite

	return null

func get_path_follow() -> PathFollow2D:
	if _helper:
		return _helper.path2d_follow

	return null

func invert() -> EffectsExecuter:
	_helper.invert()

	return self

func invert_path() -> EffectsExecuter:
	_helper.invert_path()

	return self

func load_curve(curve_name: String) -> EffectsExecuter:
	_helper.load_curve(curve_name)

	return self

func load_image(category: int, image: String, other_path: bool = false) -> EffectsExecuter:
	_helper.load_image(category, image, other_path)

	return self

func move_to_path() -> EffectsExecuter:
	_helper.move_to_path()

	return self

func put_texture(texture: Texture) -> EffectsExecuter:
	_helper.put_texture(texture)

	return self

func positionate_float(x: float, y: float, wait_to_finish: bool = false) -> EffectsExecuter:
	return positionate(Vector2(x, y), wait_to_finish)

func positionate(position: Vector2, wait_to_finish: bool = false) -> EffectsExecuter:
	_helper.positionate(position, wait_to_finish)

	return self

func positionate_path_float(x: float, y: float, wait_to_finish: bool = false) -> EffectsExecuter:
	return positionate_path(Vector2(x, y), wait_to_finish)

func positionate_path(position: Vector2, wait_to_finish: bool = false) -> EffectsExecuter:
	_helper.positionate_path(position, wait_to_finish)

	return self

func scale(scale: Vector2, wait_to_finish: bool = false) -> EffectsExecuter:
	_helper.scale(scale, wait_to_finish)

	return self

func scale_path(scale: Vector2, wait_to_finish: bool = false) -> EffectsExecuter:
	_helper.scale_path(scale, wait_to_finish)

	return self

func rotate(degrees_or_radians: float, radians: bool = false) -> EffectsExecuter:
	if radians:
		_helper.rotate_radians(degrees_or_radians)
	else:
		_helper.rotate_degrees(degrees_or_radians)

	return self

func visible_image(visible: bool) -> EffectsExecuter:
	_helper.visible_image(visible)

	return self

##############################################################################
#
#----------------------------------------------------------------------------
#	INNER CLASS
#----------------------------------------------------------------------------
#
##############################################################################
class Helper extends Node:
	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	signal effector_completed()
	signal effect_successed()

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	CONSTANTS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ENUMS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	enum EffectCategory {PLAYER, NPC, OTHERS, CUSTOMS}

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	EXPORTED VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################
	var animated_sprite: AnimatedSprite = null
	var path2d: Path2D = null
	var path2d_follow: PathFollow2D = null

	#Array of FuncRefExt
	var _tasks: Array = []
	#Dictionary of int with FuncRefExt
	var _action_on_screen: Dictionary = {}
	#Dictionary of int with FuncRefExt
	var _action_out_screen: Dictionary = {}
	var _await_to_finish: bool = false
	var _counter: int = 0

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ONREADY VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VIRTUAL METHODS FROM GODOT
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func _ready() -> void:
		animated_sprite = AnimatedSprite.new()
		animated_sprite.name = "%sAnimatedSprite" % name
		path2d = Path2D.new()
		path2d.name = "%sPath" % name
		path2d_follow = PathFollow2D.new()
		path2d_follow.name = "%sPathFollow" % name
		path2d_follow.loop = false
		path2d_follow.rotate = false
		path2d.self_modulate = Color(0, 0, 0, 0)

		add_child(animated_sprite, true)
		add_child(path2d, true)
		path2d.add_child(path2d_follow, true)

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS CALLBACK
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func _on_out_screen(notifier: int, exited: bool) -> void:
		if exited:
			if not _action_out_screen.has(notifier):
				return

			var action: FuncRefExt = _action_out_screen[notifier]
			action.call_func()
		else:
			if not _action_on_screen.has(notifier):
				return

			var action: FuncRefExt = _action_on_screen[notifier]
			action.call_func()

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	FUNCTIONS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func create() -> void:
		while(_tasks.size() > 0):
			_await_to_finish = false
			var task = _tasks.pop_front()
			if task is FuncRefExt:
				task.call_func()

			if _await_to_finish:
				yield(self, "effect_successed")

		emit_signal("effector_completed")

	func destroy() -> void:
		animated_sprite.queue_free()
		path2d.queue_free()
		path2d_follow.queue_free()
		queue_free()

	func attach(node: Node) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "add_child", [node, true]))

	func attach_visibility_notifier(on_screen: FuncRefExt, out_screen: FuncRefExt, view_port: bool) -> void:
		var notifier: VisibilityNotifier2D = VisibilityNotifier2D.new()
		var where: String = "viewport" if view_port else "screen"

		_action_on_screen[_counter] = on_screen
		_action_out_screen[_counter] = out_screen

		var _err = notifier.connect("%s_entered" % where, self, "_on_out_screen", [_counter, false])
		_err = notifier.connect("%s_exited" % where, self, "_on_out_screen", [_counter, true])

		_counter += 1

		attach(notifier)

	func load_image(category: int, image: String, other_path: bool) -> void:
		var path: String = "effects/%s/%s"

		if other_path:
			path = image
		else:
			path = path % [Enums.get_name(EffectCategory, category), image]

		path = path.to_lower()

		var frame_result: SpriteFrames = Resources.process_texture(path)

		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_sprite_frames", frame_result))

	func put_texture(texture: Texture = null) -> void:
		if texture != null:
			var sprite_frame: SpriteFrames = SpriteFrames.new()
			sprite_frame.add_frame("default", texture, 0)
			sprite_frame.set_animation_speed("default", GlobalData.FPS_ANIMATION)

			_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_sprite_frames", sprite_frame))

	func breakk(slices: int) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(self, "_break", slices))

	func invert() -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_scale", animated_sprite.scale * Vector2(-1, 1)))

	func invert_path() -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(path2d, "set_scale", animated_sprite.scale * Vector2(-1, 1)))

	func load_curve(curve_name: String) -> void:
		var curve: Curve2D = Resources.get_custom("curve_%s" % curve_name)

		if curve != null:
			_tasks.push_back(Instanceator.make_funcref_ext(path2d, "set_curve", curve))
		else:
			print("curve not loaded")

	func move_to_path() -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(self, "remove_child", [animated_sprite]))
		_tasks.push_back(Instanceator.make_funcref_ext(path2d_follow, "add_child", animated_sprite))

	func positionate(position: Vector2, wait_to_finish: bool) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_position", position))

		if wait_to_finish:
			_tasks.push_back(Instanceator.make_funcref_ext(self, "_wait"))

	func positionate_path(position: Vector2, wait_to_finish: bool) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(path2d, "set_position", position))

		if wait_to_finish:
			_tasks.push_back(Instanceator.make_funcref_ext(self, "_wait"))

	func scale(scale: Vector2, wait_to_finish: bool) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_scale", scale))

		if wait_to_finish:
			_tasks.push_back(Instanceator.make_funcref_ext(self, "_wait"))

	func scale_path(scale: Vector2, wait_to_finish: bool) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(path2d, "set_scale", scale))

		if wait_to_finish:
			_tasks.push_back(Instanceator.make_funcref_ext(self, "_wait"))

	func rotate_degrees(degrees: float) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_rotation_degrees", degrees))

	func rotate_radians(radians: float) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_rotation", radians))

	func visible_image(visible: bool) -> void:
		_tasks.push_back(Instanceator.make_funcref_ext(animated_sprite, "set_visible", visible))

	func _break(slices: int) -> void:
		_await_to_finish = true
		var factor: int = slices / 4
		var width: int = slices / 2
		var height: int = slices - width
		var middle_point_width: int = width / 2
		var middel_point_height: int = height / 2
		var texture: Texture = animated_sprite.frames.get_frame(animated_sprite.animation, 0)
		var size_width: float = texture.get_size().x / width
		var size_height: float = texture.get_size().y / height

		var rigidbodies: Array = Arrays.build_2D_array(width, height)

		for y in range(0, height):
			for x in range(0, width):
				var rect_image: Image = null
				if texture is AtlasTexture:
					rect_image = texture.atlas.get_data().get_rect(Rect2(x * size_width, y * size_height, size_width, size_height))
				else:
					rect_image = texture.get_data().get_rect(Rect2(x * size_width, y * size_height, size_width, size_height))
				var rigidbody: RigidBody2D = RigidBody2D.new()
				var sprite: Sprite = Sprite.new()
				var image_texture: ImageTexture = ImageTexture.new()
				image_texture.create_from_image(rect_image)
				sprite.texture = image_texture
				sprite.scale = Vector2.ONE * 0.85
				rigidbody.name = "Slice<%d><%d>" % [x, y]
				rigidbody.add_child(sprite)
				rigidbody.position = animated_sprite.position
				rigidbodies[x][y] = rigidbody

		var impulse: float = (GlobalData.BLOCK_SIZE * 5.3) / factor
		var direction: Vector2 = Vector2.ZERO

		for y in range(0, height):
			for x in range(0, width):
				add_child(rigidbodies[x][y])

				if x < middle_point_width:
					direction.x = -1
				else:
					direction.x = 1

				if y < middel_point_height:
					direction.y = -1
				else:
					direction.y = 1

				var rigidbody: RigidBody2D = rigidbodies[x][y]
				rigidbody.inertia = 1
				rigidbody.gravity_scale *= 10
				rigidbody.applied_force.x = 20.3 * direction.x
				rigidbody.apply_impulse(Vector2.ZERO, direction * impulse)

		yield(get_tree().create_timer(0.8), "timeout")
		emit_signal("effect_successed")

	func _wait() -> void:
		_await_to_finish = true
		yield(get_tree().create_timer(0.2), "timeout")
		emit_signal("effect_successed")
