class_name Enums
"""
Collection of enum utility.
"""

enum BlockNotifyType {UPDATE_COLLISION = 1, UPDATE_GRAPHICS = 2, UPDATE_ALL = 3}
enum DataInfoType {NONE = -1, BLOCK = 1, BLOCK_REWARD = 2, ENEMY = 3, POWERUP = 4, OBJECT = 5, PROJECTIL = 6, TILE = 7, ACTIVATABLE = 8, PLAYER = 10}
enum Direction {NO_MOVE = -1, RANDOM = 9, LEFT = 1, RIGHT = 2, UP = 3, DOWN = 4}
enum PlayerKind {MARIO = 0, LUIGI = 1, TOAD = 2, PEACH = 3, LINK = 4, CUSTOM = 999}
enum PowerupType {NONE = -1, DIE = 1, SMALL = 2, BIG = 3, FIRE_FLOWER = 4, CUSTOM = 999, DAMAGE = 9999}
enum ObjectNpcType {NONE = -1, COIN = 1, LIFE = 2, POINTS = 3}
enum Collision {PLAYER = 0, ENEMY = 1, POWERUP = 2, BLOCK = 3, OBJECT = 4}

static func add(_enum: int, _add: int) -> int:
	return _enum | _add

static func equals(_enum: int, search: int) -> bool:
	return _enum == search

static func get_name(enum_class: Dictionary, enum_int_value: int) -> String:
	var keys = enum_class.keys()
	if enum_int_value < 0 or enum_int_value >= keys.size():
		printerr("given enum int value out of range for enum class")
		return ""
	return keys[enum_int_value]

static func get_names(enum_class: Dictionary) -> Array:
	return enum_class.keys()

static func get_value(enum_class: Dictionary, enum_string: String) -> int:
	if not enum_class.has(enum_string.to_upper()):
		printerr("given enum string is not in enum class")
		return -1
	return enum_class[enum_string.to_upper()]

static func get_values(enum_class: Dictionary) -> Array:
	return enum_class.values()

static func has(_enum: int, search: int) -> bool:
	return (_enum & search) == search

static func has_in_enum(key_to_search: String, to_search: Array) -> Array:
	var count: int = 0
	for key in to_search[0]:
		if key_to_search in key:
			return [true, to_search[1][count]]
		count += 1

	return [false, -1]

static func remove(_enum: int, _remove: int) -> int:
	return _enum & ~_remove
