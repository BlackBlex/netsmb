extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const EVENT_AT_ONCE: int = 10
const NO_TRIGGER: int = -1

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum ProcessingMode {IDLE, PHYSICS}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _processing: int = ProcessingMode.IDLE
var _lock_events: Mutex = Mutex.new()
var _lock_receivers: Mutex = Mutex.new()
# Array of EventPublisher
var _events: Array = []
# Dictionary of event name with Dictionary of int with array of EventSubscriber
var _receivers: Dictionary = {}
# Dictionary of event name with array of Object
var _event_called: Dictionary = {}

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	set_process(_processing == ProcessingMode.IDLE)
	set_physics_process(_processing == ProcessingMode.PHYSICS)

func _process(_delta: float) -> void:
	_process_events()
	yield(get_tree(), "idle_frame")
	_event_called.clear()

func _physics_process(_delta: float) -> void:
	_process_events()
	yield(get_tree(), "idle_frame")
	_event_called.clear()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func any_subscribe(event_name: String, object: Object) -> GDScript:
	if _receivers.has(event_name):
		for trigger_id in _receivers[event_name]:
			for receiver in _receivers[event_name][trigger_id]:
				if receiver.receiver.get_class() == object.get_class():
					return receiver

	return null

#See EventAllList for event_name
func publish(trigger: Object, event_name: String, values: Array = []) -> void:
	if not _event_called.has(event_name):
		_event_called[event_name] = []

	_event_called[event_name].push_back(trigger)

	if not _receivers.has(event_name):
		return

	var trigger_id: int = trigger.get_instance_id()
	var receivers: Array = []

	if _receivers[event_name].has(trigger_id):
		receivers.append_array(_receivers[event_name][trigger_id])

	receivers.append_array(_receivers[event_name][NO_TRIGGER])

	for receiver in receivers:
		var event_publisher: EventPublisher = EventPublisher.new()

		event_publisher.receiver = receiver.receiver
		event_publisher.method = receiver.method
		if receiver.default_args != null:
			if typeof(receiver.default_args) == TYPE_ARRAY:
				event_publisher.values.append_array(receiver.default_args)
			else:
				event_publisher.values.append(receiver.default_args)
			event_publisher.values.append_array(values)
		else:
			event_publisher.values = values

		event_publisher.ignore_args = receiver.ignore_args

		_lock_events.lock()
		_events.append(event_publisher)
		_lock_events.unlock()

#See EventList for event_name
func subscribe(event_name: String, receiver: Object, method: String, trigger: Object = null, default_args = null) -> void:
	if not receiver.has_method(method):
		printerr(receiver, " doesn't have method: ", method)
		return

	_lock_receivers.lock()

	if not event_name in _receivers:
		_receivers[event_name] = {NO_TRIGGER: []}

	_lock_receivers.unlock()

	var ignore_args: bool = false

	if "_set_visible_hud" == method:
		pass

	for method_info in receiver.get_method_list():
		if method in method_info["name"]:
			var parameters: Array = method_info["args"] as Array

			if parameters is Array:
				ignore_args = parameters.empty()

	var new_event_subscriber: EventSubscriber = EventSubscriber.new()

	new_event_subscriber.receiver = receiver
	new_event_subscriber.method = method
	new_event_subscriber.ignore_args = ignore_args
	new_event_subscriber.default_args = default_args

	if is_instance_valid(trigger):
		var trigger_id: int = trigger.get_instance_id()
		if not _receivers[event_name].has(trigger_id):
			_receivers[event_name][trigger_id] = []

		_receivers[event_name][trigger_id].append(new_event_subscriber)
	else:
		_receivers[event_name][NO_TRIGGER].append(new_event_subscriber)

#See EventList.gd for event_name
func unsubscribe(event_name: String, receiver: Object) -> void:
	for trigger_id in _receivers[event_name]:
		var elements: Array = _receivers[event_name][trigger_id] as Array

		for element in elements:
			if element is EventSubscriber:
				if element.receiver == receiver:
					_receivers[event_name][trigger_id].erase(element)

func unsubscribe_all_events(receiver: Object) -> void:
	for event_name in _receivers.keys():
		unsubscribe(event_name, receiver)

func wait(event_name: String, trigger: Object, _receiver: Object = null):
	var correct: bool = false
	while (not correct):
		if _event_called.has(event_name):
			var last_caller: Object = _event_called[event_name].pop_back()
			correct = last_caller == trigger

		yield(get_tree(), "idle_frame")


func _process_events() -> void:
	if _queue_size() == 0:
		return

	var number: int = int(min(EVENT_AT_ONCE, _queue_size()))

	for _i in range(0, number):
		_lock_events.lock()
		var event_publisher = _events.pop_front() as EventPublisher
		_lock_events.unlock()

		if not is_instance_valid(event_publisher.receiver):
			unsubscribe_all_events(event_publisher.receiver)
			continue

		if not event_publisher.values.empty() and not event_publisher.ignore_args:
			event_publisher.receiver.callv(event_publisher.method, event_publisher.values)
		else:
			event_publisher.receiver.call(event_publisher.method)

func _queue_size() -> int:
	return _events.size()

##############################################################################
#
#----------------------------------------------------------------------------
#	INNER CLASS
#----------------------------------------------------------------------------
#
##############################################################################
class EventData:
	var receiver: Object = null
	var method: String = ""
	var ignore_args: bool = false

class EventPublisher extends EventData:
	var values: Array = []

class EventSubscriber extends EventData:
	var default_args = null
