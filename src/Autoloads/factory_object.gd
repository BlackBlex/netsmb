extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var npc: NpcCreator = null
var block: BlockCreator = null


##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	Event.subscribe(A.EventList.level.initialized, self, "_on_level_intializated")

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_level_intializated(level: LevelRoot) -> void:
	if not npc:
		npc = NpcCreator.new()

	if not block:
		block = BlockCreator.new()

	npc.set_level_current(level)
	block.set_level_current(level)
	Logger.msg_info("Level initialized, updating references")

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	INNER CLASS
#----------------------------------------------------------------------------
#
##############################################################################
class BlockCreator:
	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	CONSTANTS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ENUMS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	EXPORTED VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################
	var _level_current: LevelRoot = null
	var _visible: bool = true
	var _is_disable: bool = false
	var _is_transformable: bool = true

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ONREADY VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VIRTUAL METHODS FROM GODOT
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS CALLBACK
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	FUNCTIONS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func not_visible() -> BlockCreator:
		_visible = false
		return self

	func disable() -> BlockCreator:
		_is_disable = true
		return self

	func not_transformable() -> BlockCreator:
		_is_transformable = false
		return self

	func create_brick(who: Entity, layer: int, variant: String, identifier: int, position: Vector2, reward = null) -> Array:
		var error: int = ERR_CANT_CREATE
		var to_result: Block = null

		var block_info = null

		if reward == null:
			block_info = BlockInfo.new()
		else:
			block_info = BlockRewardInfo.new()

		block_info.root = "blocks"
		block_info.category = "actions"
		block_info.subcategory = "brick"
		block_info.variant = variant
		block_info.identifier = identifier
		block_info.load_frames()

		to_result = Resources.BLOCK.instance() as Block

		if to_result:
			to_result.info = block_info
			to_result.is_transformable = _is_transformable
			_is_transformable = true

			if _level_current:
				Event.publish(_level_current, A.EventList.block.spawned, [who, to_result])
				_level_current.get_layer().call_deferred("add_block", layer, to_result, _visible)

				error = OK

			to_result.position = position
			_visible = true

			if _is_disable:
				to_result.call_deferred("disable", true)
				_is_disable = false

		return [error, to_result]

	func set_level_current(new_level: LevelRoot) -> void:
		_level_current = new_level

class NpcCreator:
	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	CONSTANTS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ENUMS
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	EXPORTED VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################
	var _level_current: LevelRoot = null
	var _visible: bool = true
	var _is_disable: bool = false
	var _is_transformable: bool = true

	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	ONREADY VARIABLES
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	VIRTUAL METHODS FROM GODOT
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	SIGNALS CALLBACK
	#----------------------------------------------------------------------------
	#
	##############################################################################


	##############################################################################
	#
	#----------------------------------------------------------------------------
	#	FUNCTIONS
	#----------------------------------------------------------------------------
	#
	##############################################################################
	func not_visible() -> NpcCreator:
		_visible = false
		return self

	func disable() -> NpcCreator:
		_is_disable = true
		return self

	func not_transformable() -> NpcCreator:
		_is_transformable = false
		return self

	#return Error and Enemy
	func instance_enemy(layer: int, subcategory: String, variant: String, position: Vector2, is_friendly: bool = false, instant_kill: bool = false, direction: int = Enums.Direction.RANDOM) -> Array:
		var error: int = ERR_CANT_CREATE
		var to_result: Enemy = null

		var enemy_info: EnemyInfo = EnemyInfo.new()
		enemy_info.root = "npcs"
		enemy_info.category = "enemies"
		enemy_info.subcategory = subcategory
		enemy_info.variant = variant
		enemy_info.direction = direction
		enemy_info.is_friendly = is_friendly
		enemy_info.instant_kill = instant_kill
		enemy_info.load_frames()

		to_result = Resources.ENEMY.instance() as Enemy

		if to_result:
			to_result.info = enemy_info
			to_result.is_collision_active = not _is_disable

			if _level_current:
				Event.publish(_level_current, A.EventList.npc.enemy_created, [to_result])
				_level_current.get_layer().call_deferred("add_enemy", layer, to_result, _visible)

				error = OK

			to_result.position = position
			_visible = true

			if _is_disable:
				to_result.call_deferred("disable")
				_is_disable = false

		return [error, to_result]

	#return Error and ObjectNpc
	func instance_object(layer: int, subcategory: String, variant: String, position: Vector2, is_friendly: bool = false, object_given: int = Enums.ObjectNpcType.COIN, total: int = 1, direction: int = Enums.Direction.RANDOM) -> Array:
		var error: int = ERR_CANT_CREATE
		var to_result: ObjectNpc = null

		var object_info: ObjectInfo = ObjectInfo.new()
		object_info.root = "npcs"
		object_info.category = "objects"
		object_info.subcategory = subcategory
		object_info.variant = variant
		object_info.direction = direction
		object_info.is_friendly = is_friendly
		object_info.object_given = object_given
		object_info.total = total
		object_info.load_frames()

		to_result = Resources.OBJECT_NPC.instance() as ObjectNpc

		if to_result:
			to_result.info = object_info
			to_result.is_collision_active = not _is_disable
			to_result.is_transformable = _is_transformable
			_is_transformable = true

			if _level_current:
				Event.publish(_level_current, A.EventList.npc.object_created, [to_result])
				_level_current.get_layer().call_deferred("add_npc", layer, to_result, _visible)

				error = OK

			to_result.position = position
			_visible = true

			if _is_disable:
				to_result.call_deferred("disable")
				_is_disable = false

		return [error, to_result]

	#return Error and Powerup
	func instance_powerup(layer: int, subcategory: String, variant: String, position: Vector2, is_friendly: bool = false, direction: int = Enums.Direction.RANDOM) -> Array:
		var error: int = ERR_CANT_CREATE
		var to_result: Powerup = null

		var powerup_info: PowerupInfo = PowerupInfo.new()
		powerup_info.root = "npcs"
		powerup_info.category = "powerups"
		powerup_info.subcategory = subcategory
		powerup_info.variant = variant
		powerup_info.direction = direction
		powerup_info.is_friendly = is_friendly
		powerup_info.load_frames()

		to_result = Resources.POWERUP.instance() as Powerup

		if to_result:
			to_result.info = powerup_info
			to_result.is_collision_active = not _is_disable

			if _level_current:
				Event.publish(_level_current, A.EventList.npc.powerup_created, [to_result])
				_level_current.get_layer().call_deferred("add_powerup", layer, to_result, _visible)

				error = OK

			to_result.position = position
			_visible = true

			if _is_disable:
				to_result.call_deferred("disable")
				_is_disable = false

		return [error, to_result]

	func set_level_current(new_level: LevelRoot) -> void:
		_level_current = new_level
