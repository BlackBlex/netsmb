extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const SQLITE = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
const BLOCK_SIZE: int = 32
const BLOCK_SIZE_HALF: int = BLOCK_SIZE / 2
const FPS_ANIMATION: float = 8.0
const GRID_SIZE_LIMIT: int = 160000
const LEVEL_TIME: int = 300
const STEP_GRID_CONTROL: int = 8

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var editor_theme: String = "flat_dark"
var editor_version: Version = Version.new().init_number(0, 4, 0, 0, Version.State.ALPHA, 0) setget _set_version, _get_version
var map_size_render: Vector2 = Vector2(25, 19) * BLOCK_SIZE
var is_mouse_on_level_window: bool = false
var is_mouse_on_level_canvas: bool = false
var is_on_level_editor: bool = false
var is_on_game: bool = false
var is_on_test_level: bool = false
var previewer_oppened: ScrollContainer = null
var open_panel: Panel = null
var data_info_hover: DataInfo = null
var current_player: PlayerInfo = null
var current_level: LevelData = LevelData.new()
var current_layer: int = 1
#Dictionary of int with Dictionary of string with variant
var level_values: Dictionary = {}
var object_database: ObjectDB = null
# Database
var db: SQLITE = null
# Array of String
var block_exclusion: Array = []
var player_camera: Camera2D = null

var _data_info_current: DataInfo = null
var _min_size_window: Vector2 = Vector2(800, 608)
var _history_version: Dictionary = {"0.1.0.0-alpha.0": "0.4.0.0-alpha.0"}

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	current_level._ready()
	object_database = ObjectDB.new()

	# //TODO: Change this methods
	# Event.subscribe(A.EventList.editor.npc_added, self, "_save_object_database")
	# Event.subscribe(A.EventList.editor.block_added, self, "_save_object_database")

	var _err = get_tree().root.connect("size_changed", self, "_on_root_size_changed")

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_root_size_changed() -> void:
	var current_size: Vector2 = OS.window_size

	current_size.x = _min_size_window.x if current_size.x < _min_size_window.x else current_size.x
	current_size.y = _min_size_window.y if current_size.y < _min_size_window.y else current_size.y

	OS.window_size = current_size

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_data_info() -> DataInfo:
	return _data_info_current

func get_next_version_for_conversion(version: String) -> String:
	var to_result: String = ""

	if _history_version.has(version):
		to_result = _history_version[version]

	return to_result

func set_data_info(info: DataInfo = null) -> void:
	if not info:
		if _data_info_current:
			_data_info_current.destroy()
			_data_info_current = null

		return

	if _data_info_current:
		_data_info_current.destroy()
	_data_info_current = info


func _get_version() -> Version:
	return editor_version

func _save_object_database() -> void:
	var json: String = JSONConvert.serialize(object_database)

	var _err = Utility.save_file(A.Paths.object_database_file, json)

func _set_version(_new_version: Version) -> void:
	pass
