extends Node2D

func get_particle(position: Vector2) -> Particle:
	var to_result: Particle = Resources.PARTICLE.instance() as Particle
	to_result.position = position
	add_child(to_result)

	return to_result
