class_name PathList
extends BaseScript

var game: String = "res://" setget _unused_set, _get_game
var assets: String = "" setget _unused_set, _get_assets
var scripts: String = "" setget _unused_set, _get_scripts
var data: String = "" setget _unused_set, _get_data
var audios: String = "" setget _unused_set, _get_audios
var behaviours: String = "" setget _unused_set, _get_behaviours
var events: String = "" setget _unused_set, _get_events
var definitions: String = "" setget _unused_set, _get_definitions
var states: String = "" setget _unused_set, _get_states
var actions: String = "" setget _unused_set, _get_actions

var file_hud: String = "" setget _unused_set, _get_file_hud

var level: String = "" setget _set_level, _get_level
var level_file_name: String = ""

var level_audios: String = "" setget _unused_set, _get_level_audios
var level_scripts: String = "" setget _unused_set, _get_level_scripts
var level_behaviours: String = "" setget _unused_set, _get_level_behaviours
var level_events: String = "" setget _unused_set, _get_level_events
var level_definitions: String = "" setget _unused_set, _get_level_definitions
var level_states: String = "" setget _unused_set, _get_level_states
var level_actions: String = "" setget _unused_set, _get_level_actions

var level_file_hud: String = "" setget _unused_set, _get_level_file_hud

var db_file: String = "assets/datas/data.db"
func _init() -> void:
	_load_local_paths()


func _load_local_paths() -> void:
	if OS.has_feature("editor"):
		game = ProjectSettings.globalize_path(game)
	else:
		game = OS.get_executable_path().get_base_dir()

	db_file = game.plus_file(db_file)
	assets = game.plus_file("assets")
	audios = assets.plus_file("audios")
	scripts = assets.plus_file("scripts")
	data = assets.plus_file("datas")
	behaviours = scripts.plus_file("behaviours")
	events = scripts.plus_file("events")
	definitions = scripts.plus_file("definitions")
	states = scripts.plus_file("states")
	actions = scripts.plus_file("actions")
	file_hud = scripts.plus_file("hud.gd")

func _load_level_paths() -> void:
	level_audios = level.plus_file("audios")
	level_scripts = level.plus_file("scripts")
	level_behaviours = level_scripts.plus_file("behaviours")
	level_events = level_scripts.plus_file("events")
	level_definitions = level_scripts.plus_file("definitions")
	level_states = level_scripts.plus_file("states")
	level_actions = level_scripts.plus_file("actions")
	level_file_hud = level_scripts.plus_file("hud.gd")


func _get_func() -> String:
	return ""

func _get_level() -> String:
	return level

func _get_game() -> String:
	return game

func _get_assets() -> String:
	return assets

func _get_scripts() -> String:
	return scripts

func _get_audios() -> String:
	return audios

func _get_data() -> String:
	return data

func _get_behaviours() -> String:
	return behaviours

func _get_events() -> String:
	return events

func _get_definitions() -> String:
	return definitions

func _get_states() -> String:
	return states

func _get_actions() -> String:
	return actions

func _get_file_hud() -> String:
	return file_hud

func _get_level_scripts() -> String:
	return level_scripts

func _get_level_audios() -> String:
	return level_audios

func _get_level_behaviours() -> String:
	return level_behaviours

func _get_level_events() -> String:
	return level_events

func _get_level_definitions() -> String:
	return level_definitions

func _get_level_states() -> String:
	return level_states

func _get_level_actions() -> String:
	return level_actions

func _get_level_file_hud() -> String:
	return level_file_hud

func _unused_set(_new_value: String) -> void:
	pass

func _set_level(new_level: String) -> void:
	level = new_level
	_load_level_paths()
