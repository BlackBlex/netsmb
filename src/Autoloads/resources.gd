extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal resources_loaded()

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const Creator = preload("res://src/UI/Creator.tscn")

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum Type {FONTS = 0, IMAGES = 1, FRAMES = 2, THEMES = 3, AUDIOS = 4, CUSTOMS = 5}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var CREATOR = Creator.instance()

var LEVEL_ROOT: PackedScene = load("res://src/Levels/LevelRoot.tscn")
var PREVIEWER_SINGLE: PackedScene = load("res://src/UI/Editor/PreviewerSingle.tscn")
var PREVIEWER_LIST: PackedScene = load("res://src/UI/Editor/PreviewerList.tscn")
var BLOCK: PackedScene = load("res://src/Blocks/Block.tscn")
var BLOCK_DEAD: PackedScene = load("res://src/Blocks/BlockDead.tscn")
var PLAYER: PackedScene = load("res://src/Entities/Characters/Players/Player.tscn")
var ENEMY: PackedScene = load("res://src/Entities/Npcs/Enemy.tscn")
var OBJECT_NPC: PackedScene = load("res://src/Entities/Npcs/ObjectNpc.tscn")
var POWERUP: PackedScene = load("res://src/Entities/Npcs/Powerup.tscn")
var ACTIVATABLE: PackedScene = load("res://src/Entities/Npcs/Activatable.tscn")
var HUD_LIFE: PackedScene = load("res://src/UI/HUD/LifeSection.tscn")
var HUD_COIN: PackedScene = load("res://src/UI/HUD/CoinSection.tscn")
var HUD_TIME: PackedScene = load("res://src/UI/HUD/TimeSection.tscn")
var HUD_ITEM_SLOT: PackedScene = load("res://src/UI/HUD/ItemSlotSection.tscn")
var HUD_SCORE: PackedScene = load("res://src/UI/HUD/ScoreSection.tscn")
var PARTICLE: PackedScene = load("res://src/Autoloads/Utils/Particle.tscn")
var PROJECTILE: PackedScene = load("res://src/Entities/Projectile.tscn")
var default_block: Resource = null

# Dictionary of string with Dictionary of string with Resource
var _all_directories: Dictionary = {
	fonts = {},
	images = {},
	frames = {},
	themes = {},
	audios = {},
	customs = {},
} setget _unused_set, _unused_get

var _is_loaded: bool = false
var _extensions_excluded : Array = [".import", ".json", ".txt", ".png", ".csv", ".translation", "default-block.tex", ".ttf", ".sfd", ".gdc", ".remap", ".gd", ".srp", ".db", ".bkp"]

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_font(path: String) -> Resource:
	return _get_resource(Type.FONTS, path)

func get_fonts(common_path: String, start_with: String = "") -> Dictionary:
	return _get_resources(Type.FONTS, common_path, start_with)

func get_frames_from_image(path: String) -> Resource:
	return _get_resource(Type.FRAMES, path)

func get_image(path: String) -> Resource:
	return _get_resource(Type.IMAGES, path)

func get_images(common_path: String, start_with: String = "") -> Dictionary:
	return _get_resources(Type.IMAGES, common_path, start_with)

func get_theme(path: String) -> Resource:
	return _get_resource(Type.THEMES, path)

func get_themes(common_path: String, start_with: String = "") -> Dictionary:
	return _get_resources(Type.THEMES, common_path, start_with)

func get_audio(path: String) -> Resource:
	return _get_resource(Type.AUDIOS, path, false)

func get_audios(common_path: String, start_with: String = "") -> Dictionary:
	return _get_resources(Type.AUDIOS, common_path, start_with)

func get_custom(path: String) -> Resource:
	return _get_resource(Type.CUSTOMS, path)

func get_customs(common_path: String, start_with: String = "") -> Dictionary:
	return _get_resources(Type.CUSTOMS, common_path, start_with)

func load_all(_splasher) -> void:
	if _is_loaded:
		return

	_load_recursive(A.Paths.assets)

	default_block =  get_frames_from_image("blocks/default-block")

func process_texture(path: String) -> SpriteFrames:
	var to_result: SpriteFrames = null
	var frames: Resource = get_frames_from_image(path)

	if frames is SpriteFrames:
		to_result = frames.duplicate(true) as SpriteFrames
	else:
		var image: Resource = get_image(path)

		if image is Texture:
			to_result = SpriteFrames.new()
			to_result.add_frame("default", image, 0)

	return to_result

func save(path: String, resource: Resource, flag: int = 0) -> int:
	var to_result: int = ResourceSaver.save(path, resource, flag)
	var extension: String = "." + path.get_extension()

	if to_result == OK and not _extensions_excluded.has(extension) and extension != ".tres":
		var parsed: PoolStringArray = _parse_path(path, "replace_me")
		var path_clean: String = parsed[0]
		var category: String = parsed[1]

		var to_return_resource: Resource = load(path)

		Logger.msg_info("Loading the resource: %s | in: %s | Is loaded: %s" % [path, path_clean, to_return_resource != null], Logger.CATEGORY_ASSET)

		if _all_directories.has(category):
			_all_directories[category][path_clean] = to_return_resource
		else:
			_all_directories["customs"][path_clean] = to_return_resource

	return to_result


func _get_resource(type: int, path: String, remove_extension: bool = true) -> Resource:
	assert(type in Type.values(), "Expected to be a Type value")

	var base_name: String = path.get_file().get_basename()
	var type_str: String = Type.keys()[type].to_lower()

	if remove_extension:
		path = path.replace(path.get_file(), base_name)

	if _all_directories[type_str].has(path):
		return _all_directories[type_str][path]
	else:
		Logger.msg_warn("Resource %s not found: %s" % [type_str, path])

	return null

func _get_resources(type: int, path: String, start_with: String = "") -> Dictionary:
	assert(type in Type.values(), "Expected to be a Type value")

	var to_result: Dictionary = {}
	var type_str = Type.keys()[type].to_lower()
	var keys: PoolStringArray = []

	var _final_path: String = ""
	var _to_return: bool = false
	for key in _all_directories[type_str].keys():
		_final_path = key.replace(path, "")
		_to_return = key.begins_with(path) and _final_path.begins_with("/")

		if not start_with.empty() and _to_return:
			_final_path = _final_path.substr(1, _final_path.length() - 1)
			_to_return = _to_return and _final_path.begins_with(start_with)

		if _to_return:
			keys.append(key)

	var to_return_resource: Resource = null

	for key in keys:
		match(type):
			Type.CUSTOMS:
				to_return_resource = get_custom(key)
			Type.FONTS:
				to_return_resource = get_font(key)
			Type.IMAGES:
				to_return_resource = get_image(key)
			Type.THEMES:
				to_return_resource = get_theme(key)
			Type.AUDIOS:
				to_return_resource = get_audio(key)
			Type.FRAMES:
				to_return_resource = get_frames_from_image(key)

		to_result[key] = to_return_resource

	return to_result

func _load_recursive(path: String) -> void:
	var asset_directory: Directory = Directory.new()

	if asset_directory.open(path) == OK:
		var _err = asset_directory.list_dir_begin(true)

		var current_point: String = asset_directory.get_next()

		while(not current_point.empty()):
			if asset_directory.current_is_dir():
				_load_recursive(path.plus_file(current_point))
			else:
				var extension: String = "." + current_point.get_extension()

				if not _extensions_excluded.has(extension) or ("background" in current_point and not "import" in current_point):
					var parsed: PoolStringArray = _parse_path(path, current_point)

					if parsed.empty():
						continue

					var path_clean: String = parsed[0]
					var category: String = parsed[1]

					var to_return_resource: Resource = null

					if not extension.ends_with("spc") and not extension.ends_with("nsf"):
						if extension.ends_with("ogg"):
							var file: File = File.new()
							_err = file.open(path.plus_file(current_point), File.READ)
							var bytes: PoolByteArray = file.get_buffer(file.get_len())
							to_return_resource = AudioStreamOGGVorbis.new()
							to_return_resource.loop = "music" in path
							to_return_resource.data = bytes
						elif extension.ends_with("mp3"):
							var file: File = File.new()
							_err = file.open(path.plus_file(current_point), File.READ)
							var bytes: PoolByteArray = file.get_buffer(file.get_len())
							to_return_resource = AudioStreamMP3.new()
							to_return_resource.loop = "music" in path
							to_return_resource.data = bytes
						else:
							to_return_resource = load(path.plus_file(current_point))

						if "audios" in category:
							path_clean += extension

						Logger.msg_info("Loading the resource: %s | in: %s | Is loaded: %s" % [path.plus_file(current_point), path_clean, to_return_resource != null], Logger.CATEGORY_ASSET)

					if _all_directories.has(category):
						if not _all_directories[category].has(path_clean):
							_all_directories[category][path_clean] = to_return_resource
					else:
						if not _all_directories["customs"].has(path_clean):
							_all_directories["customs"][path_clean] = to_return_resource

			current_point = asset_directory.get_next()

		asset_directory.list_dir_end()

		if path in A.Paths.assets:
			emit_signal("resources_loaded")
			_is_loaded = true
	else:
		printerr("Path not found: ", path)

func _parse_path(path: String, current_point: String) -> PoolStringArray:
	var to_result: PoolStringArray = []

	var path_clean: String = path.plus_file(current_point).replace(A.Paths.assets + "/", "").replace("/replace_me", "")

	var category_position: int = path_clean.find("/")
	var category: String = path_clean.substr(0, category_position)

	path_clean = path_clean.replace(category + "/", "")

	if not path_clean.empty():
		#Remove extension
		var base_name: String = path_clean.get_file().get_basename()

		path_clean = path_clean.replace(path_clean.get_file(), base_name)

		if path_clean.ends_with(".frames"):
			category = "frames"
			path_clean = path_clean.replace(".frames", "")

		to_result.append(path_clean)
		to_result.append(category)

	return to_result

func _unused_set(_newValue: Dictionary) -> void:
	pass

func _unused_get() -> Dictionary:
	return _all_directories
