extends Node

func apply_theme(control: Control, theme: String) -> bool:
	var to_result: bool = false
	var load_theme: Resource = Resources.get_theme(theme)

	if load_theme is Theme:
		control.theme = load_theme
		to_result = true

	return to_result

func button(control: Control, theme: String) -> void:
	#Get style rounded firts, if not loaded then load normal style
	var _result: bool = disabled(control, theme, "button")
	_result = hover(control, theme, "button")
	_result = pressed(control, theme, "button")
	_result = focus(control, theme, "button")
	_result = normal(control, theme, "button")

func button_rounded(control: Control, theme: String) -> void:
	#Get style rounded firts, if not loaded then load normal style
	var _result: bool = false
	if not disabled(control, theme, "button", "rounded"):
		_result = disabled(control, theme, "button")
	if not hover(control, theme, "button", "rounded"):
		_result = hover(control, theme, "button")
	if not pressed(control, theme, "button", "rounded"):
		_result = pressed(control, theme, "button")
	if not focus(control, theme, "button", "rounded"):
		_result = focus(control, theme, "button")
	if not normal(control, theme, "button", "rounded"):
		_result = normal(control, theme, "button")


# control -> to which the style will be applied
# theme ->
# type -> to control, ex: button, label, text_edit, line_edit
# subtype -> another extra type, ex: rounded, strech, 10alpha
func disabled(control: Control, theme: String, type: String, subtype: String = "") -> bool:
	var to_result: bool = false

	var style_str: String = "%s/disabled/%s" % [type, theme]

	if not subtype.empty():
		style_str += "_%s" % subtype

	var load_style: Resource = Resources.get_theme(style_str)

	if load_style is StyleBoxFlat:
		control.set("custom_styles/disabled", load_style)
		to_result = true


	return to_result

func focus(control: Control, theme: String, type: String, subtype: String = "") -> bool:
	var to_result: bool = false

	var style_str: String = "%s/focus/%s" % [type, theme]

	if not subtype.empty():
		style_str += "_%s" % subtype

	var load_style: Resource = Resources.get_theme(style_str)

	if load_style is StyleBoxFlat:
		control.set("custom_styles/focus", load_style)
		to_result = true

	return to_result

func hover(control: Control, theme: String, type: String, subtype: String = "") -> bool:
	var to_result: bool = false

	var style_str: String = "%s/hover/%s" % [type, theme]

	if not subtype.empty():
		style_str += "_%s" % subtype

	var load_style: Resource = Resources.get_theme(style_str)

	if load_style is StyleBoxFlat:
		control.set("custom_styles/hover", load_style)
		to_result = true

	return to_result

func normal(control: Control, theme: String, type: String, subtype: String = "") -> bool:
	var to_result: bool = false

	var style_str: String = "%s/normal/%s" % [type, theme]

	if not subtype.empty():
		style_str += "_%s" % subtype

	var load_style: Resource = Resources.get_theme(style_str)

	if load_style is StyleBoxFlat:
		control.set("custom_styles/normal", load_style)
		to_result = true

	return to_result

func pressed(control: Control, theme: String, type: String, subtype: String = "") -> bool:
	var to_result: bool = false

	var style_str: String = "%s/pressed/%s" % [type, theme]

	if not subtype.empty():
		style_str += "_%s" % subtype

	var load_style: Resource = Resources.get_theme(style_str)

	if load_style is StyleBoxFlat:
		control.set("custom_styles/pressed", load_style)
		to_result = true

	return to_result
