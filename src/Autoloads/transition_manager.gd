extends CanvasLayer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal transition_completed()

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _DEFAULT_DURATION = 1
const _DEFAULT_DELAY = 0

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum Kind {FADE, FADE_IN, FADE_OUT}
enum Type {SCENE, ELEMENT, INSTANCE, NONE}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _transition_type: int = Type.NONE
var _path: String = ""
var _element: Node = null
var _scene_current: Node = null
var _execute: FuncRef = null
var _duration = _DEFAULT_DURATION
var _delay = _DEFAULT_DELAY

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _background: Panel = $Background
onready var _animation: Tween = $Animation

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_default_execute()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_animation() -> Tween:
	return _animation

func set_duration(value: float) -> void:
	_duration = value

func set_delay(value: float) -> void:
	_delay = value

#kind see Transition.Kind
func to_scene(path: String, kind: int) -> void:
	_set_scene(path)
	_process_transition(kind)

#kind see Transition.Kind
func to_instance(scene: Node, kind: int, execute: FuncRef = null) -> void:
	if execute:
		_execute = execute

	_set_instance(scene)
	_process_transition(kind)

#kind see Transition.Kind
func to_element(scene: Node, element: Node, kind: int) -> void:
	_set_element(scene, element)
	_process_transition(kind)


func _active_animation(original, final, obj: Object, property: NodePath) -> void:
	var _err = _animation.interpolate_property(obj, property, original, final, _duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT, _delay)

	_err = _animation.start()

func _activator() -> void:
	if _transition_type != Type.NONE:
		match _transition_type:
			Type.SCENE:
				if not _path.empty():
					var _err = get_tree().change_scene(_path)
					_path = ""
			Type.ELEMENT:
				if _element != null and _scene_current != null:
					_scene_current.add_child(_element)
					_element = null
					_scene_current = null
			Type.INSTANCE:
					if _element:
						get_tree().current_scene = _element
						_execute.call_func()

						_default_execute()

	_transition_type = Type.NONE

func _default_execute() -> void:
	_execute = funcref(self, "_set_element_visible")

func _effect_fade() -> void:
	_effect_fade_in(true)
	yield(_animation, "tween_completed")
	_activator()
	_effect_fade_out(true)
	yield(_animation, "tween_completed")
	emit_signal("transition_completed")
	_background.visible = false
	_reset()

func _effect_fade_in(full_effect: bool = false) -> void:
	_background.visible = true
	_active_animation(Color(1, 1, 1, 0), Color(1, 1, 1, 1), _background, "modulate")

	if not full_effect:
		yield(_animation, "tween_complete")
		_background.visible = false

func _effect_fade_out(full_effect: bool = false) -> void:
	_background.visible = true
	_active_animation(Color(1, 1, 1, 1), Color(1, 1, 1, 0), _background, "modulate")

	if not full_effect:
		yield(_animation, "tween_completed")
		_background.visible = false

#kind see Transition.Kind
func _process_transition(kind: int) -> void:
	match kind:
		Kind.FADE:
			_effect_fade()
		Kind.FADE_IN:
			_effect_fade_in()
			yield(_animation, "tween_completed")
			_activator()
			_reset()
		Kind.FADE_OUT:
			_effect_fade_out()
			yield(_animation, "tween_completed")
			_activator()
			_reset()

func _reset() -> void:
	_delay = _DEFAULT_DELAY
	_duration = _DEFAULT_DURATION
	_background.modulate = Color(1, 1, 1, 0)

func _set_element_visible() -> void:
	if _element is CanvasItem:
		_element.visible = true

func _set_element(scene: Node, element: Node) -> void:
	_scene_current = scene
	_element = element
	_transition_type = Type.ELEMENT

func _set_instance(scene: Node) -> void:
	_element = scene
	_transition_type = Type.INSTANCE

func _set_scene(path: String) -> void:
	_path = path
	_transition_type = Type.SCENE
