class_name Block
extends StaticBody2D

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: BlockInfo = null
var is_transformable: bool = true
var layer_id: int = -1

var _is_hitted: bool = false
var _transform_npc: Npc = null
var _is_transformed: bool = false
var _last_entity_enter: Entity = null
var _last_player_hitted: Player = null
var _on_hit: BaseAction = null
var _position_original: Vector2 = Vector2.ZERO

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _collision: CollisionShape2D = $Collision
onready var _collision_up: CollisionShape2D = $UpArea/Collision
onready var _collision_down: CollisionShape2D = $DownArea/Collision
onready var _collision_left: CollisionShape2D = $LeftArea/Collision
onready var _collision_right: CollisionShape2D = $RightArea/Collision
onready var _area_up: Area2D = $UpArea
onready var _area_down: Area2D = $DownArea
onready var _area_left: Area2D = $LeftArea
onready var _area_right: Area2D = $RightArea

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	info.place(self)

	_subscribe_events()
	_build_actions()

	_make_collision()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_area_body_exited(_body: Node) -> void:
	_last_entity_enter = null

func _on_block_hit(body: Node) -> void:
	if body is Player or body is Enemy:
		if not body is Player:
			if body is Enemy and _last_entity_enter != null:
				_last_entity_enter.kicked_by = null

			_last_entity_enter = body
		else:
			_last_player_hitted = body

func _on_area_body_entered(body: Node, normal: Vector2) -> void:
	_on_block_hit(body)
	if not _is_hitted and _on_hit and not _is_transformed:
		if _on_hit.is_valid([body, normal]):
			_on_hit.execute([body, normal])

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func break_block(body: Entity) -> void:
	Event.publish(self, A.EventList.block.breakk, [body])
	Audio.play_effect("block-smash.ogg")
	_break()
	destroy()

func bounce(normal: Vector2 = Vector2.ZERO) -> void:
	var position_y_final: float = -(GlobalData.BLOCK_SIZE_HALF / 2)

	var animator: AnimatorExecuter = EntityExt.animator(self)
	animator.interpolate_property(info.get_image(), "position:y", 0, position_y_final, 0.1).interpolate_property(info.get_image(), "position:y", position_y_final, 0, 0.1, 0, 0, 3, true).start()

	if _last_entity_enter and normal == Vector2.DOWN:
		_last_entity_enter.kicked_by = _last_player_hitted
		_last_entity_enter.velocity.y = _last_entity_enter.sm.get_state_current().get_physic_constants().MAX_JUMP_VELOCITY * 0.65
		_last_player_hitted = null

func destroy() -> void:
	_unsubscribe_events()
	queue_free()

func disable(all: bool = false) -> void:
	_collision.disabled = true
	set_physics_process(false)
	set_process(false)

	if all:
		_collision_up.disabled = true
		_collision_down.disabled = true
		_collision_left.disabled = true
		_collision_right.disabled = true

func enable(all: bool = false) -> void:
	_collision.disabled = false
	set_physics_process(true)
	set_process(true)

	if all:
		_collision_up.disabled = false
		_collision_down.disabled = false
		_collision_left.disabled = false
		_collision_right.disabled = false

func get_transform():
	return _transform_npc


func _build_actions() -> void:
	# Load on_hit func
	_on_hit = Entity.build_action("on_hit", info)

	if _on_hit:
		_on_hit.set("_OWNER", self)
		_on_hit.setup()
	else:
		Logger.msg_error("Action: %s not found..." % ("%s/on_hit.gd" % info), Logger.CATEGORY_FILE_LOAD)

func _break() -> void:
	visible = false
	var texture: Texture = info.get_image().frames.get_frame(info.get_image().animation, 0)
	var width: int = int(texture.get_size().x)
	var effects: EffectsExecuter = EntityExt.effects(self)

	effects.visible_image(false).put_texture(texture).positionate(position).breakk((width * 4) / GlobalData.BLOCK_SIZE).create()

func _load_transform_npc(category: String) -> void:
	var result: Array = Factory.npc.not_visible().disable().not_transformable().instance_object(layer_id, category, info.variant, position)

	if result[0] == OK:
		Event.subscribe(A.EventList.npc.grabbed, self, "destroy", result[1])
		_transform_npc = result[1]

func _make_collision() -> void:
	var current_animation: String = "default"
	var size: Vector2 = info.get_image().frames.get_frame(current_animation, 0).get_size() / 2

	var hit_size_left_right: Vector2 = Vector2(1, size.y - 2)
	var hit_size_up_down: Vector2 = Vector2(size.x - 2, 1)

	var rect_collision: RectangleShape2D = RectangleShape2D.new()
	rect_collision.extents = Vector2(size.x - 0.1, size.y)
	_collision.shape = rect_collision

	_area_up.position.y = -(size.y + 0.1)
	_area_down.position.y = size.y + 0.1
	_area_left.position.x = -(size.x)
	_area_right.position.x = size.x

	var collision_up:RectangleShape2D = RectangleShape2D.new()
	collision_up.extents = hit_size_up_down
	_collision_up.shape = collision_up

	var downCollision:RectangleShape2D = RectangleShape2D.new()
	downCollision.extents = hit_size_up_down
	_collision_down.shape = downCollision

	var leftCollision:RectangleShape2D = RectangleShape2D.new()
	leftCollision.extents = hit_size_left_right
	_collision_left.shape = leftCollision

	var rightCollision:RectangleShape2D = RectangleShape2D.new()
	rightCollision.extents = hit_size_left_right
	_collision_right.shape = rightCollision

	var _err = _area_up.connect("body_entered", self, "_on_area_body_entered", [Vector2.UP])
	_err = _area_up.connect("body_exited", self, "_on_area_body_exited")
	_err = _area_down.connect("body_entered", self, "_on_area_body_entered", [Vector2.DOWN])
	_err = _area_down.connect("body_exited", self, "_on_area_body_exited")
	_err = _area_left.connect("body_entered", self, "_on_area_body_entered", [Vector2.LEFT])
	_err = _area_left.connect("body_exited", self, "_on_area_body_exited")
	_err = _area_right.connect("body_entered", self, "_on_area_body_entered", [Vector2.RIGHT])
	_err = _area_right.connect("body_exited", self, "_on_area_body_exited")

func _subscribe_events() -> void:
	if info is BlockRewardInfo:
		if not info.has_reward() and not info.is_question() and not "solid" in info.variant and is_transformable:
			_load_transform_npc("coin")
			Event.subscribe(A.EventList.pswitch.started, self, "_transform_to_coin")
			Event.subscribe(A.EventList.pswitch.ended, self, "_transform_to_block")

func _unsubscribe_events() -> void:
	if info is BlockRewardInfo:
		if not info.has_reward() and not info.is_question() and not "solid" in info.variant and is_transformable:
			Event.unsubscribe(A.EventList.pswitch.started, self)
			Event.unsubscribe(A.EventList.pswitch.ended, self)

	# Clean remain events subscrition
	Event.unsubscribe_all_events(self)

func _transform_to_coin() -> void:
	_is_transformed = _transform_npc != null
	if _is_transformed:
		visible = false
		disable(true)
		_transform_npc.visible = true
		_transform_npc.enable()

func _transform_to_block() -> void:
	visible = true

	if _transform_npc:
		_transform_npc.disable()
		_transform_npc.visible = false
		_is_transformed = false

	enable(true)
