class_name BlockDead
extends StaticBody2D

onready var _collision: CollisionShape2D = $Collision
onready var _collision_up: CollisionShape2D = $UpArea/Collision
onready var _collision_down: CollisionShape2D = $DownArea/Collision
onready var _collision_left: CollisionShape2D = $LeftArea/Collision
onready var _collision_right: CollisionShape2D = $RightArea/Collision
onready var _area_up: Area2D = $UpArea
onready var _area_down: Area2D = $DownArea
onready var _area_left: Area2D = $LeftArea
onready var _area_right: Area2D = $RightArea


func make_collision(size: Vector2) -> void:
	var rect_collision: RectangleShape2D = RectangleShape2D.new()
	rect_collision.extents = Vector2(size.x - 0.4, size.y - 0.4)
	_collision.shape = rect_collision

	var hit_size_left_right: Vector2 = Vector2(1, size.y - 2)
	var hit_size_up_down: Vector2 = Vector2(size.x - 2, 1)

	_collision_up.position.y = -(size.y - 1)
	_collision_down.position.y = size.y - 1
	_collision_left.position.x = -(size.x - 1)
	_collision_right.position.x = (size.x - 1)

	var collision_up:RectangleShape2D = RectangleShape2D.new()
	collision_up.extents = hit_size_up_down
	_collision_up.shape = collision_up

	var downCollision:RectangleShape2D = RectangleShape2D.new()
	downCollision.extents = hit_size_up_down
	_collision_down.shape = downCollision

	var leftCollision:RectangleShape2D = RectangleShape2D.new()
	leftCollision.extents = hit_size_left_right
	_collision_left.shape = leftCollision

	var rightCollision:RectangleShape2D = RectangleShape2D.new()
	rightCollision.extents = hit_size_left_right
	_collision_right.shape = rightCollision
