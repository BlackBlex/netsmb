class_name ActivatableInfo
extends NpcInfo

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum TypeSelect {NONE, EVENT, LAYER}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var type_select: int = TypeSelect.NONE setget _set_type_select, _get_type_select
var item: String = "" setget _set_item, _get_item
var _on_level_put: BaseAction = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _init() -> void:
	type = Enums.DataInfoType.ACTIVATABLE

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_level_editor_putted(data: DataInfo) -> void:
	if GlobalData.is_on_level_editor:
		if _on_level_put and _on_level_put.is_valid(data):
			_on_level_put.execute([])

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clone(new_frames: bool = false) -> DataInfo:
	var to_result: ActivatableInfo = load(get_script().resource_path).new() as ActivatableInfo
	to_result.root = root
	to_result.category = category
	to_result.subcategory = subcategory
	to_result.variant = variant
	to_result.type_select = type_select
	to_result.item = item
	to_result._on_level_put = _on_level_put

	if not new_frames:
		to_result.get_image().frames = get_image().frames
		to_result.set_metadata(get_metadata())
	else:
		to_result.load_frames()

	to_result.subscribe_events()

	return to_result

func get_class() -> String:
	return "ActivatableInfo"

func subscribe_events() -> void:
	if GlobalData.is_on_level_editor:
		if not Event.any_subscribe(A.EventList.level_editor.putted, self):
			Event.subscribe(A.EventList.level_editor.putted, self, "_on_level_editor_putted")

		if not _on_level_put:
			_on_level_put = Entity.build_action("on_level_put", self)

			if _on_level_put:
				_on_level_put.set("_OWNER", GlobalData.get_tree().get_nodes_in_group("editor")[0])
				_on_level_put.setup()
			else:
				Logger.msg_error("Action: %s not found..." % ("%s/on_level_put.gd" % self), Logger.CATEGORY_FILE_LOAD)


func _set_item(new_item: String) -> void:
	item = new_item

func _get_item() -> String:
	if item.empty():
		if get_metadata().has("attach"):
			if get_metadata()["attach"].has("item"):
				item = get_metadata()["attach"]["item"]

	return item

func _set_type_select(new_type: int) -> void:
	type_select = new_type

func _get_type_select() -> int:
	if type_select == TypeSelect.NONE:
		if get_metadata().has("attach"):
			if get_metadata()["attach"].has("type"):
				type_select = int(get_metadata()["attach"]["type"])

	return type_select
