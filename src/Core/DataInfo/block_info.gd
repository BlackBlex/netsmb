class_name BlockInfo
extends DataInfo

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var invisible: bool = false setget _set_invisible, _get_invisible
var identifier: int = -999

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _init() -> void:
	type = Enums.DataInfoType.BLOCK

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clone(new_frames:bool = false) -> DataInfo:
	var to_result: BlockInfo = load(get_script().resource_path).new() as BlockInfo
	to_result.root = root
	to_result.category = category
	to_result.subcategory = subcategory
	to_result.variant = variant
	to_result.identifier = identifier
	to_result.invisible = invisible

	if not new_frames:
		to_result.get_image().frames = get_image().frames
		to_result.set_metadata(get_metadata())
	else:
		to_result.load_frames()

	return to_result

func get_class() -> String:
	return "BlockInfo"

func load_frames() -> void:
	.load_frames()

	var path: String = _to_string()

	if GlobalData.is_on_game or not GlobalData.is_on_level_editor:
		if root.begins_with("blocks"):
			if category.begins_with("actions"):
				if subcategory.begins_with("pushable"):
					path = path + "-pressed"

					var spriteframes: SpriteFrames = Resources.process_texture(path) as SpriteFrames

					get_image().frames.add_animation("pressed")

					if spriteframes:
						SpriteFramesExt.add_frames(get_image().frames, "pressed", SpriteFramesExt.get_frames(spriteframes, "default"))
						get_image().frames.set_animation_loop("pressed", false)
					else:
						var frames_default: SpriteFrames = Resources.default_block as SpriteFrames
						SpriteFramesExt.add_frames(get_image().frames, "pressed", SpriteFramesExt.get_frames(frames_default, "default"))


func _set_invisible(new_value: bool) -> void:
	invisible = new_value

	if new_value:
		get_image().modulate = Color(get_image().modulate.r, get_image().modulate.g, get_image().modulate.b, 0.3)
	else:
		get_image().modulate = Color(get_image().modulate.r, get_image().modulate.g, get_image().modulate.b, 1)

func _get_invisible() -> bool:
	return invisible
