class_name BlockRewardInfo
extends BlockInfo

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var reward: RewardInfo = RewardInfo.new()

var _counter: Label = Label.new()
var _image_reward: AnimatedSprite = AnimatedSprite.new()

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _init() -> void:
	type = Enums.DataInfoType.BLOCK_REWARD

	if GlobalData.is_on_level_editor:
		_counter.theme = Resources.get_theme("label/label_counter") as Theme
		_counter.align = Label.ALIGN_CENTER
		_counter.valign = Label.VALIGN_CENTER
		_counter.visible = false
		_image_reward.frames = SpriteFrames.new()
		_image_reward.scale = Vector2(0.9, 0.9)
		_image_reward.visible = false
		_image_reward.modulate = Color(1, 1, 1, 0.8)
		get_image().add_child(_image_reward)
		get_image().add_child(_counter)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func add_reward(npc: int, amount: int = 1) -> void:
	reward.npc_identificator = npc
	reward.total = amount

	if GlobalData.is_on_level_editor:
		_update_content()

func add_amount(amount: int = 1) -> void:
	if has_reward():
		reward.total += amount

		if GlobalData.is_on_level_editor:
			_update_counter()

func clone(new_frames:bool = false) -> DataInfo:
	var to_result: BlockRewardInfo = load(get_script().resource_path).new() as BlockRewardInfo

	to_result.root = root
	to_result.category = category
	to_result.subcategory = subcategory
	to_result.variant = variant
	to_result.invisible = invisible

	if has_reward():
		to_result.add_reward(reward.npc_identificator, reward.total)

	if not new_frames:
		to_result.get_image().frames = get_image().frames
		to_result.set_metadata(get_metadata())
	else:
		to_result.load_frames()

	return to_result

func get_class() -> String:
	return "BlockRewardInfo"

func has_reward() -> bool:
	return reward.npc_identificator != -1 and reward.total > 0

func is_question() -> bool:
	return "question" in variant.to_lower()

func load_frames() -> void:
	.load_frames()

	if GlobalData.is_on_level_editor:
		var texture: Texture = get_image().frames.get_frame("default", 0) as Texture

		_counter.rect_min_size = texture.get_size()
		_counter.rect_position = texture.get_size() / -2
		_update_content()

	if GlobalData.is_on_game or not GlobalData.is_on_level_editor:
		#//TODO: Load solid
		var path: String = to_string().replace(variant, "")

		if path.ends_with("/"):
			path = path.substr(0, path.length() - 1)

		if has_reward():
			var variant_clean: String = variant
			var index_of_separator: int = variant.find("-")

			if index_of_separator != -1:
				variant_clean = variant.substr(0, index_of_separator)

			var all_block_variants: Array = Resources.get_images(path, variant_clean).keys()

			var solid_variant = ""
			for variant_available in all_block_variants:
				if "solid" in variant_available:
					solid_variant = variant_available
					break

			get_image().frames.add_animation("solid")
			var spriteframes: SpriteFrames = Resources.process_texture(solid_variant) as SpriteFrames

			if spriteframes:
				SpriteFramesExt.add_frames(get_image().frames, "solid", SpriteFramesExt.get_frames(spriteframes, "default"))
				get_image().frames.set_animation_speed("solid", spriteframes.get_animation_speed("default"))
			else:
				var frame_default: SpriteFrames = Resources.default_block as SpriteFrames

				SpriteFramesExt.add_frames(get_image().frames, "solid", SpriteFramesExt.get_frames(frame_default, "default"))
				get_image().frames.set_animation_speed("solid", frame_default.get_animation_speed("default"))


func _update_content() -> void:
	if not has_reward():
		return

	if not _image_reward.visible:
		_image_reward.visible = true
		_counter.visible = true

	var npc_reward: NpcInfo = GlobalData.current_level.rewards[reward.npc_identificator]

	var texture: Texture = npc_reward.get_image().frames.get_frame("default", 0)

	_image_reward.frames.add_frame("default", texture, 0)
	_update_counter()

func _update_counter() -> void:
	_counter.text = str(reward.total)
