class_name PlayerInfo
extends DataInfo

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var coins: int = 0
var score: int = 0
var lifes: int = 3

#Enums.PowerupType
var powerup_reserved: int = Enums.PowerupType.NONE
#Enums.PowerupType
var powerup_current: int = Enums.PowerupType.SMALL
#Enums.PlayerKind
var kind_current: int = Enums.PlayerKind.MARIO

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _init() -> void:
	type = Enums.DataInfoType.PLAYER

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clone(new_frames: bool = false) -> DataInfo:
	var to_result: PlayerInfo = load(get_script().resource_path).new() as PlayerInfo
	to_result.root = root
	to_result.category = category
	to_result.subcategory = subcategory
	to_result.variant = variant
	to_result.powerup_current = powerup_current
	to_result.powerup_reserved = powerup_reserved
	to_result.kind_current = kind_current
	to_result.lifes = lifes
	to_result.coins = coins
	to_result.score = score

	if not new_frames:
		to_result.get_image().frames = get_image().frames
		to_result.set_metadata(get_metadata())
	else:
		to_result.load_frames()

	return to_result

func get_class() -> String:
	return "PlayerInfo"

func load_frames() -> void:
	var path: String = "player"
	path = path.plus_file(variant)

	if GlobalData.is_on_level_editor:
		path = path.plus_file("small").to_lower()

		load_metadata(Resources.get_image(path))

		var file: SpriteFrames = Resources.get_frames_from_image(path) as SpriteFrames
		var textures: Array = []

		if file:
			textures = SpriteFramesExt.get_frames(file, "walk")

		var spriteframes: SpriteFrames = SpriteFrames.new()
		SpriteFramesExt.add_frames(spriteframes, "default", textures)
		spriteframes.set_animation_speed("default", GlobalData.FPS_ANIMATION)

		get_image().frames = spriteframes
		get_image().playing = spriteframes.get_frame_count("default") > 1
	elif not GlobalData.is_on_level_editor or GlobalData.is_on_game:
		path = path.plus_file(Enums.PowerupType.keys()[powerup_current]).to_lower()

		load_metadata(Resources.get_image(path))

		var file: Resource = Resources.get_frames_from_image(path)

		if file is SpriteFrames:
			get_image().frames = file
		else:
			push_error("Invalid file")
			breakpoint
