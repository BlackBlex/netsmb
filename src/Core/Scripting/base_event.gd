class_name BaseEvent
extends BaseScript

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var arg = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func attach():
	return ""

func setup() -> void:
	pass

func execute(args) -> void:
	arg = args

	var wait = pre_do()
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = do()
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	wait = post_do()
	if wait is GDScriptFunctionState and wait.is_valid():
		yield(wait, "completed")

	_clean()

func pre_do():
	pass

func do():
	pass

func post_do():
	pass


func _clean() -> void:
	arg = null
