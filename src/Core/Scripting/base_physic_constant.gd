class_name BasePhysicConstant
extends BaseScript

# Walk physics
var SPEED: float = 3.5 * GlobalData.BLOCK_SIZE
var SPEED_RUN: float = round(SPEED * 1.54)
var FACTOR_ACCELERATION: float = 0.6
var FACTOR_DECELERATION: float = FACTOR_ACCELERATION / GlobalData.BLOCK_SIZE
var MULTIPLER_ACCELERATION: float = 1.0
var MULTIPLER_DECELERATION: float = 2.2
var MULTIPLER_RUN_ACCELERATION: float = 1.2
var MULTIPLER_RUN_DECELERATION: float = 2.6
var MULTIPLER_RUN_JUMP_IMPULSE: float = 1.1
var FIX_MIN_ERROR: float = 0.1 * GlobalData.BLOCK_SIZE

# Jump physics
var MAX_JUMP_HEIGHT: float = 4.25 * GlobalData.BLOCK_SIZE
var MIN_JUMP_HEIGHT: float = MAX_JUMP_HEIGHT * 0.2
var JUMP_DURATION: float = 1.0
var FALL_DURATION: float = JUMP_DURATION * 0.6

# World physics
var GRAVITY: float = 2.5 * MAX_JUMP_HEIGHT / pow(JUMP_DURATION, 2)
var FALL_GRAVITY: float = 2 * MAX_JUMP_HEIGHT / pow(FALL_DURATION, 2)
var FLOOR_NORMAL: Vector2 = Vector2.UP
var MAX_JUMP_VELOCITY: float = -sqrt(2 * GRAVITY * MAX_JUMP_HEIGHT)
var MIN_JUMP_VELOCITY: float = -sqrt(2 * GRAVITY * MIN_JUMP_HEIGHT)
