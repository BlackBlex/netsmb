extends BaseBehaviour

# You have the variable: _OWNER to access all the content of current entity
# You have the variable: description to describe the behaviour
# You have the function: load_resource(path) to load any resource of local path or level path ("local://path" or "level://path")

#Is called only when the behaviour is created
func _init() -> void:
	description = "New behaviour"


#Is called only once when the entity is loaded
func setup() -> void:
	pass

#Is always called during the level
func execute() -> void:
	pass
