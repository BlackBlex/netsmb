extends BaseEvent

# You have the variable: _LEVEL to access all the content of current level
# You have the variable: arg to access to some parameters
# You have the variable: name to name the event
# You have the variable: description to describe the event
# You have the function: attach() to attach script to a event
# You have the function: load_resource(path) to load any resource of local path or level path ("local://path" or "level://path")

#Is called only when the event is created
func _init() -> void:
	name = "New event"
	description = "New description event"


#Is called only when the event is created
func attach():
	return A.EventList.level.started #Occurs when the level is started | see A.EventList. for more triggers

#Is called only once when the level is loaded
func setup() -> void:
	pass

#Is called before do is executed
func pre_do():
	pass

#Is called when the event occurs
func do():
	pass

#Is called after do is executed
func post_do():
	pass
