extends BaseHUD

# You have the variable: _LEVEL to access all the content of current level
# You have the function: get_life_section() to get a life section
# You have the function: get_coin_section() to get a coin section
# You have the function: get_item_section() to get a item section
# You have the function: get_time_section() to get a time section
# You have the function: get_score_section() to get a score section
# You have the function: load_resource(path) to load any resource of local path or level path ("local://path" or "level://path")

#Is called only once when the level is loaded
func setup() -> void:
	pass

#Is always called during the level
func loop(_delta: float) -> void:
	pass
