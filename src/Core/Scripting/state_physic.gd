class_name StatePhysic
extends State

var max_speed: float = 0
var is_running: bool = false

var _PHYSIC_CONST: BasePhysicConstant = null

func get_physic_constants() -> BasePhysicConstant:
	return _PHYSIC_CONST

func move(_delta: float, _direction: Vector2 = Vector2.INF, _multipler_acceleration: float = _PHYSIC_CONST.MULTIPLER_ACCELERATION, _multipler_deceleration: float = _PHYSIC_CONST.MULTIPLER_DECELERATION) -> void:
	pass
