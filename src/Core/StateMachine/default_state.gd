class_name DefaultState
extends State

# State machine callback called during transition when entering this state
func on_enter_state() -> void:
	push_warning("Unimplemented on_enter_state in default state")

# State machine callback called during transition when leaving this state
func on_leave_state():
	push_warning("Unimplemented on_leave_state in default state")

func on_input(_event: InputEvent) -> void:
	push_warning("Unimplemented on_input in default state")

func on_physics_process(_delta: float) -> void:
	push_warning("Unimplemented on_physics_process in default state")

func on_process(_delta: float) -> void:
	push_warning("Unimplemented on_process in default state")
