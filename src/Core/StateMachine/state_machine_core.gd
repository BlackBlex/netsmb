class_name StateMachineCore
extends Node

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal state_changed(from_state, to_state)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var active: bool = false

#The state machine's target object, node, etc
var _target: Object = null
#Dictionary of string with State
var _states: Dictionary = {}
#Dictionary of string with Array of string
var _transitions = {}
#Reference to current state object
var _state_current = DefaultState.new()
#Reference to last state object
var _state_last = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _init() -> void:
	_states = {}
	_transitions = {}

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func input(event: InputEvent) -> void:
	if not active:
		return

	if _state_current.has_method("on_input"):
		_state_current.on_inpout(event)

func process(delta: float) -> void:
	if not active:
		return

	if _state_current.has_method("on_process"):
		_state_current.on_process(delta)

func physics_process(delta: float) -> void:
	if not active:
		return

	if _state_current.has_method("on_physics_process"):
		_state_current.on_physics_process(delta)

#Sets the target object, which could be a node or some other object that the states expect
func set_target(new_target: Object) -> void:
	_target = new_target

	for state in _states.values():
		state._OWNER = _target

#Returns the target object (node, object, etc)
func get_target() -> Object:
	return _target

# Add a state to the states dictionary
func set_state(state_name: String, state: State) -> void:
	if not _states.has(state_name):
		_states[state_name] = state

	state.name = state_name

	if _target:
		state._OWNER = _target

#Return the state from the states dictionary by state id if it exists
func get_state(state_name: String) -> State:
	var to_result: State = null

	if _states.has(state_name):
		to_result = _states[state_name]
	else:
		push_warning("Cannot get state, invalid state: " + state_name)

	return to_result

#Expects an array of state definitions to generate the dictionary of states
func set_states(states: Dictionary) -> void:
	for key in states.keys():
		set_state(key, states[key])

#Returns the dictionary of states
func get_states() -> Dictionary:
	return _states

#Set valid transitions for a state. Expects state id and array of to state ids.
#If a state id does not exist in states dictionary, the transition will NOT be added.
func set_transition(state_name: String, to_states: Array) -> void:
	if _states.has(state_name):
		if not _transitions.has(state_name):
			_transitions[state_name] = to_states
	else:
		push_error("Cannot set transition, invalid state: " + state_name)
		breakpoint

#Return the transition from the transitions dictionary by state id if it exists
func get_transition(state_name: String) -> Array:
	var to_result: Array = []

	if _transitions.has(state_name):
		to_result = _transitions[state_name]
	else:
		push_warning("Cannot get transition, invalid state: " + state_name)

	return to_result

#Expects an array of transition definitions to generate the dictionary of transitions
func set_transitions(transitions: Dictionary) -> void:
	for key in transitions.keys():
		if not key.empty() and transitions[key].size() > 0:
			set_transition(key, transitions[key])

#Returns the dictionary of transitions
func get_transitions() -> Dictionary:
	return _transitions

#This is a "just do it" method and does not validate transition change
func set_state_current(state_name: String) -> void:
	if _states.has(state_name):
		_state_last = _state_current
		_state_current = _states[state_name]
	else:
		push_error("Cannot set current state, invalid state: " + state_name)
		breakpoint

#Returns the Reference of the current state
func get_state_current() -> State:
	return _state_current

func get_state_current_name() -> String:
	return _get_state_name(_state_current)

func get_state_last() -> State:
	return _state_last

func get_state_last_name() -> String:
	return _get_state_name(_state_last)

# //DEPRECATED: Remove in next release
# Expects an array of states to iterate over and pass self to the state's Machine/_MACHINE property
# func set_state_machine(states: Array) -> void:
# 	for state in states:
# 		state._MACHINE = self

# Add a transition for a state. This adds a single state to transitions whereas
#set_transition is a full replace.
func add_transition(from_state_name: String, to_state_name: String) -> void:
	if not _states.has(from_state_name) or not _states.has(to_state_name):
		push_error("Cannot add transition, invalid state(s): from_state=" + from_state_name + ", to_state=" + to_state_name)

	if _transitions.has(from_state_name):
		_transitions[from_state_name].append(to_state_name)
	else:
		_transitions[from_state_name] = [to_state_name]

#Transition to new state by state id.
#Callbacks will be called on the from and to states if the states have implemented them.
func transition(state_name: String) -> void:
	var id: String = _get_state_name(_state_current)

	if not _transitions.has(id):
		push_error("No transitions defined for state " + id)
		breakpoint

	if not _states.has(state_name) or not _transitions[id].has(state_name):
		push_error("Invalid transition from " + id + " to " + state_name)
		breakpoint

	var from_state: State = get_state(id)
	var to_state: State = get_state(state_name)

	if from_state.has_method("on_leave_state"):
		from_state.on_leave_state()

	set_state_current(state_name)

	if to_state.has_method("on_enter_state"):
		to_state.on_enter_state()

	emit_signal("state_changed", [from_state, to_state])

func transition_initial(state_initial: String) -> void:
	var to_state = get_state(state_initial)

	set_state_current(state_initial)

	if to_state.has_method("on_enter_state"):
		to_state.on_enter_state()

	emit_signal("state_changed", [null, to_state])

func replace_transition(from_state_name: String, to_old_state_name: String, to_new_state_name: String) -> void:
	if not _states.has(from_state_name) or not _states.has(to_old_state_name) or not _states.has(to_new_state_name):
		push_error("Cannot add transition, invalid state(s): from_state_name=" + from_state_name + ", to_old_state_name=" + to_old_state_name + ", to_new_state_name=" + to_new_state_name)
		breakpoint

	if _transitions.has(from_state_name):
		if _transitions[from_state_name].has(to_old_state_name):
			_transitions[from_state_name].erase(to_old_state_name)

		add_transition(from_state_name, to_new_state_name)
	else:
		push_error("Cannot replace transition, invalid state: from_state_name=" + from_state_name)
		breakpoint


func _get_state_name(state: State) -> String:
	return state.name
