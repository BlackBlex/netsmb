class_name InputManagement
extends Object

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _UP_SUFFIX = "_up"
const _DOWN_SUFFIX = "_down"
const _LEFT_SUFFIX = "_left"
const _RIGHT_SUFFIX = "_right"
const _JUMP_SUFFIX = "_jump"
const _ALTJUMP_SUFFIX = "_altjump"
const _RUN_SUFFIX = "_run"
const _ALTRUN_SUFFIX = "_altrun"

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var direction: Vector2 = Vector2.ZERO
var up_key: String = "" setget _set_null, _get_up
var down_key: String = "" setget _set_null, _get_down
var left_key: String = "" setget _set_null, _get_left
var right_key: String = "" setget _set_null, _get_right
var jump_key: String = "" setget _set_null, _get_jump
var altjump_key: String = "" setget _set_null, _get_altjump
var run_key: String = "" setget _set_null, _get_run
var altrun_key: String = "" setget _set_null, _get_altrun

var _jump_pressed: bool = false
var _jump_just_pressed: bool = false
var _jump_released: bool = false
var _altjump_pressed: bool = false
var _altjump_just_pressed: bool = false
var _altjump_released: bool = false
var _run_pressed: bool = false
var _run_just_pressed: bool = false
var _run_released: bool = false
var _altrun_pressed: bool = false
var _altrun_just_pressed: bool = false
var _altrun_released: bool = false
var _active: bool = true

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_default_update() -> FuncRef:
	return funcref(self, "_default_update")

func get_jump_pressed() -> bool:
	return _jump_pressed

func get_jump_just_pressed() -> bool:
	return _jump_just_pressed

func get_jump_released() -> bool:
	return _jump_released

func get_altjump_pressed() -> bool:
	return _altjump_pressed

func get_altjump_just_pressed() -> bool:
	return _altjump_just_pressed

func get_altjump_released() -> bool:
	return _altjump_released

func get_run_pressed() -> bool:
	return _run_pressed

func get_run_just_pressed() -> bool:
	return _run_just_pressed

func get_run_released() -> bool:
	return _run_released

func get_altrun_pressed() -> bool:
	return _altrun_pressed

func get_altrun_just_pressed() -> bool:
	return _altrun_just_pressed

func get_altrun_released() -> bool:
	return _altrun_released

func resume() -> void:
	_active = true

func setup_with_player(player_number: int) -> void:
	up_key = "p" + str(player_number) + _UP_SUFFIX
	down_key = "p" + str(player_number) + _DOWN_SUFFIX
	left_key = "p" + str(player_number) + _LEFT_SUFFIX
	right_key = "p" + str(player_number) + _RIGHT_SUFFIX
	jump_key = "p" + str(player_number) + _JUMP_SUFFIX
	altjump_key = "p" + str(player_number) + _ALTJUMP_SUFFIX
	run_key = "p" + str(player_number) + _RUN_SUFFIX
	altrun_key = "p" + str(player_number) + _ALTRUN_SUFFIX

func setup_with_key(up: String, down: String, left: String, right: String, jump: String, altjump: String, run: String, altrun: String) -> void:
	up_key = up
	down_key = down
	left_key = left
	right_key = right
	jump_key = jump
	altjump_key = altjump
	run_key = run
	altrun_key = altrun

func stop() -> void:
	_active = false


func _default_update() -> void:
	if not _active:
		return

	direction = Vector2(int(Input.is_action_pressed(right_key)) - int(Input.is_action_pressed(left_key)),
		int(Input.is_action_pressed(down_key)) - int(Input.is_action_pressed(up_key)))

	_jump_pressed = Input.is_action_pressed(jump_key)
	_jump_just_pressed = Input.is_action_just_pressed(jump_key)
	_jump_released = Input.is_action_just_released(jump_key)

	_altjump_pressed = Input.is_action_pressed(altjump_key)
	_altjump_just_pressed = Input.is_action_just_pressed(altjump_key)
	_altjump_released = Input.is_action_just_released(altjump_key)

	_run_pressed = Input.is_action_pressed(run_key)
	_run_just_pressed = Input.is_action_just_pressed(run_key)
	_run_released = Input.is_action_just_released(run_key)

	_altrun_pressed = Input.is_action_pressed(altrun_key)
	_altrun_just_pressed = Input.is_action_just_pressed(altrun_key)
	_altrun_released = Input.is_action_just_released(altrun_key)

func _set_null(_newValue: String) -> void:
	pass

func _get_up() -> String:
	return up_key

func _get_down() -> String:
	return down_key

func _get_left() -> String:
	return left_key

func _get_right() -> String:
	return right_key

func _get_jump() -> String:
	return jump_key

func _get_altjump() -> String:
	return altjump_key

func _get_run() -> String:
	return run_key

func _get_altrun() -> String:
	return altrun_key
