class_name Player
extends Character

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: PlayerInfo = null
var powerup_current: int setget _set_powerup_current, _get_powerup_current
var powerup_variant: String = ""
var powerup_current_variant: String = ""
var powerup_reserved_variant: String = ""

#Array of array of powerup - 0=current, 1=new
var _queue_powerup: Array = []
var _process_queue_powerup: bool = false
var _return_normal: bool = false
var _available_shot: bool = true

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var camera: Camera2D = $Camera
onready var delay_shot: Timer = $DelayShot

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	image = info.get_image()
	info.place(position_pivot)

	build_state_machine(info.to_string())

	input = InputManagement.new()
	input.setup_with_key("action_up", "action_down", "action_left", "action_right", "action_jump", "action_altjump", "action_run", "action_altrun")

	funcrefs_handle_input.append(input.get_default_update())

	Event.subscribe(A.EventList.level.finished, self, "_on_level_finish")

func _process(delta: float) -> void:
	._process(delta)

	if not _process_queue_powerup:
		if raycast_up.is_colliding() and raycast_down.is_colliding():
			if input.direction.y != 1 and powerup_current != Enums.PowerupType.SMALL:
				_return_normal = false
				input.direction = Vector2(input.direction.x, 1)
		else:
			if not _return_normal:
				_return_normal = true
				input.direction = Vector2(input.direction.x, 0)

func _physics_process(delta: float) -> void:
	._physics_process(delta)

	if info.powerup_current > Enums.PowerupType.BIG:
		if input.get_run_just_pressed() and _available_shot:
			_available_shot = false
			sm.transition("throw")
			delay_shot.start()

	if not _process_queue_powerup:
		if not _queue_powerup.empty():
			_process_queue_powerup = true

			while not _queue_powerup.empty():
				var powerup_for_process = _queue_powerup.pop_front()

				_process_powerup(powerup_for_process[0], powerup_for_process[1])

			_process_queue_powerup = false

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_level_finish(exit_type: int) -> void:
	Event.unsubscribe(A.EventList.level.finished, self)
	if exit_type == ERR_TIMEOUT:
		powerup_current = Enums.PowerupType.DIE
		info.lifes -= 1

func _on_delay_shot_timeout() -> void:
	_available_shot = true

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func destroy(_args: Array = []) -> void:
	Event.publish(self, A.EventList.player.died)
	queue_free()

func get_compound_name(delimiter: String = "") -> String:
	var name: String = Enums.get_name(Enums.PlayerKind, info.kind_current)
	#if info.kind_current == custoom
	return "%s%s%s" % [name, delimiter, Enums.get_name(Enums.PowerupType, info.powerup_current).replace("_", "-")]


func _powerup_start() -> void:
	visible = false
	is_invincible = true

func _process_powerup(old_powerup: int, new_powerup: int) -> void:
	var is_damage: bool = new_powerup == Enums.PowerupType.DAMAGE
	if is_damage:
		if old_powerup > Enums.PowerupType.SMALL:
			new_powerup = old_powerup - 1
		else:
			new_powerup = Enums.PowerupType.DIE

	if new_powerup == Enums.PowerupType.DIE:
		input.stop()
		sm.active = false
		var player_die: String = "%s-die" % Enums.get_name(Enums.PlayerKind, info.kind_current)

		var position_up: Vector2 = position + Vector2(0, -30)
		var position_down: Vector2 = position + Vector2(0, 400)

		var effects: EffectsExecuter = EntityExt.effects(self)

		var _err = effects.load_image(EffectsExecuter.Helper.EffectCategory.PLAYER, player_die).positionate(position).attach_visibility_notifier(Instanceator.make_funcref_ext(self, "_powerup_start"), Instanceator.make_funcref_ext(self, "destroy")).animate().interpolate_property(effects.get_fx_object(), "position", position, position_up, 0.2).interpolate_property(effects.get_fx_object(), "position", position_up, position_down, 1, true)
		Audio.play_effect("player-died.ogg")
		effects.create()
	else:
		info.powerup_current = new_powerup
		var frames_old: SpriteFrames = image.frames
		var frames_new: SpriteFrames = Resources.get_frames_from_image("player/%s" % get_compound_name("/").to_lower()) as SpriteFrames

		if frames_new:
			if not is_damage:
				if new_powerup < old_powerup:
					#Save item
					info.powerup_reserved = new_powerup
					powerup_reserved_variant = powerup_variant
					powerup_variant = ""
					Audio.play_effect("has-item.ogg")
					return
				elif new_powerup == old_powerup:
					#Replace save item if item is less
					if info.powerup_reserved < new_powerup:
						info.powerup_reserved = new_powerup
						powerup_reserved_variant = powerup_variant
						Audio.play_effect("has-item.ogg")
					return

			_set_invincible_time(0.7)
			_set_invincible(true)

			var size: Vector2 = frames_new.get_frame("idle", 0).get_size()

			var collision_extents: Vector2 = Vector2(size.x / 2.8, size.y / 2.4)
			var collision_position: Vector2 = Vector2(collision.position.x, (size.y / 2) - collision_extents.y)

			var rectangle_shape: RectangleShape2D = collision.shape as RectangleShape2D

			var animator: AnimatorExecuter = EntityExt.animator(self)

			var _err = animator.interpolate_property(rectangle_shape, "extents", rectangle_shape.extents, collision_extents, 0.2).interpolate_property(collision, "position", collision.position, collision_position, 0.4).alternate_sprite(image, frames_old, frames_new, 0.6).blink(position_pivot, 0, 0.5)

			if old_powerup < new_powerup:
				Audio.play_effect("player-grow.ogg")
				if old_powerup > Enums.PowerupType.SMALL:
					info.powerup_reserved = old_powerup
					powerup_reserved_variant = powerup_current_variant
			else:
				Audio.play_effect("player-shrink.ogg")
				# If has item reserved drop it

			powerup_current_variant = powerup_variant
			animator.start()

func _set_powerup_current(powerup: int) -> void:
	if powerup != Enums.PowerupType.NONE:
		_queue_powerup.push_back([info.powerup_current, powerup])

func _get_powerup_current() -> int:
	return info.powerup_current
