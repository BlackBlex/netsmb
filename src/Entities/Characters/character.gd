class_name Character
extends Entity


func _input(event: InputEvent) -> void:
	._input(event)

	handle_input()

	if input.direction.x != 0:
		position_pivot.scale.x = input.direction.x
