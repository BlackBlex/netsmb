class_name Enemy
extends Npc

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: EnemyInfo = null
var is_grabbed_by_player: bool = false setget _unused_set, _is_grabbed_by_player
var player_who_grabbed: Player = null setget _unused_set, _get_player_who_grabbed
var is_colliding: bool = false
var can_grab: bool = true
var kicked_by: Entity = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var area_player: Area2D = $AreaPlayer
onready var collision_player: CollisionShape2D = $AreaPlayer/Collision
onready var collision_timer: Timer = $CollisionTimer

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	set_process(false)
	._ready()

	image = info.get_image()
	info.place(position_pivot)

	if not GlobalData.is_on_level_editor and not info.get_metadata().empty():
		if info.get_metadata().has("name"):
			name = info.get_metadata()["name"]
			add_to_group(name)

	var direction_initial: Vector2 = Vector2.ZERO

	if info.direction == Enums.Direction.LEFT:
		direction_initial = Vector2.LEFT
	elif info.direction == Enums.Direction.RIGHT:
		direction_initial = Vector2.RIGHT
	elif info.direction == Enums.Direction.UP:
		direction_initial = Vector2.UP
	elif info.direction == Enums.Direction.DOWN:
		direction_initial = Vector2.DOWN
	elif info.direction == Enums.Direction.RANDOM:
		randomize()
		if randf() < 0.5:
			direction_initial = Vector2.RIGHT
		else:
			direction_initial = Vector2.LEFT

	make_collision_and_detector(info)
	build_actions(info)

	if not "no_movement" in info.behaviour and info.direction != Enums.Direction.NO_MOVE:
		build_state_machine(info.to_string())

		input = InputManagement.new()
		funcrefs_handle_input.append(build_ia(info))

		if not EntityExt.is_shell(self):
			input.direction = direction_initial
		else:
			set_collision_mask_bit(Enums.Collision.ENEMY, false)
			raycast_up.set_collision_mask_bit(Enums.Collision.ENEMY, false)
			raycast_down.set_collision_mask_bit(Enums.Collision.ENEMY, false)
			raycast_look_to.set_collision_mask_bit(Enums.Collision.ENEMY, false)
			raycast_look_to_back.set_collision_mask_bit(Enums.Collision.ENEMY, false)

		set_process(true)
	else:
		sm = null

func _process(delta: float) -> void:
	._process(delta)

	handle_input()

	if not input:
		return

	if input.direction.x != 0:
		position_pivot.scale.x = input.direction.x

	if not is_visible_on_camera and is_instance_valid(GlobalData.player_camera):
		if sm and sm.active:
			var camera_pos: Vector2 = GlobalData.player_camera.get_camera_screen_center()
			var distance: float = abs((position.x - camera_pos.x) - (GlobalData.map_size_render.x / 2))
			sm.active = distance <= 200

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_area_player_body_entered(body: Node) -> void:
	if _on_hit and is_collision_active:
		if _on_hit.is_valid_player(body):
			_on_hit.execute_player([body])

func _on_collision_timer_timeout() -> void:
	is_colliding = false

func _on_visibility_notifier_screen_entered() -> void:
	is_visible_on_camera = true

	if sm and not sm.active:
		sm.active = true

func _on_visibility_notifier_screen_exited() -> void:
	is_visible_on_camera = false

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func destroy(args: Array = []) -> void:
	var is_player_killer = args[0] is Player
	var enemy: String = info._to_string()

	if EntityExt.is_koopa_troopa(self):
		enemy = enemy.replace("koopa_troopa", "shell")

	if is_player_killer:
		Audio.play_effect("stomped.ogg")
		if EntityExt.is_koopa_troopa(self):
			var _err = Factory.npc.instance_enemy(layer_id, "shell", info.variant, position)
		else:
			var effects = EntityExt.effects(self)
			var size_height = (get_size().y - (get_size().y * 0.1)) / 2
			effects.load_image(-1, enemy, true).scale(Vector2(1, 0.1)).positionate_float(position.x, position.y + (size_height - 0.2), true).create()
	else:
		Audio.play_effect("shell-hit.ogg")
		var effects = EntityExt.effects(self)
		effects.load_image(-1, enemy, true).rotate(180).positionate_path(position)

		var invert: bool = false

		if args.size() == 2 and args[1] == -1:
			invert = true

		if not invert:
			if args[0].input.direction.x == -1:
				invert = true

		if invert:
			effects.invert_path()

		effects.move_to_path().load_curve("kill_with_shell").animate().interpolate_property(effects.get_path_follow(), "unit_offset", 0, 1, 1.2)
		effects.create()

	queue_free()

func disable() -> void:
	is_collision_active = false
	set_physics_process(false)
	set_process(false)
	collision.disabled = true
	collision_hit.disabled = true
	collision_player.disabled = true

func enable() -> void:
	is_collision_active = true
	collision.disabled = false
	collision_hit.disabled = false
	collision_player.disabled = false
	set_physics_process(true)
	set_process(true)

func make_collision_and_detector(_info: DataInfo) -> void:
	.make_collision_and_detector(_info)

	var shape: RectangleShape2D = collision_hit.shape as RectangleShape2D
	shape.extents.y -= 2
	shape.extents.x += 1
	collision_hit.position.y += 1.25

	var collision_area: RectangleShape2D = RectangleShape2D.new()
	collision_player.position = raycast_up.position - Vector2(0, -0.5)
	collision_area.extents = Vector2(shape.extents.x - 2, 0.25)
	collision_player.shape = collision_area


func _get_player_who_grabbed() -> Player:
	var entity = get_parent().get_parent()

	if entity is Player:
		return entity as Player

	return null

func _is_grabbed_by_player() -> bool:
	return _get_player_who_grabbed() != null

func _unused_set(_val) -> void:
	pass
