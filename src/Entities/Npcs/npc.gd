class_name Npc
extends Entity

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var is_transformable: bool = true

var _behaviour: BaseBehaviour = null
var _on_hit: BaseAction = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var area_hit: Area2D = $AreaHit
onready var collision_hit: CollisionShape2D = $AreaHit/Collision

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_area_body_entered(body: Node) -> void:
	if _on_hit and is_collision_active:
		if _on_hit.is_valid(body):
			_on_hit.execute([body])

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func activate_sm() -> void:
	if sm:
		sm.active = true

func build_ia(info: NpcInfo) -> FuncRef:
	var to_result: FuncRef = null

	var path_absolute: String = "%s/%s.gd" % [info, info.behaviour]
	var path_file: String = path_absolute.get_file()
	var path_to_search: String = ""
	var path_base_to_search: String = ""
	var is_on_level: bool = A.directory_loader.dir_exists(A.Paths.level_behaviours)
	var metadata: Dictionary = info.get_metadata()

	if not metadata.empty():
		if metadata.has("only_local"):
			if bool(metadata["only_local"]):
				is_on_level = false

	if is_on_level:
		path_to_search = A.Paths.level_behaviours.plus_file(path_absolute)
		path_base_to_search = A.Paths.level_behaviours
		path_to_search = Utility.find_exist_path_file(path_to_search.replace(path_file, ""), path_base_to_search, path_file, path_file)

	if not is_on_level or (not A.file_loader.file_exists(path_to_search)):
		path_to_search = A.Paths.behaviours.plus_file(path_absolute)
		path_base_to_search = A.Paths.behaviours
		path_to_search = Utility.find_exist_path_file(path_to_search.replace(path_file, ""), path_base_to_search, path_file, path_file)

	_behaviour = Utility.load_script(path_to_search).new() as BaseBehaviour

	if _behaviour:
		_behaviour.set("_OWNER", self)
		_behaviour.setup()

		to_result = funcref(_behaviour, "execute")
	else:
		Logger.msg_error("Behaviour: %s not found..." % path_absolute, Logger.CATEGORY_FILE_LOAD)

	return to_result

func build_actions(info: NpcInfo) -> void:
	_on_hit = Entity.build_action("on_hit", info)

	if _on_hit:
		_on_hit.set("_OWNER", self)
		_on_hit.setup()
	else:
		Logger.msg_error("Action: %s not found..." % ("%s/on_hit.gd" % info), Logger.CATEGORY_FILE_LOAD)

func make_collision_and_detector(info: DataInfo) -> void:
	.make_collision_and_detector(info)

	collision_hit.position = collision.position
	var shape: RectangleShape2D = collision.shape as RectangleShape2D

	var collision_area: RectangleShape2D = RectangleShape2D.new()
	collision_area.extents = shape.extents + Vector2(0.5, 0)
	collision_hit.shape = collision_area

func make_collision_manual() -> void:
	.make_collision_manual()

	var current_animation: String = "idle" if image.frames.has_animation("idle") else image.animation
	var size: Vector2 = .get_size(current_animation)

	var extents: Vector2 = Vector2(size.x / 2, size.y / 2)
	var collision_area: RectangleShape2D = RectangleShape2D.new()
	collision_area.extents = extents
	collision_hit.shape = collision_area
