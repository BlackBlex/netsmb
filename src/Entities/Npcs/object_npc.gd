class_name ObjectNpc
extends Npc

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: ObjectInfo = null
var grabbed: bool = false

var _transform_block = null # //TODO: Check why Block is error

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	set_process(false)
	._ready()

	_subscribe_events()

	image = info.get_image()
	info.place(position_pivot)

	if not GlobalData.is_on_level_editor and not info.get_metadata().empty():
		if info.get_metadata().has("name"):
			name = info.get_metadata()["name"]
			add_to_group(name)

	sm = null

	make_collision_and_detector(info)
	build_actions(info)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func disable() -> void:
	is_collision_active = false
	set_physics_process(false)
	set_process(false)
	collision.disabled = true
	collision_hit.disabled = true

func enable() -> void:
	is_collision_active = true
	set_physics_process(true)
	set_process(true)
	collision.disabled = false
	collision_hit.disabled = false

func destroy(_args = []) -> void:
	_unsubscribe_events()
	queue_free()

func get_transform():
	return _transform_block


func _load_transform_npc(_category: String) -> void:
	var result: Array = Factory.block.not_visible().disable().not_transformable().create_brick(self, layer_id, info.variant, -1, position, true)

	if result[0] == OK:
		Event.subscribe(A.EventList.block.breakk, self, "destroy", result[1])
		_transform_block = result[1]

func _subscribe_events() -> void:
	if info.object_given == Enums.ObjectNpcType.COIN and is_transformable:
		_load_transform_npc("block")
		Event.subscribe(A.EventList.pswitch.started, self, "_transform_to_block")
		Event.subscribe(A.EventList.pswitch.ended, self, "_transform_to_coin")

func _unsubscribe_events() -> void:
	if info.object_given == Enums.ObjectNpcType.COIN and is_transformable:
		Event.unsubscribe(A.EventList.pswitch.started, self)
		Event.unsubscribe(A.EventList.pswitch.ended, self)

	# Clean remain events subscrition
	Event.unsubscribe_all_events(self)

func _transform_to_block() -> void:
	if is_instance_valid(_transform_block):
		visible = false
		disable()
		_transform_block.visible = true
		_transform_block.enable(true)

func _transform_to_coin() -> void:
	visible = true

	if is_instance_valid(_transform_block):
		_transform_block.disable(true)
		_transform_block.visible = false

	enable()
