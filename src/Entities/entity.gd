class_name Entity
extends KinematicBody2D

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var velocity: Vector2 = Vector2.ZERO
var is_invincible: bool = false setget _set_invincible, _get_invincible
var invincible_time: float = 5 setget _set_invincible_time, _get_invincible_time
#Array of funcref
var funcrefs_handle_input: Array = []
#Array of funcref
var funcrefs_on_invincible_timer_timeout: Array = []
var input: InputManagement = null
var image: AnimatedSprite = null
var is_collision_active: bool = true
var is_visible_on_camera: bool = false
var layer_id: int = -1
# //REVIEW: why is this needed?
var fix_detectors: int = 4

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var raycast_up: RayCast2D = $Position/Raycasts/Up
onready var raycast_down: RayCast2D = $Position/Raycasts/Down
onready var raycast_look_to: RayCast2D = $Position/Raycasts/LookTo
onready var raycast_look_to_back: RayCast2D = $Position/Raycasts/LookToBack
onready var collision: CollisionShape2D = $Collision
onready var position_pivot: Position2D = $Position
onready var sm: StateMachineCore = $StateMachine
onready var invincible_timer: Timer = $InvincibleTimer
onready var visibility_notifier: VisibilityNotifier2D = $VisibilityNotifier

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	invincible_time = 5

func _input(event: InputEvent) -> void:
	if sm:
		sm.input(event)

func _process(delta: float) -> void:
	if sm:
		sm.process(delta)

func _physics_process(delta: float) -> void:
	if sm:
		sm.physics_process(delta)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_invincible_timer_timeout() -> void:
	is_invincible = false
	invincible_time = 5

	for action in funcrefs_on_invincible_timer_timeout:
		if action is FuncRef and action.is_valid():
			action.call_func()

func _on_visibility_notifier_screen_entered() -> void:
	pass

func _on_visibility_notifier_screen_exited() -> void:
	pass

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
static func build_action(action: String, info: DataInfo) -> BaseAction:
	var path_absolute: String = "%s/%s.gd" % [info, action]
	var path_base: String = path_absolute.substr(0, path_absolute.find("/"))
	var path_file: String = path_absolute.get_file()
	var path_to_search: String = ""
	var path_base_to_search: String = ""
	var is_on_level: bool = A.directory_loader.dir_exists(A.Paths.level_actions)

	if is_on_level:
		path_to_search = A.Paths.level_actions.plus_file(path_absolute)
		path_base_to_search = A.Paths.level_actions.plus_file(path_base)
		path_to_search = Utility.find_exist_path_file(path_to_search.replace(path_file, ""), path_base_to_search, path_file, path_file)

	if not is_on_level or (not A.file_loader.file_exists(path_to_search)):
		path_to_search = A.Paths.actions.plus_file(path_absolute)
		path_base_to_search = A.Paths.actions.plus_file(path_base)
		path_to_search = Utility.find_exist_path_file(path_to_search.replace(path_file, ""), path_base_to_search, path_file, path_file)

	return Utility.load_script(path_to_search).new() as BaseAction

func build_state_machine(info: String) -> void:
	var path_definition_absolute: String = "%s.gd" % info
	var path_definition_base: String = path_definition_absolute.substr(0, path_definition_absolute.find("/"))
	var path_definition_file: String = path_definition_absolute.get_file()
	var path_definition_to_search: String = ""
	var path_definition_base_to_search: String = ""
	var is_on_level: bool = A.directory_loader.dir_exists(A.Paths.level_definitions)

	if is_on_level:
		path_definition_to_search = A.Paths.level_definitions.plus_file(path_definition_absolute)
		path_definition_base_to_search = A.Paths.level_definitions.plus_file(path_definition_base)
		path_definition_to_search = Utility.find_exist_path_file(path_definition_to_search.replace(path_definition_file, ""), path_definition_base_to_search, path_definition_file, "base.gd")

	if not is_on_level or (not A.file_loader.file_exists(path_definition_to_search)):
		path_definition_to_search = A.Paths.definitions.plus_file(path_definition_absolute)
		path_definition_base_to_search = A.Paths.definitions.plus_file(path_definition_base)
		path_definition_to_search = Utility.find_exist_path_file(path_definition_to_search.replace(path_definition_file, ""), path_definition_base_to_search, path_definition_file, "base.gd")

	var definition_instance: BaseDefinition = Utility.load_script(path_definition_to_search).new() as BaseDefinition

	if definition_instance:
		var path_physic_absolute: String = path_definition_absolute.replace(path_definition_file, "physic.gd")
		var path_physic_file: String = path_physic_absolute.get_file()
		var path_physic_to_search: String = ""
		var path_physic_base_to_search: String = ""
		var is_physic_on_level: bool = A.directory_loader.dir_exists(A.Paths.level_definitions)

		if is_physic_on_level:
			path_physic_to_search = A.Paths.level_definitions.plus_file(path_physic_absolute)
			path_physic_base_to_search = A.Paths.level_definitions
			path_physic_to_search = Utility.find_exist_path_file(path_physic_to_search.replace(path_physic_file, ""), path_physic_base_to_search, path_physic_file, "physic.gd")

		if not is_physic_on_level or (not A.file_loader.file_exists(path_physic_to_search)):
			path_physic_to_search = A.Paths.definitions.plus_file(path_physic_absolute)
			path_physic_base_to_search = A.Paths.definitions
			path_physic_to_search = Utility.find_exist_path_file(path_physic_to_search.replace(path_physic_file, ""), path_physic_base_to_search, path_physic_file, "physic.gd")

		var physics: BasePhysicConstant = Utility.load_script(path_physic_to_search).new() as BasePhysicConstant

		var states: Dictionary = definition_instance.state_machine()
		var state_initial: String = definition_instance.initial_state

		if not states.empty():
			var path_state_absolute: String = path_definition_absolute.replace(path_definition_file, "")
			var path_state_base: String = path_state_absolute.substr(0, path_state_absolute.find("/"))
			var path_state_to_search: String = ""
			var path_state_base_to_search: String = ""
			var is_state_on_level: bool = A.directory_loader.dir_exists(A.Paths.level_states)

			if is_state_on_level:
				path_state_to_search = A.Paths.level_states.plus_file(path_state_absolute)
				path_state_base_to_search = A.Paths.level_states.plus_file(path_state_base)
				path_state_to_search = Utility.find_exist_path(path_state_to_search, path_state_base_to_search)

			if not is_state_on_level or (not A.file_loader.file_exists(path_state_to_search)):
				path_state_to_search = A.Paths.states.plus_file(path_state_absolute)
				path_state_base_to_search = A.Paths.states.plus_file(path_state_base)
				path_state_to_search = Utility.find_exist_path(path_state_to_search, path_state_base_to_search)

			if "player" in path_state_to_search and path_state_to_search == path_state_base_to_search:
				if A.directory_loader.dir_exists(path_state_to_search.plus_file("common")):
					path_state_base_to_search = path_state_base_to_search.plus_file("common")

			var state_path: String = ""
			var transitions: Dictionary = {}

			for key in states.keys():
				state_path = path_state_to_search

				state_path =  Utility.find_exist_path_file(state_path, path_state_base_to_search, "%s.gd" % key)

				var state_instance: StatePhysic = Utility.load_script(state_path).new() as StatePhysic

				if state_instance:
					state_instance.set("name", key)
					state_instance._PHYSIC_CONST = physics
					sm.set_state(key, state_instance)

					transitions[key] = states[key]
				else:
					Logger.msg_error("State: %s not found..." % state_path, Logger.CATEGORY_FILE_LOAD)

			sm.set_target(self)
			sm.set_transitions(transitions)
			sm.transition_initial(state_initial)
		else:
			push_error("No states found: %s" % path_definition_absolute)
			breakpoint
	else:
		Logger.msg_error("Definition: %s not found..." % path_definition_absolute, Logger.CATEGORY_FILE_LOAD)

func destroy(_args: Array = []) -> void:
	push_error("Not implement")
	breakpoint

func get_size(animation: String = "") -> Vector2:
	if image:
		if animation.empty():
			animation = image.animation
		return image.frames.get_frame(animation, 0).get_size()

	return Vector2(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE)

func handle_input() -> void:
	if not sm.active:
		return

	for action in funcrefs_handle_input:
		if action is FuncRef and action.is_valid():
			action.call_func()

func make_collision_and_detector(info: DataInfo) -> void:
	var collision_made: bool = false
	var detector_made: bool = false

	if not info.get_metadata().empty():
		var metadata: Dictionary = info.get_metadata()

		if metadata.has("collision"):
			var collision_values: Dictionary = metadata["collision"] as Dictionary

			if collision_values:
				var extents: Vector2 = collision_values["extents"] as Vector2
				var position: Vector2 = collision_values["position"] as Vector2

				collision.position = position
				var rect_collision: RectangleShape2D = RectangleShape2D.new()
				rect_collision.extents = extents
				collision.shape = rect_collision

				collision_made = true

		if metadata.has("detector"):
			var detector: Dictionary = metadata["detector"] as Dictionary

			if detector:
				var position_down: Vector2 = detector.positions["down"] as Vector2
				var size_down: float = float(detector.sizes["down"])
				var position_up: Vector2 = detector.positions["up"] as Vector2
				var size_up: float = float(detector.sizes["up"])
				var position_look_to: Vector2 = detector.positions["look_to"] as Vector2
				var size_look_to: float = float(detector.sizes["look_to"])

				raycast_down.cast_to = Vector2(fix_detectors, size_down)
				raycast_down.position = position_down
				raycast_up.cast_to = Vector2(fix_detectors, -size_up)
				raycast_up.position = position_up + (Vector2.UP * 2)
				raycast_look_to.cast_to = Vector2(size_look_to, 0)
				raycast_look_to.position = position_look_to + (Vector2.RIGHT * 2)
				raycast_look_to_back.cast_to = Vector2(-size_look_to, 0)
				raycast_look_to_back.position = -(position_look_to + (Vector2.RIGHT * 2))

				detector_made = true

	if not collision_made:
		make_collision_manual()

	if not detector_made:
		make_detector_manual()

	var size: Vector2 = get_size()
	visibility_notifier.call_deferred("set", "rect", Rect2(size.x / 2, size.y / 2, size.x, size.y))

func make_collision_manual() -> void:
	var current_animation: String = "idle" if image.frames.has_animation("idle") else image.animation
	var size: Vector2 = get_size(current_animation)
	var extents: Vector2 = Vector2(size.x / 2.5, size.y / 2)
	var rect_collision: RectangleShape2D = RectangleShape2D.new()
	rect_collision.extents = extents
	collision.shape = rect_collision

func make_detector_manual() -> void:
	var current_animation: String = "idle" if image.frames.has_animation("idle") else image.animation
	var size: Vector2 = get_size(current_animation)

	raycast_down.position.y = size.y / 2
	raycast_up.position.y = -size.y / 2
	raycast_look_to.position.x = size.x / 2.5
	raycast_look_to_back.position.x = -size.x / 2.5


func _set_invincible(new_value: bool) -> void:
	is_invincible = new_value

	if new_value:
		invincible_timer.start()

func _set_invincible_time(new_value: float) -> void:
	invincible_time = new_value

	if not invincible_timer.is_stopped():
		yield(invincible_timer, "timeout")

	invincible_timer.wait_time = new_value

func _get_invincible() -> bool:
	return is_invincible

func _get_invincible_time() -> float:
	return invincible_time
