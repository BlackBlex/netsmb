class_name Projectile
extends KinematicBody2D

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: ProjectileInfo = null
var input: InputManagement = null
var gravity: float = 300
var max_velocity: float = 150
var velocity: Vector2 = Vector2.ZERO
var image: AnimatedSprite = null

var _who_shotted: Entity = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var collision: CollisionShape2D = $Collision
onready var position_pivot: Position2D = $Position
onready var visibility_notifier: VisibilityNotifier2D = $VisibilityNotifier
onready var area_hit: Area2D = $AreaHit
onready var collision_hit: CollisionShape2D = $AreaHit/Collision

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	set_process(false)

	input = InputManagement.new()

	image = info.get_image()
	info.place(position_pivot)

	if not GlobalData.is_on_level_editor and not info.get_metadata().empty():
		if info.get_metadata().has("name"):
			name = info.get_metadata()["name"]
			add_to_group(name)

func _process(delta: float) -> void:
	if not input:
		return

	if input.direction.x != 0:
		position_pivot.scale.x = input.direction.x

	velocity.x = lerp(velocity.x, input.direction.x * max_velocity, 2)

	# Apply gravity
	velocity.y += gravity * delta

	velocity = move_and_slide(velocity, Vector2.UP)

	if is_on_floor():
		velocity.y = -gravity / 2

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_area_hit_body_entered(_body: Node) -> void:
	return
#	if body is Entity:
#		if info.can_harm_himself and body == _who_shotted:
#			if body is Player:
#				if info.instant_kill:
#					body.powerup_current = Enums.PowerupType.DIE
#				else:
#					body.powerup_current = Enums.PowerupType.DAMAGE
#			else:
#				body.destroy()
#		elif info.can_harm_other_players and body is Player and body != _who_shotted:
#			if info.instant_kill:
#				body.powerup_current = Enums.PowerupType.DIE
#			else:
#				body.powerup_current = Enums.PowerupType.DAMAGE
#	elif info.can_destroy_block and body is Block:
#		body.break_block(self)
#
#	queue_free()

func _on_visibility_notifier_screen_exited() -> void:
	queue_free()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func set_direction(direction: Vector2) -> void:
	input.direction = direction
	set_process(true)

func shot(who_shotted: Entity) -> void:
	_who_shotted = who_shotted
