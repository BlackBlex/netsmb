extends Node2D

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const FIX_LAYER: int = -40

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
# Dictionary of int with bool
var _layer_visible: Dictionary = {}
# Dictionary of int with Array of Entity
var _register_entities: Dictionary = {}
# Dictionary of int with Tilemap
var _maps: Dictionary = {}
var _map_collision_layer: int = 0
var _map_collision_mask: int = 0

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var blocks: Node2D = $Blocks
onready var enemies: Node2D = $Enemies
onready var npcs: Node2D = $Npcs
onready var powerups: Node2D = $Powerups
onready var layer_one: TileMap = $Layer1

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	add_to_group(name)
	_register_entities[1] = []
	_layer_visible[1] = true
	_maps[1] = layer_one

	layer_one.z_index += FIX_LAYER
	_map_collision_layer = layer_one.collision_layer
	_map_collision_mask = layer_one.collision_mask

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func add_block(layer: int, block: Block, visible = null) -> void:
	check_layer(layer, false)
	_register_entities[layer].append(block)

	block.z_index = layer + FIX_LAYER
	block.layer_id = layer
	# block.z_as_relative = false
	blocks.add_child(block, true)
	if visible != null and typeof(visible) == TYPE_BOOL:
		block.set_visible(visible)
	else:
		block.set_visible(_layer_visible[layer])

func add_enemy(layer: int, enemy: Enemy, visible = null) -> void:
	check_layer(layer, false)
	_register_entities[layer].append(enemy)

	enemy.z_index = layer + FIX_LAYER
	enemy.layer_id = layer
	# enemy.z_as_relative = false
	enemies.add_child(enemy, true)
	if visible != null and typeof(visible) == TYPE_BOOL:
		enemy.set_visible(visible)
	else:
		enemy.set_visible(_layer_visible[layer])

func add_powerup(layer: int, powerup: Powerup, visible = null) -> void:
	check_layer(layer, false)
	_register_entities[layer].append(powerup)

	powerup.z_index = layer + FIX_LAYER
	powerup.layer_id = layer
	# powerup.z_as_relative = false
	powerups.add_child(powerup, true)
	if visible != null and typeof(visible) == TYPE_BOOL:
		powerup.set_visible(visible)
	else:
		powerup.set_visible(_layer_visible[layer])

func add_npc(layer: int, npc: Npc, visible = null) -> void:
	check_layer(layer, false)
	_register_entities[layer].append(npc)

	npc.z_index = layer + FIX_LAYER
	npc.layer_id = layer
	# npc.z_as_relative = false
	npcs.add_child(npc, true)
	if visible != null and typeof(visible) == TYPE_BOOL:
		npc.set_visible(visible)
	else:
		npc.set_visible(_layer_visible[layer])

func check_layer(layer: int, is_tile: bool = true) -> void:
	var is_new: bool = false

	if is_tile:
		if not _maps.has(layer):
			_maps[layer] = _build_map(layer)
			is_new = true
	elif not _register_entities.has(layer):
		_register_entities[layer] = []
		is_new = true

	if is_new:
		_layer_visible[layer] = false

func clean() -> void:
	for layer in _maps:
		_maps[layer].clear()

	_maps.clear()
	_maps[1] = layer_one

	NodeExt.clear_children(layer_one)
	NodeExt.clear_children(blocks)
	NodeExt.clear_children(npcs)
	NodeExt.clear_children(enemies)
	NodeExt.clear_children(powerups)

func editor_place(layer: int, data: DataInfo) -> void:
	check_layer(layer, false)
	_register_entities[layer].append(data)
	data.place(_maps[layer])
	data.get_image().z_index = layer + FIX_LAYER
	data.get_image().z_as_relative = false
	data.set_visible(_layer_visible[layer])

func editor_remove(layer: int, data: DataInfo) -> void:
	if _register_entities[layer].has(data):
		_register_entities[layer].erase(data)

func get_entities_of_layer(layer: int)-> Array:
	if _register_entities.has(layer):
		return Arrays.of_type_object(_register_entities[layer], Entity)

	return []

func get_entities_by_category(category: String) -> Dictionary:
	var to_result: Dictionary = {}

	for layer in _register_entities:
		to_result[layer] = []

		for item in _register_entities[layer]:
			if typeof(item) == TYPE_OBJECT:
				if item.get("info"):
					if category in item.info.category:
						to_result[layer].append(item)

	return to_result

func get_entities_by_subcategory(subcategory: String) -> Dictionary:
	var to_result: Dictionary = {}

	for layer in _register_entities:
		to_result[layer] = []

		for item in _register_entities[layer]:
			if typeof(item) == TYPE_OBJECT:
				if item.get("info"):
					if subcategory in item.info.subcategory:
						to_result[layer].append(item)

	return to_result

func is_layer_visible(layer: int) -> bool:
	if _register_entities.has(layer) or _maps.has(layer):
		return _layer_visible[layer]
	return false

func is_in_level_editor() -> void:
	layer_one.collision_mask = 0
	layer_one.collision_layer = 0
	_map_collision_layer = 0
	_map_collision_mask = 0

func remove_layer(layer: int) -> bool:
	var to_result: bool = false

	if _register_entities.has(layer):
		for item in _register_entities[layer]:
			# its block, enemy, powerup, npc
			if typeof(item) == TYPE_OBJECT:
				if is_instance_valid(item):
					item.queue_free()

		_register_entities[layer].clear()

		to_result = _register_entities.erase(layer) and _layer_visible.erase(layer)
	if _maps.has(layer):
		var map_individual: TileMap = _maps[layer]
		map_individual.queue_free()
		to_result = _maps.erase(layer)

	return to_result

func remove_tile(layer: int, position: Vector2) -> void:
	check_layer(layer)

	var map_individual: TileMap = _maps[layer]
	map_individual.set_cell(int(position.x), int(position.y), -1)
	map_individual.update_bitmask_area(position)

func set_layer_visible(layer: int, value: bool) -> void:
	_layer_visible[layer] = value

	if _register_entities.has(layer):
		var entities: Array = _register_entities[layer]
		for entitiy in entities:
			if is_instance_valid(entitiy):
				entitiy.set_visible(value)

				if entitiy.has_method("disable") and entitiy.has_method("enable"):
					if not value:
						if entitiy is Block:
							entitiy.call_deferred("disable", true)
						else:
							entitiy.call_deferred("disable")
					else:
						if entitiy is Block:
							entitiy.call_deferred("enable", true)
						else:
							entitiy.call_deferred("enable")
			else:
				_register_entities[layer].erase(entitiy)

	if _maps.has(layer):
		var map_individual: TileMap = _maps[layer]
		map_individual.visible = value
		if not value:
			map_individual.collision_layer = 0
			map_individual.collision_mask = 0
		else:
			map_individual.collision_layer = _map_collision_layer
			map_individual.collision_mask = _map_collision_mask

func set_tile(layer: int, tile_id: int, position: Vector2) -> void:
	check_layer(layer)

	var map_individual: TileMap = _maps[layer]
	map_individual.set_cell(int(position.x), int(position.y), tile_id)
	map_individual.update_bitmask_area(position)

func _build_map(layer: int) -> TileMap:
	var tilemap: TileMap = TileMap.new()
	tilemap.cell_size = layer_one.cell_size
	tilemap.cell_custom_transform = layer_one.cell_custom_transform
	tilemap.collision_use_kinematic = layer_one.collision_use_kinematic
	tilemap.collision_friction = tilemap.collision_friction
	tilemap.collision_layer = _map_collision_layer
	tilemap.collision_mask = _map_collision_mask
	tilemap.tile_set = layer_one.tile_set
	tilemap.z_index = layer + FIX_LAYER
	tilemap.name = 'Layer%d' % layer
	add_child(tilemap, true)

	return tilemap
