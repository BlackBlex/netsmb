class_name LevelRoot
extends Node2D

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var info: LevelData = null

var _grabbed_datainfo: Color = Color(0.03, 0.04, 0.78, 0.5)
var _level_origin: Vector2 = Vector2.ZERO
var _level_size: Vector2 = Vector2.ZERO
var _player: Player = null
var _initial_msec: int = 0
var _limit_left: int = 0
var _limit_right: int = 0
var _limit_top: int = 0
var _limit_bottom: int = 0

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var scripts: Node = $Scripts

onready var _timer: Timer = $Timer
onready var _background: TextureRect = $CanvasBottom/Background
onready var _layer_node: Node2D = $Layers

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	Event.publish(self, A.EventList.level.initialized, [self])

func _process(_delta: float) -> void:
	if is_instance_valid(_player):
		_check_borders(_player)

	for script_node in scripts.get_children():
		script_node.get_script().execute()

func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("action_esc") and GlobalData.is_on_test_level:
		_return_to_editor()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_player_died() -> void:
	if GlobalData.is_on_test_level:
		_return_to_editor(int(FAILED))
	else:
		var _err = get_tree().reload_current_scene()
		Audio.play_music(info.music)
		Audio.clean_effects()

func _on_pswitch_started() -> void:
	print("PSwitch started")
	_initial_msec = OS.get_ticks_msec()

func _on_pswitch_ended() -> void:
	print("PSwitch ended in: ", int(OS.get_ticks_msec() - _initial_msec), " msec")

func _on_timer_timeout() -> void:
	Event.publish(self, A.EventList.level.finished, [ERR_TIMEOUT])

func _on_delay_timeout() -> void:
	_load_level()

	set_process(false)
	set_process_input(false)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func set_background(background: Texture) -> void:
	_background.texture = background

func start() -> void:
	OS.window_size = GlobalData.map_size_render
	OS.center_window()
	Event.publish(self, A.EventList.level.started, [self])
	var vol_temp = Audio.volume_general
	Audio.volume_general = 0
	yield(get_tree().create_timer(0.5), "timeout")
	Audio.player.play()
	Audio.volume_general = vol_temp

	yield(get_tree().create_timer(0.5), "timeout")
	_timer.start()

	set_process(true)
	set_process_input(true)
	get_tree().call_group("npc", "activate_sm")


func _create_borders() -> void:
	var left: BlockDead = Resources.BLOCK_DEAD.instance() as BlockDead
	var right: BlockDead = Resources.BLOCK_DEAD.instance() as BlockDead

	left.name = "BlockDeadLeft"
	right.name = "BlockDeadRight"
	add_child(left)
	add_child(right)

	left.make_collision(Vector2(GlobalData.BLOCK_SIZE / 2, _limit_bottom / 2))
	right.make_collision(Vector2(GlobalData.BLOCK_SIZE / 2, _limit_bottom / 2))

	left.position = Vector2(_limit_left - (GlobalData.BLOCK_SIZE / 2), _limit_bottom / 2)
	right.position = Vector2(_limit_right + (GlobalData.BLOCK_SIZE / 2), _limit_bottom / 2)

func _check_borders(entity: Entity) -> void:
	if GlobalData.current_level.connect_sides_vertically:
		if entity.position.y > _limit_bottom:
			entity.position = Vector2(entity.position.x, 0)
			entity.velocity = Vector2.ZERO

	if GlobalData.current_level.connect_sides_horizontally:
		entity.position.x = wrapf(entity.position.x, _limit_left, _limit_right)

func _load_events() -> void:
	for event in info.events:
		event += ".gd"
		var path_event: String = A.Paths.events.plus_file(event)

		if "level:" in event:
			event = event.replace("level:")
			path_event = A.Paths.level_events.plus_file(event)

		var script_ins: BaseEvent = Utility.load_script(path_event).new() as BaseEvent
		var attach = script_ins.attach()

		script_ins.set("_OWNER", self)

		if typeof(attach) == TYPE_STRING:
			if attach.empty():
				var node_new: Node = Node.new()
				node_new.name = script_ins.name
				node_new.set_script(script_ins)
				scripts.add_child(node_new)
				script_ins.setup()
			else:
				script_ins.setup()
				Event.subscribe(attach, script_ins, "execute")
		elif typeof(attach) == TYPE_DICTIONARY:
			if not attach.empty():
				var event_name: String = ""
				if attach.has("event"):
					event_name = attach.event

				if not event_name.empty():
					pass
		else:
			Logger.msg_error("Event %s not recognize attach" % script_ins.name)

func _load_level() -> void:
	var level_time: Array = get_int("level_time")

	if level_time[0] == OK:
		_timer.wait_time = level_time[1]
	else:
		_timer.wait_time = GlobalData.LEVEL_TIME

	var level_size = get_vector2("level_size")
	var level_origin = get_vector2("level_origin")

	if level_origin[0] == OK:
		_level_origin = level_origin[1]
	else:
		push_error("Error level origin not found")
		breakpoint

	if level_size[0] == OK:
		_level_size = level_size[1]

		var level_size_fix: Vector2 = Utility.convert_position_to_real(_level_size)
		_background.rect_size = Utility.convert_position_to_real(_level_size)
		_background.rect_min_size = Utility.convert_position_to_real(_level_size)

		_limit_left = 0
		_limit_top = 0
		_limit_right = int(level_size_fix.x)
		_limit_bottom = int(level_size_fix.y)
	else:
		push_error("Error level size not found")
		breakpoint

	if not GlobalData.current_level.connect_sides_horizontally:
		_create_borders()

	_load_tilesets()

	if info and _level_origin != Vector2.ZERO:
		var tiles_to_load: Dictionary = info.tiles

		for tile_id in tiles_to_load:
			for layer in tiles_to_load[tile_id]:
				for tile_position in tiles_to_load[tile_id][layer]:
					_layer_node.set_tile(layer, tile_id, tile_position)

		var data_to_load = info.data
		var data_grabbed = info.grabbed
		var data_deleted = info.delete

		for layer in data_to_load.keys():
			for data_position in data_to_load[layer].keys():
				if data_deleted.has(data_position):
					continue
				if data_grabbed.has(data_position):
					data_to_load[layer][data_position].get_image().modulate = _grabbed_datainfo

				var position_normalized = Utility.convert_position_to_real(data_position)

				position_normalized += Vector2(16, 16)

				if position_normalized.x < _limit_left:
					_limit_left = position_normalized.x
				if position_normalized.x > _limit_right:
					_limit_right = position_normalized.x
				if position_normalized.y < _limit_top:
					_limit_top = position_normalized.y
				if position_normalized.y > _limit_bottom:
					_limit_bottom = position_normalized.y

				var new_data = data_to_load[layer][data_position].clone(true)

				match (new_data.type):
					Enums.DataInfoType.BLOCK:
						var block: Block = Resources.BLOCK.instance() as Block
						block.info = new_data as BlockInfo
						block.position = position_normalized
						block.name = "%s%s" % [block.name, position_normalized]
						_layer_node.add_block(layer, block)
					Enums.DataInfoType.BLOCK_REWARD:
						var block_reward: Block = Resources.BLOCK.instance() as Block
						block_reward.name = "BlockReward"
						block_reward.info = new_data as BlockRewardInfo
						block_reward.position = position_normalized
						block_reward.name = "%s%s" % [block_reward.name, position_normalized]
						_layer_node.add_block(layer, block_reward)
					Enums.DataInfoType.ENEMY:
						var enemy: Enemy = Resources.ENEMY.instance() as Enemy
						enemy.info = new_data as EnemyInfo
						enemy.position = position_normalized
						_layer_node.add_enemy(layer, enemy)
					Enums.DataInfoType.OBJECT:
						var object: ObjectNpc = Resources.OBJECT_NPC.instance() as ObjectNpc
						object.info = new_data as ObjectInfo
						object.position = position_normalized
						_layer_node.add_npc(layer, object)
					Enums.DataInfoType.POWERUP:
						var powerup: Powerup = Resources.POWERUP.instance() as Powerup
						powerup.info = new_data as PowerupInfo
						powerup.position = position_normalized
						_layer_node.add_powerup(layer, powerup)
					Enums.DataInfoType.ACTIVATABLE:
						var activatable: Activatable = Resources.ACTIVATABLE.instance() as Activatable
						activatable.info = new_data as ActivatableInfo
						activatable.position = position_normalized
						_layer_node.add_npc(layer, activatable)
					Enums.DataInfoType.PLAYER:
						_player = Resources.PLAYER.instance() as Player
						_player.info = new_data as PlayerInfo
						_player.position = position_normalized

		Event.publish(self, A.EventList.level.loaded, [self])

		if _player:
			# get_tree().get_nodes_in_group("players")[0].add_child(_player)
			add_child(_player)
			_player.make_collision_and_detector(_player.info)
			_player.sm.active = true
			_player.camera.limit_left = _limit_left
			_player.camera.limit_top = _limit_top
			_player.camera.limit_right = _limit_right
			_player.camera.limit_bottom = _limit_bottom
			GlobalData.player_camera = _player.camera


		var layers: Dictionary = info.layers

		for layer in layers:
			_layer_node.set_layer_visible(layer, layers[layer][1])

		_load_events()

		Event.subscribe(A.EventList.pswitch.started, self, "_on_pswitch_started")
		Event.subscribe(A.EventList.pswitch.ended, self, "_on_pswitch_ended")
		Event.subscribe(A.EventList.player.died, self, "_on_player_died")

func _load_tilesets() -> void:
	var tile_set: TileSet = Resources.get_image("blocks/ground/floor") as TileSet

	if tile_set:
		_layer_node.layer_one.tile_set = tile_set
	else:
		Logger.msg_fatal("Floor could not be loaded, please verify its integrity", Logger.CATEGORY_SYSTEM)
		Alert.show_error(tr("KEY_CORRUPT_FILE") % "floor")

func _return_to_editor(exit_type: int = int(ERR_SKIP)):
	Audio.play_music(info.music)
	Audio.clean_effects()
	#if exit_type != FAILED:
	Event.publish(self, A.EventList.level.finished, [exit_type])

	var level_editor = get_tree().get_nodes_in_group("editor")[0]

	if level_editor:
		visible = false
		#OS.window_maximized = true
		OS.window_size = OS.get_screen_size()
		OS.center_window()
		level_editor.visible = true
		get_tree().current_scene = level_editor

	queue_free()

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPOSED FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_layer() -> Node2D:
	return _layer_node

func get_layer_id(name: String) -> int:
	for layer in info.layers:
		if name in info.layers[layer]:
			return layer

	return -1

func get_left_time(hud: bool = true) -> int:
	if hud:
		return int(_timer.time_left)
	else:
		return int(_timer.wait_time)

func get_player_powerup_current_variant() -> String:
	return _player.powerup_current_variant

func get_player_powerup_reserved_variant() -> String:
	return _player.powerup_reserved_variant

func get_player() -> Player:
	return _player

func get_player_info() -> PlayerInfo:
	return _player.info

#return Error and int
func get_int(key: String) -> Array:
	var error: int = OK
	var to_result: int = -1

	if not GlobalData.level_values[info.identifier].has(key):
		error = ERR_UNAVAILABLE
	else:
		var result: int = int(GlobalData.level_values[info.identifier][key])

		if result:
			to_result = result
		else:
			error = ERR_PARSE_ERROR

	return [error, to_result]

#return Error and vector2
func get_vector2(key: String) -> Array:
	var error: int = OK
	var to_result: Vector2 = Vector2.INF

	if not GlobalData.level_values[info.identifier].has(key):
		error = ERR_UNAVAILABLE
	else:
		var result: Vector2 = GlobalData.level_values[info.identifier][key] as Vector2

		if result:
			to_result = result
		else:
			error = ERR_PARSE_ERROR

	return [error, to_result]

#return Error and int
func get_int_local(key: String) -> Array:
	var error: int = OK
	var to_result: int = -1

	if not info.values.has(key):
		error = ERR_UNAVAILABLE
	else:
		var result: int = int(info.values[key])

		if result:
			to_result = result
		else:
			error = ERR_PARSE_ERROR

	return [error, to_result]

#return Error and vector2
func get_vector2_local(key: String) -> Array:
	var error: int = OK
	var to_result: Vector2 = Vector2.INF

	if not info.values.has(key):
		error = ERR_UNAVAILABLE
	else:
		var result: Vector2 = info.values[key] as Vector2

		if result:
			to_result = result
		else:
			error = ERR_PARSE_ERROR

	return [error, to_result]

#return Error
func set_value(key: String, value, override: bool = false) -> int:
	var to_result: int = OK

	if GlobalData.level_values[info.identifier].has(key) and not override:
		to_result = ERR_ALREADY_EXISTS
	else:
		GlobalData.level_values[info.identifier][key] = value

	return to_result

#return Error.
func set_value_local(key: String, value, override: bool = false) -> int:
	var to_result: int = OK

	if info.values.has(key) and not override:
		to_result = ERR_ALREADY_EXISTS
	else:
		info.values[key] = value

	return to_result
