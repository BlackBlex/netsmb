#https://github.com/RichardEllicott/GodotSnippets
class_name Arrays

static func append_all(destination: Array, source: Array) -> void:
	for item in source:
		destination.append(item)

"""
builds a nested array of nulls (backwards like python array)
to access the array use:
array[y][x]
array[row][col]
the snippet builds the array by using resize for speed
"""
static func build_2D_array(x: int, y: int) -> Array:
	var to_result: Array = []
	to_result.resize(y) # resize should be faster than appending several times
	for i in y:
		var row: Array = []
		row.resize(x)
		to_result[i] = row

	return to_result

static func has_all(source: Array, items: Array) -> bool:
	for item in items:
		if not source.has(item):
			return false

	return true

static func has_any(source: Array, items: Array) -> bool:
	for item in items:
		if source.has(item):
			return true

	return false

static func intersection(source: Array, source_other: Array) -> Array:
	var to_result: Array = []
	for item in source_other:
		if not source.has(item):
			to_result.append(item)

	return to_result

#type = See TYPE_ constants
static func of_type(array: Array, type: int) -> Array:
	var to_result: Array = []

	if type:
		for obj in array:
			if typeof(obj) == type:
				to_result.append(obj)

	return to_result

#type = class_name
static func of_type_object(array: Array, type: GDScript) -> Array:
	var to_result: Array = []

	if type:
		for obj in array:
			if obj is type:
				to_result.append(obj)

	return to_result

#type = class_name
static func of_type_native(array: Array, type: GDScriptNativeClass) -> Array:
	var to_result: Array = []

	if type:
		for obj in array:
			if obj.get_class() == type.get_class():
				to_result.append(obj)

	return to_result

static func random_choice(array: Array, _seed = null):
	var rand_generator: RandomNumberGenerator = null
	if _seed:
		rand_generator = RandomNumberGenerator.new()
		rand_generator.set_seed(_seed)

	var position_rand: int #random integer

	if not _seed: # if no seed was provided use default random
		position_rand = randi()
	else:
		position_rand = rand_generator.randi()

	return array[position_rand % array.size()]

"""
removes the nulls from an array
like:
	[1,2,null,3,null,'foo', null, 'bar'] => [1,2,3,'foo','bar']
the idea of this is the array is only resized a few times after searching and deleting by placing nulls
similar to how lua uses None, which introduces a hole within luas table system (but lua is fast)
This is useful as a dynamic programming pattern, the idea is to prevent resizing arrays too often under the hood
In this pattern setting null is equivalent to deleting a record in a database for example (but the hole is still there)
# EXAMPLE 1
print(remove_nulls([1,2,null,3,null,'foo', null, 'bar'])) # [1, 2, 3, foo, bar]
# EXAMPLE 2
print(remove_match([1,-1,2,3,-1,4,-1,5],-1)) # [1, 2, 3, 4, 5]
"""
static func remove_nulls(array: Array) -> Array:
	# returns a new list with all the nulls removed
	# i.e [1,2,null,3,null,4] => [1,2,3,4]
	# for usage of lua nulls style pattern
	# the array is only resized a few times with this method for better performance

	var to_result: Array = [] # create a return array
	to_result.resize(array.size()) # allocate maximum possible size in case no nulls
	var current_reference: int = 0 # current reference
	var value = null

	for i in range(array.size()):
		value = array[i]
		if value:
			to_result[current_reference] = value # set the data by reference
			current_reference += 1

	to_result.resize(current_reference) # chop array to final size
	return to_result

# an example of using this pattern in another function, that could remove matches from an array
static func remove_match(array: Array, _match = -1) -> Array:
	# example of using remove_nulls pattern
	# i.e removing the -1's results in:
	# [62,-1,33,-1] => [62,33]
	# the array is only resized a few times with this method for better performance

	var to_result: Array = array.duplicate() # copy the array in one go

	for i in range(to_result.size()):
		if to_result[i] == _match:
			to_result[i] = null # set null instead of deleting each time of rebuilding a new array step by step

	to_result = remove_nulls(to_result) # strip the nulls with the null stripping function
	return to_result

"""
shuffle an array similar to python's shuffle
there is already a built in shuffle in Godot, but I've kept this for seeded shuffles
this function does not affect the seed of the RandomNumberGenerator as it creates it's own one
"""
#seed = int
static func shuffle_array(array: Array, _seed = null) -> Array:
	# returns a shuffled copy of an array
	# if _seed is not null
	# will use a seed for procedural results

	array = array.duplicate()
	var rand_generator: RandomNumberGenerator = null
	if _seed:
		rand_generator = RandomNumberGenerator.new()
		rand_generator.set_seed(_seed)

	var shuffled_array: Array = [] # return this
	shuffled_array.resize(array.size()) # allocate correct length

	var position_rand: int #random integer
	var position_array: int #random position
	for i in range(array.size()):
		if not _seed: # if no seed was provided use default random
			position_rand = randi()
		else:
			position_rand = rand_generator.randi()

		position_array = position_rand % array.size()

		shuffled_array[i] = array[position_array]

		array.remove(position_array) # a bit slower maybe

	return shuffled_array

class NumberSorter:
	static func sort_ascending(a, b):
		return int(a) < int(b)
	static func sort_descending(a, b):
		return int(a) > int(b)
