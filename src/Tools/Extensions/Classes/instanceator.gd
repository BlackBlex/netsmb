class_name Instanceator

static func create_state_machine(target: Object = null, current_state_name: String = "", states: Dictionary = {}, transitions: Dictionary = {}) -> StateMachineCore:
	var to_result: StateMachineCore = StateMachineCore.new()

	if not states.empty():
		to_result.set_states(states)

	if target:
		to_result.set_target(target)

	if not transitions.empty():
		to_result.set_transitions(transitions)

	if not current_state_name.empty():
		to_result.set_state_current(current_state_name)

	return to_result

static func make_funcref_ext(instance: Object, func_name: String, args = null) -> FuncRefExt:
	var new_ref: FuncRefExt = FuncRefExt.new()
	new_ref.function = func_name
	new_ref.default_args = args
	new_ref.set_instance(instance)

	return new_ref

static func parse_version(version: String) -> Version:
	var to_result: Version = Version.new()
	return to_result.init_string(version)
