class_name Maths

static func sin_value(x: float, amplitude: float = 1, period: float = 1, offset_horizontal: float = 0, offset_vertical: float = 0) -> float:
	return amplitude * sin(period * (x + offset_horizontal)) + offset_vertical

static func sin_values(range_func: PoolRealArray = [-1, 1, 0.1], amplitude: float = 1, period: float = 1, offset_horizontal: float = 0, offset_vertical: float = 0) -> Dictionary:
	var to_result: Dictionary = {}

	if range_func.size() < 2:
		Logger.msg_error("Maths.sin_values expected two numbers in range_func", Logger.CATEGORY_MATH)
		return to_result

	var min_value: float = 0
	var max_value: float = 0
	var step: float = 0.5

	if range_func.size() == 2:
		min_value = range_func[0]
		max_value = range_func[1]
	elif range_func.size() == 3:
		min_value = range_func[0]
		max_value = range_func[1]
		step = range_func[2]

	var count_values_max: float = max_value / step
	var count_values_min: float = abs(min_value) / step

	var rang: PoolRealArray = []

	var result: float = 0
	for i in range(count_values_min):
		result = min_value + (i * step)
		rang.append(result)

	rang.append(0)

	for i in range(1, count_values_max + 1):
		result = max_value - ((count_values_max - i) * step)
		rang.append(result)

	for x in rang:
		var result_to = sin_value(x, amplitude, period, offset_horizontal, offset_vertical)
		to_result[x] = result_to

	return to_result
