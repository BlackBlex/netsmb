class_name EntityExt

static func is_enemy(entity: Entity) -> bool:
	if entity:
		return entity.info.category == "enemies"

	return false

static func is_shell(entity: Entity) -> bool:
	if entity:
		return entity.info.subcategory == "shell"

	return false

static func is_koopa_troopa(entity: Entity) -> bool:
	if entity:
		return entity.info.subcategory == "koopa_troopa"

	return false

static func emit_particle(who_call: Entity, particle: String, color: String = "#fff", lifetime: float = 1, one_shot: bool = true) -> void:
	var path: String = "effects/particles/%s" % particle

	var texture: Texture = Resources.get_image(path) as Texture

	if texture:
		var particle_instance = ParticleEmitter.get_particle(who_call.position)

		Event.publish(ParticleEmitter, A.EventList.particle.created, [who_call, particle_instance])

		particle_instance.set_who_call(who_call)
		particle_instance.texture = texture

		if color.nocasecmp_to("#fff") != 0:
			particle_instance.modulate = Color(color)

		particle_instance.lifetime = lifetime
		particle_instance.one_shot = one_shot
		particle_instance.emitting = true
		particle_instance.set_emit(true)
	else:
		Logger.msg_warn("Can't load to texture: %s" % path, Logger.CATEGORY_ASSET)


static func effects(parent: Node2D) -> EffectsExecuter:
	var effect: EffectsExecuter = EffectsExecuter.new()
	effect.setup(parent)
	return effect

static func animator(parent: Node2D) -> AnimatorExecuter:
	var animation: AnimatorExecuter = AnimatorExecuter.new()
	animation.setup(parent)
	return animation
