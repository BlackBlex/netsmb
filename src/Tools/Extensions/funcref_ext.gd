class_name FuncRefExt
extends FuncRef

# Any data
var default_args
var ignore_args: bool = false

func call_func(args = null):
	if is_valid():
		if not ignore_args:
			var builded_args: Array = []
			if default_args != null:
				if typeof(default_args) == TYPE_ARRAY:
					builded_args.append_array(default_args)
				else:
					builded_args.append(default_args)

			if args != null:
				if typeof(args) == TYPE_ARRAY:
					builded_args.append_array(args)
				else:
					builded_args.append(args)
			return .call_funcv(builded_args)
		else:
			return .call_func()

func set_instance(instance: Object) -> void:
	.set_instance(instance)

	for method_info in instance.get_method_list():
		if function in method_info["name"]:
			var parameters: Array = method_info["args"] as Array

			if parameters is Array:
				ignore_args = parameters.empty()
