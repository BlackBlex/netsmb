class_name NodeExt

static func clear_children(node: Node) -> void:
	for child in node.get_children():
		child.queue_free()
