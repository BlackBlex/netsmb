class_name OptionButtonExt

static func select(option_button: OptionButton, item) -> void:
	var items: Array = Arrays.of_type(option_button.items, TYPE_STRING)
	var id: int = items.find(item)
	var idx: int = option_button.get_item_id(id)
	option_button.select(idx)

static func select_component(option_component: OptionComponent, item) -> void:
	var items: Array = Arrays.of_type(option_component.items, TYPE_STRING)
	var id: int = items.find(item)
	option_component.selected = id
