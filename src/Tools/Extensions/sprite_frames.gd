class_name SpriteFramesExt

#frames: Array of texture
static func add_frames(sprite_frame: SpriteFrames, animation: String, frames: Array) -> void:
	var count: int = 0

	for frame in frames:
		sprite_frame.add_frame(animation, frame, count)
		count += 1

	sprite_frame.set_animation_loop(animation, (count + 1) > 1)

static func clear_complete(sprite_frame: SpriteFrames) -> void:
	var animations: Array = sprite_frame.get_animation_names()

	for animation in animations:
		sprite_frame.remove_animation(animation)

#return Array of texture
static func get_frames(sprite_frame: SpriteFrames, animation: String) -> Array:
	var to_result: Array = []
	var count: int = sprite_frame.get_frame_count(animation)

	for i in range(0, count):
		to_result.append(sprite_frame.get_frame(animation, i))

	return to_result
