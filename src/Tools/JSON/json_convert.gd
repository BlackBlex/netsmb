class_name JSONConvert

static func serialize(obj) -> String:
	var to_result: String = _serialize_data(obj).replace("'\"", "\"").replace("\"'", "\"").replace("'", "\"")

	var err = validate_json(to_result)

	if not err.empty():
		push_error("JSON not valid: " + err)
		breakpoint

	return to_result

static func deserialize(json: String):
	var json_parse: JSONParseResult = JSON.parse(json)

	if json_parse.error != OK:
		push_error("JSON not valid in line: " + str(json_parse.error_line) + " | " + json_parse.error_string)
		breakpoint

	return _deserialize_data(json_parse.result)


static func _serialize_data(obj) -> String:
	var to_result: String = ""

	if typeof(obj) >= TYPE_ARRAY:
		to_result = "["

		for value in obj:
			to_result += _serialize_data(value) + ","

		if to_result.length() > 2:
			to_result = to_result.substr(0, to_result.length() - 1)
		to_result += "]"
	elif typeof(obj) == TYPE_DICTIONARY:
		to_result = "{"

		for key in obj.keys():
			to_result += "'" + _serialize_data(key) + "':" + _serialize_data(obj[key]) + ","

		if to_result.length() > 2:
			to_result = to_result.substr(0, to_result.length() - 1)
		to_result += "}"
	elif obj is JSONObject:
		to_result = "{"

		var dic: Dictionary = obj.fetch_data()
		for key in dic.keys():
			to_result += '"' + key + '":' + _serialize_data(dic[key]) + ","

		if to_result.length() > 2:
			to_result = to_result.substr(0, to_result.length() - 1)
		to_result += "}"
	else:
		to_result += to_json(obj)

	return to_result

static func _deserialize_data(obj):
	var to_result = null

	if typeof(obj) >= TYPE_ARRAY:
		to_result = []
		for value in obj:
			to_result.append(_deserialize_data(value))
	elif typeof(obj) == TYPE_DICTIONARY:
		to_result = {}
		if obj.has(JSONObject.KEY_TYPE):
			to_result = load(A.get_path_of_class(obj[JSONObject.KEY_TYPE])).new()
			obj.erase(JSONObject.KEY_TYPE)

			for key in obj.keys():
				to_result.set(key, _deserialize_data(obj[key]))
		else:
			for key in obj.keys():
				to_result[_deserialize_data(key)] = _deserialize_data(obj[key])
	elif typeof(obj) == TYPE_STRING:
		if "(" in obj and ")" in obj and "," in obj: #Its a vector2?3
			if obj.count(",") == 3: # Its Rect2
				to_result = str2var("Rect2%s" % obj)
			elif obj.countn(",") == 2: #Its vector3
				to_result = str2var("Vector3%s" % obj)
			else:
				to_result = str2var("Vector2%s" % obj)
		else:
			to_result = str2var(obj)
	else:
		to_result = obj

	return to_result
