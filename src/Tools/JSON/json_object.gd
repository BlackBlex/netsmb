class_name JSONObject

const KEY_TYPE = "$@t"

# Normally put all the variables to be serialized, the ones you don't want to name them with an underscore

func get_class() -> String:
	return "JSONObject"

# //DEPRECATED: Remove in next release
func get_path() -> String:
	push_error("Not implement")
	breakpoint
	return ""

func fetch_data() -> Dictionary:
	var to_result: Dictionary = {KEY_TYPE: get_class()}

	var properties: Array = get_property_list()

	for property in properties:
		if property["name"].to_lower() in ["reference", "script", "script variables"] or property["name"].begins_with("_"):
			continue

		to_result[property["name"]] = get(property["name"])

	return to_result
