class_name PropertyInfo
extends Reference

static func category(name: String) -> Dictionary:
	return {
		"name": name,
		"type": TYPE_NIL,
		"usage": PROPERTY_USAGE_CATEGORY
	}

static func group(name: String) -> Dictionary:
	return {
		"name": name,
		"type": TYPE_NIL,
		"usage": PROPERTY_USAGE_GROUP
	}

static func var_array(name: String) -> Dictionary:
	return _build(name, TYPE_ARRAY)

static func var_array_raw(name: String) -> Dictionary:
	return _build(name, TYPE_RAW_ARRAY)

static func var_array_int(name: String) -> Dictionary:
	return _build(name, TYPE_INT_ARRAY)

static func var_array_float(name: String) -> Dictionary:
	return _build(name, TYPE_REAL_ARRAY)

static func var_array_string(name: String) -> Dictionary:
	return _build(name, TYPE_STRING_ARRAY)

static func var_array_vector2(name: String) -> Dictionary:
	return _build(name, TYPE_VECTOR2_ARRAY)

static func var_array_vector3(name: String) -> Dictionary:
	return _build(name, TYPE_VECTOR3_ARRAY)

static func var_array_color(name: String) -> Dictionary:
	return _build(name, TYPE_COLOR_ARRAY)

static func var_bool(name: String) -> Dictionary:
	return _build(name, TYPE_BOOL)

static func var_int(name: String) -> Dictionary:
	return _build(name, TYPE_INT)

static func var_float(name: String) -> Dictionary:
	return _build(name, TYPE_REAL)

static func var_string(name: String) -> Dictionary:
	return _build(name, TYPE_STRING)

static func var_vector2(name: String) -> Dictionary:
	return _build(name, TYPE_VECTOR2)

static func var_vector3(name: String) -> Dictionary:
	return _build(name, TYPE_VECTOR3)

static func var_rect2(name: String) -> Dictionary:
	return _build(name, TYPE_RECT2)

static func var_transform2d(name: String) -> Dictionary:
	return _build(name, TYPE_TRANSFORM2D)

static func var_plane(name: String) -> Dictionary:
	return _build(name, TYPE_PLANE)

static func var_quat(name: String) -> Dictionary:
	return _build(name, TYPE_QUAT)

static func var_aabb(name: String) -> Dictionary:
	return _build(name, TYPE_AABB)

static func var_basis(name: String) -> Dictionary:
	return _build(name, TYPE_BASIS)

static func var_transform(name: String) -> Dictionary:
	return _build(name, TYPE_TRANSFORM)

static func var_color(name: String) -> Dictionary:
	return _build(name, TYPE_COLOR)

static func var_node_path(name: String) -> Dictionary:
	return _build(name, TYPE_NODE_PATH)

static func var_rid(name: String) -> Dictionary:
	return _build(name, TYPE_RID)

static func var_object(name: String, classname: String) -> Dictionary:
	return _build(name, TYPE_OBJECT, classname, PROPERTY_HINT_RESOURCE_TYPE)

static func var_dictionarty(name: String) -> Dictionary:
	return _build(name, TYPE_DICTIONARY)

static func var_range_int(name: String, _min: int, _max: int, _step: int = 0, greater: bool = false, lesser: bool = false) -> Dictionary:
	return _build_range(TYPE_INT, PROPERTY_HINT_RANGE, name, _min, _max, _step, greater, lesser)

static func var_range_float(name: String, _min: float, _max: float, _step: float = 0, greater: bool = false, lesser: bool = false) -> Dictionary:
	return _build_range(TYPE_REAL, PROPERTY_HINT_RANGE, name, _min, _max, _step, greater, lesser)

static func var_range_int_exp(name: String, _min: int, _max: int, _step: int = 0, greater: bool = false, lesser: bool = false) -> Dictionary:
	return _build_range(TYPE_INT, PROPERTY_HINT_EXP_RANGE, name, _min, _max, _step, greater, lesser)

static func var_range_float_exp(name: String, _min: float, _max: float, _step: float = 0, greater: bool = false, lesser: bool = false) -> Dictionary:
	return _build_range(TYPE_REAL, PROPERTY_HINT_EXP_RANGE, name, _min, _max, _step, greater, lesser)

static func var_enum_int(name: String, items: PoolIntArray) -> Dictionary:
	return _build(name, TYPE_INT, PoolStringArray(items).join(","), PROPERTY_HINT_ENUM)

static func var_enum_float(name: String, items: PoolRealArray) -> Dictionary:
	return _build(name, TYPE_REAL, PoolStringArray(items).join(","), PROPERTY_HINT_ENUM)

static func var_enum_string(name: String, items: PoolStringArray) -> Dictionary:
	return _build(name, TYPE_STRING, items.join(","), PROPERTY_HINT_ENUM)

static func var_file(name: String, extension: String = "\"*.*\"") -> Dictionary:
	return _build(name, TYPE_STRING, extension, PROPERTY_HINT_FILE)

static func var_dir(name: String) -> Dictionary:
	return _build(name, TYPE_STRING, "", PROPERTY_HINT_DIR)

static func var_global_file(name: String, extension: String = "\"*.*\"") -> Dictionary:
	return _build(name, TYPE_STRING, extension, PROPERTY_HINT_GLOBAL_FILE)

static func var_global_dir(name: String) -> Dictionary:
	return _build(name, TYPE_STRING, "", PROPERTY_HINT_GLOBAL_DIR)

static func _build_range(type: int, hint: int, name: String, _min, _max, _step, greater: bool, lesser: bool) -> Dictionary:
	var hint_string: String = "%d,%d" % [_min, _max]

	if _step != 0:
		hint_string += ",%d" % _step

	if greater:
		hint_string += ",or_greater"

	if lesser:
		hint_string += ",or_lesser"

	return _build(name, type, hint_string, hint)

static func _build(name: String = "", type: int = TYPE_NIL, hint_string: String = "", hint: int = PROPERTY_HINT_NONE, usage: int = PROPERTY_USAGE_DEFAULT) -> Dictionary:
	return {
		"name": name,
		"type": type,
		"hint": hint,
		"hint_string": hint_string,
		"usage": usage | PROPERTY_USAGE_SCRIPT_VARIABLE,
		# "tooltip": "Testing tooltip",
		# "string_hint": "Meho"
		#"default_value": 50
	}
