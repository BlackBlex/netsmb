class_name Utility

var _key_secret: String = "048ca4ca2f8fb2edbc2c42b0813c49541617cc0167a073e5f67c855b21c88ac7e219bf8f901f3c9fcf7ee360d05e9fb34b034e817038674964d2352ee84254fb" setget _set_null, _get_null

static func convert_position_to_normal(position: Vector2) -> Vector2:
	return Vector2(int(position.x / GlobalData.BLOCK_SIZE), int(position.y / GlobalData.BLOCK_SIZE))

static func convert_position_to_real(position: Vector2) -> Vector2:
	return position * GlobalData.BLOCK_SIZE

# static func get_current_script_path():
# 	return get_stack()[1].source

#https://github.com/RichardEllicott/GodotSnippets
static func get_children_recursive(root: Node, depth: int = 0) -> Array:
	var array: Array = []
	for child in root.get_children():
		array.append(child)
		if depth < 8:
			array += get_children_recursive(child, depth + 1) # append results of recursing on children of the child
	return array

static func get_files(root_path: String, extension: String = "*", file_exact: bool = false) -> Array:
	var files: Array = []
	var directory: Directory = Directory.new()

	if directory.open(root_path) == OK:
		var _err: int = directory.list_dir_begin(true)
		var file_name: String = directory.get_next()

		while (file_name != ""):
			var path: String = directory.get_current_dir().plus_file(file_name)

			if directory.current_is_dir():
				files = files + get_files(path, extension, file_exact)
			else:
				if file_exact and path.ends_with(extension):
					files.append(path)
				elif extension == "*" or path.get_extension().ends_with(extension):
					files.append(path)

			file_name = directory.get_next()

	return files

# Format the time duration to a text to display
# - - - - - - - - - -
# *Parameters*
# * [seconds: float] The time duration in seconds
# * [always_show_hours: bool = `false`] Show hours even if the time is less than one hour
# - - - - - - - - - -
# *Returns* String
# * The time duration as `hh:mm:ss`
static func format_time_duration(second: float, always_show_hours: bool = false) -> String:
	var h: float = floor(second / (60.0 * 60.0))
	var m: float  = floor((second - h * 60.0 * 60.0) / 60.0)
	var s: float  = int(second) % 60
	var ret: String = ""
	if h > 0 or always_show_hours:
		if h < 10: ret += "0"
		ret += str(h, ":")
	if m < 10: ret += "0"
	ret += str(m, ":")
	if s < 10: ret += "0"
	ret += str(s)
	return ret

static func find_exist_path(path: String, path_base: String) -> String:
	if path.ends_with("/"):
		path = path.substr(0, path.find_last("/"))

	var exist_path: String = path_base

	if path_base.ends_with("/"):
		path_base = path_base.substr(0, path_base.find_last("/"))

	if path.nocasecmp_to(path_base) == 1:
		if not A.directory_loader.dir_exists(path):
			path = path.substr(0, path.find_last("/"))
			exist_path = find_exist_path(path, path_base)
		else:
			exist_path = path
	else:
		exist_path = path_base

	return exist_path

static func find_exist_path_file(path: String, path_base: String, file_name: String, alternative: String = "") -> String:
	var exist_path: String = find_exist_path(path, path_base)
	var exist_path_file: String = exist_path.plus_file(file_name)

	if exist_path.nocasecmp_to(path_base) == 1:
		var file_exist: bool = A.file_loader.file_exists(exist_path_file)
		if not file_exist:
			if not alternative.empty():
				exist_path_file = exist_path.plus_file(alternative)
				file_exist = A.file_loader.file_exists(exist_path_file)

		if not file_exist:
			exist_path = exist_path.substr(0, exist_path.find_last("/"))
			exist_path_file = find_exist_path_file(exist_path, path_base, file_name, alternative)
	else:
		exist_path_file = exist_path.plus_file(file_name)

		if not A.file_loader.file_exists(exist_path_file):
			exist_path_file = exist_path.plus_file(alternative)

	return exist_path_file

static func load_file(path: String, encrypt: bool = false, password: String = ""):
	var to_result: String = ""
	var is_open: bool = false

	if encrypt and not password.empty():
		is_open = A.file_loader.open_encrypted_with_pass(path, File.READ, password) == OK
	else:
		is_open = A.file_loader.open(path, File.READ) == OK

	if is_open:
		to_result = A.file_loader.get_as_text()
		A.file_loader.close()

	return to_result

static func load_script(path: String) -> GDScript:
	var to_result: GDScript = null
	var _err: int = 0

	if A.cache_scripts.has(path):
		to_result = A.cache_scripts[path].duplicate(true)
		to_result.resource_local_to_scene = true
	else:
		var code: String = load_file(path)

		if not code.empty():
			var file_name: String = path.get_file()
			to_result = GDScript.new()
			code = _replace_relative_path(code, path)
			to_result.source_code = code
			to_result.resource_path = path
			to_result.resource_name = file_name
			_err = to_result.reload()

			A.cache_scripts[path] = to_result

	return to_result

static func save_file(path: String, text: String, append: bool = false, encrypt: bool = false, password: String = "") -> int:
	var to_result: int = FAILED
	var mode: int = File.WRITE

	if append:
		mode = File.READ_WRITE

	if encrypt and not password.empty():
		to_result = A.file_loader.open_encrypted_with_pass(path, mode, password)
	else:
		to_result = A.file_loader.open(path, mode)

	if to_result == OK:
		A.file_loader.store_string(text)
		A.file_loader.close()

	return to_result

static func save_script(path: String, script: GDScript) -> int:
	return save_file(path, script.source_code)

static func _replace_relative_path(code: String, path: String) -> String:
	var lines: PoolStringArray = code.split("\n")
	var backup_line: String = ""
	var is_relative: bool = false
	for line in lines:
		if "extends " in line:
			backup_line = line
			is_relative = '"' in backup_line
			break

	if not backup_line.empty() and is_relative:
		var filename: String = path.get_file()
		path = path.replace("/%s" % filename, "")
		var parts: PoolStringArray = backup_line.split(" ")
		var part_file: String = parts[1].replace('"', '')
		var count_path: int = part_file.count("../")
		var path_fixed: String = path
		for _i in range(0, count_path):
			path_fixed = path_fixed.substr(0, path_fixed.find_last("/"))

		var new_line: String = '%s "%s"' % [parts[0], path_fixed.plus_file(part_file.replace("../", ""))]

		code = code.replace(backup_line, new_line)

	return code

func _set_null(_new_value: String):
	pass

func _get_null() -> String:
	return ""
