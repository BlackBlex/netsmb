class_name Version
extends JSONObject

enum State {ALPHA, BETA, RC, RELEASE, DEV}

var major: int = 0 # Breaking changes
var minor: int = 0 # Features
var revision: int = 0 # Fixs errors
var build: int = 0 # Fixs typos
var state: int = State.RELEASE
var state_version: int = 0 # Changes in public version


func _to_string() -> String:
	return "%d.%d.%d.%d-%s.%d" % [major, minor, revision, build, _get_state_name(state), state_version]


func init_string(version: String) -> Version:
	if "." in version:
		var data: PoolStringArray = version.split(".")

		if data.size() == 5:
			if "-" in data[3]:
				var data_secondary: PoolStringArray = data[3].split("-")
				data[3] = data_secondary[0]
				data.append(data[4])
				data[4] = data_secondary[1]
			major = int(data[0])
			minor = int(data[1])
			revision = int(data[2])
			build = int(data[3])
			state = _get_state_number(data[4])
			state_version = int(data[5])
		if data.size() == 4:
			if "-" in data[3]:
				var data_secondary: PoolStringArray = data[3].split("-")
				data[3] = data_secondary[0]
				state = _get_state_number(data_secondary[1])
			major = int(data[0])
			minor = int(data[1])
			revision = int(data[2])
			build = int(data[3])
		if data.size() == 3:
			major = int(data[0])
			minor = int(data[1])
			revision = int(data[2])
		if data.size() == 2:
			major = int(data[0])
			minor = int(data[1])
		if data.size() == 1:
			major = int(data[0])

	return self

func init_number(major_version: int = 0, minor_version: int = 0, revision_version: int = 0, build_version: int = 0, state_number: int = State.RELEASE, state_number_version: int = 0) -> Version:
	major = major_version
	minor = minor_version
	revision = revision_version
	build = build_version
	state = state_number
	state_version = state_number_version

	return self

func clone() -> Version:
	var to_result = load(get_script().resource_path).new() as Version
	return to_result.init_string(to_string())

func compare_to(other: Version) -> int:
	var to_result: int = -5

	if is_less_than(other):
		to_result = -1
	elif is_greater_than(other):
		to_result = 1
	elif equals(other):
		to_result = 0

	return to_result

func equals(other: Version) -> bool:
	var to_result: bool = major == other.major

	to_result = to_result and (minor == other.minor)
	to_result = to_result and (revision == other.revision)
	to_result = to_result and (build == other.build)
	to_result = to_result and (state == other.state)
	to_result = to_result and (state_version == other.state_version)

	return to_result

func get_class() -> String:
	return "Version"

func is_greater_than(other: Version) -> bool:
	var to_result: bool = major > other.major

	if not to_result:
		to_result = minor > other.minor
		if not to_result:
			to_result = revision > other.revision
			if not to_result:
				to_result = build > other.build
				if not to_result:
					to_result = state > other.state
					if not to_result:
						to_result = state_version > other.state_version

	return to_result

func is_less_than(other: Version) -> bool:
	var to_result: bool = major < other.major

	if not to_result:
		to_result = minor < other.minor
		if not to_result:
			to_result = revision < other.revision
			if not to_result:
				to_result = build < other.build
				if not to_result:
					to_result = state < other.state
					if not to_result:
						to_result = state_version < other.state_version

	return to_result

func is_greater_than_or_equals(value: Version) -> bool:
	return is_greater_than(value) or equals(value)

func is_less_than_or_equals(value: Version) -> bool:
	return is_less_than(value) or equals(value)


func _get_state_number(state_name: String) -> int:
	var to_result: int = State.ALPHA

	if state_name == "beta":
		to_result = State.BETA
	elif state_name == "rc":
		to_result = State.RC
	elif state_name == "release":
		to_result = State.RELEASE
	elif state_name == "dev":
		to_result = State.DEV

	return to_result

func _get_state_name(state_number: int) -> String:
	var to_result: String = "alpha"

	if state_number == State.BETA:
		to_result = "beta"
	elif state_number == State.RC:
		to_result = "rc"
	elif state_number == State.RELEASE:
		to_result = "release"
	elif state_number == State.DEV:
		to_result = "dev"

	return to_result
