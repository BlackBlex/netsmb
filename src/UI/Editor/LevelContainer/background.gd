class_name Background
extends TextureRect

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _COLOR_RECT_BACKGROUND = Color(0, 0, 0, 1)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _data_container = get_parent().get_parent()

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _process(_delta: float) -> void:
	update()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_Background_mouse_entered() -> void:
	GlobalData.is_mouse_on_level_window = true

func _on_Background_mouse_exited() -> void:
	GlobalData.is_mouse_on_level_window = false

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup() -> void:
	rect_position = GlobalData.current_level.get_map_origin_real()
	rect_min_size = GlobalData.current_level.get_map_size_real()

	_setup()


func _build(background_name: String, background_texture: Texture) -> Dictionary:
	var result: Dictionary = {}
	var background_data: Dictionary = GlobalData.object_database.backgrounds[background_name]

	result["bg"] = background_texture

	for key in background_data.keys():
		if typeof(background_data[key]) == TYPE_STRING:
			result[key] = _build(background_data[key], Resources.get_image("backgrounds/%s" % background_data[key]) as Texture)
		elif background_data[key] is Image:
			result[key] = background_data[key] as Image

	return result

func _preprocess(bg_size: Vector2, format: int, texture: Image, texture_bottom: Image, filled: bool = false) -> ImageTexture:
	var result: ImageTexture = null
	var texture_size: Vector2 = texture.get_size()
	var texture_missing_width = texture_size.x < bg_size.x
	var texture_missing_size = Vector2(bg_size.x - texture_size.x, texture_size.y)

	if texture_missing_width:
		var complement = texture.get_rect(Rect2(Vector2.ZERO, texture_missing_size))
		var new_texture_temp = Image.new()

		new_texture_temp.create(int(texture_size.x + texture_missing_size.x), int(texture_size.y), false, texture.get_format())
		new_texture_temp.blit_rect(texture, Rect2(Vector2.ZERO, texture_size), Vector2.ZERO)
		new_texture_temp.blit_rect(complement, Rect2(Vector2.ZERO, texture_missing_size), Vector2(texture_size.x, 0))
		texture = new_texture_temp

	if filled:
		texture_size = texture.get_size()
		var texture_size2: Vector2 = texture_bottom.get_size()
		var texture_missing: Vector2 = (texture_size + texture_size2) - (GlobalData.current_level.get_map_size_real() + Vector2(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE))
		if texture_missing.y < 0:
			var missing_blocks: float = ceil(abs(texture_missing.y) / GlobalData.BLOCK_SIZE)
			var image_data: PoolByteArray = []
			var original_image_data = texture.get_data()
			for _i in range(missing_blocks):
				image_data = texture.get_data()
				texture.create_from_data(int(texture_size.x), int(texture_size.y) + GlobalData.BLOCK_SIZE, false, format, original_image_data + image_data)
				texture_size = texture.get_size()

	var image_top: PoolByteArray = texture.get_data()
	var image_bottom: PoolByteArray = texture_bottom.get_data()

	var new_image: Image = Image.new()
	new_image.create_from_data(int(bg_size.x), int(texture_bottom.get_size().y + texture_size.y), false, format, image_top + image_bottom)

	result = ImageTexture.new()
	result.create_from_image(new_image)

	return result

func _setup() -> void:
	if GlobalData.current_level.background != "None" and texture == null:
		var background: Dictionary = _build(GlobalData.current_level.background, Resources.get_image("backgrounds/%s" % GlobalData.current_level.background) as Texture)
		var new_texture: ImageTexture = null
		var simple_bg: bool = false

		if background.size() > 0:
			var bg_size: Vector2 = background.bg.get_size()
			if GlobalData.current_level.get_map_size_real().y < bg_size.y:
				new_texture = ImageTexture.new()
				new_texture.create_from_image(background.bg.get_data())
			elif GlobalData.current_level.get_map_size_real().y > bg_size.y:
				var remaining_size = bg_size - GlobalData.current_level.get_map_size_real()
				var missing_heigth = remaining_size.y < 0

				var texture: Image = null

				if missing_heigth:
					if background.has("top"):
						if background.top is Image:
							texture = background.top
						elif background.top is ImageTexture or background.top is StreamTexture:
							texture = background.top.get_data()
						elif background.top.bg is Image:
							texture = background.top.bg
						elif background.top.bg is ImageTexture or background.top.bg is StreamTexture:
							texture = background.top.bg.get_data()

						new_texture = _preprocess(bg_size, background.bg.get_data().get_format(), texture, background.bg.get_data())

						if GlobalData.current_level.get_map_size_real().y > new_texture.get_size().y:
							if background.top is Dictionary:
								if background.top.has("top"):
									new_texture = _preprocess(bg_size, background.bg.get_data().get_format(), background.top.top, new_texture.get_data(), true)
							else:
								new_texture = _preprocess(bg_size, background.bg.get_data().get_format(), background.top, new_texture.get_data(), true)
					elif background.has("bottom"):
						pass
					else:
						simple_bg = true
						new_texture = ImageTexture.new()
						new_texture.create_from_image(background.bg.get_data())

			if simple_bg:
				var final_texture = ImageTexture.new()
				final_texture.create_from_image(new_texture.get_data().get_rect(Rect2(Vector2.ZERO, Vector2(GlobalData.current_level.get_map_size_real().x if new_texture.get_size().x > GlobalData.current_level.get_map_size_real().x else new_texture.get_size().x, new_texture.get_size().y))))
				texture = final_texture
			else:
				var remaining: Vector2 = new_texture.get_size() - GlobalData.current_level.get_map_size_real()
				var final_remaining: Vector2 = Vector2(0, remaining.y)

				var final_texture = ImageTexture.new()
				final_texture.create_from_image(new_texture.get_data().get_rect(Rect2(final_remaining, Vector2(new_texture.get_size().x if GlobalData.current_level.get_map_size_real().x > new_texture.get_size().x else GlobalData.current_level.get_map_size_real().x, GlobalData.current_level.get_map_size_real().y))))

				texture = final_texture
	else:
		var color_image: Image = Image.new()
		color_image.create(int(GlobalData.current_level.get_map_size_real().x), int(GlobalData.current_level.get_map_size_real().y), false, Image.FORMAT_RGBA8)
		color_image.fill(_COLOR_RECT_BACKGROUND)
		var color_texture = ImageTexture.new()
		color_texture.create_from_image(color_image)
		texture = color_texture

	rect_size = GlobalData.current_level.get_map_size_real()
