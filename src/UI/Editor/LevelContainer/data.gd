class_name LevelContainerData
extends Container

#//TODO: Implement this
#export(NodePath) var _target_node
#onready var target_node = get_node(_target_node)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _LINE_WIDTH = 1

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _coord_mouse_last: Vector2 = Vector2.INF
var _map_offset: Vector2 = Vector2.ZERO
var _coord_mouse: Vector2 = Vector2.INF
var _change_position: Vector2 = Vector2.ZERO
var _can_put_block: bool = false
var _can_block_axis: bool = false
var _is_blocked_axis: bool = false

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _status_bar: PanelContainer = get_node("../../Statusbar")
onready var _scroll_container: ScrollContainer = get_parent()
onready var _background: Background = $Canvas/Background
onready var _grid: Control = $Canvas/Background/Grid
onready var _layer_node: Node2D = $Layer

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	rect_min_size = Vector2(GlobalData.GRID_SIZE_LIMIT, GlobalData.GRID_SIZE_LIMIT)
	rect_size = rect_min_size

	yield(get_tree(), "idle_frame")
	_scroll_container.scroll_vertical = int(GlobalData.current_level.get_map_origin_real().y)

	if GlobalData.current_level.map_size == Vector2.ZERO:
		GlobalData.current_level.map_size = GlobalData.map_size_render

	_background.setup()
	_layer_node.position = GlobalData.current_level.get_map_origin_real()

func _process(delta: float) -> void:
	_check_inputs()
	if GlobalData.is_mouse_on_level_canvas:
		_move_grid_control(delta)

	update()

	if GlobalData.is_mouse_on_level_window and GlobalData.is_mouse_on_level_canvas:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	elif not GlobalData.is_mouse_on_level_window:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

	_can_put_block = Input.is_action_pressed('button_left')

	yield(get_tree(), "idle_frame")

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_grid_on_cell_selected(cell: Vector2) -> void:
	_put_block(cell)

func _on_level_data_mouse_entered() -> void:
	GlobalData.is_mouse_on_level_canvas = true

func _on_level_data_mouse_exited() -> void:
	GlobalData.is_mouse_on_level_canvas = false
	GlobalData.is_mouse_on_level_window = false

func _on_level_data_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		var position_mouse: Vector2 = event.position
		var position: Vector2 = _layer_node.layer_one.world_to_map(position_mouse)

		if position != _coord_mouse:
			_coord_mouse = position

			var coord_map_origin = GlobalData.current_level.map_origin
			var coord_map_end = GlobalData.current_level.map_origin + GlobalData.current_level.map_size
			coord_map_end -= Vector2(1, 1)

			_status_bar.get_node("Container").get_node("valueReal").text = str(position_mouse)

			GlobalData.is_mouse_on_level_window = (_coord_mouse.x >= coord_map_origin.x and _coord_mouse.x <= coord_map_end.x) and (_coord_mouse.y >= coord_map_origin.y and _coord_mouse.y <= coord_map_end.y)

			_status_bar.get_node("Container").get_node("lblPlane").visible = GlobalData.is_mouse_on_level_window
			_status_bar.get_node("Container").get_node("valuePlane").visible = GlobalData.is_mouse_on_level_window

			_coord_mouse -= coord_map_origin
			_status_bar.get_node("Container").get_node("valuePlane").text = str(_coord_mouse)

		if _can_put_block:
			_put_block()

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if not GlobalData.get_data_info():
				if _can_put_block:
					var position: Vector2 = _coord_mouse

					if GlobalData.current_level.data.has(GlobalData.current_layer) and GlobalData.current_level.data[GlobalData.current_layer].has(position):
						_can_put_block = false
						var data_info_to_remove: DataInfo = GlobalData.current_level.data[GlobalData.current_layer][position]
						var _err = GlobalData.current_level.data[GlobalData.current_layer].erase(position)

						data_info_to_remove.remove()
						data_info_to_remove.get_image().blink_stop()
						_layer_node.editor_remove(GlobalData.current_layer, data_info_to_remove)
						GlobalData.set_data_info(data_info_to_remove)
					elif GlobalData.current_level.has_tile(GlobalData.current_layer, position):
						_can_put_block = false
						var tile_id = _layer_node.layer_one.get_cell(int(position.x), int(position.y))
						if tile_id != TileMap.INVALID_CELL:
							var _err = GlobalData.current_level.tiles[tile_id][GlobalData.current_layer].erase(position)
							_layer_node.remove_tile(GlobalData.current_layer, position)

							var block_info: BlockInfo = BlockInfo.new()
							block_info.root = "blocks"
							block_info.category = "grounds"
							block_info.subcategory = "meh"
							block_info.variant = "tile"
							block_info.identifier = tile_id

							var icon_size: Vector2 = _layer_node.layer_one.tile_set.autotile_get_size(tile_id)
							var icon_pos: Vector2 = _layer_node.layer_one.tile_set.autotile_get_icon_coordinate(tile_id) * icon_size
							var image: Image = Image.new()
							image.copy_from(_layer_node.layer_one.tile_set.tile_get_texture(tile_id).get_data())
							var image_texture: ImageTexture = ImageTexture.new()
							image_texture.create_from_image(image.get_rect(Rect2(icon_pos, icon_size)))

							block_info.get_image().frames = SpriteFrames.new()
							block_info.get_image().frames.add_frame("default", image_texture)
							block_info.get_image().frames.set_animation_speed("default", GlobalData.FPS_ANIMATION)
							GlobalData.set_data_info(block_info)

			if not _can_put_block:
				_coord_mouse_last = Vector2.INF
				_is_blocked_axis = false

			if GlobalData.is_mouse_on_level_canvas:
				if GlobalData.previewer_oppened:
					GlobalData.previewer_oppened.visible = false
					GlobalData.previewer_oppened = null

				if GlobalData.open_panel:
					GlobalData.open_panel.visible = false
					GlobalData.open_panel = null


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func add_data_info(layer: int, position: Vector2, info: DataInfo, exist: bool = false) -> void:
	var new_info: DataInfo = info

	if not exist:
		new_info = info.clone()
		if not GlobalData.current_level.data.has(layer):
			GlobalData.current_level.data[layer] = {}

		GlobalData.current_level.data[layer][position] = new_info

		Event.publish(self, A.EventList.level_editor.putted, [new_info])

	_layer_node.editor_place(layer, new_info)
	new_info.set_position(position)
	new_info.load_frames()
	new_info.set_visible(_layer_node.is_layer_visible(layer))

func add_reward_info(position: Vector2, block_reward: BlockRewardInfo, info: NpcInfo) -> void:
	var new_info = info.clone() as NpcInfo
	new_info.set_position(position)

	var identifier: int = 0

	var keys = GlobalData.current_level.rewards.keys()
	if keys.size() > 0:
		keys.invert()
		identifier = keys[0] + 1

	GlobalData.current_level.rewards[identifier] = new_info
	block_reward.add_reward(identifier, 1)

func clean_all_data() -> void:
	_layer_node.clean()

	GlobalData.current_level = LevelData.new()
	GlobalData.current_level._ready()
	refresh_level_background()
	Audio.player_spc.stop_music()
	Audio.player.stop()

func get_background_texture() -> Texture:
	return _background.texture

func get_layer() -> Node2D:
	return _layer_node

func get_tile_id(name: String) -> int:
	return _layer_node.layer_one.tile_set.find_tile_by_name(name.replace("-", " "))

func refresh_level_background() -> void:
	_background.texture = null
	_background.setup()
	_grid.setup(GlobalData.current_level.map_size * GlobalData.BLOCK_SIZE, Vector2(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE), Vector2.ZERO, GlobalData.map_size_render)

func set_level_background(new_value: String) -> void:
	GlobalData.current_level.background = new_value
	refresh_level_background()

func set_tile(layer: int, position: Vector2, tile_id: int, existent: bool = false) -> void:
	_layer_node.set_tile(layer, tile_id, position)

	if not _layer_node.is_layer_visible(layer):
		_layer_node.layer_one.set_cellv(position, -1)

	if not existent:
		if not GlobalData.current_level.tiles.has(tile_id):
			GlobalData.current_level.tiles[tile_id] = {}

		if not GlobalData.current_level.tiles[tile_id].has(GlobalData.current_layer):
			GlobalData.current_level.tiles[tile_id][GlobalData.current_layer] = []

		GlobalData.current_level.tiles[tile_id][GlobalData.current_layer].append(position)

func set_current_layer_name(name: String) -> void:
	_status_bar.get_node("Container").get_node("valueLayer").text = name

func get_coord_mouse() -> Vector2:
	return _coord_mouse


func _check_inputs() -> void:
	_can_block_axis = Input.is_key_pressed(KEY_CONTROL)

func _move_grid_control(_delta: float) -> void:
	yield(get_tree(), "idle_frame")

	#Move to -x
	if Input.is_action_pressed("ui_left"):
		_scroll_container.scroll_horizontal -= GlobalData.STEP_GRID_CONTROL
	#Move to x
	elif Input.is_action_pressed("ui_right"):
		_scroll_container.scroll_horizontal += GlobalData.STEP_GRID_CONTROL

	#Move to -y
	if Input.is_action_pressed("ui_down"):
		_scroll_container.scroll_vertical += GlobalData.STEP_GRID_CONTROL
	#Move to y
	elif Input.is_action_pressed("ui_up"):
		_scroll_container.scroll_vertical -= GlobalData.STEP_GRID_CONTROL

func _put_block(position: Vector2 = Vector2.INF) -> void:
	if _can_put_block and GlobalData.get_data_info() and GlobalData.is_mouse_on_level_window:
		var is_player: bool = GlobalData.get_data_info() is PlayerInfo

		if is_player:
			var level_contains_player: bool = false
			for layer in GlobalData.current_level.data:
				level_contains_player = Arrays.of_type_object(GlobalData.current_level.data[layer].values(), PlayerInfo).size() >= 1

				if level_contains_player:
					break

			if level_contains_player:
				Alert.show_information(tr("KEY_ONLY_ONE_SPAWN"))
				return

		if position == Vector2.INF:
			position = _coord_mouse

		if _coord_mouse_last == Vector2.INF:
			_coord_mouse_last = _coord_mouse
			_change_position = Vector2.ZERO

		_is_blocked_axis = _change_position != Vector2.ZERO

		if _is_blocked_axis:
			var position_normal = _coord_mouse_last

			if _change_position.x != 0:
				position.y = position_normal.y
			elif _change_position.y != 0:
				position.x = position_normal.x
		else:
			_change_position = ((_coord_mouse_last - _coord_mouse) * int(_can_block_axis)) * int(not _is_blocked_axis)

		if GlobalData.current_level.data.has(GlobalData.current_layer) and GlobalData.current_level.data[GlobalData.current_layer].has(position):
			#Check if block is reward
			var block_reward = GlobalData.current_level.data[GlobalData.current_layer][position] as BlockRewardInfo

			if block_reward:
				if GlobalData.get_data_info() is NpcInfo:
					if not block_reward.has_reward():
						add_reward_info(position, block_reward,
						GlobalData.get_data_info() as NpcInfo)
					else:
						var identifier_npc: int = block_reward.reward.npc_identificator
						var npc_reward: NpcInfo = GlobalData.current_level.rewards[identifier_npc]

						var is_same_npc := npc_reward.to_string().nocasecmp_to(GlobalData.get_data_info().to_string()) == 0

						if is_same_npc:
							block_reward.add_amount()
						else:
							var _err = GlobalData.current_level.rewards.erase(block_reward.reward.npc_identificator)
							add_reward_info(position, block_reward, GlobalData.get_data_info() as NpcInfo)
				else:
					Alert.show_information("KEY_ONLY_NPC_REWARD")
		else:
			if "tile" in GlobalData.get_data_info().variant:
				set_tile(GlobalData.current_layer, position, GlobalData.get_data_info().identifier)
			else:
				add_data_info(GlobalData.current_layer, position, GlobalData.get_data_info())

		yield(get_tree().create_timer(0.1), "timeout")
