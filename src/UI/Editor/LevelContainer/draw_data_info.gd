extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _data_container = get_parent().get_parent().get_parent()

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _process(_delta: float) -> void:
	if GlobalData.is_mouse_on_level_window:
		_draw_cursor()

func _input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT:
			if has_node("DataInfoCurrent"):
				remove_child(get_node("DataInfoCurrent"))
				GlobalData.set_data_info()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func _draw_cursor() -> void:
	var position: Vector2 = _data_container.get_coord_mouse()

	if GlobalData.get_data_info():
		if not GlobalData.get_data_info().get_image().visible:
			GlobalData.get_data_info().get_image().visible = true

		if not has_node("DataInfoCurrent"):
			GlobalData.get_data_info().place(self)
			GlobalData.get_data_info().get_image().name = "DataInfoCurrent"
		else:
			GlobalData.get_data_info().get_image().position = Utility.convert_position_to_real(position) + Vector2(16, 16)

		# if GlobalData.get_data_info().get_image().visible:
		# 	if GlobalData.get_data_info().get_image().z_index != 21:
		# 		GlobalData.get_data_info().get_image().z_index = 21
	else:
		if GlobalData.current_level.data.has(GlobalData.current_layer) and GlobalData.current_level.data[GlobalData.current_layer].has(position):
			if GlobalData.data_info_hover != GlobalData.current_level.data[GlobalData.current_layer][position]:
				if GlobalData.data_info_hover and is_instance_valid(GlobalData.data_info_hover.get_image()):
					GlobalData.data_info_hover.get_image().blink_stop()

				GlobalData.data_info_hover = GlobalData.current_level.data[GlobalData.current_layer][position]
				GlobalData.data_info_hover.get_image().blink()
		else:
			if GlobalData.data_info_hover and is_instance_valid(GlobalData.data_info_hover.get_image()):
				GlobalData.data_info_hover.get_image().blink_stop()

			GlobalData.data_info_hover = null
