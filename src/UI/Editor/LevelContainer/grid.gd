extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _LINE_WIDTH = 1
const _COLOR_GRID_LINE = Color(1, 1, 1, 0.4)
const _COLOR_GRID_LINE_LIMIT = Color(1, 1, 1)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var size: Vector2 = Vector2.ZERO
var step: Vector2 = Vector2.ZERO
var spacing: Vector2 = Vector2.ZERO
var section_size: Vector2 = Vector2.ZERO

var _can_draw_section: bool = false
var _is_setupped: bool = false
var _line_width: Vector2 = Vector2(_LINE_WIDTH, _LINE_WIDTH)

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _process(_delta: float) -> void:
	update()

func _draw() -> void:
	if _is_setupped:
		_draw_grid()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup(size_grid: Vector2, step_grid: Vector2 = Vector2.ZERO, spacing_grid: Vector2 = Vector2.ZERO, section_size_grid: Vector2 = Vector2.ZERO) -> void:
	rect_size = size_grid
	rect_min_size = size_grid
	size = (size_grid / step_grid).floor()
	step = step_grid
	spacing = spacing_grid

	section_size = section_size_grid

	_can_draw_section = section_size != Vector2.ZERO
	_is_setupped = true

	if spacing != Vector2.ZERO:
		_line_width = Vector2(floor(_LINE_WIDTH * spacing.x), floor(_LINE_WIDTH * spacing.y))
	else:
		_line_width = Vector2(_LINE_WIDTH, _LINE_WIDTH)


func _draw_grid() -> void:
	var original_real: Vector2 = Vector2.ZERO
	var complete_section: bool = false
	var limit: float = (size.y * step.y) + (spacing.y * size.y)
	var space: int = 0

	for x in range(0, size.x + 1):
		space = int(floor((x * spacing.x) - (spacing.x / 2)))

		x *= int(step.x)
		x += space

		if _can_draw_section:
			complete_section = x % int(section_size.x) == 0
			if complete_section:
				draw_line(Vector2(x, original_real.y), Vector2(x, limit), _COLOR_GRID_LINE_LIMIT, _line_width.x)

		if not _can_draw_section or not complete_section:
			draw_line(Vector2(x, original_real.y), Vector2(x, limit), _COLOR_GRID_LINE, _line_width.x)

	limit = (size.x * step.x) + (spacing.x * size.x)

	for y in range(0, size.y + 1):
		space = int(floor((y * spacing.y) - (spacing.y / 2)))

		y *= int(step.y)
		y += space

		if _can_draw_section:
			complete_section = y % int(section_size.y) == 0
			if complete_section:
				draw_line(Vector2(original_real.x, y), Vector2(limit, y), _COLOR_GRID_LINE_LIMIT, _line_width.y)

		if not _can_draw_section or not complete_section:
			draw_line(Vector2(original_real.x, y), Vector2(limit, y), _COLOR_GRID_LINE, _line_width.y)
