extends "grid.gd"

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal on_cell_selected(cell)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
export(Color, RGBA) var color_rect_border: Color = Color(0.114155, 0.242677, 0.292969, 0.498039)

##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var current_cell: Vector2 = Vector2.ZERO

var _temporal: Vector2 = Vector2.ZERO
var _rect: Panel = Panel.new()
var _style_rect: StyleBoxFlat = StyleBoxFlat.new()

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	var _err = connect("gui_input", self, "_on_gui_input")

	_style_rect.draw_center = false
	_style_rect.border_width_bottom = 2
	_style_rect.border_width_left = 2
	_style_rect.border_width_right = 2
	_style_rect.border_width_top = 2
	_style_rect.border_color = color_rect_border

	mouse_default_cursor_shape = CURSOR_POINTING_HAND
	_rect.mouse_default_cursor_shape = CURSOR_POINTING_HAND
	_rect.mouse_filter = Control.MOUSE_FILTER_PASS
	_rect.rect_size = Vector2(34, 34)
	_rect.set("custom_styles/panel", _style_rect)

	add_child(_rect, true)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseMotion:
		current_cell = (event.position / step).floor()

		if spacing != Vector2.ZERO:
			_temporal = spacing * current_cell
			current_cell = ((event.position - _temporal) / step).floor()

		if current_cell.x > size.x - 1:
			current_cell.x = size.x
		if current_cell.y > size.y - 1:
			current_cell.y = size.y - 1

		update_rect()

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			emit_signal("on_cell_selected", current_cell)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup(size_grid: Vector2, step_grid: Vector2 = Vector2.ZERO, spacing_grid: Vector2 = Vector2.ZERO, section_size_grid: Vector2 = Vector2.ZERO) -> void:
	.setup(size_grid, step_grid, spacing_grid, section_size_grid)

	update_rect()

func update_rect(cell: Vector2 = Vector2.INF) -> void:
	if cell != Vector2.INF:
		current_cell = cell

	_rect.rect_size = step + Vector2(2, 2)

	_rect.rect_position = current_cell * step + (spacing * current_cell)
	_rect.rect_position -= Vector2(1, 1)
