class_name ResizePanel
extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _COLOR_RECT_NORMAL_RESIZE = Color(0.77, 0.75, 0.16, 1)
const _COLOR_RECT_HOVER_RESIZE = Color(0.77, 0.16, 0.16, 1)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _resize_offset: Vector2 = Vector2.ZERO
var _coord_mouse_start: Vector2 = Vector2.INF
var _is_resize_mode: bool = false
var _is_level_resize_panel_pressed: bool = false
var _is_mouse_hover_on_resize_panel: bool = false
var _level_resizable_panel: Dictionary = {}

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _data_container = get_parent()

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _process(_delta: float) -> void:
	_check_input()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_resize_panel_gui_input(event: InputEvent, direction: String) -> void:
	if not _level_resizable_panel.has(direction):
		_level_resizable_panel[direction] = get_node(direction)

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			_is_level_resize_panel_pressed = event.pressed

			if not _is_level_resize_panel_pressed:
				_resize_offset = Vector2.ZERO
			else:
				_coord_mouse_start = _data_container.get_coord_mouse()

	if event is InputEventMouseMotion:
		if _is_level_resize_panel_pressed:
			_resize_offset = _data_container.get_coord_mouse() - _coord_mouse_start
			_resize_offset *= GlobalData.BLOCK_SIZE
			var prevent: Vector2 = rect_size - _resize_offset

			match direction:
				"Top":
					_resize_offset.x = 0

					if prevent.y >= GlobalData.map_size_render.y:
						rect_position += _resize_offset
						rect_size -= _resize_offset
						_data_container.get_map_origin().y = rect_position.y
				"Bottom":
					_resize_offset.x = 0
					rect_size += _resize_offset
				"Left":
					_resize_offset.y = 0

					if prevent.x >= GlobalData.map_size_render.x:
						rect_position += _resize_offset
						rect_size -= _resize_offset
						_data_container.get_map_origin().x = rect_position.x
				"Right":
					_resize_offset.y = 0
					_resize_offset.x *= -1
					rect_size -= _resize_offset

			GlobalData.current_level.map_size = Utility.convert_position_to_normal(rect_size)
			_coord_mouse_start = _data_container.get_coord_mouse()

			_data_container._background.texture = null
			_data_container._background.setup()
			get_tree().get_nodes_in_group("editor")[0].get_map_size_component().set_value(Utility.convert_position_to_normal(rect_size))

func _on_resize_panel_mouse_entered(direction: String) -> void:
	if not _level_resizable_panel.has(direction):
		_level_resizable_panel[direction] = get_node(direction)

	_level_resizable_panel[direction].get_stylebox("panel").bg_color = _COLOR_RECT_HOVER_RESIZE
	_is_mouse_hover_on_resize_panel = true

func _on_resize_panel_mouse_exited(direction: String) -> void:
	if not _level_resizable_panel.has(direction):
		_level_resizable_panel[direction] = get_node("ResizePanel/" + direction)

	_level_resizable_panel[direction].get_stylebox("panel").bg_color = _COLOR_RECT_NORMAL_RESIZE
	_is_mouse_hover_on_resize_panel = false

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup() -> void:
	rect_min_size = GlobalData.current_level.get_map_size_real()
	rect_size = GlobalData.current_level.get_map_size_real()

func is_level_resize_panel_pressed() -> bool:
	return _is_level_resize_panel_pressed

func is_mouse_hover_on_resize_panel() -> bool:
	return _is_mouse_hover_on_resize_panel

func is_resize_mode() -> bool:
	return _is_resize_mode


func _check_input() -> void:
	if Input.is_action_just_pressed("action_resize_level"):
		visible = !visible
		_is_resize_mode = visible
