tool
class_name CheckComponent
extends MarginContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal check_changed(value)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var text: String = "Text" setget _set_text, _get_text
var check_text: String = "On" setget _set_check_text, _get_check_text
var disable_check: bool = false setget _set_disable_check, _get_disable_check
var visible_text: bool = true setget _set_visible_text, _get_visible_text
var visible_check_text: bool = true setget _set_visible_check_text, _get_visible_check_text
var checked: bool = false setget _set_checked, _get_checked

var _changes_option_notified: bool = true
var _old_text: String = check_text

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _text: Label = $HContainer/Text
onready var _check: CheckBox = $HContainer/Check

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_text(text)
	_set_check_text(check_text)
	_set_disable_check(disable_check)
	_set_visible_text(visible_text)
	_set_visible_check_text(visible_check_text)
	_set_checked(checked)

	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
		set_process_input(false)
		return

func _get_property_list() -> Array:
	return _property_list()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_check_pressed() -> void:
	_changes_option_notified = false

func _on_check_exited() -> void:
	if not _changes_option_notified:
		_changes_option_notified = true
		_emit_signal_convert(is_checked())

func _on_check_gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_ENTER and event.pressed:
			if not _changes_option_notified:
				_changes_option_notified = true
				_emit_signal_convert(is_checked())

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func is_checked() -> bool:
	return _check.pressed


func _emit_signal_convert(value) -> void:
	emit_signal("check_changed", value)

func _property_list() -> Array:
	var properties: Array = [
		PropertyInfo.category("CheckComponent"),
		PropertyInfo.group("View"),
		PropertyInfo.var_string("text"),
		PropertyInfo.var_string("check_text"),
		PropertyInfo.var_bool("checked"),
		PropertyInfo.group("Behaviour"),
		PropertyInfo.var_bool("disable_check"),
		PropertyInfo.var_bool("visible_text"),
		PropertyInfo.var_bool("visible_check_text")
		]

	return properties

func _get_text() -> String:
	return text

func _set_text(new_text: String) -> void:
	text = new_text
	property_list_changed_notify()

	if _text:
		_text.text = text

func _get_check_text() -> String:
	return check_text

func _set_check_text(new_text: String) -> void:
	check_text = new_text
	_old_text = check_text
	property_list_changed_notify()

	if _check:
		_check.text = check_text

func _get_disable_check() -> bool:
	return disable_check

func _set_disable_check(new_value: bool) -> void:
	disable_check = new_value

	property_list_changed_notify()

	if _check:
		_check.disabled = disable_check

func _get_visible_text() -> bool:
	return visible_text

func _set_visible_text(new_value: bool) -> void:
	visible_text = new_value

	property_list_changed_notify()

	if _text:
		_text.visible = visible_text

func _get_visible_check_text() -> bool:
	return visible_check_text

func _set_visible_check_text(new_value: bool) -> void:
	visible_check_text = new_value

	property_list_changed_notify()

	if _check:
		if visible_check_text:
			_check.text = _old_text
		else:
			_old_text = _check.text
			_check.text = ""

func _get_checked() -> bool:
	return _check.pressed

func _set_checked(new_value: bool) -> void:
	checked = new_value

	property_list_changed_notify()

	if _check:
		_check.pressed = checked
