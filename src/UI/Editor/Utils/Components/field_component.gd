tool
class_name FieldComponent
extends MarginContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal text_input_x_changed(value)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _TRANSPARENCY: Color = Color(1, 1, 1, 0.4)
const _NO_TRANSPARENCY: Color = Color(1, 1, 1)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var text: String = "Text" setget _set_text, _get_text
var prefix_x: String = "X" setget _set_prefix_x, _get_prefix_x
var suffix_x: String = "px" setget _set_suffix_x, _get_suffix_x
var disable_input_x: bool = false setget _set_disable_input_x, _get_disable_input_x
var visible_text: bool = true setget _set_visible_text, _get_visible_text
var visible_prefix_x: bool = true setget _set_visible_prefix_x, _get_visible_prefix_x
var visible_suffix_x: bool = false setget _set_visible_suffix_x, _get_visible_suffix_x
var only_transparency: bool = true

var _changes_input_x_notified: bool = true
var _last_correct_input_x: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _text: Label = $HContainer/Text
onready var _input_x: LineEdit = $HContainer/XInput
onready var _prefix_x: Label = $HContainer/XInput/Prefix
onready var _suffix_x: Label = $HContainer/XInput/Suffix

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_text(text)
	_set_prefix_x(prefix_x)
	_set_disable_input_x(disable_input_x)
	_set_visible_text(visible_text)
	_set_visible_prefix_x(visible_prefix_x)
	_set_visible_suffix_x(visible_suffix_x)

	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
		set_process_input(false)
		return

func _get_property_list() -> Array:
	return _property_list()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_input_focus_entered() -> void:
	if not _input_x.editable:
		return
	if only_transparency:
		_prefix_x.modulate = _TRANSPARENCY
		_prefix_x.update()
	else:
		_prefix_x.visible = false

func _on_input_focus_exited() -> void:
	if only_transparency:
		_prefix_x.modulate = _NO_TRANSPARENCY
		_prefix_x.update()
	else:
		_prefix_x.visible = true

	if not _changes_input_x_notified:
		_changes_input_x_notified = true
		_emit_signal_convert(_input_x.text)

func _on_field_component_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if not _prefix_x.visible or _prefix_x.modulate == _TRANSPARENCY:
			if only_transparency:
				_prefix_x.modulate = _NO_TRANSPARENCY
			else:
				_prefix_x.visible = true

func _on_input_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if _prefix_x.visible or _prefix_x.modulate == _NO_TRANSPARENCY:
			if not _input_x.editable:
				return
			if only_transparency:
				_prefix_x.modulate = _TRANSPARENCY
			else:
				_prefix_x.visible = false

func _on_input_text_changed(new_text: String) -> void:
	if new_text.empty():
		return

	_last_correct_input_x = new_text
	_changes_input_x_notified = false

func _on_placeholder_visibility_changed() -> void:
	pass

func _on_input_text_entered(_new_text: String) -> void:
	if not _changes_input_x_notified:
		_changes_input_x_notified = true
		_emit_signal_convert(_input_x.text)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_value() -> String:
	return _input_x.text

func grab_focus() -> void:
	_input_x.grab_focus()

func set_value(value: String) -> void:
	_input_x.text = value


func _emit_signal_convert(value: String) -> void:
	emit_signal("text_input_x_changed", value)

func _property_list() -> Array:
	var properties: Array = [
		PropertyInfo.category("FieldComponent"),
		PropertyInfo.group("View"),
		PropertyInfo.var_string("text"),
		PropertyInfo.var_string("prefix_x"),
		PropertyInfo.var_string("suffix_x"),
		PropertyInfo.group("Behaviour"),
		PropertyInfo.var_bool("only_transparency"),
		PropertyInfo.var_bool("disable_input_x"),
		PropertyInfo.var_bool("visible_text"),
		PropertyInfo.var_bool("visible_prefix_x"),
		PropertyInfo.var_bool("visible_suffix_x"),
		]

	return properties

func _get_text() -> String:
	return text

func _set_text(new_text: String) -> void:
	text = new_text
	property_list_changed_notify()

	if _text:
		_text.text = text

func _get_prefix_x() -> String:
	return prefix_x

func _set_prefix_x(new_text: String) -> void:
	prefix_x = new_text.left(1).to_upper()

	if prefix_x.empty():
		prefix_x = "X"

	property_list_changed_notify()

	if _prefix_x:
		_prefix_x.text = prefix_x

func _get_suffix_x() -> String:
	return suffix_x

func _set_suffix_x(new_text: String) -> void:
	suffix_x = new_text.left(2).to_lower()

	if suffix_x.empty():
		suffix_x = "px"

	property_list_changed_notify()

	if _suffix_x:
		_suffix_x.text = suffix_x

func _get_disable_input_x() -> bool:
	return disable_input_x

func _set_disable_input_x(new_value: bool) -> void:
	disable_input_x = new_value

	property_list_changed_notify()

	if _input_x:
		_input_x.editable = not disable_input_x

func _get_visible_text() -> bool:
	return visible_text

func _set_visible_text(new_value: bool) -> void:
	visible_text = new_value

	property_list_changed_notify()

	if _text:
		_text.visible = visible_text

func _get_visible_prefix_x() -> bool:
	return visible_prefix_x

func _set_visible_prefix_x(new_value: bool) -> void:
	visible_prefix_x = new_value

	property_list_changed_notify()

	if _prefix_x:
		_prefix_x.visible = visible_prefix_x

func _get_visible_suffix_x() -> bool:
	return visible_suffix_x

func _set_visible_suffix_x(new_value: bool) -> void:
	visible_suffix_x = new_value

	property_list_changed_notify()

	if _suffix_x:
		_suffix_x.visible = visible_suffix_x
