tool
class_name OptionComponent
extends MarginContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal option_changed(value)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var text: String = "Text" setget _set_text, _get_text
var disable_option: bool = false setget _set_disable_option, _get_disable_option
var visible_text: bool = true setget _set_visible_text, _get_visible_text
var items: Array = [] setget _set_items, _get_items
var selected: int = -1 setget _set_selected, _get_selected

var _changes_option_notified: bool = true

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _text: Label = $HContainer/Text
onready var _option_button: OptionButton = $HContainer/Options

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_text(text)
	_set_disable_option(disable_option)
	_set_visible_text(visible_text)
	_set_items(items)
	_set_selected(selected)

	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
		set_process_input(false)
		return

func _get_property_list() -> Array:
	return _property_list()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_options_item_selected(_index: int) -> void:
	_changes_option_notified = false

func _on_options_focus_exited() -> void:
	if not _changes_option_notified:
		_changes_option_notified = true
		_emit_signal_convert(get_item_selected())

func _on_option_mouse_exited() -> void:
	if not _changes_option_notified:
		_changes_option_notified = true
		_emit_signal_convert(get_item_selected())

func _on_options_gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_ENTER and event.pressed:
			if not _changes_option_notified:
				_changes_option_notified = true
				_emit_signal_convert(get_item_selected())

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clear() -> void:
	items.clear()
	_option_button.clear()

func get_item_selected():
	return items[_option_button.selected]

#Check this implementation
func set_item_selected(value) -> void:
	var id = items.find(value)
	selected = id
	_option_button.select(id)


func _emit_signal_convert(value) -> void:
	emit_signal("option_changed", value)

func _property_list() -> Array:
	var properties: Array = [
		PropertyInfo.category("OptionComponent"),
		PropertyInfo.group("View"),
		PropertyInfo.var_string("text"),
		PropertyInfo.var_array("items"),
		PropertyInfo.var_range_int("selected", -1, items.size() - 1),
		PropertyInfo.group("Behaviour"),
		PropertyInfo.var_bool("disable_option"),
		PropertyInfo.var_bool("visible_text")
		]

	return properties

func _get_text() -> String:
	return text

func _set_text(new_text: String) -> void:
	text = new_text
	property_list_changed_notify()

	if _text:
		_text.text = text

func _get_disable_option() -> bool:
	return disable_option

func _set_disable_option(new_value: bool) -> void:
	disable_option = new_value

	property_list_changed_notify()

	if _option_button:
		_option_button.disabled = disable_option

func _get_visible_text() -> bool:
	return visible_text

func _set_visible_text(new_value: bool) -> void:
	visible_text = new_value

	property_list_changed_notify()

	if _text:
		_text.visible = visible_text

func _get_items() -> Array:
	return items

func _set_items(new_value: Array) -> void:
	items = new_value

	property_list_changed_notify()

	if _option_button:
		for i in range(items.size()):
			_option_button.add_item(items[i], i)

func _get_selected() -> int:
	return selected

func _set_selected(new_value: int) -> void:
	selected = new_value

	property_list_changed_notify()

	if selected <= 0:
		return

	if _option_button:
		_option_button.selected = selected
		#_option_button.select(selected)
