tool
class_name SliderComponent
extends MarginContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal value_changed(value)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var text: String = "Text" setget _set_text, _get_text
var editable_slider: bool = true setget _set_editable_slider, _get_editable_slider
var visible_text: bool = true setget _set_visible_text, _get_visible_text
var min_value: float = 0 setget _set_min_value, _get_min_value
var max_value: float = 100 setget _set_max_value, _get_max_value
var step: float = 1 setget _set_step, _get_step
var rounded: bool = false setget _set_rounded, _get_rounded

var _changes_value_notified: bool = true
var _last_correct_input: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _text: Label = $HContainer/Text
onready var _input: LineEdit = $HContainer/VContainer/Input
onready var _slider: Slider = $HContainer/VContainer/Slider

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_text(text)
	_set_editable_slider(editable_slider)
	_set_visible_text(visible_text)
	_set_min_value(min_value)
	_set_max_value(max_value)
	_set_step(step)
	_set_rounded(rounded)

	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
		set_process_input(false)
		return

func _get_property_list() -> Array:
	return _property_list()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_slider_value_changed(value: float) -> void:
	_changes_value_notified = false
	_input.text = str(value)

func _on_slider_focus_exited() -> void:
	if not _changes_value_notified:
		_changes_value_notified = true
		_emit_signal_convert(_slider.value)

func _on_slider_gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		if event.scancode == KEY_ENTER and event.pressed:
			if not _changes_value_notified:
				_changes_value_notified = true
				_emit_signal_convert(_slider.value)

func _on_input_text_changed(new_text: String) -> void:
	if new_text.empty():
		return

	if new_text.begins_with("0") and new_text.length() > 1:
		_input.text = _input.text.substr(1)
		_input.set_cursor_position(_input.text.length())

	var is_valid: bool = new_text.is_valid_float() or new_text.is_valid_integer()

	if is_valid:
		_last_correct_input = new_text
		_changes_value_notified = false
	else:
		_input.text = _last_correct_input
		_input.set_cursor_position(_input.text.length())

func _on_input_text_entered(_new_text: String) -> void:
	if not _changes_value_notified:
		_changes_value_notified = true

		_slider.value = float(_input.text)
		_emit_signal_convert(float(_input.text))

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_value() -> float:
	return _slider.value

func set_value(value: float) -> void:
	_slider.value = value


func _emit_signal_convert(value: float) -> void:
	emit_signal("value_changed", value)

func _property_list() -> Array:
	var properties: Array = [
		PropertyInfo.category("SliderComponent"),
		PropertyInfo.group("View"),
		PropertyInfo.var_string("text"),
		PropertyInfo.group("Behaviour"),
		PropertyInfo.var_bool("editable_slider"),
		PropertyInfo.var_float("min_value"),
		PropertyInfo.var_float("max_value"),
		PropertyInfo.var_float("step"),
		PropertyInfo.var_bool("rounded"),
		PropertyInfo.var_bool("visible_text")
		]

	return properties

func _get_text() -> String:
	return text

func _set_text(new_text: String) -> void:
	text = new_text
	property_list_changed_notify()

	if _text:
		_text.text = text

func _get_editable_slider() -> bool:
	return editable_slider

func _set_editable_slider(new_value: bool) -> void:
	editable_slider = new_value

	property_list_changed_notify()

	if _input and _slider:
		_input.editable = editable_slider
		_slider.editable = editable_slider

func _get_visible_text() -> bool:
	return visible_text

func _set_visible_text(new_value: bool) -> void:
	visible_text = new_value

	property_list_changed_notify()

	if _text:
		_text.visible = visible_text

func _get_min_value() -> float:
	return min_value

func _set_min_value(new_value: float) -> void:
	min_value = new_value

	property_list_changed_notify()

	if _slider:
		_slider.min_value = min_value

func _get_max_value() -> float:
	return max_value

func _set_max_value(new_value: float) -> void:
	max_value = new_value

	property_list_changed_notify()

	if _slider:
		_slider.max_value = max_value

func _get_step() -> float:
	return step

func _set_step(new_step: float) -> void:
	step = new_step

	property_list_changed_notify()

	if _slider:
		_slider.step = step

func _get_rounded() -> bool:
	return rounded

func _set_rounded(new_rounded: bool) -> void:
	rounded = new_rounded

	property_list_changed_notify()

	if _slider:
		_slider.rounded = rounded
