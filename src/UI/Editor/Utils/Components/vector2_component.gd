tool
class_name Vector2Component
extends MarginContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal text_input_x_changed(value)
signal text_input_y_changed(value)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _TRANSPARENCY: Color = Color(1, 1, 1, 0.4)
const _NO_TRANSPARENCY: Color = Color(1, 1, 1)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var text: String = "Text" setget _set_text, _get_text
var prefix_x: String = "X" setget _set_prefix_x, _get_prefix_x
var prefix_y: String = "Y" setget _set_prefix_y, _get_prefix_y
var disable_input_x: bool = false setget _set_disable_input_x, _get_disable_input_x
var disable_input_y: bool = false setget _set_disable_input_y, _get_disable_input_y
var visible_text: bool = true setget _set_visible_text, _get_visible_text
var visible_prefix_x: bool = true setget _set_visible_prefix_x, _get_visible_prefix_x
var visible_prefix_y: bool = true setget _set_visible_prefix_y, _get_visible_prefix_y
var visible_placeholder_unit: bool = true setget _set_visible_placeholder_unit, _get_visible_placeholder_unit
var placeholder_unit: String = "px" setget _set_placeholder_unit, _get_placeholder_unit
var only_integer: bool = false
var only_transparency: bool = true

var _changes_input_x_notified: bool = true
var _changes_input_y_notified: bool = true
var _last_correct_input_x: String = ""
var _last_correct_input_y: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _text: Label = $HContainer/Text
onready var _input_x: LineEdit = $HContainer/VContainer/XInput
onready var _prefix_x: Label = $HContainer/VContainer/XInput/Prefix
onready var _input_y: LineEdit = $HContainer/VContainer/YInput
onready var _prefix_y: Label = $HContainer/VContainer/YInput/Prefix
onready var _units_x: Label = $HContainer/VContainer/XInput/Units
onready var _units_y: Label = $HContainer/VContainer/YInput/Units

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_text(text)
	_set_prefix_x(prefix_x)
	_set_prefix_y(prefix_y)
	_set_disable_input_x(disable_input_x)
	_set_disable_input_y(disable_input_y)
	_set_visible_text(visible_text)
	_set_visible_prefix_x(visible_prefix_x)
	_set_visible_prefix_y(visible_prefix_y)
	_set_placeholder_unit(placeholder_unit)

	if Engine.editor_hint:
		set_process(false)
		set_physics_process(false)
		set_process_input(false)
		return

func _get_property_list() -> Array:
	var properties: Array = [
		PropertyInfo.category("Vector2Component"),
		PropertyInfo.group("View"),
		PropertyInfo.var_string("text"),
		PropertyInfo.var_string("prefix_x"),
		PropertyInfo.var_string("prefix_y"),
		PropertyInfo.var_string("placeholder_unit"),
		PropertyInfo.group("Behaviour"),
		PropertyInfo.var_bool("only_integer"),
		PropertyInfo.var_bool("only_transparency"),
		PropertyInfo.var_bool("disable_input_x"),
		PropertyInfo.var_bool("disable_input_y"),
		PropertyInfo.var_bool("visible_text"),
		PropertyInfo.var_bool("visible_prefix_x"),
		PropertyInfo.var_bool("visible_prefix_y"),
		PropertyInfo.var_bool("visible_placeholder_unit"),
		]

	return properties

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_input_focus_entered(x_input: bool) -> void:
	if x_input:
		if not _input_x.editable:
			return
		if only_transparency:
			_prefix_x.modulate = _TRANSPARENCY
			_prefix_x.update()
		else:
			_prefix_x.visible = false
	else:
		if not _input_y.editable:
			return
		if only_transparency:
			_prefix_y.modulate = _TRANSPARENCY
		else:
			_prefix_y.visible = false

func _on_input_focus_exited(x_input: bool) -> void:
	if x_input:
		if only_transparency:
			_prefix_x.modulate = _NO_TRANSPARENCY
			_prefix_x.update()
		else:
			_prefix_x.visible = true

		if not _changes_input_x_notified:
			_changes_input_x_notified = true
			_emit_signal_int_float(_input_x.text, true)
	else:
		if only_transparency:
			_prefix_y.modulate = _NO_TRANSPARENCY
		else:
			_prefix_y.visible = true

		if not _changes_input_y_notified:
			_changes_input_y_notified = true
			_emit_signal_int_float(_input_y.text)

func _on_vector2_component_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if not _prefix_x.visible or _prefix_x.modulate == _TRANSPARENCY:
			if only_transparency:
				_prefix_x.modulate = _NO_TRANSPARENCY
			else:
				_prefix_x.visible = true
		if not _prefix_y.visible or _prefix_y.modulate == _TRANSPARENCY:
			if only_transparency:
				_prefix_y.modulate = _NO_TRANSPARENCY
			else:
				_prefix_y.visible = true

func _on_input_gui_input(event: InputEvent, x_input: bool) -> void:
	if event is InputEventMouseButton:
		if (_prefix_x.visible or _prefix_x.modulate == _NO_TRANSPARENCY) and x_input:
			if not _input_x.editable:
				return
			if only_transparency:
				_prefix_x.modulate = _TRANSPARENCY
			else:
				_prefix_x.visible = false
		if (_prefix_y.visible or _prefix_y.modulate == _NO_TRANSPARENCY) and not x_input:
			if not _input_y.editable:
				return
			if only_transparency:
				_prefix_y.modulate = _TRANSPARENCY
			else:
				_prefix_y.visible = false

func _on_input_text_changed(new_text: String, x_input: bool) -> void:
	if new_text.empty():
		return

	if new_text.begins_with("0") and new_text.length() > 1:
		if x_input:
			_input_x.text = _input_x.text.substr(1)
			_input_x.set_cursor_position(_input_x.text.length())
		else:
			_input_y.text = _input_y.text.substr(1)
			_input_y.set_cursor_position(_input_y.text.length())

	var is_valid: bool = new_text.is_valid_float()

	if only_integer:
		is_valid = new_text.is_valid_integer()

	if not is_valid:
		is_valid = "-" in new_text and new_text.length() < 2

	if not is_valid:
		is_valid = "0" == new_text

	if is_valid:
		if x_input:
			_last_correct_input_x = new_text
			_changes_input_x_notified = false
		else:
			_last_correct_input_y = new_text
			_changes_input_y_notified = false
	else:
		if x_input:
			_input_x.text = _last_correct_input_x
			_input_x.set_cursor_position(_input_x.text.length())
		else:
			_input_y.text = _last_correct_input_y
			_input_y.set_cursor_position(_input_y.text.length())

func _on_placeholder_visibility_changed(x_input: bool) -> void:
	if _input_x:
		if x_input:
			if _input_x.text.empty():
				_input_x.text = "0"
		else:
			if _input_y.text.empty():
				_input_y.text = "0"

func _on_input_text_entered(_new_text: String, x_input: bool) -> void:
	if x_input:
		if not _changes_input_x_notified:
			_changes_input_x_notified = true
			_emit_signal_int_float(_input_x.text, true)
	else:
		if not _changes_input_y_notified:
			_changes_input_y_notified = true
			_emit_signal_int_float(_input_y.text)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_value() -> Vector2:
	var to_result: Vector2 = Vector2.ZERO
	if only_integer:
		to_result = Vector2(int(_input_x.text), int(_input_y.text))
	else:
		to_result = Vector2(float(_input_x.text), float(_input_y.text))

	return to_result

func set_value(value: Vector2) -> void:
	_input_x.text = str(value.x)
	_input_y.text = str(value.y)


func _emit_signal_int_float(value: String, x_input: bool = false) -> void:
	var value_convert = float(value)

	if only_integer:
		value_convert = int(value)

	if x_input:
		emit_signal("text_input_x_changed", value_convert)
	else:
		emit_signal("text_input_y_changed", value_convert)

func _get_text() -> String:
	return text

func _set_text(new_text: String) -> void:
	text = new_text
	property_list_changed_notify()

	if _text:
		_text.text = text

func _get_prefix_x() -> String:
	return prefix_x

func _set_prefix_x(new_text: String) -> void:
	prefix_x = new_text.left(1).to_upper()

	if prefix_x.empty():
		prefix_x = "X"

	property_list_changed_notify()

	if _prefix_x:
		_prefix_x.text = prefix_x

func _get_prefix_y() -> String:
	return prefix_y

func _set_prefix_y(new_text: String) -> void:
	prefix_y = new_text.left(1).to_upper()

	if prefix_y.empty():
		prefix_y = "Y"

	property_list_changed_notify()

	if _prefix_y:
		_prefix_y.text = prefix_y

func _get_placeholder_unit() -> String:
	return placeholder_unit

func _set_placeholder_unit(new_text: String) -> void:
	placeholder_unit = new_text.left(2).to_lower()

	if placeholder_unit.empty():
		placeholder_unit = "px"

	property_list_changed_notify()

	if _units_x and _units_y:
		_units_x.text = placeholder_unit
		_units_y.text = placeholder_unit

func _get_disable_input_x() -> bool:
	return disable_input_x

func _set_disable_input_x(new_value: bool) -> void:
	disable_input_x = new_value

	property_list_changed_notify()

	if _input_x:
		_input_x.editable = not disable_input_x

func _get_disable_input_y() -> bool:
	return disable_input_y

func _set_disable_input_y(new_value: bool) -> void:
	disable_input_y = new_value

	property_list_changed_notify()

	if _input_y:
		_input_y.editable = not disable_input_y

func _get_visible_text() -> bool:
	return visible_text

func _set_visible_text(new_value: bool) -> void:
	visible_text = new_value

	property_list_changed_notify()

	if _text:
		_text.visible = visible_text

func _get_visible_prefix_x() -> bool:
	return visible_prefix_x

func _set_visible_prefix_x(new_value: bool) -> void:
	visible_prefix_x = new_value

	property_list_changed_notify()

	if _prefix_x:
		_prefix_x.visible = visible_prefix_x

func _get_visible_prefix_y() -> bool:
	return visible_prefix_y

func _set_visible_prefix_y(new_value: bool) -> void:
	visible_prefix_y = new_value

	property_list_changed_notify()

	if _prefix_y:
		_prefix_y.visible = visible_prefix_y

func _get_visible_placeholder_unit() -> bool:
	return visible_placeholder_unit

func _set_visible_placeholder_unit(new_value: bool) -> void:
	visible_placeholder_unit = new_value

	property_list_changed_notify()

	if _units_x and _units_y:
		_units_x.visible = visible_placeholder_unit
		_units_y.visible = visible_placeholder_unit
