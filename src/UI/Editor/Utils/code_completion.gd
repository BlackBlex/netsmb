class_name CodeCompletion
extends HBoxContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
#Array of Keyword
var _completion_current: Array = []
var _is_position_changed: bool = false

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _content: VBoxContainer = $Scroll/Content
onready var _complement: Panel = $Complement
onready var _hint: Label = $Complement/Content/Hint
onready var _usage: Label = $Complement/Content/Usage
#_editor_script: EditorScripting
onready var _editor_script = get_parent()

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("action_esc") and _any_item_has_focus():
		_clear_completions()
		_hint.text = ""
		_usage.text = ""
		visible = false
		_editor_script.grab_focus()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_button_pressed(index: int, index_completion: int) -> void:
	var completion: String = _completion_current[index].completion[index_completion]
	var lines: PoolStringArray = _editor_script.get_current_line().trim_prefix("").split(" ")
	lines.invert()
	var line: String = lines[0]

	if line:
		if line.ends_with(" ") and completion.begins_with(" "):
			completion = completion.trim_prefix("")

	_editor_script.grab_focus_editor()

	completion = completion.replace(line, "")
	_editor_script.insert_text(completion)

	_clear_completions()
	_completion_current.clear()

	_hint.text = ""
	_usage.text = ""
	visible = false

	_editor_script.check_if_line_contains_parameters()

func _on_button_focus_entered(index: int):
	var completion: Keyword = _completion_current[index]

	if not completion.hint.empty():
		_hint.text = "%s\n%s" % [tr("KEY"), tr(completion.hint)]

	if not completion.usage.empty():
		_usage.text = "%s\n%s" % [tr("KEY"), tr(completion.usage)]

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_width_content() -> float:
	return _complement.rect_size.x + (_content.rect_size.x / 1.5)

func show_completion(position: Vector2, change_pos: bool = false) -> void:
	rect_position = position
	_is_position_changed = change_pos
	_change_position()
	_load_completion()


func _any_item_has_focus() -> bool:
	var to_result: bool = false
	for button in Arrays.of_type_native(_content.get_children(), Button):
		if button.has_focus():
			to_result = true
			break

	return to_result

func _change_position() -> void:
	var position_child: int = 0 if _is_position_changed else 1
	move_child(_complement, position_child)

func _clear_completions() -> void:
	NodeExt.clear_children(_content)

func _create_button(text: String, index: int, index_completion: int) -> Button:
	var to_result = Button.new()
	to_result.flat = true
	to_result.clip_text = true
	to_result.align = Button.ALIGN_LEFT
	to_result.text = text

	to_result.connect("focus_entered", self, "_on_button_focus_entered", [index])
	to_result.connect("pressed", self, "_on_button_pressed", [index, index_completion])

	return to_result

func _create_dummy_button() -> Button:
	var to_result = Button.new()
	to_result.flat = true
	to_result.clip_text = true
	to_result.disabled = true
	to_result.focus_mode = Control.FOCUS_NONE
	to_result.align = Button.ALIGN_LEFT
	to_result.text = "===%s===" % tr("KEY_COMPLETION")

	return to_result

func _load_completion() -> void:
	_clear_completions()
	_completion_current.clear()

	var line: String = _editor_script.get_current_line().strip_edges()

	var regex: RegEx = RegEx.new()
	#UNUSED: var list_completion: PoolStringArray = []
	var is_success: bool = false
	var completions: Dictionary = GlobalData.object_database.keywords

	_content.add_child(_create_dummy_button())

	for completion in completions.keys():
		is_success = false

		if completion.begins_with("reg:"):
			var _err = regex.compile(completion.replace("reg:", ""))
			is_success = regex.search(line) != null
		else:
			var lines: PoolStringArray = line.split(" ")
			lines.invert()
			var line_last: String = lines[0]

			if line_last:
				is_success = completion.begins_with(line_last)

		if is_success:
			_completion_current.append(completions[completion])
			var index: int = _completion_current.size() - 1
			var index_completion: int = 0

			for completion_option in completions[completion].completion:
				var button: Button = _create_button(completion_option, index, index_completion)
				_content.add_child(button)
				index_completion += 1

	if _content.get_child_count() > 1:
		Arrays.of_type_native(_content.get_children(), Button)[0].grab_focus()
		visible = true
	elif _content.get_child_count() == 1:
		visible = false
