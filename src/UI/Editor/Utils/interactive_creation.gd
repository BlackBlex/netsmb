extends WindowDialog

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _position_mapped: Dictionary = {}
var _textures: Dictionary = {}
var _current_scale: Vector2 = Vector2.ONE
var _original_size: Vector2 = Vector2.ZERO
var _rect_original_size: Vector2 = (Vector2.ONE * GlobalData.BLOCK_SIZE)

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var block_viewer: Control = get_parent()
onready var _preview: TextureRect = $Background/Content/ScrollContainer/Preview
onready var _current_block_preview: Panel = $Background/Content/HBoxContainer/Content/Panel/CurrentBlock
onready var _current_block_ground: OptionComponent = $Background/Content/HBoxContainer/VBoxContainer/CurrentBlockGround
onready var _rect_position: Vector2Component = $Background/Content/HBoxContainer/VBoxContainer/RectPosition
onready var _grid_size: Vector2Component = $Background/Content/HBoxContainer/Control/GridSize
onready var _grid_spacing: Vector2Component = $Background/Content/HBoxContainer/Control/GridSpacing
onready var _grid: Control = $Background/Content/ScrollContainer/Preview/GridSelectable
onready var _file_selector: FileSelectDialog = $FileSelectDialog
onready var _save_mapping: WindowDialog = $SaveMapping
onready var _mapping_filename: FieldComponent = $SaveMapping/Filename

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_current_block_ground_option_changed(value: String) -> void:
	var node_block: Panel = null

	for content in block_viewer._contents:
		if content.has_node(value):
			node_block = content.get_node(value)

	if node_block:
		_current_block_preview.get_node("C").visible = node_block.has_node("C")
		_current_block_preview.get_node("C2").visible = node_block.has_node("C2")
		_current_block_preview.get_node("C3").visible = node_block.has_node("C3")
		_current_block_preview.get_node("C4").visible = node_block.has_node("C4")
		_current_block_preview.set("custom_styles/panel", node_block.get("custom_styles/panel"))

	_current_block_preview.set_frames(_textures[value])

func _on_rect_position_text_input_changed(_value: int) -> void:
	_grid.update_rect(_rect_position.get_value())

func _on_zoom_out_pressed() -> void:
	if _current_scale > Vector2.ONE:
		_current_scale -= Vector2(0.5, 0.5) # If it is .25 there are problems in the grid
		_update_previewer()

func _on_zoom_in_pressed() -> void:
	_current_scale += Vector2(0.5, 0.5) # If it is .25 there are problems in the grid
	_update_previewer()

func _on_back_pressed() -> void:
	if _current_block_ground.selected > 0:
		var back: int = _current_block_ground.selected - 1
		_current_block_ground.selected = back
		_on_current_block_ground_option_changed(_current_block_ground.get_item_selected())

func _on_next_pressed() -> void:
	if _current_block_ground.selected < (_current_block_ground.items.size() - 1):
		var next: int = _current_block_ground.selected + 1
		_current_block_ground.selected = next
		_on_current_block_ground_option_changed(_current_block_ground.get_item_selected())

func _on_apply_rect_pressed() -> void:
	var current_selected: String = _current_block_ground.get_item_selected()
	var position: Vector2 = (_rect_position.get_value() * _grid_size.get_value()) + (_rect_position.get_value() * Vector2(2, 2))

	_textures[current_selected] = block_viewer.build_texture([_build_image(position)])

	if not _position_mapped.has(current_selected):
		_position_mapped[current_selected] = Vector2.ZERO

	_position_mapped[current_selected] = _rect_position.get_value()

	_current_block_preview.set_frames(_textures[current_selected])
	block_viewer.set_texture_to(current_selected, _textures[current_selected])

func _on_grid_text_input_changed(_value: int) -> void:
	_update_previewer()

func _on_grid_selectable_on_cell_selected(cell: Vector2) -> void:
	_rect_position.set_value(cell)
	_on_apply_rect_pressed()

func _on_load_mapped_pressed() -> void:
	_file_selector.popup_centered()

func _on_save_mapped_pressed() -> void:
	_save_mapping.popup_centered()

func _on_file_select_dialog_file_selected(filename: String) -> void:
	var path: String = A.Paths.data.plus_file("mapping").plus_file("%s.txt" % filename)
	if A.file_loader.file_exists(path):
		_position_mapped = parse_json(Utility.load_file(path))

	if not _position_mapped.empty():
		_grid_spacing.set_value(str2var("Vector2%s" % _position_mapped["grid_spacing"]))
		_update_previewer()

		for block in _position_mapped:
			if "grid_spacing" in block:
				continue

			_current_block_ground.set_item_selected(block)
			_on_current_block_ground_option_changed(_current_block_ground.get_item_selected())
			yield(get_tree().create_timer(0.2), "timeout")
			_on_grid_selectable_on_cell_selected(str2var("Vector2%s" % _position_mapped[block]))

func _on_save_mapping_confirmed() -> void:
	var path: String = A.Paths.data.plus_file("mapping").plus_file("%s.txt" % _mapping_filename.get_value())
	var _err = Utility.save_file(path, to_json(_position_mapped))


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup() -> void:
	var blocks: Array = []
	_textures.clear()
	_position_mapped.clear()
	for content in block_viewer._contents:
		for node_block in content.get_children():
			if "EMPTY" in node_block.name:
				continue

			blocks.append(node_block.name)
			_textures[node_block.name] = null
	_current_block_ground.items = blocks

	_current_block_ground.selected = 0
	_on_current_block_ground_option_changed(_current_block_ground.get_item_selected())

	_grid_size.set_value(_rect_original_size)
	_grid_spacing.set_value(Vector2.ZERO)
	_preview.texture = null
	_current_scale = Vector2.ONE
	_update_previewer()

	_file_selector.load_files()
	_save_mapping.get_ok().text = tr("KEY_SAVE")

func set_texture(block_image: Texture) -> void:
	_preview.texture = block_image
	_original_size = block_image.get_size()
	_current_scale = Vector2.ONE

	_grid.setup(_original_size, _grid_size.get_value(), _grid_spacing.get_value())


func _build_image(position: Vector2) -> ImageTexture:
	var image_new: Image = Image.new()
	image_new.create(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE, false, Image.FORMAT_RGBA8)
	image_new.blit_rect(_preview.texture.get_data(), Rect2(position, Vector2(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE)), Vector2.ZERO)

	var image_texture: ImageTexture = ImageTexture.new()
	image_texture.create_from_image(image_new, 0)
	image_texture.lossy_quality = 0

	return image_texture

func _update_previewer() -> void:
	_preview.rect_min_size = _original_size * _current_scale
	_preview.rect_size = _preview.rect_min_size
	_on_rect_position_text_input_changed(-1)
	_grid.setup(_preview.rect_min_size, _grid_size.get_value() * _current_scale, _grid_spacing.get_value() * _current_scale)

	if not _position_mapped.has("grid_spacing"):
		_position_mapped["grid_spacing"] = Vector2.ZERO

	_position_mapped["grid_spacing"] = _grid_spacing.get_value()
