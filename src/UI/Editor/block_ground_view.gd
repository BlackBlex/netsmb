extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum MENU_ITEM {ADD, ADD_FROM, REMOVE}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var popup: PopupMenu = PopupMenu.new()
var current_block: Panel = null

var _history_actions: Array = []
var _history_index: int = -1
var _contents: Array = []
var _is_interactive_creation_available: bool = false
var _path: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _content_sec1: GridContainer = $Viewer/Control/Section1/Content
onready var _content_sec2: GridContainer = $Viewer/Control/Section2/Content
onready var _content_sec3: GridContainer = $Viewer/Control/Section3/Content
onready var _content_sec4: GridContainer = $Viewer/Control2/Section4/Content
onready var _content_sec5: GridContainer = $Viewer/Control2/Section5/Content
onready var _content_sec6: GridContainer = $Viewer/Control2/Section6/Content
onready var _history: ItemList = $Viewer/MoreActions/History
onready var _undo_button: Button = $Viewer/Actions/Undo
onready var _redo_button: Button = $Viewer/Actions/Redo
onready var _open_interactive: Button = $Viewer/Actions/OpenInteractive
onready var _viewer: VBoxContainer = $Viewer
onready var _interactive_creation: WindowDialog = $InteractiveCreation

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_contents = [_content_sec1, _content_sec2, _content_sec3, _content_sec4, _content_sec5, _content_sec6]

	popup.add_item("Add single texture", MENU_ITEM.ADD)
	popup.add_item("Add from texture", MENU_ITEM.ADD_FROM)
	popup.add_item("Remove texture", MENU_ITEM.REMOVE)

	var _err = popup.connect("id_pressed", self, "_on_popup_id_pressed")
	_err = popup.connect("popup_hide", self, "_on_popup_hide")
	add_child(popup)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_popup_id_pressed(id: int) -> void:
	if current_block:
		current_block.on_popup_id_pressed(id)

func _on_popup_hide() -> void:
	current_block = null

func _on_undo_pressed() -> void:
	if _history_index > -1:
		var action: UndoRedo = _history_actions[_history_index]
		if action.undo():
			_history.select(_history_index)
			_history_index -= 1

			if _history_index == -1:
				_history.unselectall()

		_update_undo_redo_buttons()

func _on_redo_pressed() -> void:
	if _history_index < (_history_actions.size() - 1):
		_history_index += 1
		var action: UndoRedo = _history_actions[_history_index]
		var _err = action.redo()
		_history.select(_history_index)

		_update_undo_redo_buttons()

func _on_history_item_activated(_index: int) -> void:
	# if _history_index > index:
	# 	var distance: int = _history_index - index
	# 	for i in range(distance):
	# 		var action: UndoRedo = _history_actions[index]
	# 		if action.undo():
	# 			_history_index = index - 1
	# elif _history_index < index:
	# 	var action: UndoRedo = _history_actions[_history_index]
	# 	var _err = action.redo()
	# 	_history.select(_history_index)

	_update_undo_redo_buttons()

func _on_open_interactive_pressed() -> void:
	_interactive_creation.popup_centered()

func _on_save_pressed() -> void:
	save_file()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func build_texture(textures: Array) -> SpriteFrames:
	var to_result: SpriteFrames = SpriteFrames.new()

	for texture in textures:
		to_result.add_frame("default", texture)

	to_result.set_animation_loop("default", to_result.get_frame_count("default") > 1)
	to_result.set_animation_speed("default", GlobalData.FPS_ANIMATION)

	return to_result

func get_undo_redo() -> UndoRedo:
	var to_result: UndoRedo = UndoRedo.new()
	_history_actions.append(to_result)

	_history_index = _history_actions.size() - 1

	_update_undo_redo_buttons()
	return to_result

func interactive_creation(block_image: Texture, path: String) -> void:
	_interactive_creation.popup_centered()
	_interactive_creation.set_texture(block_image)
	_path = path.replace(".png", ".tex")

	_is_interactive_creation_available = true
	_open_interactive.visible = true

func publish_change() -> void:
	var action: UndoRedo = _history_actions.back()
	action.commit_action()

	_history.add_item(action.get_current_action_name())
	# _history.set_item_metadata(_history.get_item_count(), action)

func save_file() -> void:
	var new_image: Image = Image.new()
	new_image.create(GlobalData.BLOCK_SIZE * 17, GlobalData.BLOCK_SIZE * 3, false, Image.FORMAT_RGBA8)

	var image_texture: ImageTexture = null
	var split_name: PoolStringArray = []

	for content in _contents:
		for node_block in content.get_children():
			if "EMPTY" in node_block.name:
				continue

			var texture: Texture = node_block.get_frames()[0]
			if texture:
				split_name = node_block.name.split(",")
				var correct_name: String = "%s,%s" % [split_name[1], split_name[0]]

				var position: Vector2 = str2var("Vector2(%s)" % correct_name)
				position *= GlobalData.BLOCK_SIZE
				new_image.blit_rect(texture.get_data(), Rect2(Vector2.ZERO, texture.get_size()), position)
			else:
				continue

	image_texture = ImageTexture.new()
	image_texture.create_from_image(new_image, 0)
	image_texture.lossy_quality = 0

	var _err = Resources.save(_path, image_texture, ResourceSaver.FLAG_COMPRESS)

	if OS.has_feature("debug"):
		_err = Resources.save(_path.replace(".tex", "_new.png"), image_texture, ResourceSaver.FLAG_COMPRESS)

	if _err == OK:
		Alert.show_information("File save in: %s" % _path)

		if _is_interactive_creation_available:
			var filename: String = _path.get_file().replace(".%s" % _path.get_extension(), "")
			_path = _path.replace("/%s.tex" % filename, "")
			var style: String = _path.substr(_path.find_last("/") + 1)
			Event.publish(self, A.EventList.editor.block_ground_added, [filename, style])

func setup() -> void:
	_interactive_creation.setup()

	for content in _contents:
		for node_block in content.get_children():
			if "EMPTY" in node_block.name:
				continue

			node_block.set_frames(null)

func set_texture_to(node_name: String, frames: SpriteFrames) -> void:
	for content in _contents:
		if content.has_node(node_name):
			var node_block: Panel = content.get_node(node_name)

			node_block.set_frames(frames)
			break

func set_texture(block_image: Texture, path: String) -> void:
	_viewer.visible = true
	_interactive_creation.hide()
	_is_interactive_creation_available = false
	_path = path

	var image_new: Image = null
	var image_texture: ImageTexture = null
	var split_name: PoolStringArray = []

	for content in _contents:
		for node_block in content.get_children():
			if "EMPTY" in node_block.name:
				continue

			split_name = node_block.name.split(",")
			var correct_name: String = "%s,%s" % [split_name[1], split_name[0]]

			var position: Vector2 = str2var("Vector2(%s)" % correct_name)
			position *= GlobalData.BLOCK_SIZE
			image_new = Image.new()
			image_new.create(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE, false, Image.FORMAT_RGBA8)

			image_new.blit_rect(block_image.get_data(), Rect2(position, Vector2(GlobalData.BLOCK_SIZE, GlobalData.BLOCK_SIZE)), Vector2.ZERO)

			image_texture = ImageTexture.new()
			image_texture.create_from_image(image_new, 0)
			image_texture.lossy_quality = 0
			node_block.set_frames(build_texture([image_texture]))


func _update_undo_redo_buttons() -> void:
	_undo_button.disabled = _history_index == -1
	_redo_button.disabled = _history_index == (_history_actions.size() - 1)
