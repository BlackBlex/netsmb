class_name EditorScripting
extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _COLOR_CLASS = Color("e5c06d")
const _COLOR_KEYWORD = Color("c675d7")
const _COLOR_QUOTES = Color("94ab58")
const _COLOR_COMMENTS = Color("959595")
const _COLOR_OWN_VARIABLE = Color("CE7E16")
const _COLOR_DATA_TYPE = Color("7AC227")

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
#creator: Creator
var creator = null

var _script_current: GDScript = null
var _is_new_line: bool = false
var _on_replace_mode: bool = false
var _is_edited: bool = false
var _error: ErrorScript = ErrorScript.new()
var _error_regex: RegEx = RegEx.new()
var _path_temporal: String = ""
var _path_current: String = ""
var _file_current: String = "new.gd"
var _parameter_to_replace: int = 0
var _font_editor: DynamicFont = null
var _pattern_error: String = "(?:At\\: built-in\\:)[\\s]?(?<line>.*)(?:\\:GDScript\\:\\:reload\\(\\)\\s\\-\\s)(?<error>.*)"
var _path_log_compiler: String = ProjectSettings.get("logging/file_logging/log_path")
var _keywords: Array = [
	#Variable declaration
	"var", "const", "enum",
	#Condicional
	"if", "elif", "else", "match", "is",
	#Loop
	"for", "while", "break", "continue",
	#Creation
	"extends", "class", "func", "static",
	#Misc
	"pass", "return", "as", "self", "load_resource", "PI", "TAU", "INF", "NAN",
	#Datatypes
	"null", "int", "bool", "float", "String", "Vector2", "Rect2", "Color", "Array", "Dictionary", "void"
]
var _class_allowed: Array = [
	#Behaviour
	"BaseBehaviour", "BasicMovement",
	#StateMachine
	"State", "NpcState", "NpcIdle", "NpcMove",
	#Events
	"BaseEvent", "EventList",
	#Misc
	"BaseScript", "BaseHUD"
]
var _own_variables: Array = [
	"_HUD", "_ENTITY", "_LEVEL", "_TARGET", "_OWNER"
]
var _data_types: Array = [
	"null", "bool", "int", "float", "Vector2", "String", "Rect2", "Color", "Array", "Dictionary"
]
var _black_keywords: PoolStringArray = []

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _editor_script: TextEdit = $Editor
onready var _error_highlight: Panel = $Editor/ErrorHighlight
onready var _status: Label = $Editor/Status
onready var _status_current_file: Label = $Editor/Status/CurrentFile
#_code_completion: CodeCompletion
onready var _code_completion = $CodeCompletion #
onready var _save_load_dialog: FileDialog = $SaveLoadDialog

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_script_current = GDScript.new()
	_error.error = "Ok"
	var _err = _error_regex.compile(_pattern_error)
	_font_editor = _editor_script.get("custom_fonts/font")

	_err = _save_load_dialog.connect("file_selected", self, "_on_save_load_dialog_file_selected")

	var path_black_list: String = A.Paths.data.plus_file("blacklist.json")

	if A.file_loader.open(path_black_list, File.READ) == OK:
		_black_keywords = JSONConvert.deserialize(A.file_loader.get_as_text())
		A.file_loader.close()

	_add_keywords_highlighting()

func _process(_delta: float) -> void:
	_update_status_bar()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_confirmed_dialog_load() -> void:
	_file_current = _path_temporal.get_file()
	_path_current = _path_temporal.replace(_file_current, "")

	_script_current = Utility.load_script(_path_temporal)
	_editor_script.text = _script_current.source_code

	_is_new_line = true
	_on_editor_text_changed()
	_is_edited = false

func _on_editor_gui_input(event: InputEvent) -> void:
	if event is InputEventKey:
		_is_new_line = event.scancode == KEY_ENTER and not _on_replace_mode

	if event.is_action_pressed("action_completion") and not _on_replace_mode:
		_show_code_completion()
	elif event.is_action_pressed("action_completion") and _on_replace_mode:
		check_if_line_contains_parameters()

	if event.is_action_pressed("action_save"):
		save_file()

	if event.is_action_pressed("action_save_as"):
		clean_path()
		save_file()

	if event.is_action_pressed("action_open"):
		load_file()

func _on_editor_text_changed() -> void:
	_script_current.source_code = _editor_script.text

	if _is_new_line:
		_is_new_line = false

		var black_word: String = ""
		var has_black_keyword: bool = false

		for word in _black_keywords:
			if word in _script_current.source_code:
				black_word = word
				has_black_keyword = true
				break

		if has_black_keyword:
			_error.line = _editor_script.search(black_word, TextEdit.SEARCH_MATCH_CASE, 0, 0)[TextEdit.SEARCH_RESULT_LINE] + 1
			_error.error = "Parse error - Unknown word: %s" % black_word
		else:
			_parse_script()

	_is_edited = true

func _on_save_load_dialog_file_selected(path: String) -> void:
	if _save_load_dialog.mode == FileDialog.MODE_OPEN_FILE:
		_path_temporal = path
		if _is_edited:
			Alert.show_confirm_dialog(tr("KEY_CONFIRM"), tr("KEY_CREATOR_CHANGES_PENDING"), Instanceator.make_funcref_ext(self, "_on_confirmed_dialog"))
		else:
			_on_confirmed_dialog_load()
	elif _save_load_dialog.mode == FileDialog.MODE_SAVE_FILE:
		var error: int = Utility.save_script(path, _script_current)

		_is_edited = error != OK

		if error == OK:
			Alert.show_information(tr("KEY_CHANGES_SAVED"))

			if "behaviour" in path:
				creator.load_behaviours()

			_file_current = path.get_file()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func check_if_line_contains_parameters() -> void:
	var current_line: int = _editor_script.cursor_get_line()

	var parameter: String = "{%d}" % _parameter_to_replace
	var parameters: PoolIntArray = _editor_script.search(parameter, TextEdit.SEARCH_BACKWARDS, current_line, 0)

	_on_replace_mode = not parameters.empty()

	if _on_replace_mode:
		var line: int = parameters[TextEdit.SEARCH_RESULT_LINE]
		var column: int = parameters[TextEdit.SEARCH_RESULT_COLUMN]

		_editor_script.select(line, column, line, column + parameter.length())
		_parameter_to_replace += 1
	else:
		_parameter_to_replace = 0

func clean_path() -> void:
	_path_current = ""

func get_current_line() -> String:
	var cursor_pos: Vector2 = get_cursor_pos()
	var line: String = _editor_script.get_line(int(cursor_pos.y))
	return line.substr(0, int(cursor_pos.x))

func get_cursor_pos() -> Vector2:
	return Vector2(_editor_script.cursor_get_column(), _editor_script.cursor_get_line())

func grab_focus_editor() -> void:
	_editor_script.grab_focus()

func has_focus() -> bool:
	return _editor_script.has_focus()

func insert_text(text: String) -> void:
	_editor_script.insert_text_at_cursor(text)

func load_file() -> void:
	_save_load_dialog.mode = FileDialog.MODE_OPEN_FILE
	_save_load_dialog.current_file = ""
	_save_load_dialog.show()

func save_file() -> void:
	if _error.line != 0 or _editor_script.text.empty():
		Alert.show_alert(tr("KEY_SCRIPT_ERROR"))
		return

	_save_load_dialog.mode = FileDialog.MODE_SAVE_FILE

	if not _path_current.empty():
		_on_save_load_dialog_file_selected(_path_current.plus_file(_file_current))
	else:
		_save_load_dialog.current_file = _file_current
		_save_load_dialog.show()

func set_text(text: String) -> void:
	_editor_script.text = text
	_is_new_line = true
	_on_editor_text_changed()


func _add_keywords_highlighting() -> void:
	_editor_script.add_color_region("\"", "\"", _COLOR_QUOTES)
	_editor_script.add_color_region("'", "'", _COLOR_QUOTES)
	_editor_script.add_color_region("#", "\n", _COLOR_COMMENTS, true)
	_editor_script.add_color_region("#", "\r", _COLOR_COMMENTS, true)
	_editor_script.add_color_region("#", "\r\n", _COLOR_COMMENTS, true)

	for class_allowed in _class_allowed:
		_editor_script.add_keyword_color(class_allowed, _COLOR_CLASS)

	for own_variable in _own_variables:
		_editor_script.add_keyword_color(own_variable, _COLOR_OWN_VARIABLE)

	for data_type in _data_types:
		_editor_script.add_keyword_color(data_type, _COLOR_DATA_TYPE)

	for keyword in _keywords:
		_editor_script.add_keyword_color(keyword, _COLOR_KEYWORD)

func _parse_script() -> void:
	var _err = Utility.save_file(_path_log_compiler, "")
	var error = _script_current.reload()

	if error != OK:
		var lines: PoolStringArray = Utility.load_file(_path_log_compiler).split("\n")
		var results: Arrays = null
		var result_error: RegExMatch = null

		for line in lines:
			result_error = _error_regex.search(line)
			if result_error is RegExMatch:
				results.append(result_error)

		result_error = results.pop_front()

		if result_error:
			_error.line = int(result_error.get_string("line"))
			_error.error = result_error.get_string("error")
	else:
		_error.line = 0
		_error.error = "Ok"

func _show_code_completion() -> void:
	var position_current: Vector2 = Vector2(0, _editor_script.cursor_get_line() + 1)
	var font_size = _font_editor.get_string_size(get_current_line().replace("\t", "    ")) + Vector2(0, 4)

	position_current *= font_size
	var position: Vector2 = position_current
	position.y += 2
	position.x += 55 + font_size.x

	var editor_size = rect_size - _code_completion.rect_size

	if position.y > editor_size.y:
		position.y -= font_size * 5
		position.y -= 14

	var change_pos: bool = position.x > editor_size.x

	if change_pos:
		position.x -= _code_completion.get_width_content()

	_code_completion.show_completion(position, change_pos)

func _update_status_bar() -> void:
	_status.text = "(%d,%d): %s" % [_editor_script.cursor_get_column() + 1, _editor_script.cursor_get_line() + 1, _error.error]

	if _error.line != 0:
		_error_highlight.rect_position = Vector2(0, 2 + (_error_highlight.rect_size.y * (_error.line - 1)))
		_error_highlight.visible = true
	else:
		if _error_highlight.visible:
			_error_highlight.visible = false

	_status_current_file.text = _file_current

	if _is_edited:
		_status_current_file.text += "(*)"

##############################################################################
#
#----------------------------------------------------------------------------
#	INNER CLASS
#----------------------------------------------------------------------------
#
##############################################################################
class ErrorScript:
	var line: int = -1
	var error: String = ""
