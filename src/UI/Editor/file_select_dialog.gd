class_name FileSelectDialog
extends WindowDialog

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal file_selected(file_name)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################
enum TypeSelect {BEHAVIOUR, EVENT, MAPPING, OTHER}

##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
export(TypeSelect) var type = TypeSelect.BEHAVIOUR

##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _file_hint: String = tr("KEY_DOUBLE_CLICK_SELECT")

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _normal_file_option: StyleBoxFlat = Resources.get_theme("label/black_a60") as StyleBoxFlat
onready var _hover_option: StyleBoxFlat = Resources.get_theme("label/white_a40") as StyleBoxFlat
onready var _content: VBoxContainer = $Scroll/Content

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_file_option_mouse_entered(label: Label) -> void:
	label.set("custom_styles/normal", _hover_option)

func _on_file_option_mouse_exited(label: Label) -> void:
	label.set("custom_styles/normal", _normal_file_option)

func _on_file_option_gui_input(event: InputEvent, label: Label) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.doubleclick:
			emit_signal("file_selected", label.get_meta("file"))
			hide()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func load_files() -> void:
	var path_asset: String = A.Paths.events
	var path_level: String = A.Paths.level_events

	NodeExt.clear_children(_content)

	if type < TypeSelect.MAPPING:
		_content.add_child(_create_file_option("", "==Local==", "", true))

	if type == TypeSelect.BEHAVIOUR:
		_content.add_child(_create_file_option("no_movement", "no_movement", "There isn't movement"))
		path_asset = A.Paths.behaviours
		path_level = A.Paths.level_behaviours
	elif type == TypeSelect.MAPPING:
		path_asset = A.Paths.data.plus_file("mapping")
		path_level = ""

	var asset_files: Array = Utility.get_files(path_asset)
	var directory_file: Directory = Directory.new()

	if not asset_files.empty():
		for file in asset_files:
			var file_name: String = file.replace(path_asset + "/", "").get_basename()
			var script_ins: GDScript = null

			if type < TypeSelect.MAPPING:
				script_ins = Utility.load_script(file)

			if type == TypeSelect.BEHAVIOUR:
				var instance: BaseBehaviour = script_ins.new() as BaseBehaviour
				_content.add_child(_create_file_option(file_name, file_name, instance.description))
			elif type == TypeSelect.EVENT:
				var instance: BaseEvent = script_ins.new() as BaseEvent
				_content.add_child(_create_file_option(file_name, "%s (%s)" % [instance.name, file_name], instance.description))
			elif type == TypeSelect.MAPPING:
				_content.add_child(_create_file_option(file_name, file_name, ""))

	if not path_level.empty() and  directory_file.open(path_level) == OK:
		_content.add_child(_create_file_option("", "==Level==", "", true))

		var _err = directory_file.list_dir_begin(true)
		var current_file: String = directory_file.get_next()

		while not current_file.empty():
			if not directory_file.current_is_dir():
				var file_name: String = current_file.get_basename()
				var script_ins: GDScript = Utility.load_script(path_level.plus_file(current_file))

				if type == TypeSelect.BEHAVIOUR:
					var instance: BaseBehaviour = script_ins.new() as BaseBehaviour
					_content.add_child(_create_file_option("level:%s" % file_name, file_name, instance.description))
				elif type == TypeSelect.EVENT:
					var instance: BaseEvent = script_ins.new() as BaseEvent
					_content.add_child(_create_file_option("level:%s" % file_name, instance.name, instance.description))

			current_file = directory_file.get_next()

		directory_file.list_dir_end()


func _create_file_option(file_name: String, name: String, description: String, is_disabled: bool = false) -> Label:
	var to_result = Label.new()

	if not description.empty():
		to_result.text = "%s\n%s" % [name, description]
	else:
		to_result.text = name

	to_result.autowrap = true
	to_result.align = Label.ALIGN_FILL
	to_result.set("custom_styles/normal", _normal_file_option)

	if not is_disabled:
		to_result.set_meta("file", file_name)
		to_result.mouse_filter = Control.MOUSE_FILTER_STOP
		to_result.mouse_default_cursor_shape = Control.CURSOR_POINTING_HAND
		to_result.hint_tooltip = _file_hint % name

		to_result.connect("mouse_entered", self, "_on_file_option_mouse_entered", [to_result])
		to_result.connect("mouse_exited", self, "_on_file_option_mouse_exited", [to_result])
		to_result.connect("gui_input", self, "_on_file_option_gui_input", [to_result])
	else:
		to_result.align = Label.ALIGN_CENTER

	return to_result
