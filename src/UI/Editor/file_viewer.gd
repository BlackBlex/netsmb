class_name FileViewer
extends VBoxContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################
signal file_selected(file_name)

##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _FILE_ICON: Texture = preload("res://assets/images/icons/file.tex")
const _DIRECTORY_ICON: Texture = preload("res://assets/images/icons/action-open.tex")

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var current_path: String = ""

var _file_root: TreeItem = null
var _current_file: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _file_content: Tree = $Files

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	setup()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_BackButton_pressed() -> void:
	if current_path.ends_with("/"):
		current_path = current_path.substr(0, current_path.length() - 1)

	var last_slash: int = current_path.find_last("/")
	var previus_path: String = current_path.substr(0, last_slash)
	_clean_files()
	_load_files(previus_path)

func _on_RefreshButton_pressed() -> void:
	_clean_files()
	_load_files(current_path)

func _on_Files_item_activated() -> void:
	var is_directory: bool = _file_content.get_selected().has_meta("dir")
	_current_file = _file_content.get_selected().get_text(0)

	if is_directory:
		_clean_files()
		_load_files(current_path.plus_file(_current_file))
	else:
		emit_signal("file_selected", _current_file)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func setup() -> void:
	_current_file = ""

	_clean_files()
	_load_files(A.Paths.game)


func _clean_files() -> void:
	_file_content.clear()
	_file_root = _file_content.create_item()
	_file_root.set_text(0, tr("KEY_FILES"))

func _load_files(path: String) -> void:
	current_path = path
	var directory: Directory = Directory.new()
	# Array of string
	var files: Array = []
	# Array of string
	var directories: Array = []

	if directory.open(path) == OK:
		var _err = directory.list_dir_begin(false, true)
		var current_file: String = directory.get_next()

		while not current_file.empty():
			if (directory.current_is_dir() or current_file.ends_with(".png") or current_file.ends_with(".tex")) and current_file.nocasecmp_to(".") != 0:
				if directory.current_is_dir():
					directories.append(current_file)
				else:
					files.append(current_file)

			current_file = directory.get_next()

		directory.list_dir_end()

	files.sort()
	directories.sort()
	var item: TreeItem = null

	for dir in directories:
		item = _file_content.create_item(_file_root)
		item.set_text(0, dir)
		item.set_icon(0, _DIRECTORY_ICON)
		item.set_meta("dir", 1)

	for file in files:
		item = _file_content.create_item(_file_root)
		item.set_text(0, file)
		var file_path: String = path.plus_file(file)

		if file.ends_with(".png"):
			var image: Image = Image.new()
			var _err = image.load(file_path)
			image.resize(32, 32, Image.INTERPOLATE_NEAREST)
			var texture: ImageTexture = ImageTexture.new()
			texture.create_from_image(image)
			item.set_icon(0, texture)
		else:
			var texture: Texture = load(file_path)
			var image: Image = Image.new()
			image.copy_from(texture.get_data())
			image.resize(32, 32, Image.INTERPOLATE_NEAREST)
			var image_texture: ImageTexture = ImageTexture.new()
			image_texture.create_from_image(image)
			item.set_icon(0, image_texture)
