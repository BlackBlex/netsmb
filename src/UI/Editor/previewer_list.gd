class_name PreviewerList
extends Panel

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _COLOR_OPTION_VISIBLE = Color(1, 1, 1, 1)
const _COLOR_OPTION_NO_VISIBLE = Color(1, 1, 1, 0)

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var root: String = ""
var category: String = ""
var subcategory: String = ""
var type: int = Enums.DataInfoType.NONE
var size_panel: Vector2 = Vector2(5, 4)

#Array of string
var _available_textures: Array = []
var _current_position: int = -1
var _current_string: String = ""
var _spriteframes: SpriteFrames = SpriteFrames.new()
var _can_more: bool = false
var _can_less: bool = false

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _timer: Timer = $Timer
onready var _animation: AnimatedSprite = $Sprite
onready var _content: HBoxContainer = $ScrollContent/Content
onready var _scroll_container: ScrollContainer = $ScrollContent

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	if load_textures():
		_animation.frames = _spriteframes
		_current_position = 0
		_current_string = _available_textures[_current_position]
		_animation.animation = _current_string
		_animation.playing = _spriteframes.get_frame_count(_current_string) > 1

		var ratio: Vector2 = (_spriteframes.get_frame(_current_string, 0).get_size() / (Vector2.ONE * GlobalData.BLOCK_SIZE)) - Vector2.ONE

		if ratio > Vector2.ZERO:
			if ratio.x == 0 and ratio.y > 0:
				ratio.x = ratio.y
			_animation.scale -= (ratio / 2)

		if _spriteframes.animations.size() > 1:
			_timer.start()
	else:
		if Resources.default_block is SpriteFrames:
			_animation.frames = Resources.default_block
			_animation.playing = _animation.frames.get_frame_count("default") > 1

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_collision_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				if not _scroll_container.visible:
					_scroll_container.visible = true
					if GlobalData.previewer_oppened:
						GlobalData.previewer_oppened.visible = false

					GlobalData.previewer_oppened = _scroll_container
				elif _scroll_container.visible:
					_scroll_container.visible = false
					GlobalData.previewer_oppened = null

func _on_timer_timeout() -> void:
	_current_position += 1
	if _current_position >= (_spriteframes.animations.size() - 1):
		_current_position = 0

	_current_string = _available_textures[_current_position]
	_animation.animation = _current_string
	_animation.playing = _spriteframes.get_frame_count(_current_string) > 1

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func create_content_tile(name: String, frames: Array) -> void:
	_available_textures.append(name)
	_spriteframes.add_animation(name)
	SpriteFramesExt.add_frames(_spriteframes, name, frames)
	_spriteframes.set_animation_speed(name, GlobalData.FPS_ANIMATION)

	# Add previewersingle
	var preview_single: PreviewerSingle = Resources.PREVIEWER_SINGLE.instance() as PreviewerSingle
	_content.add_child(preview_single)
	preview_single.set_data(root, category, subcategory, "tile", frames, type, int(name))

func create_content(name_sprite: String, category_par: String, subcategory_par: String, variant: String) -> void:
	var textures: Array = []
	var frames: Resource = Resources.get_frames_from_image(name_sprite)

	if frames is SpriteFrames:
		textures = SpriteFramesExt.get_frames(frames, "default")
	else:
		textures.append(Resources.get_image(name_sprite) as Texture)

	_spriteframes.add_animation(name_sprite)
	SpriteFramesExt.add_frames(_spriteframes, name_sprite, textures)
	_spriteframes.set_animation_speed(name_sprite, GlobalData.FPS_ANIMATION)

	# Add previewersingle
	var preview_single: PreviewerSingle = Resources.PREVIEWER_SINGLE.instance() as PreviewerSingle
	_content.add_child(preview_single)
	preview_single.set_data(root, category_par, subcategory_par, variant, textures, type)

func crop_texture(tile_id: int, tile_set: TileSet, texture: Texture) -> ImageTexture:
	var icon_size: Vector2 = tile_set.autotile_get_size(tile_id)
	var icon_pos: Vector2 = tile_set.autotile_get_icon_coordinate(tile_id) * icon_size
	var image: Image = Image.new()
	image.copy_from(texture.get_data())
	var image_texture: ImageTexture = ImageTexture.new()
	image_texture.create_from_image(image.get_rect(Rect2(icon_pos, icon_size)))

	return image_texture

func load_textures() -> bool:
	var to_result: bool = false

	if not root.empty():
		var path: String = root.plus_file(category)

		if not subcategory.empty():
			path = path.plus_file(subcategory)

		name = path.replace("/", "_")

		if "ground" in path:
			var tile_set: TileSet = Resources.get_image("blocks/ground/floor") as TileSet
			var available: Dictionary = {}

			if tile_set:
				var tiles: Array = tile_set.get_tiles_ids()

				for tile in tiles:
					var image_texture: ImageTexture = crop_texture(tile, tile_set, tile_set.tile_get_texture(tile))
					available[str(tile)] = image_texture

			if not available.empty():
				for name in available.keys():
					create_content_tile(name, [available[name]])
		else:
			var future_textures: Array = Resources.get_images(path).keys()

			_available_textures = []

			var regex_to_match: RegEx = RegEx.new()
			var is_matched: bool = false

			#//TODO: Check this implementation
			for texture in future_textures:
				is_matched = false

				for exclusion in GlobalData.block_exclusion:
					var _err = regex_to_match.compile(exclusion)

					if regex_to_match.search(texture):
						is_matched = true
						break

				if not is_matched:
					_available_textures.append(texture)

			_available_textures.sort()
			var data: PoolStringArray = []
			var variant: String = ""
			var sub: String = ""
			if not _available_textures.empty():
				for name in _available_textures:
					if not name:
						break
					data = name.replace(path + "/", "").split("/")
					if data.size() > 1:
						sub = data[0]
						variant = data[1]
					else:
						sub = subcategory
						variant = data[0]
					create_content(name, category, sub, variant)

		to_result = _spriteframes.animations.size() > 0

		if to_result:
			update_size()
	return to_result

func update_size() -> void:
	if _spriteframes.animations.size() < 15:
		var width: int = _spriteframes.animations.size() - 1

		_scroll_container.rect_min_size = Vector2((width * GlobalData.BLOCK_SIZE) + (width * 8), GlobalData.BLOCK_SIZE + 8)
		_scroll_container.rect_size = _scroll_container.rect_min_size
		_content.rect_min_size.y = GlobalData.BLOCK_SIZE
		_content.rect_size.y = _content.rect_min_size.y
