class_name PreviewerSingle
extends Panel

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _root: String = ""
var _category: String = ""
var _subcategory: String = ""
var _variant: String = ""
var _identifier: int = -999
var _type: int = Enums.DataInfoType.NONE
var _spriteframes: SpriteFrames = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _animation: AnimatedSprite = $Sprite

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_collision_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			if event.pressed:
				# var path: String = _root.plus_file(_category).plus_file(_subcategory).plus_file(str(_identifier)).plus_file(_variant)

				var info: DataInfo = null
				# var allow_notify: bool = false

				match _type:
					Enums.DataInfoType.BLOCK:
						# for block_key in GlobalData.block_allow_notify.keys():
						# 	var path_allow: String = "blocks".plus_file(block_key)
						# 	allow_notify = path.begins_with(path_allow) or ("*" in path_allow and path.begins_with(path_allow.replace("*", "")))

						# 	if allow_notify:
						# 		break

						info = BlockInfo.new()
						# info.allow_notifies = allow_notify
					Enums.DataInfoType.BLOCK_REWARD:
						# for block_key in GlobalData.block_allow_notify.keys():
						# 	var path_allow: String = "blocks".plus_file(block_key)
						# 	allow_notify = path.begins_with(path_allow) or ("*" in path_allow and path.begins_with(path_allow.replace("*", "")))

						# 	if allow_notify:
						# 		break

						info = BlockRewardInfo.new()
						# info.allow_notifies = allow_notify
					Enums.DataInfoType.ENEMY:
						info = EnemyInfo.new()
					Enums.DataInfoType.OBJECT:
						info = ObjectInfo.new()
					Enums.DataInfoType.PLAYER:
						info = PlayerInfo.new()
					Enums.DataInfoType.POWERUP:
						info = PowerupInfo.new()
					Enums.DataInfoType.ACTIVATABLE:
						info = ActivatableInfo.new()

				info.root = _root
				info.category = _category
				info.subcategory = _subcategory
				info.variant = _variant

				if info is BlockInfo:
					info.identifier = _identifier

				info.get_image().frames = _spriteframes
				info.get_image().playing = _animation.playing
				GlobalData.set_data_info(info)

				if GlobalData.previewer_oppened:
					GlobalData.previewer_oppened.visible = false
					GlobalData.previewer_oppened = null

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func set_data(root: String, category: String, subcategory: String, variant: String, textures: Array, type: int, identifier: int = -999) -> void:
	_root = root
	_category = category
	_subcategory = subcategory
	_variant = variant
	_identifier = identifier
	_type = type

	_spriteframes = SpriteFrames.new()

	var ratio: Vector2 = (textures[0].get_size() / (Vector2.ONE * GlobalData.BLOCK_SIZE)) - Vector2.ONE

	if ratio > Vector2.ZERO:
		if ratio.x == 0 and ratio.y > 0:
			ratio.x = ratio.y
		_animation.scale -= (ratio / 2)

	SpriteFramesExt.add_frames(_spriteframes, "default", textures)
	_spriteframes.set_animation_speed("default", GlobalData.FPS_ANIMATION)

	_animation.frames = _spriteframes
	_animation.playing = _spriteframes.get_frame_count("default") > 1
