class_name PropertyPicker
extends VBoxContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const PROPERTY_PATH: String = "res://src/UI/Properties"
const _PROPERTY_ICON: Texture = preload("res://assets/images/icons/property.tex")

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var content: VBoxContainer = $Content/List
onready var _add_property_button: MenuButton = $AddProperty

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	var popup: PopupMenu = _add_property_button.get_popup()
	var _err = popup.connect("id_pressed", self, "_on_AddProperty_item_selected")

	# var size_property: SizeProperty = content.get_node("SizeProperty") as SizeProperty

	# if size_property:
	# 	size_property.register()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_AddProperty_item_selected(item: int) -> void:
	if item != 0:
		var property_selected: String = _add_property_button.get_popup().get_item_text(item)

		if not content.has_node(property_selected):
			var path: String = PROPERTY_PATH.plus_file("%s.tscn" % property_selected)
			var property = load(path).instance() as Property
			content.add_child(property, true)
			content.move_child(property, content.get_child_count())

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func clean_properties() -> void:
	var properties = Arrays.of_type_object(content.get_children(), Property)

	for property in properties:
		if property.name.begins_with("Size"):
			continue

		# property.destroy()
		property.queue_free()

func get_add_property_button() -> MenuButton:
	return _add_property_button

func instance_property(name: String) -> Property:
	var path: String = ProjectSettings.globalize_path(PROPERTY_PATH.plus_file("%s.tscn" % name))
	var to_result = load(path).instance() as Property

	content.add_child(to_result, true)
	# to_result.register()

	return to_result

func load_properties_exists() -> void:
	var directory_properties: Directory = Directory.new()
	var popup: PopupMenu = _add_property_button.get_popup()

	if directory_properties.open(PROPERTY_PATH) == OK:
		popup.clear()
		popup.add_item(tr("KEY_ADD_PROPERTY"), 0)
		popup.set_item_disabled(0, true)
		popup.set_item_icon(0, _PROPERTY_ICON)

		var _err = directory_properties.list_dir_begin(true)
		var current_file: String = directory_properties.get_next()
		var id: int = 1
		while not current_file.empty():
			if not directory_properties.current_is_dir():
				if not current_file.begins_with("Size") and not current_file.begins_with("Property") and not current_file.ends_with(".gd"):
					popup.add_item(current_file.replace(".tscn", "").replace(".scn", ""), id)
					popup.set_item_icon(id, _PROPERTY_ICON)
					id += 1
			current_file = directory_properties.get_next()

		directory_properties.list_dir_end()
