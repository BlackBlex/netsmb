class_name Arrow
extends Line2D

var _points: PoolVector2Array = []

func _ready() -> void:
	_points.append(Vector2.ZERO)
	_points.append(Vector2(0, 5))
	_points.append(Vector2(-5, 5))
	_points.append(Vector2(0, 12))
	_points.append(Vector2(5, 5))
	_points.append(Vector2(0, 5))

	points = _points
	width = 2


func set_size(new_size: float) -> void:
	_points[1].y = new_size
	_points[2].y = new_size
	_points[3].y = new_size + 5
	_points[4].y = new_size
	_points[5].y = new_size
