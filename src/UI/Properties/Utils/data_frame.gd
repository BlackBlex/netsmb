class_name DataFrame

var fps: float = GlobalData.FPS_ANIMATION
var width: int = 0
var height: int = 0

# Array of FrameSection
var frames: Array = []
