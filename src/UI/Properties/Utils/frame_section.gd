class_name FrameSection
extends VBoxContainer

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var vertical: bool setget _set_vertical, _get_vertical
var frames: int setget _set_frames, _get_frames
var x: int setget _set_x, _get_x
var y: int setget _set_y, _get_y

#_property: AnimationFramesProperty
var _property = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _orientation: OptionComponent = $OrientationComponent
onready var _slider_frames: SliderComponent = $FramesComponent
onready var _slider_start: Vector2Component = $StartComponent
onready var _delete_button: Button = $Buttons/DeleteButton

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_set_vertical(vertical)
	_set_frames(frames)
	_set_x(x)
	_set_y(y)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_add_pressed() -> void:
	if _property:
		_property.add_frame()

func _on_delete_pressed() -> void:
	if _property:
		_property.remove_frame(self)
		queue_free()

func _on_orientation_option_changed(_value: String) -> void:
	if _property:
		_property.reload_animation_current()

func _on_frames_value_changed(_value: int) -> void:
	if _property:
		_property.reload_animation_current()

func _on_start_text_input_x_changed(_value: int) -> void:
	if _property:
		_property.reload_animation_current()

func _on_start_text_input_y_changed(_value: int) -> void:
	if _property:
		_property.reload_animation_current()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func is_delete_disabled() -> bool:
	return _delete_button.disabled

func disable_delete() -> void:
	_delete_button.disabled = true

func enable_delete() -> void:
	_delete_button.disabled = false

#property: AnimationFramesProperty
func register_animation_property(property) -> void:
	_property = property


func _get_vertical() -> bool:
	var to_result: bool = vertical
	if _orientation:
		to_result =  _orientation.get_item_selected() == "KEY_VERTICAL"
	return to_result

func _set_vertical(new_value: bool) -> void:
	vertical = new_value

	if not _orientation:
		return

	if new_value:
		_orientation.selected = 0
	else:
		_orientation.selected = 1

func _get_frames() -> int:
	var to_result: int = frames
	if _slider_frames:
		to_result =  int(_slider_frames.get_value())
	return to_result

func _set_frames(new_value: int) -> void:
	frames = new_value

	if not _slider_frames:
		return

	_slider_frames.set_value(new_value)

func _get_x() -> int:
	var to_result: int = x
	if _slider_start:
		to_result =  int(_slider_start.get_value().x)
	return to_result

func _set_x(new_value: int) -> void:
	x = new_value

	if not _slider_start:
		return

	_slider_start.set_value(Vector2(new_value, _slider_start.get_value().y))

func _get_y() -> int:
	var to_result: int = y
	if _slider_start:
		to_result =  int(_slider_start.get_value().y)
	return to_result

func _set_y(new_value: int) -> void:
	y = new_value

	if not _slider_start:
		return

	_slider_start.set_value(Vector2(_slider_start.get_value().x, new_value))
