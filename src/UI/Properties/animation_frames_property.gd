class_name AnimationFramesProperty
extends Property

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const _FRAME_SECTION: PackedScene = preload("res://src/UI/Properties/Utils/FrameSection.tscn")

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
# Dictionary of string with DataFrame
var _information_of_animations: Dictionary = {}
var _animation_current: String = ""

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _animations: OptionComponent = $Root/Content/AnimationComponent
onready var _animation_new_name: LineEdit = $Root/Content/NameLineEdit
onready var _size: Vector2Component = $Root/Content/SizeComponent
onready var _slider_fps: SliderComponent = $Root/Content/FPSComponent
onready var _scroll_container_frames: ScrollContainer = $Root/Content/Frames
onready var _container_frames: VBoxContainer = $Root/Content/Frames/Content

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func on_image_loaded() -> void:
	_reload_animation_list()

	if _animation_current.empty():
		_animation_current = "default"

	if not _information_of_animations.has(_animation_current):
		_information_of_animations[_animation_current] = DataFrame.new()
		add_frame()

	reload_animation_current()


func _on_AnimationFramesProperty_gui_input(_event: InputEvent) -> void:
	if Input.is_action_just_pressed("action_esc") and _animation_new_name.has_focus():
		_animation_new_name.visible = false
		_animation_new_name.release_focus()

func _on_add_pressed() -> void:
	_animation_new_name.visible = true
	_animation_new_name.grab_focus()

func _on_delete_pressed() -> void:
	var _err = _information_of_animations.erase(_animation_current)
	Resources.CREATOR.preview_image_loaded.image.frames.remove_animation(_animation_current)
	_reload_animation_list()
	Resources.CREATOR.preview_image_loaded.image.animation = _animation_current
	reload_frame_list(_animation_current)

func _on_animation_option_changed(value: String) -> void:
	_animation_current = value

	if _information_of_animations.has(_animation_current):
		var info_animation: DataFrame = _information_of_animations[_animation_current]

		Resources.CREATOR.preview_image_loaded.image.animation = _animation_current
		_slider_fps.set_value(info_animation.fps)
		_size.set_value(Vector2(info_animation.width, info_animation.height))

		reload_frame_list(_animation_current)
		reload_animation_current()

		Resources.CREATOR.preview_image_loaded.image.playing = Resources.CREATOR.preview_image_loaded.image.frames.get_frame_count(_animation_current) > 1

func _on_name_line_edit_text_entered(new_text: String) -> void:
	if Resources.CREATOR.preview_image_loaded.image.frames.has_animation(new_text):
		Alert.show_alert(tr("animation_exist"))
		return

	_animation_new_name.visible = false
	_animation_new_name.text = ""
	Resources.CREATOR.preview_image_loaded.image.frames.add_animation(new_text)
	var info_animation = DataFrame.new()
	info_animation.fps = _slider_fps.get_value()
	info_animation.width = _size.get_value().x
	info_animation.height = _size.get_value().y
	_information_of_animations[new_text] = info_animation
	_reload_animation_list()

	OptionButtonExt.select_component(_animations, new_text)
	_animation_current = new_text
	Resources.CREATOR.preview_image_loaded.image.animation = _animation_current
	_clean_frames()
	add_frame()

func _on_fps_value_changed(value: int) -> void:
	var info_animation = _information_of_animations[_animation_current]
	info_animation.fps = value
	reload_animation_current()

func _on_size_text_input_x_changed(value: int) -> void:
	var info_animation = _information_of_animations[_animation_current]
	info_animation.width = value

func _on_size_text_input_y_changed(value: int) -> void:
	var info_animation = _information_of_animations[_animation_current]
	info_animation.height = value


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_data() -> Dictionary:
	var animations: Dictionary = {}
	for key in _information_of_animations.keys():
		animations[key] = {
			"fps": _information_of_animations[key].fps,
			"width": _information_of_animations[key].width,
			"height": _information_of_animations[key].height
		}

		var frames: Array = []
		for frame in _information_of_animations[key].frames:
			frames.append({
				"vertical": frame.vertical,
				"frames": frame.frames,
				"x": frame.x,
				"y": frame.y
			})

		animations[key]["frames"] = frames

	return {"animationFrames": {
		"animations": animations,
		"current": _animation_current
	}}

func set_data(values: Dictionary) -> void:
	if values.size() == 2:
		if values.has("animations"):
			var animations: Dictionary = values.animations
			SpriteFramesExt.clear_complete(Resources.CREATOR.preview_image_loaded.image.frames)

			for key in animations.keys():
				Resources.CREATOR.preview_image_loaded.image.frames.add_animation(key)
				_information_of_animations[key] = DataFrame.new()
				_information_of_animations[key].fps = animations[key].fps
				_information_of_animations[key].width = animations[key].width
				_information_of_animations[key].height = animations[key].height

				var frames: Array = animations[key].frames

				for frame in frames:
					#Cicle -> frame_section: FrameSection | as FrameSection
					var frame_section = _FRAME_SECTION.instance()

					frame_section.register_animation_property(self)

					frame_section.vertical = frame.vertical
					frame_section.frames = frame.frames
					frame_section.x = frame.x
					frame_section.y = frame.y

					_information_of_animations[key].frames.append(frame_section)

				_reload_animation(key)

		if values.has("current"):
			_animation_current = values.current

func reset_values() -> void:
	_information_of_animations.clear()
	Resources.CREATOR.preview_image_loaded.image.frames = SpriteFrames.new()

	_reload_animation_list()

	_scroll_container_frames.visible = false


func add_frame() -> void:
	#Cicle -> frame: FrameSection | as FrameSection
	var frame = _FRAME_SECTION.instance()

	_container_frames.add_child(frame, true)
	_information_of_animations[_animation_current].frames.append(frame)

	# frame.set_max_spin(Resources.CREATOR.image_loaded.get_size())
	frame.register_animation_property(self)

	if _container_frames.get_child_count() == 1:
		frame.disable_delete()

#Cicle -> frame: FrameSection
func remove_frame(frame_section) -> void:
	_container_frames.remove_child(frame_section)
	_information_of_animations[_animation_current].frames.erase(frame_section)

	if _container_frames.get_child_count() == 1:
		_container_frames.get_children()[0].disable_delete()
	else:
		#Cicle -> first_frame: FrameSection
		var first_frame = _container_frames.get_children()[0]#Arrays.of_type_object(_container_frames.get_children(), FrameSection)[0]

		if first_frame.is_delete_disabled():
			first_frame.enable_delete()

	reload_animation_current()

func reload_animation_current() -> void:
	_reload_animation(_animation_current)

func reload_frame_list(animation: String) -> void:
	_clean_frames()

	for frame_section in _information_of_animations[animation].frames:
		_container_frames.add_child(frame_section)


func _clean_frames() -> void:
	for child in _container_frames.get_children():
		_container_frames.remove_child(child)

func _reload_animation(animation: String) -> void:
	Logger.msg_debug("Reload %s animation" % animation, Logger.CATEGORY_ASSET)
	#Cicle -> frame_section: FrameSection | as FrameSection
	var frame_sections: Array = _information_of_animations[animation].frames # _container_frames.get_children()#Arrays.of_type_object(_container_frames.get_children(), FrameSection)

	var image: ImageTexture = Resources.CREATOR.image_loaded
	var frame_count = Resources.CREATOR.preview_image_loaded.image.frames.get_frame_count(animation)

	for _frame in range(0, frame_count):
		Resources.CREATOR.preview_image_loaded.image.frames.remove_frame(animation, 0)

	if Resources.CREATOR.preview_image_loaded.image.frames.get_frame_count(animation) > 0:
		Logger.msg_error("There are frames remaining, even after deleting", Logger.CATEGORY_ASSET)

	Logger.msg_debug("Processing animation", Logger.CATEGORY_ASSET)

	var current_width: float = 0
	var current_height: float = 0
	var image_width: float = _information_of_animations[animation].width # _size.get_value().x
	var image_height: float = _information_of_animations[animation].height # _size.get_value().y
	var fps: float = _information_of_animations[animation].fps # _slider_fps.get_value()
	var total_frames: int = 0

	var atlas_texture: AtlasTexture = null

	for frame_section in frame_sections:
		for frame in range(0, frame_section.frames):
			Logger.msg_debug("Process frame: %d" % frame, Logger.CATEGORY_ASSET)

			atlas_texture = AtlasTexture.new()
			atlas_texture.atlas = image
			current_width = frame_section.x
			current_height = frame_section.y

			if frame_section.vertical:
				current_height += image_height * frame # //Todo: check if is invert
			else:
				current_width += image_width * frame

			Logger.msg_debug("Frame#%d size: w=[%d], h=[%d]" % [frame, current_width, current_height], Logger.CATEGORY_ASSET)
			atlas_texture.region = Rect2(current_width, current_height, image_width, image_height)
			Logger.msg_debug("Frame#%d region: %s" % [frame, str(atlas_texture.region)], Logger.CATEGORY_ASSET)
			Resources.CREATOR.preview_image_loaded.image.frames.add_frame(animation, atlas_texture, total_frames)

			total_frames += 1

	Resources.CREATOR.preview_image_loaded.image.playing = Resources.CREATOR.preview_image_loaded.image.frames.get_frame_count(animation) > 1

	Resources.CREATOR.preview_image_loaded.image.frames.set_animation_speed(animation, fps)

func _reload_animation_list() -> void:
	_animations.clear()

	var animations: Array = Resources.CREATOR.preview_image_loaded.image.frames.get_animation_names()

	_animations.items = animations

	if not _animation_current.empty() and animations.has(_animation_current):
		OptionButtonExt.select_component(_animations, _animation_current)
		# _on_animation_option_changed(_animation_current)
	else:
		_animation_current = animations[0]

	_on_animation_option_changed(_animation_current)
