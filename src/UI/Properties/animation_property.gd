class_name AnimationProperty
extends Property

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const VERTICAL: int = 0
const HORIZONTAL: int = 1

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _orientation_value: int = VERTICAL

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _orientation: OptionComponent = $Root/Content/OrientationComponent
onready var _slider_frames: SliderComponent = $Root/Content/FramesComponent
onready var _slider_fps: SliderComponent = $Root/Content/FPSComponent

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func on_image_loaded() -> void:
	Logger.msg_debug("###Update animations###")
	Logger.msg_debug("Orientation: %s" % ["Vertical" if _orientation_value == VERTICAL else "Horizontal"], Logger.CATEGORY_ASSET)

	var frames: float = _slider_frames.get_value()
	var image: ImageTexture = Resources.CREATOR.image_loaded

	Resources.CREATOR.preview_image_loaded.image.frames = SpriteFrames.new()

	var image_width: int = image.get_width()
	var image_height: int = image.get_height()
	var current_width: int = 0
	var current_height: int = 0

	if frames == 0:
		frames = 1
		_slider_frames.set_value(frames)

	if _orientation_value == HORIZONTAL:
		image_width /= int(frames)
		Logger.msg_debug("Width frame size: %d" % image_width, Logger.CATEGORY_ASSET)
	elif _orientation_value == VERTICAL:
		image_height /= int(frames)
		Logger.msg_debug("Height frame size: %d" % image_height, Logger.CATEGORY_ASSET)

	Logger.msg_debug("Size per frame: w=[%d], h=[%d]" % [image_width, image_height])

	var atlas_texture: AtlasTexture = null
	for frame in range(0, frames):
		Logger.msg_debug("Process frame: %d" % frame, Logger.CATEGORY_ASSET)

		atlas_texture = AtlasTexture.new()
		atlas_texture.atlas = image
		current_width = 0 if _orientation_value == VERTICAL else (image_width * frame)
		current_height = 0 if _orientation_value == HORIZONTAL else (image_height * frame)

		Logger.msg_debug("Frame#%d size: w=[%d], h=[%d]" % [frame, current_width, current_height], Logger.CATEGORY_ASSET)

		atlas_texture.region = Rect2(current_width, current_height, image_width, image_height)

		Logger.msg_debug("Frame#%d region: %s" % [frame, str(atlas_texture.region)], Logger.CATEGORY_ASSET)

		Resources.CREATOR.preview_image_loaded.image.frames.add_frame("default", atlas_texture, frame)

	Resources.CREATOR.preview_image_loaded.image.playing = frames > 1
	Resources.CREATOR.preview_image_loaded.image.frames.set_animation_speed("default", _slider_fps.get_value())


func _on_orientation_option_changed(value: String) -> void:
	if "KEY_VERTICAL" in value:
		_orientation_value = VERTICAL
	else:
		_orientation_value = HORIZONTAL

	on_image_loaded()

func _on_slider_value_changed(_value: float) -> void:
	on_image_loaded()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func get_data() -> Dictionary:
	return {"animation": {
		"orientation": _orientation_value,
		"frames": _slider_frames.get_value(),
		"fps": _slider_fps.get_value()
	}}

func set_data(values: Dictionary) -> void:
	if values.size() == 3:
		if values.has("orientation"):
			_orientation.selected = values.orientation
		if values.has("frames"):
			_slider_frames.set_value(values.frames)
		if values.has("fps"):
			_slider_fps.set_value(values.fps)

func reset_values() -> void:
	_slider_frames.set_value(1)
	_orientation.selected = VERTICAL
	_slider_fps.set_value(GlobalData.FPS_ANIMATION)
