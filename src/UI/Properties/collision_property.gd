class_name CollisionProperty
extends Property

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var size: Vector2 = Vector2.INF setget _set_size, _get_size
var position: Vector2 = Vector2.INF setget _set_position, _get_position

var _collision: Panel = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _component_size: Vector2Component = $Root/Content/SizeComponent
onready var _component_position: Vector2Component = $Root/Content/PositionComponent
onready var _check_one_direction: CheckComponent = $Root/Content/OneDirectionComponent

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func on_image_loaded() -> void:
	var size_image = Resources.CREATOR.image_loaded.get_size() / 2

	if not Resources.CREATOR.preview_image_loaded.image.has_node("CollisionProperty"):
		if not _collision:
			_collision = Panel.new()
			var style: StyleBoxFlat = StyleBoxFlat.new()
			style.bg_color = Color(0, 0.850980392, 1, 0.31372549)
			_collision.set("custom_styles/panel", style)
			_collision.name = "CollisionProperty"
			Resources.CREATOR.preview_image_loaded.image.add_child(_collision)

	if size == Vector2.INF:
		_set_size(size_image)

	if position == Vector2.INF:
		_set_position(Vector2.ZERO)

	_update_collision()


func _on_component_text_input_x_changed(value: int, size_or_pos: bool) -> void:
	if size_or_pos:
		size.x = value
	else:
		position.x = value

	_update_collision()

func _on_component_text_input_y_changed(value: int, size_or_pos: bool) -> void:
	if size_or_pos:
		size.y = value
	else:
		position.y = value

	_update_collision()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func destroy() -> void:
	if Resources.CREATOR.preview_image_loaded.image.has_node("CollisionProperty"):
		Resources.CREATOR.preview_image_loaded.image.remove_child(_collision)

	.destroy()

func get_data() -> Dictionary:
	return {"collision": {
		"extents": size,
		"one_collision": _check_one_direction.checked,
		"position": position
	}}

func set_data(values: Dictionary) -> void:
	if values.size() == 3:
		if values.has("extents"):
			_set_size(values.extents)

		if values.has("one_collision"):
			_check_one_direction.checked = values.one_collision

		if values.has("position"):
			_set_position(values.position)


func _get_size() -> Vector2:
	return size

func _set_size(new_value: Vector2) -> void:
	size = new_value

	_component_size.set_value(size)

func _get_position() -> Vector2:
	return position

func _set_position(new_value: Vector2) -> void:
	position = new_value

	_component_position.set_value(position)

func _update_collision() -> void:
	# //TODO: one direction representation

	var position_negative = size * -1

	_collision.rect_min_size = Vector2(size) * 2
	_collision.rect_position = Vector2(position_negative.x + position.x, position_negative.y + position.y)
	_collision.rect_size = _collision.rect_min_size
