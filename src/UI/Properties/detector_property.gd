class_name DetectorProperty
extends Property

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################
const SIZE_DEFAULT: float = 5.0

##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var size_up: float = SIZE_DEFAULT setget _set_size_up, _get_size_up
var size_down: float = SIZE_DEFAULT setget _set_size_down, _get_size_down
var size_look_to: float = SIZE_DEFAULT setget _set_size_look_to, _get_size_look_to
var position_up: Vector2 = Vector2.ZERO setget _set_position_up, _get_position_up
var position_down: Vector2 = Vector2.ZERO setget _set_position_down, _get_position_down
var position_look_to: Vector2 = Vector2.ZERO setget _set_position_look_to, _get_position_look_to

var _detector_up: Arrow = null
var _detector_down: Arrow = null
var _detector_look_to: Arrow = null

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _component_up_check: CheckComponent = $Root/Content/UpEnableComponent
onready var _component_down_check: CheckComponent = $Root/Content/DownEnableComponent
onready var _component_look_to_check: CheckComponent = $Root/Content/LookToEnableComponent
onready var _component_up_height: FieldNumberComponent = $Root/Content/UpHeightComponent
onready var _component_down_height: FieldNumberComponent = $Root/Content/DownHeightComponent
onready var _component_look_to_height: FieldNumberComponent = $Root/Content/LookToHeightComponent
onready var _component_up_position: Vector2Component = $Root/Content/UpPositionComponent
onready var _component_down_position: Vector2Component = $Root/Content/DownPositionComponent
onready var _component_look_to_position: Vector2Component = $Root/Content/LookToPositionComponent

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func on_image_loaded() -> void:
	if not Resources.CREATOR.preview_image_loaded.image.has_node("UpDetectorProperty"):
		if not _detector_up:
			_detector_up = Arrow.new()
			_detector_up.name = "UpDetectorProperty"
			_detector_up.rotation_degrees = 180

		Resources.CREATOR.preview_image_loaded.image.add_child(_detector_up)
	if not Resources.CREATOR.preview_image_loaded.image.has_node("DownDetectorProperty"):
		if not _detector_down:
			_detector_down = Arrow.new()
			_detector_down.name = "DownDetectorProperty"

		Resources.CREATOR.preview_image_loaded.image.add_child(_detector_down)
	if not Resources.CREATOR.preview_image_loaded.image.has_node("LookToDetectorProperty"):
		if not _detector_look_to:
			_detector_look_to = Arrow.new()
			_detector_look_to.name = "LookToDetectorProperty"
			_detector_look_to.rotation_degrees = 270

		Resources.CREATOR.preview_image_loaded.image.add_child(_detector_look_to)

	_update_detector_up()
	_update_detector_down()
	_update_detector_look_to()

func _on_check_pressed(_value: bool, what: String) -> void:
	match what:
		"up":
			_component_up_height.visible = _component_up_check.checked
			_component_up_position.visible = _component_up_check.checked
		"down":
			_component_down_height.visible = _component_down_check.checked
			_component_down_position.visible = _component_down_check.checked
		"look_to":
			_component_look_to_height.visible = _component_look_to_check.checked
			_component_look_to_position.visible = _component_look_to_check.checked

func _on_height_component_changed(value: float, what: String) -> void:
	match what:
		"up":
			size_up = value
			_update_detector_up()
		"down":
			size_down = value
			_update_detector_down()
		"look_to":
			size_look_to = value
			_update_detector_look_to()

func _on_position_component_text_input_x_changed(value: float, what: String) -> void:
	match what:
		"up":
			position_up.x = value
			_update_detector_up()
		"down":
			position_down.x = value
			_update_detector_down()
		"look_to":
			position_look_to.x = value
			_update_detector_look_to()

func _on_position_component_text_input_y_changed(value: float, what: String) -> void:
	match what:
		"up":
			position_up.y = value
			_update_detector_up()
		"down":
			position_down.y = value
			_update_detector_down()
		"look_to":
			position_look_to.y = value
			_update_detector_look_to()

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func destroy() -> void:
	if Resources.CREATOR.preview_image_loaded.image.has_node("UpDetectorProperty"):
		Resources.CREATOR.preview_image_loaded.image.remove_child(_detector_up)

	if Resources.CREATOR.preview_image_loaded.image.has_node("DownDetectorProperty"):
		Resources.CREATOR.preview_image_loaded.image.remove_child(_detector_down)

	if Resources.CREATOR.preview_image_loaded.image.has_node("LookToDetectorProperty"):
		Resources.CREATOR.preview_image_loaded.image.remove_child(_detector_look_to)

	.destroy()

func get_data() -> Dictionary:
	return {"detector": {
		"sizes": {
			"up": size_up,
			"down": size_down,
			"look_to": size_look_to
		},
		"positions": {
			"up": position_up,
			"down": position_down,
			"look_to": position_look_to
		}
	}}

func set_data(values: Dictionary) -> void:
	if values.size() == 2:
		if values.has("sizes"):
			var sizes: Dictionary = values.sizes
			if sizes.has("up"):
				_set_size_up(float(sizes.up))
			if sizes.has("down"):
				_set_size_down(float(sizes.down))
			if sizes.has("look_to"):
				_set_size_look_to(float(sizes.look_to))
		if values.has("positions"):
			var positions: Dictionary = values.positions
			if positions.has("up"):
				_set_position_up(positions.up)
			if positions.has("down"):
				_set_position_down(positions.down)
			if positions.has("look_to"):
				_set_position_look_to(positions.look_to)


func _update_detector_up() -> void:
	_detector_up.set_size(size_up)
	_detector_up.position = position_up

func _update_detector_down() -> void:
	_detector_down.set_size(size_down)
	_detector_down.position = position_down

func _update_detector_look_to() -> void:
	_detector_look_to.set_size(size_look_to)
	_detector_look_to.position = position_look_to

func _get_size_up() -> float:
	return size_up

func _set_size_up(new_value: float) -> void:
	size_up = new_value

	if _component_up_height:
		_component_up_height.set_value(size_up)

	# if _spin_height_up:
	# 	_spin_height_up.value = size_up

func _get_size_down() -> float:
	return size_down

func _set_size_down(new_value: float) -> void:
	size_down = new_value

	if _component_down_height:
		_component_down_height.set_value(size_down)

	# if _spin_height_down:
	# 	_spin_height_down.value = size_down

func _get_size_look_to() -> float:
	return size_look_to

func _set_size_look_to(new_value: float) -> void:
	size_look_to = new_value

	if _component_look_to_height:
		_component_look_to_height.set_value(size_look_to)

func _get_position_up() -> Vector2:
	return position_up

func _set_position_up(new_value: Vector2) -> void:
	position_up = new_value

	if _component_up_position:
		_component_up_position.set_value(position_up)

func _get_position_down() -> Vector2:
	return position_down

func _set_position_down(new_value: Vector2) -> void:
	position_down = new_value

	if _component_down_position:
		_component_down_position.set_value(position_down)

func _get_position_look_to() -> Vector2:
	return position_look_to

func _set_position_look_to(new_value: Vector2) -> void:
	position_look_to = new_value

	if _component_look_to_position:
		_component_look_to_position.set_value(position_look_to)
