extends Panel

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var image_loaded: ImageTexture

var _current_file_name: String = ""
var _current_type: String = "none"
var _is_edited: bool = false
var _is_closed: bool = false
var _level_editor: LevelEditor
var _properties_default: PoolStringArray = []

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var preview_image_loaded: PreviewImage = $ContentRoot/Content/Design/Content/View/PreviewImage
onready var file_viewer: FileViewer = $ContentRoot/Content/FileViewer
onready var name_behaviour: NameBehaviour = $ContentRoot/Content/Design/Content/View/Attributes/NameBehaviour
onready var property_picker: PropertyPicker = $ContentRoot/Content/Design/Content/View/Attributes/PropertyPicker
onready var behaviour_dialog: FileSelectDialog = $BehaviourDialog

#_editor_script: EditorScripting
onready var _editor_script = $ContentRoot/Content/Design/EditorScript
onready var _content_view: Control = $ContentRoot/Content/Design/Content
onready var _block_viewer: Control = $ContentRoot/Content/Design/BlockGroundView
onready var _block_ground_view = $ContentRoot/Content/Design/BlockGroundView
onready var _file_menu: MenuButton = $ContentRoot/Menu/FileMenu
onready var _scripting_menu: MenuButton = $ContentRoot/Menu/ScriptMenu
onready var _block_view_menu: MenuButton = $ContentRoot/Menu/BlockViewMenu
onready var _design_tab: TabContainer = $ContentRoot/Content/Design
onready var _save_dialog: FileDialog = $SaveDialog
onready var _option_bg: Panel = $OptionBg

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_editor_script.creator = self

	var _err := behaviour_dialog.connect("file_selected", name_behaviour, "_on_behaviour_selected")
	_err = name_behaviour.connect("bypass_behaviour_pressed", behaviour_dialog, "show")

	_load_file_menu()
	_load_block_view_menu()
	_load_scripting_menu()

func _process(_delta: float) -> void:
	if visible:
		_update_title()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_OptionBg_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed("action_esc") and _option_bg.visible:
		_option_bg.visible = false

func _on_Creator_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed("action_save") and not _editor_script.has_focus():
		print("Save in creator")

func _on_confirm_ignore_no_save_confirmed() -> void:
	if _is_closed:
		_level_editor = get_tree().get_nodes_in_group("editor")[0]

		if _level_editor:
			Transition.set_duration(0.5)
			Transition.to_instance(_level_editor, Transition.Kind.FADE, funcref(self, "_change_to_editor"))
		else:
			Logger.msg_error("Level editor node not found")
	else:
		show_dialog()

func _on_file_viewer_file_selected(file_name: String) -> void:
	_current_file_name = file_name
	if not _is_edited:
		_load_image(file_name)
	else:
		_is_closed = false
		Alert.show_confirm_dialog(tr("KEY_CONFIRM"), tr("KEY_CREATOR_CHANGES_PENDING"), Instanceator.make_funcref_ext(self, "_on_confirm_ignore_no_save_confirmed"))

func _on_block_menu_pressed(item: int) -> void:
	if _design_tab.current_tab == 2:
		match item:
			0:
				_block_viewer.setup()
			1:
				_block_viewer.save_file()
			2:
				_close()

func _on_file_menu_pressed(item: int) -> void:
	if _design_tab.current_tab == 0:
		match item:
			0:
				_new_file()
			1:
				_save_file()
			2:
				_close()

func _on_scripting_menu_pressed(item: int) -> void:
	if _design_tab.current_tab == 1:
		match item:
			0:
				_editor_script.set_text(Utility.load_file("res://src/Core/Scripting/news/new_behaviour.gd"))
			1:
				_editor_script.set_text(Utility.load_file("res://src/Core/Scripting/news/new_event.gd"))
			2:
				_editor_script.set_text(Utility.load_file("res://src/Core/Scripting/news/new_hud.gd"))
			3:
				_editor_script.set_text(Utility.load_file("res://src/Core/Scripting/news/new_script.gd"))
			4:
				_editor_script.set_text(Utility.load_file("res://src/Core/Scripting/news/new_state.gd"))
			5:
				_editor_script.load_file()
			6:
				_editor_script.save_file()
			7:
				_editor_script.clean_path()
				_editor_script.save_file()

func _on_image_loaded() -> void:
	_update_title()
	get_tree().call_group("property", "on_image_loaded")

func _on_Option_pressed(what: String) -> void:
	match what:
		"block":
			_properties_default = []
			_design_tab.current_tab = 2
			_block_view_menu.visible = true
		"enemy":
			_properties_default = ["animation", "collision", "detector", "attach"]
			name_behaviour.visible = true
			behaviour_dialog.load_files()
			_design_tab.current_tab = 0
			_file_menu.visible = true
		"player":
			_properties_default = ["animationFrames", "collision", "detector"]
			#// TODO: Why?
			#name_behaviour.visible = true
			behaviour_dialog.load_files()
			_design_tab.current_tab = 0
			_file_menu.visible = true
		"powerup", "object", "activatable":
			_properties_default = ["animation", "collision", "attach"]
			name_behaviour.visible = true
			behaviour_dialog.load_files()
			_design_tab.current_tab = 0
			_file_menu.visible = true
		"texture":
			_properties_default = ["animation"]
			name_behaviour.set_without_behaviour(true)
			_design_tab.current_tab = 0
			_file_menu.visible = true
		"scripting":
			_design_tab.current_tab = 1
			_scripting_menu.visible = true

	_current_type = tr("KEY_%s" % what.to_upper())
	_option_bg.visible = false

func _on_SaveDialog_file_selected(path: String) -> void:
	if path.empty():
		Alert.show_alert(tr("KEY_LOAD_FILE_PROCESS"))
		return

	var extension: String = path.get_file().get_extension()
	var path_debug: String = ""
	var path_debug_frames: String = ""
	var path_new: String = ""
	var path_new_frames: String = ""

	if path.nocasecmp_to("current_file_meh") == 0:
		path = file_viewer.current_path.plus_file(_current_file_name)
		path_debug = path.replace(".tex", ".tres")
		path_debug_frames = path_debug.replace(".tres", ".frames.tres")
		path_new = path
		path_new_frames = path_new.replace(".tex", ".frames.res")
	else:
		path_debug = ProjectSettings.localize_path(path.replace(extension, "tres"))
		path_debug_frames = path_debug.replace(".tres", ".frames.tres")
		path_new = ProjectSettings.localize_path(path.replace(extension, "tex"))
		path_new_frames = path_new.replace(".tex", ".frames.res")

	var properties = Arrays.of_type_object(property_picker.content.get_children(), Property)

	for property in properties:
		if property.name.begins_with("Size"):
			continue

		var data: Dictionary = property.get_data()
		var key: String = data.keys()[0]
		image_loaded.set_meta(key, data[key])

	if name_behaviour.visible:
		var name: String = name_behaviour.get_name_entity()
		var behaviour: String = name_behaviour.get_behaviour()
		var only_local: bool = name_behaviour.get_only_local()

		if name.empty():
			name = "Proccesed image"

		if behaviour.empty():
			behaviour = "basic_movement"

		image_loaded.set_meta("name", name)
		if not name_behaviour.is_without_behaviour():
			image_loaded.set_meta("behaviour", behaviour)
		image_loaded.set_meta("only_local", only_local)

	# Save image
	image_loaded.lossy_quality = 0
	var _err = ResourceSaver.save(path_new, image_loaded, ResourceSaver.FLAG_COMPRESS)
	if OS.has_feature("debug"):
		_err = ResourceSaver.save(path_debug, image_loaded, ResourceSaver.FLAG_COMPRESS)

	if image_loaded.has_meta("animation") || image_loaded.has_meta("animationFrames"):
		# Open again image in tex
		image_loaded = load(path_new) as ImageTexture

		_on_image_loaded()

		yield(get_tree().create_timer(0.5), "timeout")

		# Save sprite frames
		_err = ResourceSaver.save(path_new_frames, preview_image_loaded.image.frames, ResourceSaver.FLAG_COMPRESS)

	if OS.has_feature("debug"):
			_err = ResourceSaver.save(path_debug_frames, preview_image_loaded.image.frames, ResourceSaver.FLAG_COMPRESS)

	if OS.has_feature("debug"):
		Logger.msg_debug("File loaded: %s | File binary saved: %s | File debug saved: %s" % [path, path_new, path_debug])
	else:
		Logger.msg_debug("File loaded: %s | File binary saved: %s" % [path, path_new])

	Alert.show_information(tr("KEY_FILE_SAVED"))

	_is_edited = false

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func show_dialog() -> void:
	_current_type = "none"
	_is_edited = false
	_is_closed = false
	_option_bg.visible = true

	preview_image_loaded.image.frames = SpriteFrames.new()

	if not visible:
		show()

	file_viewer.setup()

	property_picker.clean_properties()
	property_picker.load_properties_exists()

	_update_title()
	OS.center_window()


func _change_to_editor() -> void:
	if not _level_editor:
		return

	visible = false
	OS.set_window_title("Level Editor")
	OS.window_size = OS.get_screen_size()
	_level_editor.visible = true
	_level_editor = null

func _close() -> void:
	_is_closed = true

	if _is_edited:
		Alert.show_confirm_dialog(tr("KEY_CONFIRM"), tr("KEY_CREATOR_CHANGES_PENDING"), Instanceator.make_funcref_ext(self, "_on_confirm_ignore_no_save_confirmed"))
	else:
		_on_confirm_ignore_no_save_confirmed()


func _load_block_view_menu() -> void:
	var popup: PopupMenu = _block_view_menu.get_popup()
	popup.rect_min_size = Vector2(175, 0)

	var action_new: InputEventKey = InputMap.get_action_list("action_new")[0]
	var action_save: InputEventKey = InputMap.get_action_list("action_save")[0]
	var action_cancel: InputEventKey = InputMap.get_action_list("ui_cancel")[0]

	popup.add_item(tr("KEY_NEW"), 0, action_new.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_SAVE"), 1, action_save.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_EXIT"), 2, action_cancel.get_scancode_with_modifiers())

	var _err = popup.connect("id_pressed", self, "_on_block_menu_pressed")

func _load_file_menu() -> void:
	var popup: PopupMenu = _file_menu.get_popup()
	popup.rect_min_size = Vector2(175, 0)

	var action_new: InputEventKey = InputMap.get_action_list("action_new")[0]
	var action_save: InputEventKey = InputMap.get_action_list("action_save")[0]
	var action_cancel: InputEventKey = InputMap.get_action_list("ui_cancel")[0]

	popup.add_item(tr("KEY_NEW"), 0, action_new.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_SAVE"), 1, action_save.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_EXIT"), 2, action_cancel.get_scancode_with_modifiers())

	var _err = popup.connect("id_pressed", self, "_on_file_menu_pressed")

func _load_scripting_menu() -> void:
	var popup: PopupMenu = _scripting_menu.get_popup()
	popup.rect_min_size = Vector2(175, 0)

	var action_open: InputEventKey = InputMap.get_action_list("action_open")[0]
	var action_save: InputEventKey = InputMap.get_action_list("action_save")[0]
	var action_save_as: InputEventKey = InputMap.get_action_list("action_save_as")[0]

	var new_text: String = tr("KEY_NEW")
	popup.add_item("%s BaseBehaviour" % new_text, 0)
	popup.add_item("%s BaseEvent" % new_text, 1)
	popup.add_item("%s BaseHUD" % new_text, 2)
	popup.add_item("%s BaseScript" % new_text, 3)
	popup.add_item("%s State" % new_text, 4)
	popup.add_item(tr("KEY_LOAD"), 5, action_open.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_SAVE"), 6, action_save.get_scancode_with_modifiers())
	popup.add_item(tr("KEY_SAVE_AS"), 7, action_save_as.get_scancode_with_modifiers())

	var _err = popup.connect("id_pressed", self, "_on_scripting_menu_pressed")

func _load_image(file_name: String) -> void:
	var path: String = file_viewer.current_path.plus_file(file_name)

	if not A.file_loader.file_exists(path):
		Alert.show_error("%s: %s" % [tr("KEY_FILE_NOT_FOUND"), path])
		return

	if path.ends_with(".tex"):
		Logger.msg_debug("Resource selected: %s" % path, Logger.CATEGORY_ASSET)
		image_loaded = load(path) as ImageTexture

		Event.publish(self, A.EventList.editor.resource_loaded, [image_loaded])

		if _design_tab.current_tab == 2:
			if "ground" in path:
				_block_ground_view.set_texture(image_loaded, path)
			else:
				Alert.show_information("%s: %s" % [tr("KEY_FILE_NOT_BLOCK"), path])
		elif _design_tab.current_tab == 0:
			var metas: PoolStringArray = image_loaded.get_meta_list()

			if not metas.empty():
				for meta in metas:
					if meta == "name" or meta == "behaviour" or meta == "only_local":
						if meta == "name":
							name_behaviour.set_name_entity(image_loaded.get_meta(meta))
						if meta == "behaviour":
							name_behaviour.set_behaviour(image_loaded.get_meta(meta))
						if meta == "only_local":
							name_behaviour.set_only_local(image_loaded.get_meta(meta))
						continue

					var meta_property: String = "%s%sProperty" % [meta.left(1).to_upper(), meta.substr(1)]

					if property_picker.content.has_node(meta_property):
						Logger.msg_debug("Property exist: %s" % meta_property, Logger.CATEGORY_ASSET)
						var property: Property = property_picker.content.get_node(meta_property) as Property

						var data: Dictionary = image_loaded.get_meta(meta) as Dictionary
						property.set_data(data)
					else:
						Logger.msg_debug("Property doesn't exist: %s" % meta_property, Logger.CATEGORY_ASSET)
						var property: Property = property_picker.instance_property(meta_property)
						var data: Dictionary = image_loaded.get_meta(meta) as Dictionary
						property.set_data(data)
			else:
				Logger.msg_debug("No metas found", Logger.CATEGORY_ASSET)
	else:
		Logger.msg_debug("Image selected: %s" % path, Logger.CATEGORY_ASSET)

		var image: Image = Image.new()
		var _err = image.load(path)

		var image_texture: ImageTexture = ImageTexture.new()
		image_texture.create_from_image(image, 0)
		image_loaded = image_texture

		Event.publish(self, A.EventList.editor.resource_loaded, [image_loaded])

		if _design_tab.current_tab == 2:
			if "ground" in path:
				_block_ground_view.setup()
				_block_ground_view.interactive_creation(image_loaded, path)
			else:
				Alert.show_information("%s: %s" % [tr("KEY_FILE_NOT_BLOCK"), path])
		elif _design_tab.current_tab == 0:
			preview_image_loaded.image.frames = SpriteFrames.new()

			get_tree().call_group("property", "reset_values")

			# Load again properties if any were deleted
			for property in _properties_default:
				var meta_property: String = "%s%sProperty" % [property.left(1).to_upper(), property.substr(1)]

				if not property_picker.content.has_node(meta_property):
					Logger.msg_debug("%s added" % meta_property, Logger.CATEGORY_ASSET)
					var _discart = property_picker.instance_property(meta_property)

	_on_image_loaded()

func _new_file() -> void:
	if not _is_edited:
		show_dialog()
	else:
		_is_closed = false
		Alert.show_confirm_dialog(tr("KEY_CONFIRM"), tr("KEY_CREATOR_CHANGES_PENDING"), Instanceator.make_funcref_ext(self, "_on_confirm_ignore_no_save_confirmed"))

func _save_file() -> void:
	if not _current_file_name.empty():
		if _current_file_name.ends_with(".tex"):
			_on_SaveDialog_file_selected("current_file_meh")
		else:
			_save_dialog.show()
	else:
		Alert.show_alert(tr("KEY_CREATOR_NO_CHANGES"))

func _update_title() -> void:
	var title: String = "%s [%s]: %s" % [tr("KEY_CREATOR"), _current_type, _current_file_name]

	if _is_edited:
		title += "(*)"

	OS.set_window_title(title)
