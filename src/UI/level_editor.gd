class_name LevelEditor
extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _container_block_ground: Panel = null
var _loaded_blocks: int = 0
var _loaded_npcs: int = 0
var _level_test: LevelRoot = null
var _layers_added: int = 0

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
onready var _button_places_block: PanelButton = $Things/BlocksButton
onready var _button_places_background: PanelButton = $Things/BackgroundButton
onready var _button_places_npc: PanelButton = $Things/NPCsButton
onready var _button_places_warp: PanelButton = $Things/WarpsDoorsButton
onready var _button_places_liquid: PanelButton = $Things/LiquidsButton
onready var _button_places_player: PanelButton = $Things/PlayerButton
onready var _button_action_new: PanelButton = $Things/NewButton
onready var _button_action_open: PanelButton = $Things/OpenButton
onready var _button_action_save: PanelButton = $Things/SaveButton
onready var _button_action_test: PanelButton = $Things/TestButton
onready var _button_action_setting: PanelButton = $Things/SettingButton
onready var _button_action_creator: PanelButton = $Things/CreatorButton
onready var _panel_block: Panel = $BlockPanel
onready var _panel_npc: Panel = $NpcPanel
onready var _panel_player: Panel = $PlayerPanel
onready var _container_block: HBoxContainer = $BlockPanel/Container
onready var _container_npc: HBoxContainer = $NpcPanel/Container
onready var _container_player: GridContainer = $PlayerPanel/Container
onready var _data: LevelContainerData = $LevelContainer/Data
onready var _dialog: FileDialog = $FileDialog
onready var _panel_setting: Panel = $SettingPanel
onready var _event_dialog: FileSelectDialog = $EventSelectDialog
onready var _event_list: ItemList = $SettingPanel/Container/Level/Container/EventList
onready var _layer_list: Tree = $SettingPanel/Container/Level/Container/LayerList
onready var _level_setting: ScrollContainer = $SettingPanel/Container/Level
onready var _level_setting_button: Button = $SettingPanel/Container/Buttons/Level
onready var _general_setting: VBoxContainer = $SettingPanel/Container/General
onready var _general_setting_button: Button = $SettingPanel/Container/Buttons/General
onready var _name_level: FieldComponent = $SettingPanel/Container/Level/Container/Name
onready var _background_list: OptionComponent = $SettingPanel/Container/Level/Container/Background
onready var _music_list: OptionComponent = $SettingPanel/Container/Level/Container/Music
onready var _connect_sides_horizontal: CheckComponent = $SettingPanel/Container/Level/Container/ConnectSidesHorizontal
onready var _connect_sides_vertical: CheckComponent = $SettingPanel/Container/Level/Container/ConnectSidesVertical
onready var _disable_left: CheckComponent = $SettingPanel/Container/Level/Container/DisableLeftMovement
onready var _map_size: Vector2Component = $SettingPanel/Container/Level/Container/MapSize
onready var _map_origin: Vector2Component = $SettingPanel/Container/Level/Container/MapOrigin
onready var _volume_general: SliderComponent = $SettingPanel/Container/General/VolumeGeneral
onready var _volume_effect: SliderComponent = $SettingPanel/Container/General/VolumeEffect
onready var _value_version: Label = $Statusbar/Container/valueVersion

##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	OS.window_size = OS.get_screen_size()
	GlobalData.is_on_level_editor = true
	OS.center_window()

	_button_places_background.set_icon(Resources.get_image("icons/section-background"))
	_button_places_block.set_icon(Resources.get_frames_from_image("icons/section-block"))
	_button_places_liquid.set_icon(Resources.get_frames_from_image("icons/section-liquids"))
	_button_places_npc.set_icon(Resources.get_frames_from_image("icons/section-npc"))
	_button_places_warp.set_icon(Resources.get_image("icons/section-warp"))
	_button_places_player.set_icon(Resources.get_frames_from_image("icons/other-players"))

	_button_action_new.set_icon(Resources.get_image("icons/action-new"))
	_button_action_open.set_icon(Resources.get_image("icons/action-open"))
	_button_action_save.set_icon(Resources.get_image("icons/action-save"))
	_button_action_test.set_icon(Resources.get_image("icons/action-play"))
	_button_action_setting.set_icon(Resources.get_image("icons/action-setting"))
	_button_action_creator.set_icon(Resources.get_image("icons/action-importer"))

	if not ThemeUI.apply_theme(self, GlobalData.editor_theme):
		Logger.msg_warn("%s not found" % GlobalData.editor_theme, Logger.CATEGORY_GUI)

	get_tree().root.call_deferred("add_child", Resources.CREATOR)
	Resources.CREATOR.visible = false

	Event.subscribe(A.EventList.editor.block_added, self, "_load_block", GlobalData.object_database)

	Event.subscribe(A.EventList.editor.npc_added, self, "_load_npc", GlobalData.object_database)

	_load_objects()
	_load_players()
	_load_backgrounds()
	_load_musics()

	_name_level.set_value(GlobalData.current_level.name)
	_map_size.set_value(GlobalData.current_level.map_size)
	_map_origin.set_value(GlobalData.current_level.map_origin)

	_connect_sides_horizontal.checked = GlobalData.current_level.connect_sides_horizontally
	_connect_sides_vertical.checked = GlobalData.current_level.connect_sides_vertically
	_disable_left.checked = GlobalData.current_level.disable_left_movement

	_volume_general.set_value(Audio.volume_general)
	_volume_effect.set_value(Audio.volume_effect)

	_event_dialog.load_files()

	_data.refresh_level_background()

	var root: TreeItem = _layer_list.create_item()
	root.set_text(0, "Layers")

	_create_default_layer()

	_data.get_layer().is_in_level_editor()

	var tile_set: TileSet = Resources.get_image("blocks/ground/floor") as TileSet

	if tile_set:
		_data.get_layer().layer_one.tile_set = tile_set
	else:
		Logger.msg_fatal("Floor could not be loaded, please verify its integrity", Logger.CATEGORY_SYSTEM)
		Alert.show_error(tr("KEY_CORRUPT_FILE") % "floor")

	Event.subscribe(A.EventList.editor.block_ground_added, self, "_on_block_ground_added")

	_value_version.text = GlobalData.editor_version.to_string()

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_blocks_button_pressed() -> void:
	_panel_block.visible = not _panel_block.visible

	if _panel_block.visible:
		if GlobalData.open_panel and GlobalData.open_panel != _panel_block:
			GlobalData.open_panel.visible = false

		GlobalData.open_panel = _panel_block

func _on_creator_button_pressed() -> void:
	Transition.set_duration(0.5)
	Transition.to_instance(Resources.CREATOR, Transition.Kind.FADE, funcref(self, "_show_creator_editor"))

func _on_event_add_pressed() -> void:
	_event_dialog.popup_centered()

func _on_event_delete_pressed() -> void:
	if _event_list.is_anything_selected():
		for index in _event_list.get_selected_items():
			GlobalData.current_level.events.erase(_event_list.get_item_text(index))
			_event_list.remove_item(index)

func _on_event_select_dialog_file_selected(file_name: String) -> void:
	add_event(file_name)

func _on_layer_add_pressed() -> void:
	Alert.show_input_dialog(tr("KEY_LAYER_NAME"), Instanceator.make_funcref_ext(self, "_on_layer_add_dialog_confirmed"))

func _on_layer_delete_pressed() -> void:
	var selected: TreeItem = _layer_list.get_selected()

	if selected:
		if not "Default" in selected.get_text(0):
			var layer_id: int = int(selected.get_metadata(0))
			selected.free()
			_data.get_layer().remove_layer(layer_id)
			var _err = GlobalData.current_level.layers.erase(layer_id)
		else:
			Alert.show_error(tr("KEY_ERROR_DELETE_LAYER_DEFAULT"))
	else:
		Alert.show_information(tr("KEY_ERROR_DELETE_LAYER_NO_SELECT"))

func _on_layer_add_dialog_confirmed(layer_name: String) -> void:
	create_layer(layer_name)

func _on_layer_list_item_edited() -> void:
	var selected: TreeItem = _layer_list.get_selected()
	if selected:
		var layer_id: int = int(selected.get_metadata(0))
		_data.get_layer().set_layer_visible(layer_id, selected.is_checked(0))
		GlobalData.current_level.layers[layer_id][1] = selected.is_checked(0)

func _on_layer_list_item_selected() -> void:
	var selected: TreeItem = _layer_list.get_selected()
	if selected:
		var layer_id: int = int(selected.get_metadata(0))
		GlobalData.current_layer = layer_id
		_data.set_current_layer_name(selected.get_text(0))

func _on_file_dialog_file_selected(path: String) -> void:
	var path_globalize: String = ProjectSettings.globalize_path(path)
	var file_name: String = path_globalize.get_file()

	A.Paths.level = path_globalize.replace(file_name, "")
	A.Paths.level_file_name = file_name

	if _dialog.mode == FileDialog.MODE_SAVE_FILE:
		GlobalData.current_level.editor_version = GlobalData.editor_version

		if GlobalData.current_level.name == "New level":
			GlobalData.current_level.name = file_name.get_basename()

		if GlobalData.current_level.identifier.empty():
			GlobalData.current_level.identifier = UUID.v4()

		var block_rewards: Array = []

		for layer in GlobalData.current_level.data:
			for info_position in GlobalData.current_level.data[layer]:
				if GlobalData.current_level.data[layer][info_position] is BlockRewardInfo:
					block_rewards.append(GlobalData.current_level.data[layer][info_position])

		if block_rewards.empty():
			GlobalData.current_level.rewards.clear()
		else:
			var rewards = GlobalData.current_level.rewards.keys()
			var rewards_to_remove: Array = []
			var is_different: bool = false

			#//TODO: Check this implementation
			for reward in rewards:
				is_different = false
				for block_reward in block_rewards:
					if block_reward.reward.npc_identificator == reward:
						is_different = false
						break

					is_different = true

				if is_different:
					rewards_to_remove.append(reward)

			var _err: int = -1
			for reward in rewards_to_remove:
				_err = GlobalData.current_level.rewards.erase(reward)

		var json: String = JSONConvert.serialize(GlobalData.current_level)
		var _err = Utility.save_file(path_globalize, json)
	elif _dialog.mode == FileDialog.MODE_OPEN_FILE:
		var json: String = Utility.load_file(path_globalize)

		if "$@path" in json:
			var _err = Utility.save_file("%s.bkp" % path_globalize, json)
			Logger.msg_info("Backing up level before conversion: %d" % _err, Logger.CATEGORY_SYSTEM)

			Logger.msg_info("Converting old json to new json", Logger.CATEGORY_SYSTEM)
			json = _convert_old_json_to_new_json(json)

		var to_result = JSONConvert.deserialize(json) as LevelData

		_check_version(to_result)

func _on_npcs_button_pressed() -> void:
	_panel_npc.visible = not _panel_npc.visible

	if _panel_npc.visible:
		if GlobalData.open_panel and GlobalData.open_panel != _panel_npc:
			GlobalData.open_panel.visible = false

		GlobalData.open_panel = _panel_npc

func _on_new_button_pressed() -> void:
	_data.clean_all_data()
	_event_list.clear()
	_layer_list.clear()

func _on_open_button_pressed() -> void:
	_dialog.mode = FileDialog.MODE_OPEN_FILE
	_dialog.rect_min_size = Vector2(400, 500)
	_dialog.rect_size = _dialog.rect_min_size
	_dialog.popup_centered()

func _on_player_button_pressed() -> void:
	_panel_player.visible = not _panel_player.visible

	if _panel_player.visible:
		if GlobalData.open_panel and GlobalData.open_panel != _panel_player:
			GlobalData.open_panel.visible = false

		GlobalData.open_panel = _panel_player

func _on_save_button_pressed() -> void:
	_dialog.mode = FileDialog.MODE_SAVE_FILE
	_dialog.rect_min_size = Vector2(400, 500)
	_dialog.rect_size = _dialog.rect_min_size
	_dialog.popup_centered()

func _on_test_button_pressed() -> void:
	visible = false
	GlobalData.is_on_level_editor = false
	GlobalData.is_on_test_level = true

	_level_test = Resources.LEVEL_ROOT.instance() as LevelRoot
	_level_test.info = GlobalData.current_level

	if not GlobalData.level_values.has(_level_test.info.identifier):
		GlobalData.level_values[_level_test.info.identifier] = {}

		var _err = _level_test.set_value("level_origin", GlobalData.current_level.map_origin)
		_err = _level_test.set_value("level_size", GlobalData.current_level.map_size)

	get_tree().root.add_child(_level_test)
	_level_test.visible = false
	_level_test.set_background(_data.get_background_texture())

	Transition.set_duration(0.5)
	Transition.set_delay(0.75)
	Transition.to_instance(_level_test, Transition.Kind.FADE, funcref(self, "_start_level_test"))

func _on_setting_button_pressed() -> void:
	_panel_setting.visible = not _panel_setting.visible

func _on_background_option_changed(value: String) -> void:
	if value == "None":
		value = ""

	GlobalData.current_level.background = value
	_data.set_level_background(GlobalData.current_level.background)

func _on_name_input_x_changed(value: String) -> void:
	GlobalData.current_level.name = value

func _on_music_option_changed(value: String) -> void:
	Audio.player_spc.stop_music()
	Audio.player.stop()

	if value != "None":
		GlobalData.current_level.music = value
		Audio.play_music(value)

func _on_connect_sides_horizontal_check_changed(value: bool) -> void:
	GlobalData.current_level.connect_sides_horizontally = value

func _on_connect_sides_vertical_check_changed(value: bool) -> void:
	GlobalData.current_level.connect_sides_vertically = value

func _on_disable_left_movement_check_changed(value: bool) -> void:
	GlobalData.current_level.disable_left_movement = value

func _on_map_size_text_input_x_changed(value: int) -> void:
	GlobalData.current_level.map_size.x = value
	_data.refresh_level_background()

func _on_map_size_text_input_y_changed(value:int ) -> void:
	GlobalData.current_level.map_size.y = value
	_data.refresh_level_background()

func _on_map_origin_text_input_x_changed(value: int) -> void:
	GlobalData.current_level.map_origin.x = value
	_data.refresh_level_background()

func _on_map_origin_text_input_y_changed(value:int ) -> void:
	GlobalData.current_level.map_origin.y = value
	_data.refresh_level_background()

func _on_level_toggled(button_pressed: bool) -> void:
	_level_setting.visible = button_pressed
	_general_setting.visible = not button_pressed

func _on_general_toggled(button_pressed: bool) -> void:
	_level_setting.visible = not button_pressed
	_general_setting.visible = button_pressed

func _on_volume_general_value_changed(value: float) -> void:
	GlobalData.db.open_db()
	GlobalData.db.update_rows("settings", "key = 'volume_general'", {"value": value})
	GlobalData.db.close_db()
	Audio.volume_general = value

func _on_volume_effect_value_changed(value: float) -> void:
	GlobalData.db.open_db()
	GlobalData.db.update_rows("settings", "key = 'volume_effect'", {"value": value})
	GlobalData.db.close_db()
	Audio.volume_effect = value

func _on_play_stop_music_pressed() -> void:
	Audio.toggle()

func _on_block_ground_added(name: String, style: String) -> void:
	var tile_name: String = "%s %s" % [style, name]
	var tile_set: TileSet = _data.get_map().tile_set
	if tile_set.find_tile_by_name(tile_name) == TileMap.INVALID_CELL:
		var tile_id: int = tile_set.get_last_unused_tile_id()
		var compound_path: String = "blocks/ground".plus_file("%s/%s" % [style, name])
		var texture: Texture = Resources.get_image(compound_path)

		if texture:
			# Backup tile_set
			var _err: int = Resources.save(A.Paths.assets.plus_file("images/blocks/ground/floor.bkp"), tile_set)

			tile_set.create_tile(tile_id)
			tile_set.tile_set_name(tile_id, tile_name)
			tile_set.tile_set_texture(tile_id, texture)
			tile_set.set("%d/tex_offset" % tile_id, tile_set.get("0/tex_offset"))
			tile_set.set("%d/modulate" % tile_id, tile_set.get("0/modulate"))
			tile_set.set("%d/region" % tile_id, tile_set.get("0/region"))
			tile_set.set("%d/tile_mode" % tile_id, tile_set.get("0/tile_mode"))
			tile_set.set("%d/autotile/bitmask_mode" % tile_id, tile_set.get("0/autotile/bitmask_mode"))
			tile_set.set("%d/autotile/bitmask_flags" % tile_id, tile_set.get("0/autotile/bitmask_flags"))
			tile_set.set("%d/autotile/icon_coordinate" % tile_id, tile_set.get("0/autotile/icon_coordinate"))
			tile_set.set("%d/autotile/tile_size" % tile_id, tile_set.get("0/autotile/tile_size"))
			tile_set.set("%d/autotile/spacing" % tile_id, tile_set.get("0/autotile/spacing"))
			tile_set.set("%d/autotile/occluder_map" % tile_id, tile_set.get("0/autotile/occluder_map"))
			tile_set.set("%d/autotile/navpoly_map" % tile_id, tile_set.get("0/autotile/navpoly_map"))
			tile_set.set("%d/autotile/priority_map" % tile_id, tile_set.get("0/autotile/priority_map"))
			tile_set.set("%d/autotile/z_index_map" % tile_id, tile_set.get("0/autotile/z_index_map"))
			tile_set.set("%d/occluder_offset" % tile_id, tile_set.get("0/occluder_offset"))
			tile_set.set("%d/navigation_offset" % tile_id, tile_set.get("0/navigation_offset"))
			tile_set.set("%d/shape_offset" % tile_id, tile_set.get("0/shape_offset"))
			tile_set.set("%d/shape_transform" % tile_id, tile_set.get("0/shape_transform"))
			tile_set.set("%d/shape" % tile_id, tile_set.get("0/shape"))
			tile_set.set("%d/shape_one_way" % tile_id, tile_set.get("0/shape_one_way"))
			tile_set.set("%d/shape_one_way_margin" % tile_id, tile_set.get("0/shape_one_way_margin"))
			tile_set.set("%d/shapes" % tile_id, tile_set.get("0/shapes"))

			_err = Resources.save(A.Paths.assets.plus_file("images/blocks/ground/floor.res"), tile_set)

			if _err == OK:
				if _container_block_ground:
					var image_texture: ImageTexture = _container_block_ground.crop_texture(tile_id, tile_set, texture)
					_container_block_ground.create_content_tile(str(tile_id), [image_texture])
					_container_block_ground.update_size()


##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func add_event(event_name: String) -> void:
	_event_list.add_item(event_name)
	GlobalData.current_level.events.append(event_name)

func create_layer(layer_name: String = "") -> void:
	var new_layer_name: String = "New layer"
	if not layer_name.empty():
		new_layer_name = layer_name

	_layers_added += 1
	var _dis = _build_layer_node(_layer_list.get_root(), _layers_added, new_layer_name)
	_data.get_layer().check_layer(_layers_added)
	GlobalData.current_level.layers[_layers_added] = [layer_name, false]

func get_map_size_component() -> Vector2Component:
	return _map_size

func get_map_origin_component() -> Vector2Component:
	return _map_origin


func _build_layer_node(root: TreeItem, layer_id: int, name: String, editable: bool = true) -> TreeItem:
	var to_result: TreeItem = _layer_list.create_item(root)
	to_result.set_cell_mode(0, TreeItem.CELL_MODE_CHECK)
	to_result.set_editable(0, editable)
	to_result.set_checked(0, false)
	to_result.set_text(0, name)
	to_result.set_metadata(0, layer_id)

	return to_result

func _check_version(to_result: LevelData) -> void:
	if to_result.editor_version.is_greater_than(GlobalData.editor_version):
		Alert.show_confirm_dialog(tr("KEY_INFORMATION"), tr("KEY_ERROR_HIGHER_LEVEL_VERSION") % [to_result.editor_version, GlobalData.editor_version], Instanceator.make_funcref_ext(self, "_load_level", to_result))
	elif to_result.editor_version.is_less_than(GlobalData.editor_version):
		var next_version: String = GlobalData.get_next_version_for_conversion(to_result.editor_version.to_string())

		if not next_version.empty():
			var method: String = "%s_to_%s" % [to_result.editor_version.to_string().replace(".", "_").replace("-", "_"), next_version.replace(".", "_").replace("-", "_")]
			if not _convert_map(method, to_result):
				Alert.show_error(tr("KEY_ERROR_CONVERTER_NOT_FOUND"))
			else:
				Alert.show_information(tr("KEY_CONVERTER_SUCCESS"))
		else:
			Alert.show_error(tr("KEY_ERROR_CONVERTER_NOT_FOUND"))
	else:
		_load_level(to_result)

func _convert_old_json_to_new_json(json: String) -> String:
	var regex: RegEx = RegEx.new()
	var _err = regex.compile('"\\$@path":"res:/(?:\\/[^/]+)+?/(\\w+)(\\.\\w+)"')
	var results: Array = regex.search_all(json)
	var replaces: Array = []

	for result in results:
		if replaces.has(result.strings[0]):
			continue

		json = json.replace(result.strings[0], '"%s":"%s"' % [JSONObject.KEY_TYPE, result.strings[1]])
		replaces.append(result.strings[0])

	return json

func _convert_map(converter: String, level: LevelData) -> bool:
	var path_converter: String = A.Paths.data.plus_file('converters').plus_file('editor').plus_file('%s.gd' % converter)
	var to_result: bool = true

	if A.file_loader.file_exists(path_converter):
		var converter_instance: BaseScript = Utility.load_script(path_converter).new() as BaseScript
		level = converter_instance.run(level, _data)
		to_result = level != null

		if to_result:
			_check_version(level)
	else:
		to_result = false

	return to_result

func _create_default_layer() -> void:
	_layers_added += 1
	var default: TreeItem = _build_layer_node(_layer_list.get_root(), _layers_added, "Default", false)
	default.set_checked(0, true)

	GlobalData.current_layer = _layers_added

func _load_backgrounds() -> void:
	var files: Array = Resources.get_images("backgrounds").keys()
	files.sort_custom(Arrays.NumberSorter, "sort_ascending")
	var filenames: Array = ["None"]
	for file in files:
		filenames.append(file.replace("backgrounds/", ""))

	_background_list.items = filenames

#type = DataInfoType
func _load_block(path: String, type: int) -> void:
	var preview_list: PreviewerList = Resources.PREVIEWER_LIST.instance() as PreviewerList
	preview_list.root = "blocks"

	var data_path: PoolStringArray = []

	if "/" in path:
		data_path = path.split("/")
	else:
		data_path.append(path)

	preview_list.category = data_path[0]

	if data_path.size() == 2:
		preview_list.subcategory = data_path[1]

	preview_list.type = type
	preview_list.name = "%s_%s" % [preview_list.category, preview_list.subcategory]

	_container_block.add_child(preview_list, true)
	_container_block.move_child(preview_list, _loaded_blocks)

	if "blocks_ground" in preview_list.name:
		_container_block_ground = preview_list

	_loaded_blocks += 1

func _load_level(level: LevelData) -> void:
	_data.clean_all_data()

	GlobalData.current_level = level
	if GlobalData.current_level.background != "None":
		_data.refresh_level_background()
		_background_list.set_item_selected(GlobalData.current_level.background)

	if GlobalData.current_level.music != "None":
		_music_list.set_item_selected(GlobalData.current_level.music)
		_on_music_option_changed(GlobalData.current_level.music)

	# Set variables in UI
	_name_level.set_value(GlobalData.current_level.name)
	_map_size.set_value(GlobalData.current_level.map_size)
	_map_origin.set_value(GlobalData.current_level.map_origin)

	_connect_sides_horizontal.checked = GlobalData.current_level.connect_sides_horizontally
	_connect_sides_vertical.checked = GlobalData.current_level.connect_sides_vertically
	_disable_left.checked = GlobalData.current_level.disable_left_movement

	_event_list.clear()
	_layer_list.clear()

	_create_default_layer()

	if not GlobalData.current_level.events.empty():
		for event in GlobalData.current_level.events:
			_event_list.add_item(event)

	if not GlobalData.current_level.layers.empty():
		var layers: Dictionary = GlobalData.current_level.layers

		for layer in layers:
			var layer_item: TreeItem = _build_layer_node(_layer_list.get_root(), layer, layers[layer][0])
			layer_item.set_checked(0, layers[layer][1])

			if "Default" in layers[layer][0]:
				layer_item.set_editable(0, false)

			_data.get_layer().check_layer(layer)
			_data.get_layer().set_layer_visible(layer, layers[layer][1])
			_layers_added = layer

		GlobalData.current_layer = 1

	#Parse all rewards in npc
	for reward in GlobalData.current_level.rewards.values():
		reward.load_frames()

	var tiles: Dictionary = GlobalData.current_level.tiles
	for tile in tiles.keys():
		for layer in tiles[tile]:
			for tile_position in tiles[tile][layer]:
				_data.set_tile(layer, tile_position, tile, true)

	var datas: Dictionary = GlobalData.current_level.data

	for layer in datas.keys():
		for data_position in datas[layer].keys():
			match datas[layer][data_position].type:
				Enums.DataInfoType.BLOCK:
					var block_info: BlockInfo = datas[layer][data_position] as BlockInfo
					_data.add_data_info(layer, data_position, block_info, true)
				Enums.DataInfoType.BLOCK_REWARD:
					var block_reward_info: BlockRewardInfo = datas[layer][data_position] as BlockRewardInfo
					_data.add_data_info(layer, data_position, block_reward_info, true)
				Enums.DataInfoType.ENEMY:
					var enemy_info: EnemyInfo = datas[layer][data_position] as EnemyInfo
					_data.add_data_info(layer, data_position, enemy_info, true)
				Enums.DataInfoType.OBJECT:
					var object_info: ObjectInfo = datas[layer][data_position] as ObjectInfo
					_data.add_data_info(layer, data_position, object_info, true)
				Enums.DataInfoType.POWERUP:
					var powerup_info: PowerupInfo = datas[layer][data_position] as PowerupInfo
					_data.add_data_info(layer, data_position, powerup_info, true)
				Enums.DataInfoType.PLAYER:
					var player_info: PlayerInfo = datas[layer][data_position] as PlayerInfo
					_data.add_data_info(layer, data_position, player_info, true)
				Enums.DataInfoType.ACTIVATABLE:
					var activatable_info: ActivatableInfo = datas[layer][data_position] as ActivatableInfo
					_data.add_data_info(layer, data_position, activatable_info, true)

func _load_musics() -> void:
	var files: Array = Resources.get_audios("music").keys()
	files.sort_custom(Arrays.NumberSorter, "sort_ascending")
	var filenames: Array = ["None"]
	for file in files:
		filenames.append(file.replace("music/", ""))

	_music_list.items = filenames

#type = DataInfoType
func _load_npc(path: String, type: int) -> void:
	var preview_list: PreviewerList = Resources.PREVIEWER_LIST.instance() as PreviewerList
	preview_list.root = "npcs"
	var data_path: PoolStringArray = []

	if "/" in path:
		data_path = path.split("/")
	else:
		data_path.append(path)

	preview_list.category = data_path[0]

	if data_path.size() == 2:
		preview_list.subcategory = data_path[1]

	preview_list.type = type
	preview_list.name = "%s_%s" % [preview_list.category, preview_list.subcategory]

	_container_npc.add_child(preview_list, true)
	_container_npc.move_child(preview_list, _loaded_npcs)
	_loaded_npcs += 1

func _load_objects() -> void:
	var blocks: Dictionary = GlobalData.object_database.blocks
	var npcs: Dictionary = GlobalData.object_database.npcs

	for key in blocks.keys():
		_load_block(key, blocks[key])

	for key in npcs.keys():
		_load_npc(key, npcs[key])

func _load_players() -> void:
	var files: Array = Resources.get_images("player").keys()
	var files_new: Array = []
	var regex: RegEx = RegEx.new()
	var _err = regex.compile("(?<=player\\/)(.*)(?=small)")

	for file in files:
		if regex.search(file):
			files_new.append(file)

	var file_name: String = ""

	for file in files_new:
		file_name = file.replace("player/", "").replace("/small", "")

		var frames: Resource = Resources.get_frames_from_image(file)
		var textures: Array = []

		if frames is SpriteFrames:
			textures = SpriteFramesExt.get_frames(frames, "walk")

		var preview_single = Resources.PREVIEWER_SINGLE.instance() as PreviewerSingle

		_container_player.add_child(preview_single)
		preview_single.set_data("player", "", "", file_name, textures, Enums.DataInfoType.PLAYER)

func _start_level_test() -> void:
	#OS.window_maximized = false
	_level_test.visible = true
	_level_test.start()

func _show_creator_editor() -> void:
	visible = false
	GlobalData.is_mouse_on_level_canvas = false
	GlobalData.is_mouse_on_level_window = false
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	Resources.CREATOR.show_dialog()
