extends Control

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	CONSTANTS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	ENUMS
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	EXPORTED VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################
var _stopwatch: StopWatch = StopWatch.new()

##############################################################################
#
#----------------------------------------------------------------------------
#	ONREADY VARIABLES
#----------------------------------------------------------------------------
#
##############################################################################


##############################################################################
#
#----------------------------------------------------------------------------
#	VIRTUAL METHODS FROM GODOT
#----------------------------------------------------------------------------
#
##############################################################################
func _ready() -> void:
	_stopwatch.start()
	var _err = Resources.connect("resources_loaded", self, "_on_resources_loaded")
#
#
#	convert_multiples_tex_to_only_tex(A.Paths.assets.plus_file("images/blocks/ground"))

#	convert_png_to_tex("/home/blackblex/Proyectos/Godot/Git/NetSMBgd/assets/images/icons/convert")

	Logger.msg_info("Checking DB file...", Logger.CATEGORY_SYSTEM)
	if not A.file_loader.file_exists(A.Paths.db_file):
		Logger.msg_error("DB not found...", Logger.CATEGORY_SYSTEM)
	else:
		Logger.msg_info("Using version: %s" % GlobalData.editor_version, Logger.CATEGORY_SYSTEM)
		_load_db()
		Logger.msg_info("Loading resources...", Logger.CATEGORY_ASSET)
		Resources.load_all(self)

##############################################################################
#
#----------------------------------------------------------------------------
#	SIGNALS CALLBACK
#----------------------------------------------------------------------------
#
##############################################################################
func _on_resources_loaded() -> void:
	_stopwatch.stop()
	Logger.msg_info("Completed load in: %s msec" % str(_stopwatch.get_elapsed_msec()), Logger.CATEGORY_ASSET)
	Resources.disconnect("resources_loaded", self, "_on_resources_loaded")

	if GlobalData.object_database:
		GlobalData.object_database.parse_backgrounds()

	Transition.set_duration(0.75)
	Transition.set_delay(0.2)
	Transition.to_scene("res://src/UI/LevelEditor.tscn", Transition.Kind.FADE)

##############################################################################
#
#----------------------------------------------------------------------------
#	FUNCTIONS
#----------------------------------------------------------------------------
#
##############################################################################
func convert_multiples_tex_to_only_tex(path: String) -> void:
	var files: Array = Utility.get_files(path, "smw-grass.tex", true)

	files.sort_custom(Arrays.NumberSorter, "sort_ascending")

	var columns: int = 3
	var rows: int = int(ceil(float(files.size()) / float(columns)))

	print("Creating tex with columns: ", columns, " and rows: ", rows, " | Files: ", files.size())

	var new_image: Image = Image.new()
	new_image.create(GlobalData.BLOCK_SIZE * (rows + (rows - 1)), GlobalData.BLOCK_SIZE * (columns + (columns - 1)), false, Image.FORMAT_RGBA8)

	var _err: int = 0
	var tex_loaded: ImageTexture = null
	var current_file: String = ""

	var groups_of_nine: int = int(ceil(float(files.size()) / 9.0))
	var groups: Array = []

	print("Making: ", groups_of_nine, " groups")
	for _group in range(groups_of_nine):
		var file_group: Array = []
		for _i in range(9):
			if files.size() == 0:
				break
			file_group.append(files.pop_front())

		groups.append(file_group)

	var destination: Vector2 = Vector2.ZERO
	var no_more_group: bool = false

	for i in range(groups.size()):
		var group: Array = groups[i]
		print("i: ", i)
		no_more_group = false
		for y in [0, 1, 2]:
			if no_more_group:
				break

			for x in [0, 1, 2]:
				if group.size() == 0:
					no_more_group = true
					break

				current_file = group.pop_front().replace("/home/blackblex/Proyectos/Godot/Git/NetSMBgd/", "res://")
				tex_loaded = load(current_file) as ImageTexture
				if not tex_loaded:
					breakpoint

				destination = (GlobalData.BLOCK_SIZE * Vector2(x, y))
				print("Original: ", destination)

				destination += Vector2(i * 3, 0) * GlobalData.BLOCK_SIZE
				print("Result: ", destination)

				new_image.blit_rect(tex_loaded.get_data(), Rect2(Vector2.ZERO, tex_loaded.get_size()), destination)

				yield(get_tree().create_timer(0.5), "timeout")

		if no_more_group:
			break

	var new_tex: ImageTexture = ImageTexture.new()
	new_tex.create_from_image(new_image, 0)
	new_tex.lossy_quality = 0

	_err = ResourceSaver.save("user://smw-grass.tex", new_tex, ResourceSaver.FLAG_COMPRESS)
	_err = ResourceSaver.save("user://smw-grass.png", new_tex, ResourceSaver.FLAG_COMPRESS)

	if _err != OK:
		breakpoint
	else:
		print("complete")

func convert_multiples_tex_to_only_tex_vertical(path: String) -> void:
	var files: Array = Utility.get_files(path, "smb3-wood.tex", true)

	files.sort_custom(Arrays.NumberSorter, "sort_ascending")

	var columns: int = 3
	var rows: int = int(ceil(float(files.size()) / float(columns)))

	print("Creating tex with columns: ", columns, " and rows: ", rows, " | Files: ", files.size())

	var new_image: Image = Image.new()
	new_image.create(GlobalData.BLOCK_SIZE * (columns + (columns - 1)), GlobalData.BLOCK_SIZE * (rows + (rows - 1)), false, Image.FORMAT_RGBA8)

	var _err: int = 0
	var tex_loaded: ImageTexture = null
	var current_file: String = ""

	for y in range(rows):
		if files.size() == 0:
			break

		for x in range(columns):
			if files.size() == 0:
				break

			current_file = files.pop_front().replace("/home/blackblex/Proyectos/Godot/Git/NetSMBgd/", "res://")
			tex_loaded = load(current_file) as ImageTexture
			if not tex_loaded:
				breakpoint

			new_image.blit_rect(tex_loaded.get_data(), Rect2(Vector2.ZERO, tex_loaded.get_size()), (GlobalData.BLOCK_SIZE * Vector2(x, y)) + Vector2(GlobalData.BLOCK_SIZE * x, GlobalData.BLOCK_SIZE * y))

			yield(get_tree().create_timer(0.5), "timeout")

	var new_tex: ImageTexture = ImageTexture.new()
	new_tex.create_from_image(new_image, 0)
	new_tex.lossy_quality = 0

	_err = ResourceSaver.save("user://smb3-wood.tex", new_tex, ResourceSaver.FLAG_COMPRESS)
	_err = ResourceSaver.save("user://smb3-wood.png", new_tex, ResourceSaver.FLAG_COMPRESS)

	if _err != OK:
		breakpoint
	else:
		print("complete")

func convert_png_to_tex(path: String) -> void:
	var files: Array = Utility.get_files(path, "png")

	for file in files:
		var image: Image = Image.new()
		var _err = image.load(file.replace("/home/blackblex/Proyectos/Godot/Git/NetSMBgd/", "res://"))

		var image_texture: ImageTexture = ImageTexture.new()
		image_texture.create_from_image(image, 0)

		image_texture.lossy_quality = 0
		_err = ResourceSaver.save(file.replace("png", "tex"), image_texture, ResourceSaver.FLAG_COMPRESS)

		yield(get_tree().create_timer(1), "timeout")


func _load_db() -> void:
	Logger.msg_info("Loading DB file...", Logger.CATEGORY_SYSTEM)
	GlobalData.db = GlobalData.SQLITE.new()
	GlobalData.db.path = A.Paths.db_file
	GlobalData.db.open_db()

	_load_block_exclution()
	_load_object_db()

	GlobalData.db.close_db()

func _load_backgrounds() -> void:
	GlobalData.db.query("select name, top, bottom from backgrounds;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		var name: String = ""
		for i in range(results.size()):
			name = "background%s" % results[i].name
			GlobalData.object_database.backgrounds[name] = {}
			if results[i].top != null and results[i].top.count(",") > 2:
				GlobalData.object_database.backgrounds[name].top = str2var("Rect2%s" % results[i].top)
			else:
				if results[i].top != null:
					GlobalData.object_database.backgrounds[name].top = "background%s" % results[i].top
			if results[i].bottom != null and results[i].bottom.count(",") > 2:
				GlobalData.object_database.backgrounds[name].bottom = str2var("Rect2%s" % results[i].bottom)
			else:
				if results[i].bottom != null:
					GlobalData.object_database.backgrounds[name].bottom = "background%s" % results[i].bottom
	else:
		Logger.msg_warn("backgrounds table is empty", Logger.CATEGORY_SYSTEM)

func _load_blocks() -> void:
	GlobalData.db.query("select name, type from blocks;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		for i in range(results.size()):
			GlobalData.object_database.blocks[results[i].name] = results[i].type
	else:
		Logger.msg_warn("blocks table is empty", Logger.CATEGORY_SYSTEM)

func _load_npcs() -> void:
	GlobalData.db.query("select name, type from npcs;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		for i in range(results.size()):
			GlobalData.object_database.npcs[results[i].name] = results[i].type
	else:
		Logger.msg_warn("npcs table is empty", Logger.CATEGORY_SYSTEM)

func _load_keywords() -> void:
	GlobalData.db.query("select name, hint, usage, completion from keywords;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		var keyword_inst: Keyword = null
		for i in range(results.size()):
			keyword_inst = Keyword.new()
			keyword_inst.hint = results[i].hint
			if results[i].has("usage") and results[i].usage != null:
				keyword_inst.usage = results[i].usage
			keyword_inst.completion = results[i].completion.split(";")
			GlobalData.object_database.keywords[results[i].name] = keyword_inst
	else:
		Logger.msg_warn("keywords table is empty", Logger.CATEGORY_SYSTEM)

func _load_settings() -> void:
	GlobalData.db.query("select key, value from settings;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		for i in range(results.size()):
			if results[i].key.begins_with("volume"):
				Audio.set(results[i].key, float(results[i].value))
	else:
		Logger.msg_warn("Settings table is empty", Logger.CATEGORY_SYSTEM)

func _load_block_exclution() -> void:
	GlobalData.db.query("select name from block_exclusion;")
	var results = GlobalData.db.query_result
	if results.size() > 0:
		for i in range(results.size()):
			GlobalData.block_exclusion.append(results[i].name)
	else:
		Logger.msg_warn("block_exclusion table is empty", Logger.CATEGORY_SYSTEM)

func _load_object_db() -> void:
	_load_backgrounds()
	_load_blocks()
	_load_npcs()
	_load_keywords()
	_load_settings()
