# tool
extends Path2D


# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	curve.clear_points()

	var values: Dictionary = Maths.sin_values([0, 3.25, 0.25], 8)

	var keys: Array = values.keys()
	for x in keys:
		curve.add_point(Vector2(x * 24, -values[x] * 16))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta: float) -> void:
#	pass
