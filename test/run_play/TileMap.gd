extends Control

var tile_id: int = -1
var current_position: Vector2 = Vector2.ZERO

func _ready() -> void:
	tile_id = $Map.tile_set.find_tile_by_name("smw cave")
	print(tile_id)
	print($Map.tile_set.get_tiles_ids())
	print($Map.tile_set.get_last_unused_tile_id())


func _on_LevelEditor_gui_input(event: InputEvent) -> void:
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT:
			current_position = $Map.world_to_map(event.position)
			$Map.set_cell(current_position.x, current_position.y, tile_id)
			$Map.update_bitmask_area(current_position)
