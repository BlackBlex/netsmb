tool
extends Tree

func _ready() -> void:
	var item = create_item()
	item.set_text(0, "Hola")

	var item2 = create_item(item)
	item2.set_text(0, "Here")

	item2.set_editable(1, true)

	var _item3 = create_item(item)
	#item3.add_button(0, null)
