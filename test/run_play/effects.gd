extends Node2D

func _ready() -> void:
	yield(get_tree().create_timer(1), "timeout")
	setup()

func setup() -> void:
	var effects = EntityExt.effects(self)

	effects.load_image(EffectsExecuter.Helper.EffectCategory.PLAYER, "mario-die").positionate_path(Vector2(200, 200)).invert_path().move_to_path().load_curve("kill_with_shell").animate().interpolate_property(effects.get_path_follow(), "unit_offset", 0, 1, 0.5)

	effects.create()
