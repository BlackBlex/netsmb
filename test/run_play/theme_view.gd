extends TabContainer

onready var tree: Tree = $Others/HBoxContainer/VBoxContainer/Tree

func _ready() -> void:
	var root = tree.create_item()
	root.set_text(0, "Tree")
	var item = tree.create_item(root)
	item.set_text(0,"Item")

	#item.add_button(0, null)
	var item_editable = tree.create_item(root)
	item_editable.set_text(0, "Editable item")
	item_editable.set_editable(0, true)
	var subtree = tree.create_item(root)
	subtree.set_text(0, "Subtree")
	subtree.collapsed = false
	var item_check = tree.create_item(subtree)
	item_check.set_cell_mode(0, TreeItem.CELL_MODE_CHECK)
	item_check.set_editable(0, true)
	item_check.set_text(0, "Check item")
	var item_spin = tree.create_item(subtree)
	item_spin.set_cell_mode(0, TreeItem.CELL_MODE_RANGE)
	item_spin.set_editable(0, true)
	item_spin.set_range_config(0, 0, 100, 1, false)
	item_spin.set_range(0, 50)
	var item_option = tree.create_item(subtree)
	item_option.set_cell_mode(0, TreeItem.CELL_MODE_RANGE)
	item_option.set_editable(0, true)
	item_option.set_text(0, "Has,Many,Options,Here")
	#item_option.set_range(0, item_option.get_text(0).count(",") + 1)


var visibleWindow = false
func _on_WindowsDialogButton_pressed() -> void:
	if not visibleWindow:
		$Windows/VBoxContainer/WindowDialog.popup_centered()
		visibleWindow = true
	else:
		$Windows/VBoxContainer/WindowDialog.visible = false
		visibleWindow = false

var visibleAccept = false
func _on_AcceptDialogButton_pressed() -> void:
	if not visibleAccept:
		$Windows/VBoxContainer/AcceptDialog.popup_centered()
		visibleAccept = true
	else:
		$Windows/VBoxContainer/AcceptDialog.visible = false
		visibleAccept = false

var visibleConfirm = false
func _on_ConfirmationDialogButton_pressed() -> void:
	if not visibleConfirm:
		$Windows/VBoxContainer/ConfirmationDialog.popup_centered()
		visibleConfirm = true
	else:
		$Windows/VBoxContainer/ConfirmationDialog.visible = false
		visibleConfirm = false

var visibleFile = false
func _on_FileDialogButton_pressed() -> void:
	if not visibleFile:
		$Windows/VBoxContainer/FileDialog.popup_centered()
		visibleFile = true
	else:
		$Windows/VBoxContainer/FileDialog.visible = false
		visibleFile = false
