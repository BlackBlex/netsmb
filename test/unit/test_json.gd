extends GutTest

var data := Vector2(10, 20)
var expected := "\"(10, 20)\""

func test_vector2_as_json():
	var to_result = JSONConvert.serialize(data)
	assert_eq(to_result, expected, "Es igual")

func test_json_as_vector2():
	var to_result = JSONConvert.deserialize(expected)
	assert_eq(to_result, data, "Es igual")
