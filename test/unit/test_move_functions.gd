extends GutTest

var data := [-1, 1, 0.25]
var expected: Dictionary = {"-1":-0.84147, "-0.75":-0.68164, "-0.5":-0.47943, "-0.25":-0.24740, "0":0.0, "0.25":0.24740, "0.5":0.47943, "0.75":0.68164, "1":0.84147}

func test_move_sin():
	var result = Maths.sin_values(data)
	assert_eq_deep(result, expected)
