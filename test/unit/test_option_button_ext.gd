extends GutTest

var _option_button: OptionButton = null

func before_all():
	_option_button = autofree(OptionButton.new())
	add_child_autofree(_option_button)

func before_each():
	_option_button.add_item("Hola")
	_option_button.add_item("Como")
	_option_button.add_item("Estas")
	_option_button.add_item("Here")
	_option_button.add_item("Hola2")

func after_each():
	_option_button.clear()

func test_option_button_ext():
	OptionButtonExt.select(_option_button, "Estas")
	assert_eq(_option_button.selected, 2, "Expected that option selected is 2")
	assert_eq(_option_button.get_item_text(_option_button.selected), "Estas", "Expected that option selected is Estas")

	OptionButtonExt.select(_option_button, "Hola2")
	assert_eq(_option_button.selected, 4, "Expected that option selected is 4")
	assert_eq(_option_button.get_item_text(_option_button.selected), "Hola2", "Expected that option selected is Hola2")

	OptionButtonExt.select(_option_button, "Hola")
	assert_eq(_option_button.selected, 0, "Expected that option selected is 0")
	assert_eq(_option_button.get_item_text(_option_button.selected), "Hola", "Expected that option selected is Hola")

	OptionButtonExt.select(_option_button, "Here")
	assert_eq(_option_button.selected, 3, "Expected that option selected is 3")
	assert_eq(_option_button.get_item_text(_option_button.selected), "Here", "Expected that option selected is Here")
