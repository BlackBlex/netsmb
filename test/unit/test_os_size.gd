extends GutTest

var window_size = Vector2(800, 608)

func before_each():
	pass
	#OS.window_maximized = true

func after_each():
	pass
	#OS.window_maximized = true
	#gut.p("Runs once after all tests")


func test_window_size():
	#OS.window_maximized = true
	assert_ne(OS.window_size, window_size, "Expected to fail window size")
	assert_eq(OS.window_maximized, true, "Expected to pass window maximized in true")
	OS.window_fullscreen = true
	OS.window_fullscreen = false
	OS.window_maximized = false
	OS.window_size = window_size
	OS.max_window_size = window_size
	assert_eq(OS.window_size, window_size, "Expected to pass window size")
	assert_eq(OS.window_maximized, false, "Expected to pass window maximized in false")
