extends GutTest

var data_1 := "0.1"
var result_1 := Version.new().init_string(data_1)
var expected_1 := Version.new().init_number(0, 1)
var data_2 := "0.1.1"
var result_2 := Version.new().init_string(data_2)
var expected_2 := Version.new().init_number(0, 1, 1)
var data_3 := "0.1.1.1"
var result_3 := Version.new().init_string(data_3)
var expected_3 := Version.new().init_number(0, 1, 1, 1)
var data_4 := "0.1.1.1-alpha"
var result_4 := Version.new().init_string(data_4)
var expected_4 := Version.new().init_number(0, 1, 1, 1, Version.State.ALPHA)
var data_5 := "0.1.1.1-beta"
var result_5 := Version.new().init_string(data_5)
var expected_5 := Version.new().init_number(0, 1, 1, 1, Version.State.BETA)
var data_6 := "0.1.1.1-beta.2"
var result_6 := Version.new().init_string(data_6)
var expected_6 := Version.new().init_number(0, 1, 1, 1, Version.State.BETA, 2)

func test_version_equals(params=use_parameters([[result_1, expected_1], [result_2, expected_2], [result_3, expected_3], [result_4, expected_4], [result_5, expected_5], [result_6, expected_6]])):
	assert_true(params[0].equals(params[1]), "Is equals %s and %s?" % [params[0], params[1]])

func test_version_less(params=use_parameters([[result_1, expected_2], [result_2, expected_3]])):
	assert_true(params[0].is_less_than(params[1]), "Is less %s that %s?" % [params[0], params[1]])

func test_version_greater(params=use_parameters([[result_3, expected_4], [expected_5, result_4], [result_6, expected_5]])):
	assert_true(params[0].is_greater_than(params[1]), "Is greater %s that %s?" % [params[0], params[1]])
